<?php 
require "/var/www/shadowsofthebayou.com/site-inc/header.php"; 
require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; 
$query = "SELECT * FROM `create_rules` WHERE (char_type_name='Bygone')";
$result = mysqli_query($connection, $query) or die("<div  class='error'>Couldn't get character data.</div>");
$row = mysqli_fetch_array($result);
$characterType = 'Bygone';
?>
<div id="pagewrapper">
  <table width="750" border="0" cellspacing="0" align="center">
    <tr>
      <td><div class="pagetopic">Creation Rules for <?php echo $row['char_type_name'] ?></div></td>
    </tr>
    <tr>
      <td><hr></td>
    </tr>
    <tr>
      <td><div class="pagetopic">Allowed Sources:</div></td>
    </tr>
    <tr>
      <td><div class="createtxt"><?php echo $row['books'] ?></div></td>
    </tr>
    <tr><td><hr width="75%"></td></tr>
    <tr>
      <td><div class="pagetopic">Endowment:</div></td>
    </tr>
    <tr>
      <td>
        <?php
          include "/var/www/shadowsofthebayou.com/endowments/endowment_check.php" 
          ?>
      </td>
    </tr>
    <tr>
      <td><hr width="75%"></td>
    </tr>
    <tr>
      <td><div class="pagetopic">Starting Dots:</div></td>
    </tr>
    <tr>
      <td><div class="createtxt"><?php echo $row['start_dots'] ?></div></td>
    </tr>
    <tr>
      <td><hr width="75%"></td>
    </tr>
    <tr>
      <td><div class="pagetopic">Species Options:</div></td>
    </tr>
    <tr>
      <td><div class="createtxt"><?php echo $row['venue_1'] ?></div></td>
    </tr>
    <tr>
      <td><hr width="50%"></td>
    </tr>
    <tr>
      <td><div class="createtxtb">Currently Approved Bygones By Species:</div></td>
    </tr>
    <tr>
      <td>
        <?php include "/var/www/shadowsofthebayou.com/c-sheets/approved_characters.php" ?>
      </td>
    </tr>
    <tr>
      <td><hr width="75%"></td>
    </tr>
    <tr>
      <td><div class="pagetopic">Background Information:</div></td>
    </tr>
    <tr>
      <td><div class="createtxt"><?php echo $row['backgrounds'] ?></div></td>
    </tr>
    <tr>
      <td><hr width="75%"></td>
    </tr>
    <tr>
      <td><div class="pagetopic">Special Advantages and Other Venue Notes:</div></td>
    </tr>
    <tr>
      <td><div class="createtxt"><?php echo $row['venue_2'] ?></div></td>
    </tr>
    <tr>
      <td><hr width="75%"></td>
    </tr>
    <tr>
      <td><div class="pagetopic">Merits & Flaws Information:</div></td>
    </tr>
    <tr>
      <td><div class="createtxt"><?php echo $row['merit_flaws'] ?></div></td>
    </tr>
    <tr>
      <td><hr></td>
    </tr>
    <tr>
      <td><div class="warning" align="center">Last Updated by <?php echo $row['updateby'] ?> on <?php echo $row['last_update'] ?></div></td>
    </tr>
    <tr>
      <td><hr></td>
    </tr>
    <tr>
      <td><div class='item' align='center'><a href="http://shadowsofthebayou.com/c-sheets/create_bygone.php" target="_blank">Click to Start Bygone Sheet</a></div></td>
    </tr>
  </table>
  <br>
</div>

