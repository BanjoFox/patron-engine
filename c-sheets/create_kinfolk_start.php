<?php require "/var/www/shadowsofthebayou.com/site-inc/header.php"; ?>
<?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
<div id="pagewrapper">
  <?php
    $query = "SELECT * FROM `create_rules` WHERE (char_type_name='Kinfolk')";
$result = mysqli_query($connection, $query)
or die("<div  class='error'>Couldn't get character data.</div>");
$row = mysqli_fetch_array($result);
?>
  <table width="750" border="0" cellspacing="0" align="center">
    <tr>
      <td>
        <div class="pagetopic">Creation Rules for
          <?php echo $row['char_type_name'] ?>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <hr>
      </td>
    </tr>
    <tr>
      <td>
        <div class="pagetopic">Allowed Sources:</div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="createtxt">
          <?php echo $row['books'] ?>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="center">
          <hr width="75%">
      </td>
    </tr>
    <tr>
      <td>
        <div class="pagetopic">Endowment:</div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="item">
          <?php
        $query = "SELECT result,used_on FROM `endowments` WHERE (char_type='Kinfolk' AND name='$_COOKIE[logname]' AND (used_on IS NULL OR used_on=''))";
$result = mysqli_query($connection, $query)
or die("<div  class='error'>Couldn't get endowments.</div>");
$row = mysqli_fetch_array($result);
?>
          <?php if ($row['used_on'] <> '') {
              echo("<div align='center'>You have no available endowment for a Vampire.</div><br \>");
          } else {
              echo("<table width='50%' border='0' cellspacing='0' align='center'>
			<tr>
				<td><div class='success' align='center'>$row[result]<div></td>
			</tr>
		</table>
              ");
          }
?>

          <?php
$query = "SELECT * FROM `create_rules` WHERE (char_type_name='Kinfolk')";
$result = mysqli_query($connection, $query)
or die("<div  class='error'>Couldn't get character data.</div>");
$row = mysqli_fetch_array($result);
?>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="warnsm" align='center'>If you see nothing above it means you need to generate an endowment.</div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="warning" align='center'>
          <?php
$query = "SELECT * FROM endowments WHERE (name='$_COOKIE[logname]' AND char_type='Kinfolk' AND (used_on IS NULL OR used_on=''))";
$result = mysqli_query($connection, $query)
or die("<div class='error' align='center'>Could not get list.<br \>" . mysqli_error($connection) . "<br \></div>");
$num_results = mysqli_num_rows($result);
if ($num_results < 1) {
    echo("<tr>
    <td><div class='item' align='center'>Click the link to generate an endowment then return to this page: <a href='http://shadowsofthebayou.com/endowments/endowment_kinfolk.php'>Kinfolk Endowment</a></div></td>
  </tr>");
} else {
    echo("");
}
?>
      </td>
    </tr>

    <tr>
      <td>
        <div class="center">
          <hr width="75%">
      </td>
    </tr>
    <tr>
      <td>
        <div class="pagetopic">Starting Dots:</div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="createtxt">
          <?php echo $row['start_dots'] ?>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="center">
          <hr width="75%">
      </td>
    </tr>
    <tr>
      <td>
        <div class="pagetopic">Tribe and Breed Options:</div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="createtxt">
          <?php echo $row['venue_1'] ?>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="center">
          <hr width="50%">
      </td>
    </tr>
    <tr>
      <td>
        <div class="center"><u>
            <div class="createtxtb">Currently Approved Kinfolk By Tribe:</div>
          </u>
          <div class="success">
            <?php
$query = "SELECT account_type,sub_venue,sanctioned,tribe, count(*)  'count' FROM game_data WHERE (account_type='Character' and sub_venue='Kinfolk' and deleted='No' and char_type='PC' and sanctioned='Yes') GROUP BY tribe";
$result = mysqli_query($connection, $query) or die("Couldn't execute query");
while ($row = mysqli_fetch_assoc($result)) {
    echo("  {$row['tribe']} {$row['count']}, ");
}
?>
          </div>
      </td>
    </tr>
    <?php
    $query = "SELECT * FROM `create_rules` WHERE (char_type_name='Kinfolk')";
$result = mysqli_query($connection, $query)
or die("<div  class='error'>Couldn't get character data.</div>");
$row = mysqli_fetch_array($result);
?>
    <tr>
      <td>
        <div class="center">
          <hr width="75%">
      </td>
    </tr>
    <tr>
      <td>
        <div class="pagetopic">Background Information:</div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="createtxt">
          <?php echo $row['backgrounds'] ?>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="center">
          <hr width="75%">
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="pagetopic">Other Venue Notes:</div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="createtxt">
          <?php echo $row['venue_2'] ?>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="center">
          <hr width="75%">
      </td>
    </tr>
    <tr>
      <td>
        <div class="pagetopic">Merits & Flaws Information:</div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="createtxt">
          <?php echo $row['merit_flaws'] ?>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <hr>
      </td>
    </tr>
    <tr>
      <td>
        <div align="center" class="warning">Last Updated by
          <?php echo $row['updateby'] ?> on
          <?php echo $row['last_update'] ?>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <hr>
      </td>
    </tr>

    <tr>
      <td>
        <div class='item' align='center'><a href="http://shadowsofthebayou.com/c-sheets/create_kinfolk.php"
            target="_blank">Click to Start Kinfolk Sheet</a></div>
      </td>
    </tr>
  </table>
  <br \>
</div>