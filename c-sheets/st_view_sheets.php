<?php include '/var/www/shadowsofthebayou.com/site-inc/header.php'; ?>

<div id="wrapper">
    <div id="title"></div><!-- title close -->
    <div class="container">
        <div class="column-nav">
            <?php require '/var/www/shadowsofthebayou.com/site-inc/check-access.php'; ?>
        </div><!-- menu close -->
        <div class="column-main">
            <?php
            if ($_COOKIE['privilege'] >= "3") {
                echo "You do not have rights to do this";
            } else {

                require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php";

                $query = "SELECT DISTINCT log_name FROM `game_data` WHERE (account_type='Character' AND deleted<>'Yes' AND char_type<>'RET') ORDER BY log_name";
                $result = mysqli_query($connection, $query) or die("Couldn't execute query to list game_data.");
                $option = "<select name=\"log_name\" class='form'><option value=\"\" class='form'></option>";
                while ($row = mysqli_fetch_array($result)) {
                    $option .= "<option value=\"$row[log_name]\">$row[log_name]</option>";
                }
                $option .= "</select>";
                ?>
                <?php echo $sqldata['log_name'] ?>
                
                <form name="form1" method="post" action="">
                    <table style="width:90%" border="0" align="center">
                        <caption><div align=center class='pagetopic'>Select Character To View</div></caption>
                        <tr>
                            <td>
                                <div class="center"><?php echo $option?> <input name="Select" type="submit" id="Select" value="Select Character" class="form"></div>
                            </td>
                        </tr>
                    </table>
                </form>
                <form name="form2" method="post" action="">
                    <input name="log_name" type="hidden" id="log_name" value="<?php echo $_POST['log_name']?>">
                    <?php
                    if ($_POST['Select'] == "Select Character" && $_POST['log_name'] <> "") {
                        $query = "SELECT * FROM `game_data` WHERE (log_name=\"$_POST[log_name]\")";
                        $result = mysqli_query($connection, $query) or die("Couldn't execute query.");
                        $row = mysqli_fetch_array($result);
                        $sub_venue = $row['sub_venue'];
                        $view_file = "view_" . strtolower($sub_venue) . ".php";
                        if (file_exists($view_file)) {
                            include $view_file;
                        } else {
                            echo "View file not found for '$sub_venue'";
                        }
                    }
                ?>
                </form>
                <?php
            }
?>
        </div><!-- content close -->
    </div><!-- container close -->
</div><!-- wrapper close -->
