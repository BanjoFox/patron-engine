<form name="View Bygone" action="">
<link href="../layout.css" rel="stylesheet" type="text/css">
<table style="width:90%"  align="center">
   <tr>
    <td><div align="center" class="pagetopic">Bygone Character Sheet</div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'view/master-top.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/bygbar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'view/wod-bygone-top.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/bygbar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'view/master-attribute.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/bygbar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'view/wod-bygone-abil.php';
      ?>
    </td>
  </tr>
  <tr>
    <td>
      <?php
      require 'view/master-seconds.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/bygbar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'view/master-language.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/bygbar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'view/wod-bygone-bg.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/bygbar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'view/wod-bygone-traits.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/bygbar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require '/var/www/shadowsofthebayou.com/site-inc/bottom_data-view.php';
      ?>
    </td>
  </tr>
</table>
</form>