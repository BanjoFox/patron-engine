<form name="Edit Ghoul" action="edit_character.php" method="POST">
<link href="../layout.css" rel="stylesheet" type="text/css">
<table style="width:90%"  align="center">
   <tr>
    <td><div align="center" class="pagetopic">Ghoul Character Sheet</div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'creedit/master-top.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/vtmbar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'creedit/vtm-ghoul-top.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/vtmbar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'creedit/master-attribute.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/vtmbar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'creedit/vtm-abil.php';
      ?>
    </td>
  </tr>
  <tr>
    <td>
      <?php
      require 'creedit/master-seconds.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/vtmbar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'creedit/master-language.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/vtmbar.gif"></div></td>
  </tr>
    <tr>
    <td>
      <?php
      require 'creedit/background-vtm-g.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/vtmbar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'creedit/vtm-discs.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/vtmbar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'creedit/vtm-traits-ghoul.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/vtmbar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require '/var/www/shadowsofthebayou.com/site-inc/bottom_data-edit.php';
      ?>
    </td>
  </tr>
  <tr>
    <td>
      <?php
      require 'footer2.php';
      ?>
    </td>
  </tr>
</table>
</form>