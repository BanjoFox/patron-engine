<?php require "/var/www/shadowsofthebayou.com/site-inc/header.php"; ?>
<?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
<div id="pagewrapper">
<?php
    $query = "SELECT * FROM `create_rules` WHERE (char_type_name='Retainer')";
$result = mysqli_query($connection, $query)
 or die("<div  class='error'>Couldn't get character data.</div>");
$row = mysqli_fetch_array($result);
?>
<table width="750" border="0" cellspacing="0"  align="center">
  <tr>
    <td><div class="pagetopic">Creation Rules for <?php echo $row['char_type_name']?></div></td>
  </tr>
    <tr>
    <td><hr></td>
  </tr>
  <tr>
    <td><div class="pagetopic">How to Create the Retainer You Need</div></td>
  </tr>
    <tr>
    <td><div class="item"><?php echo $row['venue_1']?></div></td>
  </tr>
   <tr>
    <td><div class="center"><hr width="75%"></td>
  </tr>
  <tr>
    <td><div class="pagetopic">Starting Dots:</div></td>
  </tr>
  <tr>
    <td><div class="item"><?php echo $row['start_dots']?></div></td>
  </tr>
   <tr>
    <td><div class="center"><hr width="75%"></td>
  </tr>
  <tr>
    <td><div class="pagetopic">Background Information:</div></td>
  </tr>
    <tr>
    <td><div class="item"><?php echo $row['backgrounds']?></div></td>
  </tr>
   <tr>
    <td><div class="center"><hr width="75%"></div></td>
  </tr>
  <tr>
    <td><div class="pagetopic">Other Venue Notes:</div></td>
  </tr>
    <tr>
    <td><div class="item"><?php echo $row['venue_2']?></div></td>
  </tr>
   <tr>
    <td><div class="center"><hr width="75%"></td>
  </tr>
  <tr>
    <td><div class="pagetopic">Merits & Flaws Information:</div></td>
  </tr>
    <tr>
    <td><div class="item"><?php echo $row['merit_flaws']?></div></td>
  </tr>
   <tr>
    <td><hr></td>
  </tr>
  <tr>
    <td><div align="center" class="warning">Last Updated by <?php echo $row['updateby']?> on <?php echo $row['last_update']?></div></td>
  </tr>
   <tr>
    <td><hr></td>
  </tr>

<tr>
<td><div class='item' align='center'><a href="http://shadowsofthebayou.com/c-sheets/create_ghoul.php" target="_blank">Click to Start Ghoul Sheet</a><br \>
<a href="http://shadowsofthebayou.com/c-sheets/create_kinfolk.php" target="_blank">Click to Start Kinfolk Sheet</a><br \>
<a href="http://shadowsofthebayou.com/c-sheets/create_sorcerer.php" target="_blank">Click to Start Sorcerer / Psychic Sheet</a><br \>
<a href="http://shadowsofthebayou.com/c-sheets/create_kinain.php" target="_blank">Click to Start Kinain Sheet</a><br \>
<a href="http://shadowsofthebayou.com/c-sheets/create_mortal.php" target="_blank">Click to Start Mortal Sheet</a></div></td>
</tr>
</table>
<br \>
</div>
