<table width="100%">
  <tr>
    <td width="24%"><div class='item'>Species</div></td>
    <td width="25%"><div class='item'><input name="bygone_species" type="text" class="form" id="bygone_species" size="23" maxlength="16" value="<?php echo $data[bygone_species]?>"></div></td>
    <td width="2%">&nbsp;</td>
    <td width="24%"><div class='item'>Nature</div></td>
    <td width="25%"><div class='item'><select name="nature" size="1" class="form" id="nature">
      <option selected><?php echo $data[nature]?><value="<?php echo $data[nature]?>"></option>
        <option value="Anarchist">Anarchist</option>
        <option value="Architect">Architect</option>
        <option value="Autocrat">Autocrat</option>
        <option value="Bon Vivant">Bon Vivant</option>
        <option value="Bravo">Bravo</option>
        <option value="Capitalist">Capitalist</option>
        <option value="Caregiver">Cargiver</option>
        <option value="Celebrant">Celebrant</option>
        <option value="Chameleon">Chameleon</option>
        <option value="Child">Child</option>
        <option value="Competitor">Competitor</option>
        <option value="Conformist">Conformist</option>
        <option value="Conniver">Conniver</option>
        <option value="Creep Show">Creep Show</option>
        <option value="Critic Show">Critic</option>
        <option value="Curmudgeon">Curmudgeon</option>
        <option value="Dabbler">Dabbler</option>
        <option value="Deviant">Deviant</option>
        <option value="Director">Director</option>
        <option value="Eye of the Storm">Eye of the Storm</option>
        <option value="Fanatic">Fanatic</option>
        <option value="Gallant">Gallant</option>
        <option value="Guru">Guru</option>
        <option value="Idealist">Idealist</option>
        <option value="Judge">Judge</option>
        <option value="Loner">Loner</option>
        <option value="Martyr">Martyr</option>
        <option value="Masochist">Masochist</option>
        <option value="Monster">Monster</option>
        <option value="Nihilist">Nihilist</option>
        <option value="Pedagogue">Pedagogue</option>
        <option value="Penitent">Penitent</option>
        <option value="Perfectionist">Perfectionist</option>
        <option value="Rebel">Rebel</option>
        <option value="Rogue">Rogue</option>
        <option value="Sadist">Sadist</option>
        <option value="Scientist">Scientist</option>
        <option value="Sociopath">Sociopath</option>
        <option value="Soldier">Soldier</option>
        <option value="Survivor">Survivor</option>
        <option value="Traditionalist">Traditionalist</option>
        <option value="Trickster">Trickster</option>
        <option value="Visionary">Visionary</option></select></div></td>
  </tr>
  <tr>
    <td><div class='item'>Element</div></td>
    <td><div class="item"><input name="bygone_element" type="text" class="form" id="bygone_element" size="23" maxlength="16" value="<?php echo $data[bygone_element]?>" /></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Demeanor</div></td>
    <td><div class="item"><select name="demeanor" size="1" class="form" id="demeanor">
      <option selected><?php echo $data[demeanor]?><value="<?php echo $data[demeanor]?>"></option>
         <option value="Architect">Architect</option>
        <option value="Autocrat">Autocrat</option>
        <option value="Bon Vivant">Bon Vivant</option>
        <option value="Bravo">Bravo</option>
        <option value="Capitalist">Capitalist</option>
        <option value="Caregiver">Cargiver</option>
        <option value="Celebrant">Celebrant</option>
        <option value="Chameleon">Chameleon</option>
        <option value="Child">Child</option>
        <option value="Competitor">Competitor</option>
        <option value="Conformist">Conformist</option>
        <option value="Conniver">Conniver</option>
        <option value="Creep Show">Creep Show</option>
        <option value="Curmudgeon">Curmudgeon</option>
        <option value="Dabbler">Dabbler</option>
        <option value="Deviant">Deviant</option>
        <option value="Director">Director</option>
        <option value="Eye of the Storm">Eye of the Storm</option>
        <option value="Fanatic">Fanatic</option>
        <option value="Gallant">Gallant</option>
        <option value="Guru">Guru</option>
        <option value="Idealist">Idealist</option>
        <option value="Judge">Judge</option>
        <option value="Loner">Loner</option>
        <option value="Martyr">Martyr</option>
        <option value="Masochist">Masochist</option>
        <option value="Monster">Monster</option>
        <option value="Pedagogue">Pedagogue</option>
        <option value="Penitent">Penitent</option>
        <option value="Perfectionist">Perfectionist</option>
        <option value="Rebel">Rebel</option>
        <option value="Rogue">Rogue</option>
        <option value="Sadist">Sadist</option>
        <option value="Scientist">Scientist</option>
        <option value="Sociopath">Sociopath</option>
        <option value="Soldier">Soldier</option>
        <option value="Survivor">Survivor</option>
        <option value="Traditionalist">Traditionalist</option>
        <option value="Trickster">Trickster</option>
        <option value="Visionary">Visionary</option></select></div></td>
  </tr>
</table>