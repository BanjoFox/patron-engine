<table width="100%">
        <tr>
          <td width="24%"><div class='item'>Guild</div></td>
          <td width="25%"><div class='item'><select name="guild" size="1" class="form" id="guild">
                <option selected><?php echo $data[guild]?><value="<?php echo $data[guild]?>"></option>
                <option value=""></option>
                 <option value="Alchemists">Alchemists</option>
                 <option value="Artificers">Artificers</option>
                 <option value="Chanteurs">Chanteurs</option>
                 <option value="Harbingers">Harbingers</option>
                 <option value="Haunter">Haunter</option>
                 <option value="Masquers">Masquers</option>
                 <option value="Mnemoi">Mnemoi</option>
                 <option value="Monitors">Monitors</option>
                 <option value="Oracles">Oracles</option>
                 <option value="Pardoners">Pardoners</option>
                 <option value="Proctors">Proctors</option>
                 <option value="Puppeteers">Puppeteers</option>
                 <option value="Sandman">Sandman</option>
                 <option value="Soliciters">Soliciters</option>
                 <option value="Spooks">Spooks</option>
                 <option value="Usurers">Usurers</option>
              </select></div></td>
              <td width="2%">&nbsp;</td>
              <td width="24%"><div class='item'>Legion</div></td>
              <td width="25%"><div class='item'><select name="legion" size="1" class="form" id="legion">
                <option selected><?php echo $data[legion]?><value="<?php echo $data[legion]?>"></option>
                <option value=""></option>
                <option value="Emerald">Emerald</option>
                <option value="Fate">Fate</option>
                <option value="Grim">Grim</option>
                <option value="Iron">Iron</option>
                <option value="Paupers">Paupers</option>
                <option value="Silent">Silent</option>
                <option value="Skeletal">Skeletal</option>
              </select></div></td>
       </tr>
        <tr>
          <td><div class="item">Nature</div></td>
          <td><div class="item"><input name="nature" type="text" class="form" id="clan2" size="23" maxlength="16" value="<?php echo $data[nature]?>" /></div></td>
          <td>&nbsp;</td>
          <td><div class="item">Demeanor</div></td>
          <td><div class="item"><input name="demeanor" type="text" class="form" id="demeanor" size="23" maxlength="16" value="<?php echo $data[demeanor]?>" /></div></td>
        </tr>
</table>

<div class="center"><hr width="80%" /></div>

<table width="100%">
        <tr> 
          <td width="24%"><div class='item'>Life</div></td>
          <td width="76%"><div class='item'><input name="life_w" type="text" class="form" id="life_w" size="57" maxlength="64" value="<?php echo $data[life_w]?>" /></div></td>
        </tr>
        <tr> 
          <td><div class='item'>Death</div></td>
          <td><div class='item'><input name="death" type="text" class="form" id="death" size="57" maxlength="64" value="<?php echo $data[death]?>"></div></td>
        </tr>
        <tr> 
          <td><div class='item'>Regret</div></td>
          <td><div class='item'><input name="regret" type="text" class="form" id="regret" size="57" maxlength="64" value="<?php echo $data[regret]?>"></div></td>
        </tr>
</table>
