

<table width="100%">
  <tr>
    <td width="24%"><div class='item'>Generation</div></td>
    <td width="25%"><div class='item'><select name="generation" size="1" class="form" id="generation">
      <option selected><?php echo $data[generation]?><value="<?php echo $data[generation]?>"></option>
      <option value="15th">15th</option>
      <option value="14th">14th</option>
      <option value="13th">13th</option>
      <option value="12th">12th</option>
      <option value="11th">11th</option>
      <option value="10th">10th</option>
      <option value="9th">9th</option>
      <option value="8th">8th</option>
      <option value="7th">7th</option>
      <option value="6th">6th</option>
    </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="24%"><div class='item'>Sect</div></td>
    <td width="25%"><div class='item'><select name="sect" size="1" class="form" id="sect">
      <option selected="selected"><?php echo $data['sect']?><value="<?php echo $data['sect']?>"></option>
      <option value="Camarilla">Camarilla</option>
      <option value="Independent">Independent</option>
      <option value="Sabbat">Sabbat</option>
    </select></div></td>
  </tr>
  <tr>
    <td><div class='item'>Clan</div></td>
    <td><div class='item'><select name="clan" size="1" class="form" id="clan">
      <option selected><?php echo $data[clan]?><value="<?php echo $data[clan]?>"></option>
        <option value="Brujah">Brujah</option>
        <option value="Gangrel">Gangrel</option>
        <option value="Malkavian">Malkavian</option>
        <option value="Nosferatu">Nosferatu</option>
        <option value="Toreador">Toreador</option>
        <option value="Tremere">Tremere</option>
        <option value="Ventrue">Ventrue</option>
        <option value="Assamite">Assamite</option>
        <option value="Caitiff">Caitiff</option>
        <option value="Follower of Set">Follower of Set</option>
        <option value="Giovanni">Giovanni</option>
        <option value="Lasombra">Lasombra</option>
        <option value="Ravnos">Ravnos</option>
        <option value="Tzimisce">Tzimisce</option>
        <option value="Daughter of Cacophony">Daughter of Cacophony</option>
        <option value="Gargoyles">Gargoyles</option>
        <option value="Salubri">Salubri</option>
        <option value="Samedi">Samedi</option>
        <option value="True Brujah">True Brujah</option></select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Sire</div></td>
    <td><div class='item'><input name="sire" type="text" class="form" id="sire" size="23" maxlength="24" value="<?php echo $data[sire]?>"></div></td>
  </tr>
  <tr>
    <td><div class='item'>Bloodline</div></td>
    <td><div class='item'><input name="bloodline" type="text" class="form" id="bloodline" size="23" maxlength="24" value="<?php echo $data[bloodline]?>"></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div class='item'>Nature</div></td>
    <td><div class='item'><select name="nature" size="1" class="form" id="nature">
      <option selected><?php echo $data[nature]?><value="<?php echo $data[nature]?>"></option>
        <option value="Anarchist">Anarchist</option>
        <option value="Architect">Architect</option>
        <option value="Autocrat">Autocrat</option>
        <option value="Bon Vivant">Bon Vivant</option>
        <option value="Bravo">Bravo</option>
        <option value="Capitalist">Capitalist</option>
        <option value="Caregiver">Cargiver</option>
        <option value="Celebrant">Celebrant</option>
        <option value="Chameleon">Chameleon</option>
        <option value="Child">Child</option>
        <option value="Competitor">Competitor</option>
        <option value="Conformist">Conformist</option>
        <option value="Conniver">Conniver</option>
        <option value="Creep Show">Creep Show</option>
        <option value="Critic Show">Critic</option>
        <option value="Curmudgeon">Curmudgeon</option>
        <option value="Dabbler">Dabbler</option>
        <option value="Deviant">Deviant</option>
        <option value="Director">Director</option>
        <option value="Enigma">Enigma</option>
        <option value="Eye of the Storm">Eye of the Storm</option>
        <option value="Fanatic">Fanatic</option>
        <option value="Gallant">Gallant</option>
        <option value="Guru">Guru</option>
        <option value="Idealist">Idealist</option>
        <option value="Judge">Judge</option>
        <option value="Loner">Loner</option>
        <option value="Martyr">Martyr</option>
        <option value="Masochist">Masochist</option>
        <option value="Monster">Monster</option>
        <option value="Nihilist">Nihilist</option>
        <option value="Pedagogue">Pedagogue</option>
        <option value="Penitent">Penitent</option>
        <option value="Perfectionist">Perfectionist</option>
        <option value="Rebel">Rebel</option>
        <option value="Rogue">Rogue</option>
        <option value="Sadist">Sadist</option>
        <option value="Scientist">Scientist</option>
        <option value="Sociopath">Sociopath</option>
        <option value="Soldier">Soldier</option>
        <option value="Survivor">Survivor</option>
        <option value="Traditionalist">Traditionalist</option>
        <option value="Trickster">Trickster</option>
        <option value="Visionary">Visionary</option></select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Demeanor</div></td>
    <td><div class="item"><select name="demeanor" size="1" class="form" id="demeanor">
      <option selected><?php echo $data[demeanor]?><value="<?php echo $data[demeanor]?>"></option>
         <option value="Architect">Architect</option>
        <option value="Autocrat">Autocrat</option>
        <option value="Bon Vivant">Bon Vivant</option>
        <option value="Bravo">Bravo</option>
        <option value="Capitalist">Capitalist</option>
        <option value="Caregiver">Cargiver</option>
        <option value="Celebrant">Celebrant</option>
        <option value="Chameleon">Chameleon</option>
        <option value="Child">Child</option>
        <option value="Competitor">Competitor</option>
        <option value="Conformist">Conformist</option>
        <option value="Conniver">Conniver</option>
        <option value="Creep Show">Creep Show</option>
        <option value="Curmudgeon">Curmudgeon</option>
        <option value="Dabbler">Dabbler</option>
        <option value="Deviant">Deviant</option>
        <option value="Director">Director</option>
        <option value="Enigma">Enigma</option>
        <option value="Eye of the Storm">Eye of the Storm</option>
        <option value="Fanatic">Fanatic</option>
        <option value="Gallant">Gallant</option>
        <option value="Guru">Guru</option>
        <option value="Idealist">Idealist</option>
        <option value="Judge">Judge</option>
        <option value="Loner">Loner</option>
        <option value="Martyr">Martyr</option>
        <option value="Masochist">Masochist</option>
        <option value="Monster">Monster</option>
        <option value="Pedagogue">Pedagogue</option>
        <option value="Penitent">Penitent</option>
        <option value="Perfectionist">Perfectionist</option>
        <option value="Rebel">Rebel</option>
        <option value="Rogue">Rogue</option>
        <option value="Sadist">Sadist</option>
        <option value="Scientist">Scientist</option>
        <option value="Sociopath">Sociopath</option>
        <option value="Soldier">Soldier</option>
        <option value="Survivor">Survivor</option>
        <option value="Traditionalist">Traditionalist</option>
        <option value="Trickster">Trickster</option>
        <option value="Visionary">Visionary</option></select></div></td>
  </tr>
</table>
