<table width="100%">
  <tr>
    <td width="24%">
      <div class="pageitem">Name</div>
    </td>
    <td width="76%">
      <div class="pageitem"><?php echo $data['log_name'] ?></div>
    </td>
  </tr>
  <tr>
    <td>
      <div class="item">Player</div>
    </td>
    <td>
      <div class="item"><?php echo $data['playername'] ?></div>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <hr />
    </td>
  </tr>
</table>
<table width="100%">
  <tr>
    <td width="24%">
      <div class='item'>Gender</div>
    </td>
    <td width="25%">
      <div class='item'><select name="gender" size="1" class="form" id="gender">
          <option selected><?php echo $data['gender'] ?></option>
          <option value=""></option>
          <option value="male">Male</option>
          <option value="female">Female</option>
        </select></div>
    </td>
    <td width="2%">&nbsp;</td>
    <td width="24%">
      <div class='item'>Nationality</div>
    </td>
    <td width="25%">
      <div class='item'><input name="nationality" type="text" class="form" id="nationality" size="15" maxlength="24"
          value="<?php echo $data['nationality'] ?>"></div>
    </td>
  </tr>
  <tr>
    <td>
      <div class='item'>Age</div>
    </td>
    <td>
      <div class='item'><input name="age" type="text" class="form" id="age" size="10" maxlength="4"
          value="<?php echo $data['age'] ?>"></div>
    </td>
    <td>&nbsp;</td>
    <td>
      <div class='item'></div>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
      <div class='item'>Apparent Age</div>
    </td>
    <td>
      <div class='item'><input name="apparentage" type="text" class="form" id="apparentage" size="10" maxlength="2"
          value="<?php echo $data['apparentage'] ?>"></div>
    </td>
    <td>&nbsp;</td>
    <td>
      <div class="item">Concept</div>
    </td>
    <td>
      <div class='item'><input name="concept" type="text" class="form" id="concept" size="15" maxlength="24"
          value="<?php echo $data['concept'] ?>"></div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr>
    <td colspan="2">
      <hr />
    </td>
  </tr>
  <tr>
    <td>
      <div class='item'>Password (required)</div>
    </td>
    <td>
      <div class='item'><input name="password" type="text" class="formreq" id="password" size="24" maxlength="32"
          value="<?php echo $data['password'] ?>"></div>
    </td>
  </tr>
  <tr>
    <td>
      <div class='item'>View Password</div>
    </td>
    <td>
      <div class='item'><input name="viewpw" type="text" class="form" id="viewpw" size="24" maxlength="32"
          value="<?php echo $data['viewpw'] ?>"></div>
    </td>
  </tr>
</table>