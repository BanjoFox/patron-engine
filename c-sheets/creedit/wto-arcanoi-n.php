<div align="center" class="item"><hr width="80%" /></div>

<table width="100%">
  <tr>
    <td width="13%"><div class="item">Passions</div></td>
    <td width="90%" colspan="10"><div class="item"><textarea name="passion" cols="56" datas="2" class="form" id="passion"><?php echo $data[passion]?></textarea></div></td>
  </tr>
</table>

<div align="center" class="item"><hr width="80%" /></div>

<table width="100%">
  <tr>
    <td width="13%"><div class="item">Fetters</div></td>
    <td width="90%" colspan="10"><div class="item"><textarea name="fetter" cols="56" datas="2" class="form" id="fetter"><?php echo $data[fetter]?></textarea></div></td>
  </tr>
</table>

<div align="center" class="item"><hr width="80%" /></div>

<table width="100%" border="0" >
  <tr>
    <td colspan="8"><div class="center">
        <div class="pageitem">Arcanoi</div></td>
  </tr>
  <tr>
    <td width="21%"><div class='item'><input name="power1" type="text" class="form" id="power1" size="18" maxlength="18" value="<?php echo $data[power1]?>"></div></td>
    <td width="11%"><div class='item'><select name="power1val" size="1" class="form" id="power1val">
          <option selected><?php echo $data['power1val']?><value="<?php echo $data['power1val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><input name="power2" type="text" class="form" id="power2" size="18" maxlength="18" value="<?php echo $data[power2]?>"></div></td>
    <td width="11%"><div class='item'><select name="power2val" size="1" class="form" id="power2val">
          <option selected><?php echo $data['power2val']?><value="<?php echo $data['power2val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><input name="power3" type="text" class="form" id="power3" size="18" maxlength="18" value="<?php echo $data[power3]?>"></div></td>
    <td width="11%"><div class='item'><select name="power3val" size="1" class="form" id="power3val">
          <option selected><?php echo $data['power3val']?><value="<?php echo $data['power3val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><input name="power4" type="text" class="form" id="power4" size="18" maxlength="18" value="<?php echo $data[power4]?>"></div></td>
    <td><div class='item'><select name="power4val" size="1" class="form" id="power4val">
          <option selected><?php echo $data['power4val']?><value="<?php echo $data['power4val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><input name="power5" type="text" class="form" id="power5" size="18" maxlength="18" value="<?php echo $data[power5]?>"></div></td>
    <td><div class='item'><select name="power5val" size="1" class="form" id="power5val">
          <option selected><?php echo $data['power5val']?><value="<?php echo $data['power5val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><input name="power6" type="text" class="form" id="power6" size="18" maxlength="18" value="<?php echo $data[power6]?>"></div></td>
    <td><div class='item'><select name="power6val" size="1" class="form" id="power6val">
          <option selected><?php echo $data['power6val']?><value="<?php echo $data['power6val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><input name="power7" type="text" class="form" id="power7" size="18" maxlength="18" value="<?php echo $data[power7]?>"></div></td>
    <td><div class='item'><select name="power7val" size="1" class="form" id="power7val">
          <option selected><?php echo $data['power7val']?><value="<?php echo $data['power7val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><input name="power8" type="text" class="form" id="power8" size="18" maxlength="18" value="<?php echo $data[power8]?>"></div></td>
    <td><div class='item'><select name="power8val" size="1" class="form" id="power8val">
          <option selected><?php echo $data['power8val']?><value="<?php echo $data['power8val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><input name="power9" type="text" class="form" id="power9" size="18" maxlength="18" value="<?php echo $data[power9]?>"></div></td>
    <td><div class='item'><select name="power9val" size="1" class="form" id="power9val">
          <option selected><?php echo $data['power9val']?><value="<?php echo $data['power9val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><input name="power10" type="text" class="form" id="powe10" size="18" maxlength="18" value="<?php echo $data[power10]?>"></div></td>
    <td><div class='item'><select name="power10val" size="1" class="form" id="power10val">
          <option selected><?php echo $data['power10val']?><value="<?php echo $data['power10val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><input name="power11" type="text" class="form" id="power11" size="18" maxlength="18" value="<?php echo $data[power11]?>"></div></td>
    <td><div class='item'><select name="power11val" size="1" class="form" id="power11val">
          <option selected><?php echo $data['power11val']?><value="<?php echo $data['power11val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><input name="power12" type="text" class="form" id="power12" size="18" maxlength="18" value="<?php echo $data[power12]?>"></div></td>
    <td><div class='item'><select name="power12val" size="1" class="form" id="power12val">
          <option selected><?php echo $data['power12val']?><value="<?php echo $data['power12val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><input name="power13" type="text" class="form" id="powe13" size="18" maxlength="18" value="<?php echo $data[power13]?>"></div></td>
    <td><div class='item'><select name="power13val" size="1" class="form" id="power13val">
          <option selected><?php echo $data['power13val']?><value="<?php echo $data['power13val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><input name="power14" type="text" class="form" id="power14" size="18" maxlength="18" value="<?php echo $data[power14]?>"></div></td>
    <td><div class='item'><select name="power14val" size="1" class="form" id="power14val">
          <option selected><?php echo $data['power14val']?><value="<?php echo $data['power14val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><input name="power15" type="text" class="form" id="power15" size="18" maxlength="18" value="<?php echo $data[power15]?>"></div></td>
    <td><div class='item'><select name="power15val" size="1" class="form" id="power15val">
          <option selected><?php echo $data['power15val']?><value="<?php echo $data['power15val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
  </tr>
</table>

<div class="center"><hr width="80%" /></div>

<table width="100%" border="0" >
  <tr> 
    <td width="13%"><div class="item">Corpus</div></td>
    <td width="10%"><div align="center" class="itemsm">Perm</div></td>
    <td width="10%"><div class='item'><select name="corpusperm" size="1" class="form" id="corpusperm">
        <option selected><?php echo $data[corpusperm]?><value="<?php echo $data[corpusperm]?>"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
      </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="13%"><div class="item">Pathos</div></td>
    <td width="10%"><div align="center" class="itemsm"></div></td>
    <td width="10%"><div class='item'><select name="pathos" size="1" class="form" id="pathos">
        <option selected><?php echo $data[pathos]?><value="<?php echo $data[pathos]?>"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="13%"><div class="item">Willpower</div></td>
    <td width="10%"><div align="center" class="itemsm">Perm</div></td>
    <td width="10%"><div class='item'><select name="wpperm" size="1" class="form" id="wpperm">
        <option selected><?php echo $data[wpperm]?><value="<?php echo $data[wpperm]?>"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td><div align="center" class="itemsm">Temp</div></td>
    <td><div class='item'><select name="corpustemp" size="1" class="form" id="corpustemp">
        <option selected><?php echo $data[corpustemp]?><value="<?php echo $data[corpustemp]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center" class="itemsm">Temp</div></td>
    <td><div class='item'><select name="wptemp" size="1" class="form" id="wptemp">
        <option selected><?php echo $data[wptemp]?><value="<?php echo $data[wptemp]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
  </tr>
</table>

<div align="center" class="item"><hr width="80%" /></div>

<table width="100%">
  <tr>
    <td width="24%"><div class="item">Other Venue Details:</div></td>
    <td width="76%" colspan="10"><div class="item"><textarea name="wraith_venue" cols="56" datas="2" class="form" id="wraith_venue">This is where you put information such as: Passions, Fetters, Derangements, Shadow details. You can use formatting found in the profile guide to help organize this.</textarea></div></td>
  </tr>
</table>
