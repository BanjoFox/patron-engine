<table width="100%"  border="0" >
 <tr>
    <td width="13%"><div class="item">Willpower</div></td>
    <td width="10%"><div align="center" class="itemsm">Perm</div></td>
    <td width="10%"><div class="item"><select name="wpperm" size="1" class="form" id="wpperm">
        <option selected><?php echo $data[wpperm]?><value="<?php echo $data[wpperm]?>"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td width="2%">&nbsp;</td>
    <td colspan="2"><select name="path" size="1" class="form" id="path">
      <option selected><?php echo $data[path]?><value="<?php echo $data[path]?>"></option>
        <option value="Humanity">Humanity</option>
        <option value="Blood">Blood</option>
        <option value="Bones">Bones</option>
        <option value="Caine">Caine</option>
        <option value="Cathari">Cathari</option>
        <option value="Feral Heart">Feral Heart</option>
        <option value="Honorable Accord">Honorable Accord</option>
        <option value="Lilith">Lilith</option>   
        <option value="Metamorphosis">Metamorphosis</option>
        <option value="Night">Night</option>
        <option value="Paradox">Paradox</option>
        <option value="Power Inner Voice">Power Inner Voice</option>
        <option value="Scorched Heart">Scorched Heart</option> 
        <option value="Self Focus">Self Focus</option>
        <option value="Typhon">Typhon</option></select></div></td>
    <td width="10%"><div class="item"><select name="pathval" size="1" class="form" id="pathval">
        <option selected><?php echo $data['pathval']?><value="<?php echo $data['pathval']?>"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
    </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="13%"><div class="item"><select name="virtue1" size="1" class="form" id="virtue1">
        <option selected><?php echo $data[virtue1]?><value="<?php echo $data[virtue1]?>"></option>
        <option value="Conscience">Conscience</option>
        <option value="Conviction">Conviction</option>
    </select></div></td>
    <td width="10%"></td>
    <td width="10%"><div class="item"><select name="virtue1val" size="1" class="form" id="virtue1val">
        <option selected><?php echo $data['virtue1val']?><value="<?php echo $data['virtue1val']?>"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="center" class="itemsm">Temp</div></td>
    <td><div class="item"><select name="wptemp" size="1" class="form" id="wptemp">
        <option selected><?php echo $data[wptemp]?><value="<?php echo $data[wptemp]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td colspan="3"><div class="item">Bearing: 
        <input name="pathbearing" type="text" class="form" id="pathbearing" size="18" maxlength="16" value="<?php echo $data[pathbearing]?>"></div></td>
    <td>&nbsp;</td>
    <td><div class="item"><select name="virtue2" size="1" class="form" id="virtue2">
        <option selected><?php echo $data[virtue2]?><value="<?php echo $data[virtue2]?>"></option>
        <option value="Self-Control">Self-Control</option>
        <option value="Instinct">Instinct</option>
    </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item"><select name="virtue2val" size="1" class="form" id="virtue2val">
        <option selected><?php echo $data['virtue2val']?><value="<?php echo $data['virtue2val']?>"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="center">&nbsp;</div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2"><div align="right" class="itemsm"></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div class="item">Courage</div></td>
    <td>&nbsp;</td>
    <td><div class="item"><select name="courage" size="1" class="form" id="courage">
        <option selected><?php echo $data[courage]?><value="<?php echo $data[courage]?>"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td colspan="11"><div class="center"><hr width="75%"></div></td>
  </tr>
  <tr>
    <td><div class="item">Blood Pool</div></td>
    <td><div class="itemsm">Max</div></td>
    <td><div class="item"><select name="bloodmax" size="1" class="form" id="bloodmax">
      <option selected><?php echo $data[bloodmax]?><value="<?php echo $data[bloodmax]?>"></option>
      <option value="10">10</option>
      <option value="11">11</option>
      <option value="12">12</option>
      <option value="13">13</option>
      <option value="14">14</option>
      <option value="15">15</option>
      <option value="20">20</option>
      <option value="30">30</option>
      <option value="40">40</option>
    </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Blood Pool</div></td>
    <td><div class="itemsm">Current</div></td>
    <td><div class="item"><select name="bloodcurrent" size="1" class="form" id="bloodcurrent">
      <option selected><?php echo $data[bloodcurrent]?><value="<?php echo $data[bloodcurrent]?>"></option>
      <option value="0">0</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
      <option value="8">8</option>
      <option value="9">9</option>
      <option value="10">10</option>
      <option value="11">11</option>
      <option value="12">12</option>
      <option value="13">13</option>
      <option value="14">14</option>
      <option value="15">15</option>
      <option value="16">16</option>
      <option value="17">17</option>
      <option value="18">18</option>
      <option value="19">19</option>
      <option value="20">20</option>
      <option value="21">21</option>
      <option value="22">22</option>
      <option value="23">23</option>
      <option value="24">24</option>
      <option value="25">25</option>
      <option value="26">26</option>
      <option value="27">27</option>
      <option value="28">28</option>
      <option value="29">29</option>
      <option value="30">30</option>
      <option value="31">31</option>
      <option value="32">32</option>
      <option value="33">33</option>
      <option value="34">34</option>
      <option value="35">35</option>
      <option value="36">36</option>
      <option value="37">37</option>
      <option value="38">38</option>
      <option value="39">39</option>
      <option value="40">40</option>
    </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Blood per Turn</div></td>
    <td>&nbsp;</td>
    <td><div class="item"><select name="bloodturn" size="1" class="form" id="bloodturn">
      <option selected="selected"><?php echo $data[bloodturn]?><value="<?php echo $data[bloodturn]?>"></option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="6">6</option>   
    </select></div></td>
  </tr>
</table>


<div class="center"><hr width="80%" /></div>

<table width="100%">
  <tr>
    <td width="24%"><div class="item">Other Venue Details:</div></td>
    <td width="76%" colspan="10"><div class="item"><textarea name="vampire_venue" cols="56" datas="2" class="form" id="vampire_venue">This is where you put information such as: clan weakness, additional thaum paths, rituals, combo-disciplines, blood bonds, or derangements. You can use formatting found in the profile guide to help organize this. Include page numbers and book for quick reference.</textarea></div></td>
  </tr>
</table>