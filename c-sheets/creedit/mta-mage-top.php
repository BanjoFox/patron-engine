<table width="100%">
  <tr>
    <td width="24%"><div class='item'>Affiliation</div></td>
    <td width="25%"><div class='item'><select name="affiliation" size="1" class="form" id="affiliation">
        <option selected><?php echo $data[affiliation]?><value="<?php echo $data[affiliation]?>"></option>
        <option value="Tradition">Tradition</option>
        <option value="Disparates">Disparates</option>
        <option value="Independent">Independent</option>
      </select>
    </div></td>
    <td width="2%">&nbsp;</td>
    <td width="24%"><div class='item'>Essence</div></td>
    <td width="25%"><div class='item'><select name="essence" size="1" class="form" id="essence">
      <option selected><?php echo $data[essence]?><value="<?php echo $data[essence]?>"></option>
      <option value=""></option>
      <option value="Dynamic">Dynamic</option>
      <option value="Pattern">Pattern</option>
      <option value="Primordial">Primordial</option>
      <option value="Questing">Questing</option>
    </select></div></td>
  </tr>
  <tr>
    <td width="24%"><div class='item'>Tradition</div></td>
    <td width="25%"><div class='item'><select name="tradition" size="1" class="form" id="tradition">
<option selected><?php echo $data[tradition]?><value="<?php echo $data[tradition]?>"></option>
        <option value="Akashic Brother">Akashic Brother</option>
        <option value="Celestial Chorus">Celestial Chorus</option>
        <option value="Cult of Ecstasy">Cult of Ecstasy</option>
        <option value="Dreamspeakers">Dreamspeakers</option>
        <option value="Euthanatos">Euthanatos</option>
        <option value="Order of Hermes">Order of Hermes</option>
        <option value="Sons of Ether">Sons of Ether</option>
        <option value="Verbena">Verbena</option>
        <option value="Virtual Adepts">Virtual Adepts</option>
        <option value="Ahl-i-Batin">Ahl-i-Batin</option>
        <option value="Bata a">Bata a</option>
        <option value="Child of Knowledge">Child of Knowledge</option>
        <option value="Hollow Ones">Hollow Ones</option>
        <option value="Kopa Loei">Kopa Loei</option>
        <option value="Ngoma">Ngoma</option>
        <option value="Orphans">Orphans</option>
        <option value="Sisters of Hippolyta">Sisters of Hippolyta</option>
        <option value="The Tatfani">The Tatfani</option>
        <option value="Templar Knights">Templar Knights</option>
        <option value="Wu Lung">Wu Lung</option></select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="24%"><div class='item'>Nature</div></td>
    <td width="25%"><div class='item'><select name="nature" size="1" class="form" id="nature">
      <option selected><?php echo $data[nature]?><value="<?php echo $data[nature]?>"></option>
<option value="Activist">Activist</option>
<option value="Architect">Architect</option>
<option value="Artist">Artist</option>
<option value="Benefactor">Benefactor</option>
<option value="Bon Vivant">Bon Vivant</option>
<option value="Caregiver">Caregiver</option>
<option value="Conformist">Conformist</option>
<option value="Contrary">Contrary</option>
<option value="Crusader">Crusader</option>
<option value="Director">Director</option>
<option value="Entertainer">Entertainer</option>
<option value="Guardian">Guardian</option>
<option value="Hacker">Hacker</option>
<option value="Heretic">Heretic</option>
<option value="Idealist">Idealist</option>
<option value="Innovator">Innovator</option>
<option value="Kid">Kid</option>
<option value="Loner">Loner</option>
<option value="Machine">Machine</option>
<option value="Mad Scientist">Mad Scientist</option>
<option value="Martyr">Martyr</option>
<option value="Mentor">Mentor</option>
<option value="Monster">Monster</option>
<option value="Prophet">Prophet</option>
<option value="Rogue">Rogue</option>
<option value="Romantic">Romantic</option>
<option value="Sensualist">Sensualist</option>
<option value="Survivalist">Survivalist</option>
<option value="Traditionalist">Traditionalist</option>
<option value="Trickster">Trickster</option>
<option value="Tycoon">Tycoon</option>
<option value="Vigilante">Vigilante</option>
<option value="Visionary">Visionary</option>
<option value="Zealot">Zealot</option></select>
</div></td>
  </tr>
  <tr>
    <td><div class='item'>Faction</div></td>
    <td><div class='item'><input name="faction" type="text" class="form" id="faction" size="20" maxlength="24" value="<?php echo $data[faction]?>" /></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Demeanor</div></td>
    <td><div class='item'><select name="demeanor" size="1" class="form" id="demeanor">
      <option selected><?php echo $data[demeanor]?><value="<?php echo $data[demeanor]?>"></option>
<option value="Activist">Activist</option>
<option value="Architect">Architect</option>
<option value="Artist">Artist</option>
<option value="Benefactor">Benefactor</option>
<option value="Bon Vivant">Bon Vivant</option>
<option value="Caregiver">Caregiver</option>
<option value="Conformist">Conformist</option>
<option value="Contrary">Contrary</option>
<option value="Crusader">Crusader</option>
<option value="Director">Director</option>
<option value="Entertainer">Entertainer</option>
<option value="Guardian">Guardian</option>
<option value="Hacker">Hacker</option>
<option value="Heretic">Heretic</option>
<option value="Idealist">Idealist</option>
<option value="Innovator">Innovator</option>
<option value="Kid">Kid</option>
<option value="Loner">Loner</option>
<option value="Machine">Machine</option>
<option value="Mad Scientist">Mad Scientist</option>
<option value="Martyr">Martyr</option>
<option value="Mentor">Mentor</option>
<option value="Monster">Monster</option>
<option value="Prophet">Prophet</option>
<option value="Rogue">Rogue</option>
<option value="Romantic">Romantic</option>
<option value="Sensualist">Sensualist</option>
<option value="Survivalist">Survivalist</option>
<option value="Traditionalist">Traditionalist</option>
<option value="Trickster">Trickster</option>
<option value="Tycoon">Tycoon</option>
<option value="Vigilante">Vigilante</option>
<option value="Visionary">Visionary</option>
<option value="Zealot">Zealot</option></select>

</div></td>
  </tr>
  <tr>
    <td><div class="item">Specialty Sphere</div></td>
    <td><div class='item'><select name="spec_sphere" size="1" class="form" id="spec_sphere">
<option selected><?php echo $data[spec_sphere]?><value="<?php echo $data[spec_sphere]?>"></option>
        <option value="Correspondence">Correspondence</option>
        <option value="Entropy">Entropy</option>
        <option value="Forces">Forcer</option>
        <option value="Life">Life</option>
        <option value="Matter">Matter</option>
        <option value="Mind">Mind</option>
        <option value="Prime">Prime</option>
        <option value="Spirit">Spirit</option>
        <option value="Time">Time</option></select></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>