<table width="100%" border="0" >
  <tr>
    <td colspan="9"><div class="pageitem">Spheres</div></td>
  </tr>
  <tr>
    <td width="25%"><div class='item'>Correspondence</div></td>
    <td width="8%" colspan="2"><div class='item'><select name="coorespondence" size="1" class="form" id="coorespondence">
          <option selected><?php echo $data[coorespondence]?><value="<?php echo $data[coorespondence]?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="25%"><div class='item'>Life</div></td>
    <td width="8%"><div class='item'><select name="life" size="1" class="form" id="life">
          <option selected><?php echo $data[life]?><value="<?php echo $data[life]?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="25%"><div class='item'>Prime</div></td>
    <td width="8%"><div class='item'><select name="prime" size="1" class="form" id="prime">
          <option selected><?php echo $data[prime]?><value="<?php echo $data[prime]?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'>Entropy</div></td>
    <td colspan="2"><div class='item'><select name="entropy" size="1" class="form" id="entropy">
          <option selected><?php echo $data[entropy]?><value="<?php echo $data[entropy]?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Mind</div></td>
    <td><div class='item'><select name="mind" size="1" class="form" id="mind">
          <option selected><?php echo $data[mind]?><value="<?php echo $data[mind]?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Spirit</div></td>
    <td><div class='item'><select name="spirit" size="1" class="form" id="spirit">
          <option selected><?php echo $data[spirit]?><value="<?php echo $data[spirit]?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'>Forces</div></td>
    <td colspan="2"><div class='item'><select name="forces" size="1" class="form" id="forces">
          <option selected><?php echo $data[forces]?><value="<?php echo $data[forces]?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Matter</div></td>
    <td><div class='item'><select name="matter" size="1" class="form" id="matter">
          <option selected><?php echo $data[matter]?><value="<?php echo $data[matter]?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Time</div></td>
    <td><div class='item'><select name="time" size="1" class="form" id="time">
          <option selected><?php echo $data[time]?><value="<?php echo $data[time]?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
  </tr>
  </table>
  <table width="100%">
       <tr>
        <td colspan="2"><div align='center'><hr width='75%'></div></td>
       </tr> 
       <tr>
    <td width="25%"><div class="item">Rotes:</div></td>
    <td width="75%" colspan="10"><div class="item"><textarea name="ritualsk" cols="56" datas="2" class="form" id="ritualsk"><?php echo $data[ritualsk]?></textarea></div></td>
  </tr>
  </table>
  <table width="100%">
  <tr>
    <td colspan="9"><div align='center'><hr /></div></td>
  </tr>
  <tr>
    <td width="14%"><div class="item">Willpower</div></td>
    <td width="11%"><div class="itemsm">Perm</div></td>
    <td width="8%"><div class="item"><select name="wpperm" size="1" class="form" id="wpperm">
        <option selected><?php echo $data[wpperm]?><value="<?php echo $data[wpperm]?>"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="25%"><div class="item">Arete</div></td>
    <td width="8%"><div class="item"><select name="arete" size="1" class="form" id="arete">
        <option selected><?php echo $data[arete]?><value="<?php echo $data[arete]?>"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="25%"><div class="item">Quintessence</div></td>
    <td width="8%"><div class="item"><select name="quinttemp" size="1" class="form" id="quinttemp">
        <option selected><?php echo $data[quinttemp]?><value="<?php echo $data[quinttemp]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
      </select></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div class="itemsm">Temp</div></td>
    <td><div class="item"><select name="wptemp" size="1" class="form" id="wptemp">
        <option selected><?php echo $data[wptemp]?><value="<?php echo $data[wptemp]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div class="item">Paradox</div></td>
    <td><div class="item"><select name="paradox" size="1" class="form" id="paradox">
        <option selected><?php echo $data[paradox]?><value="<?php echo $data[paradox]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
      </select></div></td>
  </tr>
  </table>
  <table width="100%">
   <tr>
        <td colspan="2"><div align='center'><hr width='75%'></div></td>
  </tr> 
  <tr>
    <td width="25%"><div class="item">Other Details:</div></td>
    <td width="75%" colspan="10"><div class="item"><textarea name="mage_venue" cols="56" datas="2" class="form" id="mage_venue"><?php echo $data[mage_venue]?></textarea></div></td>
  </tr>
</table>