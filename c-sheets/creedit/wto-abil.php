<table width="100%" border="0" >
  <tr>
    <td colspan="8"><div class="pagetopic">Abilities</div></td>
  </tr>
  <tr>
    <td><div class="item">Alertness</div></td>
    <td><div class="item"><select name="alertness" size="1" class="form" id="alertness">
        <option selected><?php echo $data[alertness]?><value="<?php echo $data['alertness']?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Crafts</div></td>
    <td><div class="item"><select name="crafts" size="1" class="form" id="crafts">
        <option selected><?php echo $data[crafts]?><value="<?php echo $data[crafts]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Academics</div></td>
    <td><div class="item"><select name="academics" size="1" class="form" id="academics">
      <option selected><?php echo $data[academics]?><value="<?php echo $data[academics]?>"></option>
      <option value="0">0</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
    </select></div></td>
  </tr>
  <tr>
    <td><div class="item">Athletics</div></td>
    <td><div class="item"><select name="athletics" size="1" class="form" id="athletics">
        <option selected><?php echo $data[athletics]?><value="<?php echo $data[athletics]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Drive</div></td>
    <td><div class="item"><select name="drive" size="1" class="form" id="drive">
        <option selected><?php echo $data[drive]?><value="<?php echo $data[drive]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Bureaucracy</div></td>
    <td><div class="item"><select name="bureaucracy" size="1" class="form" id="bureaucracy">
        <option selected><?php echo $data[bureaucracy]?><value="<?php echo $data[bureaucracy]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="item">Awareness</div></td>
    <td><div class="item"><select name="awareness" size="1" class="form" id="awareness">
        <option selected><?php echo $data[awareness]?><value="<?php echo $data[awareness]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Etiquette</div></td>
    <td><div class="item"><select name="etiquette" size="1" class="form" id="etiquette">
        <option selected><?php echo $data[etiquette]?><value="<?php echo $data[etiquette]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Computer</div></td>
    <td><div class="item"><select name="computer" size="1" class="form" id="computer">
        <option selected><?php echo $data[computer]?><value="<?php echo $data[computer]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="item">Brawl</div></td>
    <td><div class="item"><select name="brawl" size="1" class="form" id="brawl">
        <option selected><?php echo $data[brawl]?><value="<?php echo $data[brawl]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Firearms</div></td>
    <td><div class="item"><select name="firearms" size="1" class="form" id="firearms">
        <option selected><?php echo $data[firearms]?><value="<?php echo $data[firearms]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Enigmas</div></td>
    <td><div class="item"><select name="enigmas" size="1" class="form" id="enigmas">
        <option selected><?php echo $data[enigmas]?><value="<?php echo $data[enigmas]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="item">Dodge</div></td>
    <td><div class="item"></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Larceny</div></td>
    <td><div class="item"><select name="larceny" size="1" class="form" id="larceny">
      <option selected="selected"><?php echo $data[larceny]?><value="<?php echo $data[larceny]?>"></option>
      <option value="0">0</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
    </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Investigation</div></td>
    <td><div class="item"><select name="investigation" size="1" class="form" id="investigation">
        <option selected><?php echo $data[investigation]?><value="<?php echo $data[investigation]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="item">Empathy</div></td>
    <td><div class="item"><select name="empathy" size="1" class="form" id="empathy">
        <option selected><?php echo $data[empathy]?><value="<?php echo $data[empathy]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Leadership</div></td>
    <td><div class="item"><select name="leadership" size="1" class="form" id="leadership">
      <option selected><?php echo $data[leadership]?><value="<?php echo $data[leadership]?>"></option>
      <option value="0">0</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
    </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Medicine</div></td>
    <td><div class="item"><select name="medicine" size="1" class="form" id="medicine">
        <option selected><?php echo $data[medicine]?><value="<?php echo $data[medicine]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="item">Expression</div></td>
    <td><div class="item"><select name="expression" size="1" class="form" id="expression">
        <option selected><?php echo $data[expression]?><value="<?php echo $data[expression]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Meditation</div></td>
    <td><div class="item"><select name="meditation" size="1" class="form" id="meditation">
      <option selected><?php echo $data[meditation]?><value="<?php echo $data[meditation]?>"></option>
      <option value="0">0</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
    </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Occult</div></td>
    <td><div class="item"><select name="occult" size="1" class="form" id="occult">
        <option selected><?php echo $data[occult]?><value="<?php echo $data[occult]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="item">Intimidation</div></td>
    <td><div class="item"><select name="intimidation" size="1" class="form" id="intimidation">
        <option selected><?php echo $data[intimidation]?><value="<?php echo $data[intimidation]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Melee</div></td>
    <td><div class="item"><select name="melee" size="1" class="form" id="melee">
      <option selected><?php echo $data[melee]?><value="<?php echo $data[melee]?>"></option>
      <option value="0">0</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
    </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Politics</div></td>
    <td><div class="item"><select name="politics" size="1" class="form" id="politics">
        <option selected><?php echo $data[politics]?><value="<?php echo $data[politics]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="item">Streetwise</div></td>
    <td><div class="item"><select name="streetwise" size="1" class="form" id="streetwise">
        <option selected><?php echo $data[streetwise]?><value="<?php echo $data[streetwise]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Performance</div></td>
    <td><div class="item"><select name="performance" size="1" class="form" id="performance">
      <option selected><?php echo $data[performance]?><value="<?php echo $data[performance]?>"></option>
      <option value="0">0</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
    </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Science</div></td>
    <td><div class="item"><select name="science" size="1" class="form" id="science">
        <option selected><?php echo $data['science']?><value="<?php echo $data['science']?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="item">Subterfuge</div></td>
    <td><div class="item"><select name="subterfuge" size="1" class="form" id="subterfuge">
        <option selected><?php echo $data[subterfuge]?><value="<?php echo $data[subterfuge]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Stealth</div></td>
    <td><div class="item"><select name="stealth" size="1" class="form" id="stealth">
        <option selected><?php echo $data[stealth]?><value="<?php echo $data[stealth]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Technology</div></td>
    <td><div class="item"><select name="technology" size="1" class="form" id="technology">
        <option selected><?php echo $data[technology]?><value="<?php echo $data[technology]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
</table>
