<table width="100%" border="0" >
    <tr>
            <td width="21%"><div class='item'>Merits</div></td>
            <td width="79%"><div class="item"><textarea name="merits" cols="50" datas="2" class="form" id="merits">List merits here as well as noting what book and page they are from.</textarea></div></td>
      </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
      <tr>
            <td><div class='item'>Flaws</div></td>
            <td><div class="item"><textarea name="flaws" cols="50" datas="2" class="form" id="flaws">List flaws here as well as noting what book and page they are from.</textarea></div></td>
      </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
      <tr>
            <td><div class='item'>Creation Notes</div></td>
            <td><div class="item"><textarea name="cre_notes" cols="50" datas="2" class="form" id="cre_notes">Creation notes should include on the first line the endowment used for the character, note out some specifics of how the freebies were spent such as Strength to 2, 5fbs this will help both you and staff see how everything was allotted.</textarea></div></td>
      </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
      <tr>
            <td><div class='item'>History</div></td>
            <td><div class='item'><textarea name="player_notes" cols="50" datas="2" class="form" id="player_notes">This is where you will put your character background. There is a max of 2800 characters anything past that will be ignored.</textarea></div></td>
      </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
      <tr>
            <td><div class='item'>Gear Carried</div></td>
            <td><div class="item"><textarea name="equipment" cols="50" datas="2" class="form" id="equipment">List unusual items the characters carry, things like ID, keys and phones are generally assumed, list weapons that are always carried as well.</textarea></div></td>
      </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
      <tr>
            <td><div class='item'>Possessions</div></td>
            <td><div class="item"><textarea name="othereq" cols="50" datas="2" class="form" id="othereq">List possessions like homes, businesses, automobiles and the like there. Items like weapons and such not often carried should be listed here as well.</textarea></div></td>
      </tr>
      <tr>
            <td colspan="2"><hr></td>
      </tr>
 <tr>
    <td colspan="2"><table style="width:100%">
      <tr>
        <td width="75%"><div class="warning">Alert staff character is ready for approval?</div></td>
        <td width="5%">&nbsp;</td>
        <td width="20%"><div class="item"><select name="sfa" size="1" class="form" id="sfa">
            <option selected="selected"><?php echo $data[sfa]?></option>
            <option value="No">No</option>
            <option value="Yes">Yes</option>
        </select></div></td>
      </tr>
      <tr>
        <td colspan="3"><hr /></td>
      </tr>
    </table></td>
  </tr>
</table>