<table width="100%" border="0" >
  <tr>
    <td colspan="8"><div class="pagetopic">Attributes</div></td>
  </tr>
  <tr> 
    <td width="21%"><div class='item'>Strength</div></td>
    <td width="11%"><div class="item">
        <select name="strength" id="strength" class="form">
            <option value="<?php echo $data['strength']; ?>"><?php echo $data['strength']; ?></option>
            <?php
            for ($i = 1; $i <= 7; $i++) {
                echo "<option value=\"$i\">$i</option>";
            }
            ?>
        </select>
    </div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'>Charisma</div></td>
    <td width="11%"><div class="item"><select name="charisma" id="charisma" class="form">
        <option value="<?php echo $data['charisma']; ?>"><?php echo $data['charisma']; ?></option>
        <?php for ($i = 1; $i <= 7; $i++): ?>
            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
        <?php endfor; ?>
    </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'>Perception</div></td>
    <td width="11%"><div class="item"><select name="perception" id="perception" class="form">
        <option value="<?php echo $data['perception']; ?>"><?php echo $data['perception']; ?></option>
        <?php for ($i = 1; $i <= 7; $i++): ?>
            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
        <?php endfor; ?>
    </select></div></td>
  </tr>
  <tr> 
    <td><div class='item'>Dexterity</div></td>
    <td><div class="item"><select name="dexterity" id="dexterity-select" class="form">
        <?php foreach (range(1, 7) as $value): ?>
            <option <?php echo $value == $data['dexterity'] ? 'selected' : ''; ?>><?php echo $value; ?></option>
        <?php endforeach; ?>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Manipulation</div></td>
    <td><div class="item"><select name="manipulation" id="manipulation-select" class="form">
        <?php foreach (range(1, 7) as $value): ?>
            <option <?php echo $value == $data['manipulation'] ? 'selected' : ''; ?>><?php echo $value; ?></option>
        <?php endforeach; ?>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Intelligence</div></td>
    <td><div class="item"><select name="intelligence" id="intelligence-select" class="form">
        <?php foreach (range(1, 7) as $value): ?>
            <option <?php echo $value == $data['intelligence'] ? 'selected' : ''; ?>><?php echo $value; ?></option>
        <?php endforeach; ?>
      </select></div></td>
  </tr>
  <tr> 
    <td><div class='item'>Stamina</div></td>
    <td><div class="item"><select name="stamina" id="stamina-select" class="form">
        <?php foreach (range(1, 7) as $value): ?>
            <option <?php echo $value == $data['stamina'] ? 'selected' : ''; ?>><?php echo $value; ?></option>
        <?php endforeach; ?>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Appearance</div></td>
    <td><div class="item"><select name="appearance" id="appearance-select" class="form">
        <?php foreach (range(0, 7) as $value): ?>
            <option <?php echo $value == $data['appearance'] ? 'selected' : ''; ?>><?php echo $value; ?></option>
        <?php endforeach; ?>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Wits</div></td>
    <td><div class="item"><select name="wits" id="wits-select" class="form">
        <?php foreach (range(1, 7) as $value): ?>
            <option <?php echo $value == $data['wits'] ? 'selected' : ''; ?>><?php echo $value; ?></option>
        <?php endforeach; ?>
      </select></div></td>
  </tr>
  <tr> 
    <td><div class='pageitem'>Specialties</div></td>
    <td colspan="8"><div class='item'><textarea name="attspecial" cols="57" datas="2" class="form" id="attspecial"><?php echo $data['attspecial']?></textarea></div></td>
  </tr>
</table>