<table border="0" width="100%">
  <tr> 
    <td colspan="11"><div class="pagetopic">Renown</div></td>
  </tr>
  <tr> 
    <td width="13%"><div class="item"><select name="renown1" size="1" class="form" id="renown1">
        <option selected><?php echo $data[renown1]?><value="<?php echo $data[renown1]?>"></option>
        <option value="Glory">Glory</option>
        <option value="Cunning">Cunning</option>
        <option value="Ferocity">Ferocity</option>
        <option value="Honor">Honor</option>
        <option value="Humor">Humor</option>
        <option value="Infamy">Infamy</option>
        <option value="Obedience">Obedience</option>
        <option value="Valor">Valor</option>
      </select></div></td>
    <td width="10%"><div align="center" class="itemsm">Perm</div></td>
    <td width="10%"><div class="item"><select name="renown1perm" size="1" class="form" id="renown1perm">
        <option selected><?php echo $data[renown1perm]?><value="<?php echo $data[renown1perm]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="13%"><div class="item"><select name="renown2" size="1" class="form" id="renown2">
        <option selected><?php echo $data[renown2]?><value="<?php echo $data[renown2]?>"></option>
        <option value="Honor">Honor</option>
        <option value="Ferocity">Ferocity</option>
        <option value="Cunning">Cunning</option>
        <option value="Succor">Succor</option>
        <option value="Glory">Glory</option>
        <option value="Obligation">Obligation</option>
        <option value="Harmony">Harmony</option>
      </select></div></td>
    <td width="10%"><div align="center" class="itemsm">Perm</div></td>
    <td width="10%"><div class="item"><select name="renown2perm" size="1" class="form" id="renown2perm">
        <option selected><?php echo $data[renown2perm]?><value="<?php echo $data[renown2perm]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="13%"><div class="item"><select name="renown3" size="1" class="form" id="renown3">
        <option selected><?php echo $data[renown3]?><value="<?php echo $data[renown3]?>"></option>
        <option value="Wisdom">Wisdom</option>
        <option value="Honor">Honor</option>
        <option value="Obligation">Obligation</option>
        <option value="Cunning">Cunning</option>
        <option value="Innovation">Innovation</option>
      </select></div></td>
    <td width="10%"><div align="center" class="itemsm">Perm</div></td>
    <td width="10%"><div class="item"><select name="renown3perm" size="1" class="form" id="renown3perm">
        <option selected><?php echo $data[renown3perm]?><value="<?php echo $data[renown3perm]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td><div align="center" class="itemsm">Temp</div></td>
    <td><div class="item"><select name="renown1temp" size="1" class="form" id="renown1temp">
        <option selected><?php echo $data[renown1temp]?><value="<?php echo $data[renown1temp]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td></td>
    <td><div align="center" class="itemsm">Temp</div></td>
    <td><div class="item"><select name="renown2temp" size="1" class="form" id="renown2temp">
        <option selected><?php echo $data[renown2temp]?><value="<?php echo $data[renown2temp]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center" class="itemsm">Temp</div></td>
    <td><div class="item"><select name="renown3temp" size="1" class="form" id="renown3temp">
        <option selected><?php echo $data[renown3temp]?><value="<?php echo $data[renown3temp]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
  </tr>
</table>


<div class="center"><hr width="80%" /></div>

<table width="100%">
  <tr>
    <td width="24%"><div class="item">Other Venue Details:</div></td>
    <td width="76%" colspan="10"><div class="item"><textarea name="shifter_venue" cols="56" datas="2" class="form" id="shifter_venue">This is where you put information such as: Derangements, pack affiliation, and Numina rituals. You can use formatting found in the profile guide to help organize this. Include page numbers and book for quick reference.</textarea></div></td>
  </tr>
</table>