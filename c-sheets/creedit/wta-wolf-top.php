<table width="100%">
      <tr>
        <td width="24%"><div class='item'>Deed Name</div></td>
        <td width="25%"><div class='item'><input name="alt_name" type="text" class="form" id="alt_name" size="21" maxlength="24" value="<?php echo $data[alt_name]?>" /></div></td>
        <td width="2%">&nbsp;</td>
        <td width="24%"><div class='item'>Rank</div></td>
        <td width="25%"><div class='item'><select name="rank" size="1" class="form" id="rank">
          <option selected><?php echo $data[rank]?><value="<?php echo $data[rank]?>"></option>
          <option value="0">0</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
      </tr>
      <tr>
        <td><div class='item'>Tribe</div></td>
        <td><div class='item'><select name="tribe" size="1" class="form" id="tribe">
      <option selected><?php echo $data[tribe]?><value="<?php echo $data[tribe]?>"></option>
        <option value="Black Furies">Black Furies</option>
        <option value="Bone Gnawers">Bone Gnawers</option>
        <option value="Child of Gaia">Child of Gaia</option>
        <option value="Fianna">Fianna</option>
        <option value="Get of Fenris">Get of Fenris</option>
        <option value="Glass Walkers">Glass Walkers</option>
        <option value="Red Talons">Red Talons</option>
        <option value="Shadow Lords">Shadow Lords</option>
        <option value="Silent Striders">Silent Striders</option>
        <option value="Silver Fangs">Silver Fangs</option>
        <option value="Uktena">Uktena</option>
        <option value="Wendigo">Wendigo</option></select></div></td>
        <td>&nbsp;</td>
        <td><div class='item'>Camp</div></td>
        <td><div class='item'><input name="camp" type="text" class="form" id="camp" size="15" maxlength="24" value="<?php echo $data[camp]?>" /></div></td>
      </tr>
      <tr>
        <td><div class='item'>Breed</div></td>
        <td><div class='item'><select name="breed" size="1" class="form" id="breed">
          <option selected><?php echo $data[breed]?><value="<?php echo $data[breed]?>"></option>
          <option value="Homid">Homid</option>
          <option value="Metis">Metis</option>
          <option value="Lupus">Lupus</option>
        </select></div></td>
        <td>&nbsp;</td>
        <td><div class='item'>Pack Name</div></td>
        <td><div class='item'><input name="packname" type="text" class="form" id="packname" size="15" maxlength="24" value="<?php echo $data[packname]?>" /></div></td>
      </tr>
      <tr>
        <td><div class='item'>Auspice</div></td>
        <td><div class='item'><select name="auspice" size="1" class="form" id="auspice">
          <option selected><?php echo $data[auspice]?><value="<?php echo $data[auspice]?>"></option>
          <option value="Ragabash">Ragabash</option>
          <option value="Theurge">Theurge</option>
          <option value="Philodox">Philodox</option>
          <option value="Galliard">Galliard</option>
          <option value="Ahroun">Ahroun</option>
        </select></div></td>
        <td>&nbsp;</td>
        <td><div class='item'>Pack Totem</div></td>
        <td><div class='item'><input name="packtotem" type="text" class="form" id="packtotem" size="15" maxlength="24" value="<?php echo $data[packtotem]?>" /></div></td>
      </tr>
    </table>