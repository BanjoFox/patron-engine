<table width="100%"  border="0" >
 <tr>
    <td width="13%"><div class="item">Willpower</div></td>
    <td width="10%"><div align="center" class="itemsm">Perm</div></td>
    <td width="10%"><div class="item"><select name="wpperm" size="1" class="form" id="wpperm">
        <option selected><value="<?php echo $data[wpperm]?>"><?php echo $data[wpperm]?></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td width="2%">&nbsp;</td>
    <td colspan="2"><div class="item">Humanity</div></td>
    <td width="10%"><div class="item"><select name="pathval" size="1" class="form" id="pathval">
        <option selected><value="<?php echo $data['pathval']?>"><?php echo $data['pathval']?></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
    </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="13%"><div class="item"><select name="virtue1" size="1" class="form" id="virtue1">
        <option selected><?php echo $data[virtue1]?><value="<?php echo $data[virtue1]?>"></option>
        <option value="Conscience">Conscience</option></select></div></td>
    <td width="10%"></td>
    <td width="10%"><div class="item"><select name="virtue1val" size="1" class="form" id="virtue1val">
        <option selected><value="<?php echo $data['virtue1val']?>"><?php echo $data['virtue1val']?></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>


  <tr>
    <td>&nbsp;</td>
    <td><div align="center" class="itemsm">Temp</div></td>
    <td><div class="item"><select name="wptemp" size="1" class="form" id="wptemp">
        <option selected><value="<?php echo $data[wptemp]?>"><?php echo $data[wptemp]?></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td colspan="3"><div class="item">Bearing: <input name="pathbearing" type="text" class="form" id="pathbearing" size="18" maxlength="16" value="<?php echo $data[pathbearing]?>"></div></td>
    <td>&nbsp;</td>
    <td><div class="item"><select name="virtue2" size="1" class="form" id="virtue2">
        <option selected><?php echo $data[virtue2]?><value="<?php echo $data[virtue2]?>"></option>
        <option value="Self-Control">Self-Control</option></select></div></td>
    <td>&nbsp;</td>
    <td><div class="item"><select name="virtue2val" size="1" class="form" id="virtue2val">
        <option selected><value="<?php echo $data['virtue2val']?>"><?php echo $data['virtue2val']?></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="center">&nbsp;</div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2"><div class="itemsm"></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div class="item">Courage</div></td>
    <td>&nbsp;</td>
    <td><div class="item"><select name="courage" size="1" class="form" id="courage">
        <option selected><?php echo $data[courage]?></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
    </select></div></td>
  </tr>
</table>