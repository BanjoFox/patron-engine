<table width="100%" border="0" >
  <tr>
    <td colspan="8"><div class="center">
        <div class="pageitem">Disciplines</div></td>
  </tr>
  <tr>
    <td width="21%"><div class='item'><select name="power1" size="1" class="form" id="power1">
      <option selected><?php echo $data[power1]?><value="<?php echo $data[power1]?>"></option>
        <option value=""></option>
        <option value="Animalism">Animalism</option>
        <option value="Auspex">Auspex</option>
        <option value="Celerity">Celerity</option>
        <option value="Chimerstry">Chimerstry</option>
        <option value="Dementation">Dementation</option>
        <option value="Dominate">Dominate</option>
        <option value="Flight">Flight</option>
        <option value="Fortitude">Fortitude</option>
        <option value="Melpominee">Melpominee</option>   
        <option value="Necromancy">Necromancy</option>
        <option value="Obeah">Obeah</option>
        <option value="Obfuscate">Obfuscate</option>
        <option value="Obtenebration">Obtenebration</option>
        <option value="Potence">Potence</option>
        <option value="Presence">Presence</option>
        <option value="Protean">Protean</option>
        <option value="Quietus">Quietus</option>
        <option value="Serpentis">Serpentis</option>
        <option value="Temporis">Temporis</option>
        <option value="Thaumaturgy">Thaumaturgy</option>
        <option value="Thanatosis">Thanatosis</option>
        <option value="Vicissitude">Vicissitude</option>
        <option value="Visceratika">Visceratika</option></select></div></td>
    <td width="11%"><div class='item'><select name="power1val" size="1" class="form" id="power1val">
          <option selected><?php echo $data['power1val']?><value="<?php echo $data['power1val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><select name="power2" size="1" class="form" id="power2">
      <option selected><?php echo $data[power2]?><value="<?php echo $data[power2]?>"></option>
        <option value=""></option>
        <option value="Animalism">Animalism</option>
        <option value="Auspex">Auspex</option>
        <option value="Celerity">Celerity</option>
        <option value="Chimerstry">Chimerstry</option>
        <option value="Dementation">Dementation</option>
        <option value="Dominate">Dominate</option>
        <option value="Flight">Flight</option>
        <option value="Fortitude">Fortitude</option>
        <option value="Melpominee">Melpominee</option>   
        <option value="Necromancy">Necromancy</option>
        <option value="Obeah">Obeah</option>
        <option value="Obfuscate">Obfuscate</option>
        <option value="Obtenebration">Obtenebration</option>
        <option value="Potence">Potence</option>
        <option value="Presence">Presence</option>
        <option value="Protean">Protean</option>
        <option value="Quietus">Quietus</option>
        <option value="Serpentis">Serpentis</option>
        <option value="Temporis">Temporis</option>
        <option value="Thaumaturgy">Thaumaturgy</option>
        <option value="Thanatosis">Thanatosis</option>
        <option value="Vicissitude">Vicissitude</option>
        <option value="Visceratika">Visceratika</option></select></div></td>
    <td width="11%"><div class='item'><select name="power2val" size="1" class="form" id="power2val">
          <option selected><?php echo $data['power2val']?><value="<?php echo $data['power2val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><select name="power3" size="1" class="form" id="power3">
      <option selected><?php echo $data[power3]?><value="<?php echo $data[power3]?>"></option>
        <option value=""></option>
        <option value="Animalism">Animalism</option>
        <option value="Auspex">Auspex</option>
        <option value="Celerity">Celerity</option>
        <option value="Chimerstry">Chimerstry</option>
        <option value="Dementation">Dementation</option>
        <option value="Dominate">Dominate</option>
        <option value="Flight">Flight</option>
        <option value="Fortitude">Fortitude</option>
        <option value="Melpominee">Melpominee</option>   
        <option value="Necromancy">Necromancy</option>
        <option value="Obeah">Obeah</option>
        <option value="Obfuscate">Obfuscate</option>
        <option value="Obtenebration">Obtenebration</option>
        <option value="Potence">Potence</option>
        <option value="Presence">Presence</option>
        <option value="Protean">Protean</option>
        <option value="Quietus">Quietus</option>
        <option value="Serpentis">Serpentis</option>
        <option value="Temporis">Temporis</option>
        <option value="Thaumaturgy">Thaumaturgy</option>
        <option value="Thanatosis">Thanatosis</option>
        <option value="Vicissitude">Vicissitude</option>
        <option value="Visceratika">Visceratika</option></select></div></td>
    <td width="11%"><div class='item'><select name="power3val" size="1" class="form" id="power3val">
          <option selected><?php echo $data['power3val']?><value="<?php echo $data['power3val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><select name="power4" size="1" class="form" id="power4">
      <option selected><?php echo $data[power4]?><value="<?php echo $data[power4]?>"></option>
        <option value=""></option>
        <option value="Animalism">Animalism</option>
        <option value="Auspex">Auspex</option>
        <option value="Celerity">Celerity</option>
        <option value="Chimerstry">Chimerstry</option>
        <option value="Dementation">Dementation</option>
        <option value="Dominate">Dominate</option>
        <option value="Flight">Flight</option>
        <option value="Fortitude">Fortitude</option>
        <option value="Melpominee">Melpominee</option>   
        <option value="Necromancy">Necromancy</option>
        <option value="Obeah">Obeah</option>
        <option value="Obfuscate">Obfuscate</option>
        <option value="Obtenebration">Obtenebration</option>
        <option value="Potence">Potence</option>
        <option value="Presence">Presence</option>
        <option value="Protean">Protean</option>
        <option value="Quietus">Quietus</option>
        <option value="Serpentis">Serpentis</option>
        <option value="Temporis">Temporis</option>
        <option value="Thaumaturgy">Thaumaturgy</option>
        <option value="Thanatosis">Thanatosis</option>
        <option value="Vicissitude">Vicissitude</option>
        <option value="Visceratika">Visceratika</option></select></div></td>
    <td><div class='item'><select name="power4val" size="1" class="form" id="power4val">
          <option selected><?php echo $data['power4val']?><value="<?php echo $data['power4val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power5" size="1" class="form" id="power5">
      <option selected><?php echo $data[power5]?><value="<?php echo $data[power5]?>"></option>
        <option value=""></option>
        <option value="Animalism">Animalism</option>
        <option value="Auspex">Auspex</option>
        <option value="Celerity">Celerity</option>
        <option value="Chimerstry">Chimerstry</option>
        <option value="Dementation">Dementation</option>
        <option value="Dominate">Dominate</option>
        <option value="Flight">Flight</option>
        <option value="Fortitude">Fortitude</option>
        <option value="Melpominee">Melpominee</option>   
        <option value="Necromancy">Necromancy</option>
        <option value="Obeah">Obeah</option>
        <option value="Obfuscate">Obfuscate</option>
        <option value="Obtenebration">Obtenebration</option>
        <option value="Potence">Potence</option>
        <option value="Presence">Presence</option>
        <option value="Protean">Protean</option>
        <option value="Quietus">Quietus</option>
        <option value="Serpentis">Serpentis</option>
        <option value="Temporis">Temporis</option>
        <option value="Thaumaturgy">Thaumaturgy</option>
        <option value="Thanatosis">Thanatosis</option>
        <option value="Vicissitude">Vicissitude</option>
        <option value="Visceratika">Visceratika</option></select></div></td>
    <td><div class='item'><select name="power5val" size="1" class="form" id="power5val">
          <option selected><?php echo $data['power5val']?><value="<?php echo $data['power5val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power6" size="1" class="form" id="power6">
      <option selected><?php echo $data[power6]?><value="<?php echo $data[power6]?>"></option>
        <option value=""></option>
        <option value="Animalism">Animalism</option>
        <option value="Auspex">Auspex</option>
        <option value="Celerity">Celerity</option>
        <option value="Chimerstry">Chimerstry</option>
        <option value="Dementation">Dementation</option>
        <option value="Dominate">Dominate</option>
        <option value="Flight">Flight</option>
        <option value="Fortitude">Fortitude</option>
        <option value="Melpominee">Melpominee</option>   
        <option value="Necromancy">Necromancy</option>
        <option value="Obeah">Obeah</option>
        <option value="Obfuscate">Obfuscate</option>
        <option value="Obtenebration">Obtenebration</option>
        <option value="Potence">Potence</option>
        <option value="Presence">Presence</option>
        <option value="Protean">Protean</option>
        <option value="Quietus">Quietus</option>
        <option value="Serpentis">Serpentis</option>
        <option value="Temporis">Temporis</option>
        <option value="Thaumaturgy">Thaumaturgy</option>
        <option value="Thanatosis">Thanatosis</option>
        <option value="Vicissitude">Vicissitude</option>
        <option value="Visceratika">Visceratika</option></select></div></td>
    <td><div class='item'><select name="power6val" size="1" class="form" id="power6val">
          <option selected><?php echo $data['power6val']?><value="<?php echo $data['power6val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><select name="power7" size="1" class="form" id="power7">
      <option selected><?php echo $data[power7]?><value="<?php echo $data[power7]?>"></option>
        <option value=""></option>
        <option value="Animalism">Animalism</option>
        <option value="Auspex">Auspex</option>
        <option value="Celerity">Celerity</option>
        <option value="Chimerstry">Chimerstry</option>
        <option value="Dementation">Dementation</option>
        <option value="Dominate">Dominate</option>
        <option value="Flight">Flight</option>
        <option value="Fortitude">Fortitude</option>
        <option value="Melpominee">Melpominee</option>   
        <option value="Necromancy">Necromancy</option>
        <option value="Obeah">Obeah</option>
        <option value="Obfuscate">Obfuscate</option>
        <option value="Obtenebration">Obtenebration</option>
        <option value="Potence">Potence</option>
        <option value="Presence">Presence</option>
        <option value="Protean">Protean</option>
        <option value="Quietus">Quietus</option>
        <option value="Serpentis">Serpentis</option>
        <option value="Temporis">Temporis</option>
        <option value="Thaumaturgy">Thaumaturgy</option>
        <option value="Thanatosis">Thanatosis</option>
        <option value="Vicissitude">Vicissitude</option>
        <option value="Visceratika">Visceratika</option></select></div></td>
    <td><div class='item'><select name="power7val" size="1" class="form" id="power7val">
          <option selected><?php echo $data['power7val']?><value="<?php echo $data['power7val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power8" size="1" class="form" id="power8">
      <option selected><?php echo $data[power8]?><value="<?php echo $data[power8]?>"></option>
        <option value=""></option>
        <option value="Animalism">Animalism</option>
        <option value="Auspex">Auspex</option>
        <option value="Celerity">Celerity</option>
        <option value="Chimerstry">Chimerstry</option>
        <option value="Dementation">Dementation</option>
        <option value="Dominate">Dominate</option>
        <option value="Flight">Flight</option>
        <option value="Fortitude">Fortitude</option>
        <option value="Melpominee">Melpominee</option>   
        <option value="Necromancy">Necromancy</option>
        <option value="Obeah">Obeah</option>
        <option value="Obfuscate">Obfuscate</option>
        <option value="Obtenebration">Obtenebration</option>
        <option value="Potence">Potence</option>
        <option value="Presence">Presence</option>
        <option value="Protean">Protean</option>
        <option value="Quietus">Quietus</option>
        <option value="Serpentis">Serpentis</option>
        <option value="Temporis">Temporis</option>
        <option value="Thaumaturgy">Thaumaturgy</option>
        <option value="Thanatosis">Thanatosis</option>
        <option value="Vicissitude">Vicissitude</option>
        <option value="Visceratika">Visceratika</option></select></div></td>
    <td><div class='item'><select name="power8val" size="1" class="form" id="power8val">
          <option selected><?php echo $data['power8val']?><value="<?php echo $data['power8val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power9" size="1" class="form" id="power9">
      <option selected><?php echo $data[power9]?><value="<?php echo $data[power9]?>"></option>
        <option value=""></option>
        <option value="Animalism">Animalism</option>
        <option value="Auspex">Auspex</option>
        <option value="Celerity">Celerity</option>
        <option value="Chimerstry">Chimerstry</option>
        <option value="Dementation">Dementation</option>
        <option value="Dominate">Dominate</option>
        <option value="Flight">Flight</option>
        <option value="Fortitude">Fortitude</option>
        <option value="Melpominee">Melpominee</option>   
        <option value="Necromancy">Necromancy</option>
        <option value="Obeah">Obeah</option>
        <option value="Obfuscate">Obfuscate</option>
        <option value="Obtenebration">Obtenebration</option>
        <option value="Potence">Potence</option>
        <option value="Presence">Presence</option>
        <option value="Protean">Protean</option>
        <option value="Quietus">Quietus</option>
        <option value="Serpentis">Serpentis</option>
        <option value="Temporis">Temporis</option>
        <option value="Thaumaturgy">Thaumaturgy</option>
        <option value="Thanatosis">Thanatosis</option>
        <option value="Vicissitude">Vicissitude</option>
        <option value="Visceratika">Visceratika</option></select></div></td>
    <td><div class='item'><select name="power9val" size="1" class="form" id="power9val">
          <option selected><?php echo $data['power9val']?><value="<?php echo $data['power9val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><select name="power10" size="1" class="form" id="power10">
      <option selected><?php echo $data[power10]?><value="<?php echo $data[power10]?>"></option>
        <option value=""></option>
        <option value="Animalism">Animalism</option>
        <option value="Auspex">Auspex</option>
        <option value="Celerity">Celerity</option>
        <option value="Chimerstry">Chimerstry</option>
        <option value="Dementation">Dementation</option>
        <option value="Dominate">Dominate</option>
        <option value="Flight">Flight</option>
        <option value="Fortitude">Fortitude</option>
        <option value="Melpominee">Melpominee</option>   
        <option value="Necromancy">Necromancy</option>
        <option value="Obeah">Obeah</option>
        <option value="Obfuscate">Obfuscate</option>
        <option value="Obtenebration">Obtenebration</option>
        <option value="Potence">Potence</option>
        <option value="Presence">Presence</option>
        <option value="Protean">Protean</option>
        <option value="Quietus">Quietus</option>
        <option value="Serpentis">Serpentis</option>
        <option value="Temporis">Temporis</option>
        <option value="Thaumaturgy">Thaumaturgy</option>
        <option value="Thanatosis">Thanatosis</option>
        <option value="Vicissitude">Vicissitude</option>
        <option value="Visceratika">Visceratika</option></select></div></td>
    <td><div class='item'><select name="power10val" size="1" class="form" id="power10val">
          <option selected><?php echo $data['power10val']?><value="<?php echo $data['power10val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power11" size="1" class="form" id="power11">
      <option selected><?php echo $data[power11]?><value="<?php echo $data[power11]?>"></option>
        <option value=""></option>
        <option value="Animalism">Animalism</option>
        <option value="Auspex">Auspex</option>
        <option value="Celerity">Celerity</option>
        <option value="Chimerstry">Chimerstry</option>
        <option value="Dementation">Dementation</option>
        <option value="Dominate">Dominate</option>
        <option value="Flight">Flight</option>
        <option value="Fortitude">Fortitude</option>
        <option value="Melpominee">Melpominee</option>   
        <option value="Necromancy">Necromancy</option>
        <option value="Obeah">Obeah</option>
        <option value="Obfuscate">Obfuscate</option>
        <option value="Obtenebration">Obtenebration</option>
        <option value="Potence">Potence</option>
        <option value="Presence">Presence</option>
        <option value="Protean">Protean</option>
        <option value="Quietus">Quietus</option>
        <option value="Serpentis">Serpentis</option>
        <option value="Temporis">Temporis</option>
        <option value="Thaumaturgy">Thaumaturgy</option>
        <option value="Thanatosis">Thanatosis</option>
        <option value="Vicissitude">Vicissitude</option>
        <option value="Visceratika">Visceratika</option></select></div></td>
    <td><div class='item'><select name="power11val" size="1" class="form" id="power11val">
          <option selected><?php echo $data['power11val']?><value="<?php echo $data['power11val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power12" size="1" class="form" id="power12">
      <option selected><?php echo $data[power12]?><value="<?php echo $data[power12]?>"></option>
        <option value=""></option>
        <option value="Animalism">Animalism</option>
        <option value="Auspex">Auspex</option>
        <option value="Celerity">Celerity</option>
        <option value="Chimerstry">Chimerstry</option>
        <option value="Dementation">Dementation</option>
        <option value="Dominate">Dominate</option>
        <option value="Flight">Flight</option>
        <option value="Fortitude">Fortitude</option>
        <option value="Melpominee">Melpominee</option>   
        <option value="Necromancy">Necromancy</option>
        <option value="Obeah">Obeah</option>
        <option value="Obfuscate">Obfuscate</option>
        <option value="Obtenebration">Obtenebration</option>
        <option value="Potence">Potence</option>
        <option value="Presence">Presence</option>
        <option value="Protean">Protean</option>
        <option value="Quietus">Quietus</option>
        <option value="Serpentis">Serpentis</option>
        <option value="Temporis">Temporis</option>
        <option value="Thaumaturgy">Thaumaturgy</option>
        <option value="Thanatosis">Thanatosis</option>
        <option value="Vicissitude">Vicissitude</option>
        <option value="Visceratika">Visceratika</option></select></div></td>
    <td><div class='item'><select name="power12val" size="1" class="form" id="power12val">
          <option selected><?php echo $data['power12val']?><value="<?php echo $data['power12val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><select name="power13" size="1" class="form" id="power13">
      <option selected><?php echo $data[power13]?><value="<?php echo $data[power13]?>"></option>
        <option value=""></option>
        <option value="Animalism">Animalism</option>
        <option value="Auspex">Auspex</option>
        <option value="Celerity">Celerity</option>
        <option value="Chimerstry">Chimerstry</option>
        <option value="Dementation">Dementation</option>
        <option value="Dominate">Dominate</option>
        <option value="Flight">Flight</option>
        <option value="Fortitude">Fortitude</option>
        <option value="Melpominee">Melpominee</option>   
        <option value="Necromancy">Necromancy</option>
        <option value="Obeah">Obeah</option>
        <option value="Obfuscate">Obfuscate</option>
        <option value="Obtenebration">Obtenebration</option>
        <option value="Potence">Potence</option>
        <option value="Presence">Presence</option>
        <option value="Protean">Protean</option>
        <option value="Quietus">Quietus</option>
        <option value="Serpentis">Serpentis</option>
        <option value="Temporis">Temporis</option>
        <option value="Thaumaturgy">Thaumaturgy</option>
        <option value="Thanatosis">Thanatosis</option>
        <option value="Vicissitude">Vicissitude</option>
        <option value="Visceratika">Visceratika</option></select></div></td>
    <td><div class='item'><select name="power13val" size="1" class="form" id="power13val">
          <option selected><?php echo $data['power13val']?><value="<?php echo $data['power13val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power14" size="1" class="form" id="power14">
      <option selected><?php echo $data[power14]?><value="<?php echo $data[power14]?>"></option>
        <option value=""></option>
        <option value="Animalism">Animalism</option>
        <option value="Auspex">Auspex</option>
        <option value="Celerity">Celerity</option>
        <option value="Chimerstry">Chimerstry</option>
        <option value="Dementation">Dementation</option>
        <option value="Dominate">Dominate</option>
        <option value="Flight">Flight</option>
        <option value="Fortitude">Fortitude</option>
        <option value="Melpominee">Melpominee</option>   
        <option value="Necromancy">Necromancy</option>
        <option value="Obeah">Obeah</option>
        <option value="Obfuscate">Obfuscate</option>
        <option value="Obtenebration">Obtenebration</option>
        <option value="Potence">Potence</option>
        <option value="Presence">Presence</option>
        <option value="Protean">Protean</option>
        <option value="Quietus">Quietus</option>
        <option value="Serpentis">Serpentis</option>
        <option value="Temporis">Temporis</option>
        <option value="Thaumaturgy">Thaumaturgy</option>
        <option value="Thanatosis">Thanatosis</option>
        <option value="Vicissitude">Vicissitude</option>
        <option value="Visceratika">Visceratika</option></select></div></td>
    <td><div class='item'><select name="power14val" size="1" class="form" id="power14val">
          <option selected><?php echo $data['power14val']?><value="<?php echo $data['power14val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power15" size="1" class="form" id="power15">
      <option selected><?php echo $data[power15]?><value="<?php echo $data[power15]?>"></option>
        <option value=""></option>
        <option value="Animalism">Animalism</option>
        <option value="Auspex">Auspex</option>
        <option value="Celerity">Celerity</option>
        <option value="Chimerstry">Chimerstry</option>
        <option value="Dementation">Dementation</option>
        <option value="Dominate">Dominate</option>
        <option value="Flight">Flight</option>
        <option value="Fortitude">Fortitude</option>
        <option value="Melpominee">Melpominee</option>   
        <option value="Necromancy">Necromancy</option>
        <option value="Obeah">Obeah</option>
        <option value="Obfuscate">Obfuscate</option>
        <option value="Obtenebration">Obtenebration</option>
        <option value="Potence">Potence</option>
        <option value="Presence">Presence</option>
        <option value="Protean">Protean</option>
        <option value="Quietus">Quietus</option>
        <option value="Serpentis">Serpentis</option>
        <option value="Temporis">Temporis</option>
        <option value="Thaumaturgy">Thaumaturgy</option>
        <option value="Thanatosis">Thanatosis</option>
        <option value="Vicissitude">Vicissitude</option>
        <option value="Visceratika">Visceratika</option></select></div></td>
    <td><div class='item'><select name="power15val" size="1" class="form" id="power15val">
          <option selected><?php echo $data['power15val']?><value="<?php echo $data['power15val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
  </tr>
  <tr>
</table>