<table width="100%" border="0" >
  <tr>
    <td colspan="8"><div class="pagetopic">Abilities</div></td>
  </tr>
  <tr>
    <td width="130"><div class="item">Alertness</div></td>
    <td width="50"><div class="item"><select name="alertness" id="alertness" class="form">
        <option selected><?php echo $data[alertness]?><value="<?php echo $data['alertness']?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td width="5">&nbsp;</td>
    <td width="130"><div class="item">Animal Ken</div></td>
    <td width="50"><div class="item"><select name="animalken" id="animalken" class="form">
        <option selected><?php echo $data[animalken]?><value="<?php echo $data[animalken]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td width="5">&nbsp;</td>
    <td width="130"><div class="item">Academics</div></td>
    <td width="50"><div class="item"><select name="academics" id="academics" class="form">
        <option selected><?php echo $data[academics]?><value="<?php echo $data[academics]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="item">Athletics</div></td>
    <td><div class="item"><select name="athletics" id="athletics" class="form">
        <option selected><?php echo $data[athletics]?><value="<?php echo $data[athletics]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Crafts</div></td>
    <td><div class="item"><select name="crafts" id="crafts" class="form">
        <option selected><?php echo $data[crafts]?><value="<?php echo $data[crafts]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Cosmology</div></td>
    <td><div class="item"><select name="cosmology" id="cosmology" class="form">
        <option selected><?php echo $data[cosmology]?><value="<?php echo $data[cosmology]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="item">Awareness</div></td>
    <td><div class="item"><select name="awareness" id="awareness" class="form">
        <option selected><?php echo $data[awareness]?><value="<?php echo $data[awareness]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Etiquette</div></td>
    <td><div class="item"><select name="etiquette" id="etiquette" class="form">
        <option selected><?php echo $data[etiquette]?><value="<?php echo $data[etiquette]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td width="5">&nbsp;</td>
    <td width="130"><div class="item">Enigmas</div></td>
    <td width="50"><div class="item"><select name="enigmas" id="enigmas" class="form">
        <option selected><?php echo $data[enigmas]?><value="<?php echo $data[enigmas]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="item">Brawl</div></td>
    <td><div class="item"><select name="brawl" id="brawl" class="form">
        <option selected><?php echo $data[brawl]?><value="<?php echo $data[brawl]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Larceny</div></td>
    <td><div class="item"><select name="larceny" id="larceny" class="form">
        <option selected><?php echo $data[larceny]?><value="<?php echo $data[larceny]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Esoterica</div></td>
    <td><div class="item"><select name="esoterica" id="esoterica" class="form">
        <option selected><?php echo $data[esoterica]?><value="<?php echo $data[esoterica]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="item">Empathy</div></td>
    <td><div class="item"><select name="empathy" id="empathy" class="form">
        <option selected><?php echo $data[empathy]?><value="<?php echo $data[empathy]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Meditation</div></td>
    <td><div class="item"><select name="meditation" id="meditation" class="form">
        <option selected><?php echo $data[meditation]?><value="<?php echo $data[meditaion]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Investigation</div></td>
    <td><div class="item"><select name="investigation" id="investigation" class="form">
        <option selected><?php echo $data[investigation]?><value="<?php echo $data[investigation]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="item">Expression</div></td>
    <td><div class="item"><select name="expression" id="expression" class="form">
        <option selected><?php echo $data[expression]?><value="<?php echo $data[expression]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Melee</div></td>
    <td><div class="item"><select name="melee" id="melee" class="form">
        <option selected><?php echo $data[melee]?><value="<?php echo $data[melee]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Medicine</div></td>
    <td><div class="item"><select name="medicine" id="medicine" class="form">
        <option selected><?php echo $data[medicine]?><value="<?php echo $data[medicine]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="item">Intimidation</div></td>
    <td><div class="item"><select name="intimidation" id="intimidation" class="form">
        <option selected><?php echo $data[intimidation]?><value="<?php echo $data[intimidation]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Performance</div></td>
    <td><div class="item"><div class="item"><select name="performance" id="performance" class="form">
        <option selected><?php echo $data[performance]?><value="<?php echo $data[performance]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select>
      </div></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Occult</div></td>
    <td><div class="item"><select name="occult" id="occult" class="form">
        <option selected><?php echo $data[occult]?><value="<?php echo $data[occult]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="item">Leadership</div></td>
    <td><div class="item"><select name="leadership" id="leadership" class="form">
        <option selected><?php echo $data[leadership]?><value="<?php echo $data[leadership]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Stealth</div></td>
    <td><div class="item"><select name="stealth" id="stealth" class="form">
        <option selected><?php echo $data[stealth]?><value="<?php echo $data[stealth]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Politics</div></td>
    <td><div class="item"><select name="politics" id="politics" class="form">
        <option selected><?php echo $data[politics]?><value="<?php echo $data[politics]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class="item">Subterfuge</div></td>
    <td><div class="item"><select name="subterfuge" id="subterfuge" class="form">
        <option selected><?php echo $data[subterfuge]?><value="<?php echo $data[subterfuge]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Survival</div></td>
    <td><div class="item"><select name="survival" id="survival" class="form">
        <option selected><?php echo $data['survival']?><value="<?php echo $data[survival]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Science</div></td>
    <td><div class="item"><select name="science" id="science" class="form">
        <option selected><?php echo $data['science']?><value="<?php echo $data['science']?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  
</table>
