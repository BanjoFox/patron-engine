<table border="0" >
  <tr> 
    <td colspan="2"><div class="center"><p class="pageitem">Gifts</div></td>
  </tr>
  <tr> 
    <td width="24%"><div class='item'>Rank 1 Gifts</div></td>
    <td width="76%"><div class='item'><input name="gift1" type="text" class="form" id="gift1" size="57" maxlength="255" value="<?php echo $data[gift1]?>"></div></td>
  </tr>
  </tr>
  <tr> 
    <td colspan="2"><hr></td>
  </tr>
  <tr> 
    <td><div class='item'>Rites</div></td>
    <td><div class='item'><textarea name="ritualsk" cols="56" datas="2" class="form" id="ritualsk">Here is where you list out your rituals, divide them by level. You can use formatting found in the profile guide to help organize this. Include page numbers and book for quick reference.</textarea></div></td></tr>
</table>
<center><hr align="center" width="75%" />
<table width="100%" border="0" >
  <tr>
    <td colspan="8"><div class="center">
        <div class="pageitem">Numina</div></td>
  </tr>
 <tr>
    <td width="21%"><div class='item'><input name="power1" type="text" class="form" id="power1" size="18" maxlength="18" value="<?php echo $data[power1]?>"></div></td>
    <td width="11%"><div class='item'><select name="power1val" size="1" class="form" id="power1val">
          <option selected><?php echo $data['power1val']?><value="<?php echo $data['power1val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><input name="power2" type="text" class="form" id="power2" size="18" maxlength="18" value="<?php echo $data[power2]?>"></div></td>
    <td width="11%"><div class='item'><select name="power2val" size="1" class="form" id="power2val">
          <option selected><?php echo $data['power2val']?><value="<?php echo $data['power2val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><input name="power3" type="text" class="form" id="power3" size="18" maxlength="18" value="<?php echo $data[power3]?>"></div></td>
    <td width="11%"><div class='item'><select name="power3val" size="1" class="form" id="power3val">
          <option selected><?php echo $data['power3val']?><value="<?php echo $data['power3val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><input name="power4" type="text" class="form" id="power4" size="18" maxlength="18" value="<?php echo $data[power4]?>"></div></td>
    <td><div class='item'><select name="power4val" size="1" class="form" id="power4val">
          <option selected><?php echo $data['power4val']?><value="<?php echo $data['power4val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><input name="power5" type="text" class="form" id="power5" size="18" maxlength="18" value="<?php echo $data[power5]?>"></div></td>
    <td><div class='item'><select name="power5val" size="1" class="form" id="power5val">
          <option selected><?php echo $data['power5val']?><value="<?php echo $data['power5val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><input name="power6" type="text" class="form" id="power6" size="18" maxlength="18" value="<?php echo $data[power6]?>"></div></td>
    <td><div class='item'><select name="power6val" size="1" class="form" id="power6val">
          <option selected><?php echo $data['power6val']?><value="<?php echo $data['power6val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><input name="power7" type="text" class="form" id="power7" size="18" maxlength="18" value="<?php echo $data[power7]?>"></div></td>
    <td><div class='item'><select name="power7val" size="1" class="form" id="power7val">
          <option selected><?php echo $data['power7val']?><value="<?php echo $data['power7val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><input name="power8" type="text" class="form" id="power8" size="18" maxlength="18" value="<?php echo $data[power8]?>"></div></td>
    <td><div class='item'><select name="power8val" size="1" class="form" id="power8val">
          <option selected><?php echo $data['power8val']?><value="<?php echo $data['power8val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><input name="power9" type="text" class="form" id="power9" size="18" maxlength="18" value="<?php echo $data[power9]?>"></div></td>
    <td><div class='item'><select name="power9val" size="1" class="form" id="power9val">
          <option selected><?php echo $data['power9val']?><value="<?php echo $data['power9val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
  </tr>
</table>