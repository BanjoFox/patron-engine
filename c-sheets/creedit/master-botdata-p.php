<table width="100%" border="0" cellpadding="1" cellspacing="0">
  <tr>
    <td width="21%"><div class="item">Merits</div></td>
    <td width="79%"><div class="item"><textarea name="merits" cols="50" datas="2" class="form" id="merits"><?php echo $data[merits]?></textarea></div></td>
  </tr>
  <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
  <tr>
    <td><div class="item">Flaws</div></td>
    <td><div class="item"><textarea name="flaws" cols="50" datas="2" class="form" id="flaws"><?php echo $data[flaws]?></textarea></div></td>
  </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
  <tr>
    <td><div class="item">Creation Notes</div></td>
    <td><div class="item"><textarea name="cre_notes" cols="50" datas="2" class="form" id="cre_notes"><?php echo $data[cre_notes]?></textarea></div></td>
  </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
  <tr>
    <td><div class="item">History</div></td>
    <td><div class="item"><textarea name="player_notes" cols="50" datas="2" class="form" id="player_notes" maxlength="2800"><?php echo $data[player_notes]?></textarea></div></td>
  </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
  <tr>
    <td><div class="item">Gear Carried</div></td>
    <td><div class="item"><textarea name="equipment" cols="50" datas="2" class="form" id="equipment"><?php echo $data[equipment]?></textarea></div></td>
  </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
  <tr>
    <td><div class="item">Possessions</div></td>
    <td><div class="item"><textarea name="othereq" cols="50" datas="2" class="form" id="othereq"><?php echo $data[othereq]?></textarea></div></td>
  </tr>
  <tr>
    <td colspan="2"><hr /></td>
  </tr>
  <tr>
    <td colspan="2"><table style="width:100%">
      <tr>
        <td width="75%"><div class="warning">Alert staff character is ready for approval?</div></td>
        <td width="5%">&nbsp;</td>
        <td width="20%"><div class="item"><select name="sfa" size="1" class="form" id="sfa">
            <option selected="selected"><?php echo $data[sfa]?></option>
            <option value="No">No</option>
            <option value="Yes">Yes</option>
        </select></div></td>
      </tr>
      <tr>
        <td colspan="3"><hr /></td>
      </tr>
    </table></td>
  </tr>
</table>