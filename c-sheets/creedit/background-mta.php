<table width="100%" border="0" >
  <tr>
    <td colspan="8"><div class="pagetopic">Backgrounds</div></td>
  </tr>
  <tr>
    <td width="21%"><div class='item'>Avatar</div></td>
    <td width="11%"><div class='item'><select name="avatar" size="1" class="form" id="avatar">
        <option selected><?php echo $data['avatar']?></option>
        <option value=""></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><select name="bg2" size="1" class="form" id="bg2">
      <option selected><?php echo $data['bg2']?></option>
        <option value=""></option>
        <option value="Allies">Allies</option>
        <option value="Alt Identity">Alt Identity</option>
        <option value="Arcane">Arcane</option>
        <option value="Armory">Armory</option>
        <option value="Avatar">Avatar</option>
        <option value="Blessing">Blessing</option>
        <option value="Chantry">Chantry</option>
        <option value="Contacts">Contacts</option>
        <option value="Cult">Cult</option>
        <option value="Demense">Demense</option>
        <option value="Destiny">Destiny</option>
        <option value="Dream">Dream</option>
        <option value="Enhancement+">Enhancement+</option>
        <option value="Fame">Fame</option>
        <option value="Familiar+">Familiar+</option>
        <option value="Influence">Influence</option>
        <option value="Legend">Legend</option>
        <option value="Library">Library</option>
        <option value="Mentor">Mentor</option>
        <option value="Past Lives">Past Lives</option>
        <option value="Resources">Resources</option>
        <option value="Retainers">Retainers</option>
        <option value="Sanctum+">Sanctum+</option>
        <option value="Status">Status</option>
        <option value="Totem+">Totem+</option>
        <option value="Wonder+">Wonder+</option>
</select></div></td>
    <td width="11%"><div class='item'><select name="bg2val" size="1" class="form" id="bg2val">
        <option selected><?php echo $data['bg2val']?></option>
        <option value=""></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><input name="bg3" type="text" class="form" id="bg3" size="15" maxlength="24" value="<?php echo $data['bg3']?>"></div></td>
    <td width="11%"><div class='item'><select name="bg3val" size="1" class="form" id="bg3val">
        <option selected><?php echo $data['bg3val']?></option>
        <option value=""></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><select name="bg4" size="1" class="form" id="bg4">
      <option selected><?php echo $data['bg4']?></option>
        <option value=""></option>
        <option value="Allies">Allies</option>
        <option value="Alt Identity">Alt Identity</option>
        <option value="Arcane">Arcane</option>
        <option value="Armory">Armory</option>
        <option value="Avatar">Avatar</option>
        <option value="Blessing">Blessing</option>
        <option value="Chantry">Chantry</option>
        <option value="Contacts">Contacts</option>
        <option value="Cult">Cult</option>
        <option value="Demense">Demense</option>
        <option value="Destiny">Destiny</option>
        <option value="Dream">Dream</option>
        <option value="Enhancement+">Enhancement+</option>
        <option value="Fame">Fame</option>
        <option value="Familiar+">Familiar+</option>
        <option value="Influence">Influence</option>
        <option value="Legend">Legend</option>
        <option value="Library">Library</option>
        <option value="Mentor">Mentor</option>
        <option value="Past Lives">Past Lives</option>
        <option value="Resources">Resources</option>
        <option value="Retainers">Retainers</option>
        <option value="Sanctum+">Sanctum+</option>
        <option value="Status">Status</option>
        <option value="Totem+">Totem+</option>
        <option value="Wonder+">Wonder+</option>
</select></div></td>
    <td><div class='item'><select name="bg4val" size="1" class="form" id="bg4val">
        <option selected><?php echo $data['bg4val']?></option>
        <option value=""></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="bg5" size="1" class="form" id="bg5">
      <option selected><?php echo $data['bg5']?></option>
        <option value=""></option>
        <option value="Allies">Allies</option>
        <option value="Alt Identity">Alt Identity</option>
        <option value="Arcane">Arcane</option>
        <option value="Armory">Armory</option>
        <option value="Avatar">Avatar</option>
        <option value="Blessing">Blessing</option>
        <option value="Chantry">Chantry</option>
        <option value="Contacts">Contacts</option>
        <option value="Cult">Cult</option>
        <option value="Demense">Demense</option>
        <option value="Destiny">Destiny</option>
        <option value="Dream">Dream</option>
        <option value="Enhancement+">Enhancement+</option>
        <option value="Fame">Fame</option>
        <option value="Familiar+">Familiar+</option>
        <option value="Influence">Influence</option>
        <option value="Legend">Legend</option>
        <option value="Library">Library</option>
        <option value="Mentor">Mentor</option>
        <option value="Past Lives">Past Lives</option>
        <option value="Resources">Resources</option>
        <option value="Retainers">Retainers</option>
        <option value="Sanctum+">Sanctum+</option>
        <option value="Status">Status</option>
        <option value="Totem+">Totem+</option>
        <option value="Wonder+">Wonder+</option>
</select></div></td>
    <td><div class='item'><select name="bg5val" size="1" class="form" id="bg5val">
        <option selected><?php echo $data['bg5val']?></option>
        <option value=""></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><input name="bg6" type="text" class="form" id="bg6" size="15" maxlength="24" value="<?php echo $data['bg6']?>"></div></td>
    <td><div class='item'><select name="bg6val" size="1" class="form" id="bg6val">
        <option selected><?php echo $data['bg6val']?></option>
        <option value=""></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><select name="bg7" size="1" class="form" id="bg7">
      <option selected><?php echo $data['bg7']?></option>
        <option value=""></option>
        <option value="Allies">Allies</option>
        <option value="Alt Identity">Alt Identity</option>
        <option value="Arcane">Arcane</option>
        <option value="Armory">Armory</option>
        <option value="Avatar">Avatar</option>
        <option value="Blessing">Blessing</option>
        <option value="Chantry">Chantry</option>
        <option value="Contacts">Contacts</option>
        <option value="Cult">Cult</option>
        <option value="Demense">Demense</option>
        <option value="Destiny">Destiny</option>
        <option value="Dream">Dream</option>
        <option value="Enhancement+">Enhancement+</option>
        <option value="Fame">Fame</option>
        <option value="Familiar+">Familiar+</option>
        <option value="Influence">Influence</option>
        <option value="Legend">Legend</option>
        <option value="Library">Library</option>
        <option value="Mentor">Mentor</option>
        <option value="Past Lives">Past Lives</option>
        <option value="Resources">Resources</option>
        <option value="Retainers">Retainers</option>
        <option value="Sanctum+">Sanctum+</option>
        <option value="Status">Status</option>
        <option value="Totem+">Totem+</option>
        <option value="Wonder+">Wonder+</option>
</select></div></td>
    <td><div class='item'><select name="bg7val" size="1" class="form" id="bg7val">
        <option selected><?php echo $data['bg7val']?></option>
        <option value=""></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="bg8" size="1" class="form" id="bg8">
      <option selected><?php echo $data['bg8']?></option>
        <option value=""></option>
        <option value="Allies">Allies</option>
        <option value="Alt Identity">Alt Identity</option>
        <option value="Arcane">Arcane</option>
        <option value="Armory">Armory</option>
        <option value="Avatar">Avatar</option>
        <option value="Blessing">Blessing</option>
        <option value="Chantry">Chantry</option>
        <option value="Contacts">Contacts</option>
        <option value="Cult">Cult</option>
        <option value="Demense">Demense</option>
        <option value="Destiny">Destiny</option>
        <option value="Dream">Dream</option>
        <option value="Enhancement+">Enhancement+</option>
        <option value="Fame">Fame</option>
        <option value="Familiar+">Familiar+</option>
        <option value="Influence">Influence</option>
        <option value="Legend">Legend</option>
        <option value="Library">Library</option>
        <option value="Mentor">Mentor</option>
        <option value="Past Lives">Past Lives</option>
        <option value="Resources">Resources</option>
        <option value="Retainers">Retainers</option>
        <option value="Sanctum+">Sanctum+</option>
        <option value="Status">Status</option>
        <option value="Totem+">Totem+</option>
        <option value="Wonder+">Wonder+</option>
</select></div></td>
    <td><div class='item'><select name="bg8val" size="1" class="form" id="bg8val">
        <option selected><?php echo $data['bg8val']?></option>
        <option value=""></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><input name="bg9" type="text" class="form" id="bg9" size="15" maxlength="24" value="<?php echo $data['bg9']?>"></div></td>
    <td><div class='item'><select name="bg9val" size="1" class="form" id="bg9val">
        <option selected><?php echo $data['bg9val']?></option>
        <option value=""></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><select name="bg10" size="1" class="form" id="bg10">
      <option selected><?php echo $data['bg10']?></option>
        <option value=""></option>
        <option value="Allies">Allies</option>
        <option value="Alt Identity">Alt Identity</option>
        <option value="Arcane">Arcane</option>
        <option value="Armory">Armory</option>
        <option value="Avatar">Avatar</option>
        <option value="Blessing">Blessing</option>
        <option value="Chantry">Chantry</option>
        <option value="Contacts">Contacts</option>
        <option value="Cult">Cult</option>
        <option value="Demense">Demense</option>
        <option value="Destiny">Destiny</option>
        <option value="Dream">Dream</option>
        <option value="Enhancement+">Enhancement+</option>
        <option value="Fame">Fame</option>
        <option value="Familiar+">Familiar+</option>
        <option value="Influence">Influence</option>
        <option value="Legend">Legend</option>
        <option value="Library">Library</option>
        <option value="Mentor">Mentor</option>
        <option value="Past Lives">Past Lives</option>
        <option value="Resources">Resources</option>
        <option value="Retainers">Retainers</option>
        <option value="Sanctum+">Sanctum+</option>
        <option value="Status">Status</option>
        <option value="Totem+">Totem+</option>
        <option value="Wonder+">Wonder+</option>
</select></div></td>
    <td><div class='item'><select name="bg10val" size="1" class="form" id="bg10val">
        <option selected><?php echo $data['bg10val']?></option>
        <option value=""></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="bg11" size="1" class="form" id="bg11">
      <option selected><?php echo $data['bg11']?></option>
        <option value=""></option>
        <option value="Allies">Allies</option>
        <option value="Alt Identity">Alt Identity</option>
        <option value="Arcane">Arcane</option>
        <option value="Armory">Armory</option>
        <option value="Avatar">Avatar</option>
        <option value="Blessing">Blessing</option>
        <option value="Chantry">Chantry</option>
        <option value="Contacts">Contacts</option>
        <option value="Cult">Cult</option>
        <option value="Demense">Demense</option>
        <option value="Destiny">Destiny</option>
        <option value="Dream">Dream</option>
        <option value="Enhancement+">Enhancement+</option>
        <option value="Fame">Fame</option>
        <option value="Familiar+">Familiar+</option>
        <option value="Influence">Influence</option>
        <option value="Legend">Legend</option>
        <option value="Library">Library</option>
        <option value="Mentor">Mentor</option>
        <option value="Past Lives">Past Lives</option>
        <option value="Resources">Resources</option>
        <option value="Retainers">Retainers</option>
        <option value="Sanctum+">Sanctum+</option>
        <option value="Status">Status</option>
        <option value="Totem+">Totem+</option>
        <option value="Wonder+">Wonder+</option>
</select></div></td>
    <td><div class='item'><select name="bg11val" size="1" class="form" id="bg11val">
        <option selected><?php echo $data['bg11val']?></option>
        <option value=""></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><input name="bg12" type="text" class="form" id="bg12" size="15" maxlength="24" value="<?php echo $data['bg12']?>"></div></td>
    <td><div class='item'><select name="bg12val" size="1" class="form" id="bg12val">
        <option selected><?php echo $data['bg12val']?></option>
        <option value=""></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><select name="bg13" size="1" class="form" id="bg13">
      <option selected><?php echo $data['bg13']?></option>
        <option value=""></option>
        <option value="Allies">Allies</option>
        <option value="Alt Identity">Alt Identity</option>
        <option value="Arcane">Arcane</option>
        <option value="Armory">Armory</option>
        <option value="Avatar">Avatar</option>
        <option value="Blessing">Blessing</option>
        <option value="Chantry">Chantry</option>
        <option value="Contacts">Contacts</option>
        <option value="Cult">Cult</option>
        <option value="Demense">Demense</option>
        <option value="Destiny">Destiny</option>
        <option value="Dream">Dream</option>
        <option value="Enhancement+">Enhancement+</option>
        <option value="Fame">Fame</option>
        <option value="Familiar+">Familiar+</option>
        <option value="Influence">Influence</option>
        <option value="Legend">Legend</option>
        <option value="Library">Library</option>
        <option value="Mentor">Mentor</option>
        <option value="Past Lives">Past Lives</option>
        <option value="Resources">Resources</option>
        <option value="Retainers">Retainers</option>
        <option value="Sanctum+">Sanctum+</option>
        <option value="Status">Status</option>
        <option value="Totem+">Totem+</option>
        <option value="Wonder+">Wonder+</option>
</select></div></td>
    <td><div class='item'><select name="bg13val" size="1" class="form" id="bg13val">
      <option selected><?php echo $data['bg13val']?></option>
      <option value=""></option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
    </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="bg14" size="1" class="form" id="bg14">
      <option selected><?php echo $data['bg14']?></option>
        <option value=""></option>
        <option value="Allies">Allies</option>
        <option value="Alt Identity">Alt Identity</option>
        <option value="Arcane">Arcane</option>
        <option value="Armory">Armory</option>
        <option value="Avatar">Avatar</option>
        <option value="Blessing">Blessing</option>
        <option value="Chantry">Chantry</option>
        <option value="Contacts">Contacts</option>
        <option value="Cult">Cult</option>
        <option value="Demense">Demense</option>
        <option value="Destiny">Destiny</option>
        <option value="Dream">Dream</option>
        <option value="Enhancement+">Enhancement+</option>
        <option value="Fame">Fame</option>
        <option value="Familiar+">Familiar+</option>
        <option value="Influence">Influence</option>
        <option value="Legend">Legend</option>
        <option value="Library">Library</option>
        <option value="Mentor">Mentor</option>
        <option value="Past Lives">Past Lives</option>
        <option value="Resources">Resources</option>
        <option value="Retainers">Retainers</option>
        <option value="Sanctum+">Sanctum+</option>
        <option value="Status">Status</option>
        <option value="Totem+">Totem+</option>
        <option value="Wonder+">Wonder+</option>
</select></div></td>
    <td><div class='item'><select name="bg14val" size="1" class="form" id="bg14val">
      <option selected><?php echo $data['bg14val']?></option>
      <option value=""></option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
    </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><input name="bg15" type="text" class="form" id="bg15" size="15" maxlength="24" value="<?php echo $data['bg15']?>"></div></td>
    <td><div class='item'><select name="bg15val" size="1" class="form" id="bg15val">
      <option selected><?php echo $data['bg15val']?></option>
      <option value=""></option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
    </select></div></td>
  </tr>
</table>