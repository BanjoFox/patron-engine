<table width="100%" border="0" >
    <tr>
            <td width="21%"><div class='item'>Merits</div></td>
            <td width="79%"><div class="item"><textarea name="merits" cols="50" datas="2" class="form" id="merits"><?php echo $data[merits]?></textarea></div></td>
      </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
      <tr>
            <td><div class='item'>Flaws</div></td>
            <td><div class="item"><textarea name="flaws" cols="50" datas="2" class="form" id="flaws"><?php echo $data[flaws]?></textarea></div></td>
      </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
      <tr>
            <td><div class='item'>Creation Notes</div></td>
            <td><div class="item"><textarea name="cre_notes" cols="50" datas="2" class="form" id="cre_notes"><?php echo $data[cre_notes]?></textarea></div></td>
      </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
      <tr>
            <td><div class='item'>History</div></td>
            <td><div class='item'><textarea name="player_notes" cols="50" datas="2" class="form" id="player_notes"><?php echo $data[player_notes]?></textarea></div></td>
      </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
      <tr>
            <td><div class='item'>Gear Carried</div></td>
            <td><div class="item"><textarea name="equipment" cols="50" datas="2" class="form" id="equipment"><?php echo $data[equipment]?></textarea></div></td>
      </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
      <tr>
            <td><div class='item'>Possessions</div></td>
            <td><div class="item"><textarea name="othereq" cols="50" datas="2" class="form" id="othereq"><?php echo $data[othereq]?></textarea></div></td>
      </tr>
      <tr>
            <td colspan="2"><hr></td>
      </tr>
</table>

<table style="width:100%">
    <tr>
        <td width="21%"><div class='item'>Sanctioned?</td>
        <td width="24%"><div class="item"><select name="sanctioned" size="1" class="form" id="sanctioned">
                  <option selected><?php echo $data[sanctioned]?></option>
                  <option value="No">No</option>
                  <option value="Yes">Yes</option>
                   <option value="RIP">RIP</option>
                      <option value="Hold">Hold</option>
                    </select></div></td>
        <td width="10%">&nbsp;</td>
        <td width="21%"><div class='item'>Character Type</div></td>
        <td width="24%"><div class="item"><select name="char_type" size="1" class="form" id="char_type">
                  <option selected><?php echo $data[char_type]?></option>
                       <option value="PC">PC</option>
                     <option value="NPC">NPC</option>
                     <option value="RET">RET</option>
                      </select></div></td>
    </tr>
    <tr>
      <td><div class='item'>&nbsp;</div></td>
      <td><div class='item'>&nbsp;</div></td>
      <td>&nbsp;</td>
      <td><div class='item'>Retainer Of:</div></td>
      <td><div class='item'><input name="retainer_of" type="text" class="form" id="retainer_of" size="15" maxlength="24" value="<?php echo $data[retainer_of]?>"></div></td>
  </tr>
    <tr>
        <td colspan="5"><hr></td>
    </tr>
       <tr>
              <td width="21%"><div class='item'>Earned XP</div></td>
              <td width="24%"><div class='item'><?php echo $data[xptotal]?></div></td>
              <td width="10%">&nbsp;</td>
              <td width="21%"><div class='item'>Add XP</div></td>
              <td width="24%"><div class="item"><input name="givexp" type="text" class="form" id="givexp" value="0" size="3" maxlength="3" /></div></td>
        </tr>
        <tr>
            <td><div class='item'>Spend XP</div></td>
              <td><div class='item'><?php echo $data[xpspent]?></div></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
    </tr>
    <tr>
              <td><div class='item'>Available XP</div></td>
              <td><div class='item'><?php echo $data[xptotal] - $data[xpspent]?></div></td>
              <td>&nbsp;</td>
              <td><div class='item'>Spend XP</div></td>
              <td><div class="item"><input name="takexp" type="text" class="form" id="takexp" value="0" size="3" maxlength="3" /></div></td>
    </tr>
</table>


<table style="width:100%"> 
    <tr>
        <td colspan="2"><hr></td>
    </tr>
    <tr>
      <td colspan="2"><table style="width:100%">
        <tr>
          <td width="45%"><div class="warning">Alert staff character is ready for approval?</div></td>
          <td width="5%">&nbsp;</td>
          <td width="50%" align="left"><div class="item"><select name="sfa" size="1" class="form" id="sfa">
              <option selected="selected"><?php echo $data[sfa]?></option>
              <option value="No">No</option>
              <option value="Pre">Pre</option>
              <option value="Yes">Yes</option>
              <option value="RIP">RIP</option>
          </select></div></td>
        </tr>
       </table></td>
  </tr>
  <tr>
      <td colspan="2"><hr /></td>
  </tr>
    <tr>
        <td width="21%"><div class='item'>Storyteller Notes</div></td>
        <td width="79%"><div class="item"><textarea name="stnotesadd" cols="50" id="stnotesadd" class="form"></textarea></div></td>
    </tr>
    <tr>
        <td colspan="2"><hr></td>
    </tr>
        <tr>
          <td colspan="2"><div class="center"><input type="submit" name="Select2" value="Update" class="form"></div></td>
        </tr>
    <tr>
        <td colspan="2"><hr></td>
    </tr>
    <tr>
        <td><div class='item'>Previous ST Notes</div></td>
        <td><div class='item'><?php echo $data[st_notes]?></div></td>
    </tr>
    <tr>
        <td colspan="2"><hr></td>
    </tr>
</table>