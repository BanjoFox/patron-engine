<table width="100%" border="0" >
  <tr>
    <td colspan="8"><div class="pagetopic">Arts</div></td>
  </tr>
  <tr>
    <td width="21%"><div class='item'><select name="power1" size="1" class="form" id="power1">
      <option selected><?php echo $data[power1]?><value="<?php echo $data[power1]?>"></option>
        <option value="Autumn">Autumn</option>
        <option value="Chicanery">Chicanery</option>
        <option value="Chronos">Chronos</option>
        <option value="Contract">Contract</option>
        <option value="Dragons Ire">Dragons Ire</option>
        <option value="Legerdemain">Legerdemain</option>
        <option value="Metamorphosis">Metamorphosis</option>
        <option value="Naming">Naming</option>   
        <option value="Oneiromancy">Oneiromancy</option>
        <option value="Primal">Primal</option>
        <option value="Pyretics">Pyretics</option>
        <option value="Skycraft">Skycraft</option>
        <option value="Soothsay">Soothsay</option>
        <option value="Sovereign">Sovereign</option>
        <option value="Spring">Spring</option>
        <option value="Summer">Summer</option>
        <option value="Wayfare">Wayfare</option>
        <option value="Winter">Winter</option></select></div></td>
    <td width="11%"><div class='item'><select name="power1val" size="1" class="form" id="power1val">
          <option selected><?php echo $data['power1val']?><value="<?php echo $data['power1val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><select name="power2" size="1" class="form" id="power2">
      <option selected><?php echo $data[power2]?><value="<?php echo $data[power2]?>"></option>
        <option value="Autumn">Autumn</option>
        <option value="Chicanery">Chicanery</option>
        <option value="Chronos">Chronos</option>
        <option value="Contract">Contract</option>
        <option value="Dragons Ire">Dragons Ire</option>
        <option value="Legerdemain">Legerdemain</option>
        <option value="Metamorphosis">Metamorphosis</option>
        <option value="Naming">Naming</option>   
        <option value="Oneiromancy">Oneiromancy</option>
        <option value="Primal">Primal</option>
        <option value="Pyretics">Pyretics</option>
        <option value="Skycraft">Skycraft</option>
        <option value="Soothsay">Soothsay</option>
        <option value="Sovereign">Sovereign</option>
        <option value="Spring">Spring</option>
        <option value="Summer">Summer</option>
        <option value="Wayfare">Wayfare</option>
        <option value="Winter">Winter</option></select></div></td>
    <td width="11%"><div class='item'><select name="power2val" size="1" class="form" id="power2val">
          <option selected><?php echo $data['power2val']?><value="<?php echo $data['power2val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><select name="power3" size="1" class="form" id="power3">
      <option selected><?php echo $data[power3]?><value="<?php echo $data[power3]?>"></option>
        <option value="Autumn">Autumn</option>
        <option value="Chicanery">Chicanery</option>
        <option value="Chronos">Chronos</option>
        <option value="Contract">Contract</option>
        <option value="Dragons Ire">Dragons Ire</option>
        <option value="Legerdemain">Legerdemain</option>
        <option value="Metamorphosis">Metamorphosis</option>
        <option value="Naming">Naming</option>   
        <option value="Oneiromancy">Oneiromancy</option>
        <option value="Primal">Primal</option>
        <option value="Pyretics">Pyretics</option>
        <option value="Skycraft">Skycraft</option>
        <option value="Soothsay">Soothsay</option>
        <option value="Sovereign">Sovereign</option>
        <option value="Spring">Spring</option>
        <option value="Summer">Summer</option>
        <option value="Wayfare">Wayfare</option>
        <option value="Winter">Winter</option></select></div></td>
    <td width="11%"><div class='item'><select name="power3val" size="1" class="form" id="power3val">
          <option selected><?php echo $data['power3val']?><value="<?php echo $data['power3val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><select name="power4" size="1" class="form" id="power4">
      <option selected><?php echo $data[power4]?><value="<?php echo $data[power4]?>"></option>
        <option value="Autumn">Autumn</option>
        <option value="Chicanery">Chicanery</option>
        <option value="Chronos">Chronos</option>
        <option value="Contract">Contract</option>
        <option value="Dragons Ire">Dragons Ire</option>
        <option value="Legerdemain">Legerdemain</option>
        <option value="Metamorphosis">Metamorphosis</option>
        <option value="Naming">Naming</option>   
        <option value="Oneiromancy">Oneiromancy</option>
        <option value="Primal">Primal</option>
        <option value="Pyretics">Pyretics</option>
        <option value="Skycraft">Skycraft</option>
        <option value="Soothsay">Soothsay</option>
        <option value="Sovereign">Sovereign</option>
        <option value="Spring">Spring</option>
        <option value="Summer">Summer</option>
        <option value="Wayfare">Wayfare</option>
        <option value="Winter">Winter</option></select></div></td>
    <td><div class='item'><select name="power4val" size="1" class="form" id="power4val">
          <option selected><?php echo $data['power4val']?><value="<?php echo $data['power4val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power5" size="1" class="form" id="power5">
      <option selected><?php echo $data[power5]?><value="<?php echo $data[power5]?>"></option>
        <option value="Autumn">Autumn</option>
        <option value="Chicanery">Chicanery</option>
        <option value="Chronos">Chronos</option>
        <option value="Contract">Contract</option>
        <option value="Dragons Ire">Dragons Ire</option>
        <option value="Legerdemain">Legerdemain</option>
        <option value="Metamorphosis">Metamorphosis</option>
        <option value="Naming">Naming</option>   
        <option value="Oneiromancy">Oneiromancy</option>
        <option value="Primal">Primal</option>
        <option value="Pyretics">Pyretics</option>
        <option value="Skycraft">Skycraft</option>
        <option value="Soothsay">Soothsay</option>
        <option value="Sovereign">Sovereign</option>
        <option value="Spring">Spring</option>
        <option value="Summer">Summer</option>
        <option value="Wayfare">Wayfare</option>
        <option value="Winter">Winter</option></select></div></td>
    <td><div class='item'><select name="power5val" size="1" class="form" id="power5val">
          <option selected><?php echo $data['power5val']?><value="<?php echo $data['power5val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power6" size="1" class="form" id="power6">
      <option selected><?php echo $data[power6]?><value="<?php echo $data[power6]?>"></option>
        <option value="Autumn">Autumn</option>
        <option value="Chicanery">Chicanery</option>
        <option value="Chronos">Chronos</option>
        <option value="Contract">Contract</option>
        <option value="Dragons Ire">Dragons Ire</option>
        <option value="Legerdemain">Legerdemain</option>
        <option value="Metamorphosis">Metamorphosis</option>
        <option value="Naming">Naming</option>   
        <option value="Oneiromancy">Oneiromancy</option>
        <option value="Primal">Primal</option>
        <option value="Pyretics">Pyretics</option>
        <option value="Skycraft">Skycraft</option>
        <option value="Soothsay">Soothsay</option>
        <option value="Sovereign">Sovereign</option>
        <option value="Spring">Spring</option>
        <option value="Summer">Summer</option>
        <option value="Wayfare">Wayfare</option>
        <option value="Winter">Winter</option></select></div></td>
    <td><div class='item'><select name="power6val" size="1" class="form" id="power6val">
          <option selected><?php echo $data['power6val']?><value="<?php echo $data['power6val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><select name="power7" size="1" class="form" id="power7">
      <option selected><?php echo $data[power7]?><value="<?php echo $data[power7]?>"></option>
        <option value="Autumn">Autumn</option>
        <option value="Chicanery">Chicanery</option>
        <option value="Chronos">Chronos</option>
        <option value="Contract">Contract</option>
        <option value="Dragons Ire">Dragons Ire</option>
        <option value="Legerdemain">Legerdemain</option>
        <option value="Metamorphosis">Metamorphosis</option>
        <option value="Naming">Naming</option>   
        <option value="Oneiromancy">Oneiromancy</option>
        <option value="Primal">Primal</option>
        <option value="Pyretics">Pyretics</option>
        <option value="Skycraft">Skycraft</option>
        <option value="Soothsay">Soothsay</option>
        <option value="Sovereign">Sovereign</option>
        <option value="Spring">Spring</option>
        <option value="Summer">Summer</option>
        <option value="Wayfare">Wayfare</option>
        <option value="Winter">Winter</option></select></div></td>
    <td><div class='item'><select name="power7val" size="1" class="form" id="power7val">
          <option selected><?php echo $data['power7val']?><value="<?php echo $data['power7val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power8" size="1" class="form" id="power8">
      <option selected><?php echo $data[power8]?><value="<?php echo $data[power8]?>"></option>
        <option value="Autumn">Autumn</option>
        <option value="Chicanery">Chicanery</option>
        <option value="Chronos">Chronos</option>
        <option value="Contract">Contract</option>
        <option value="Dragons Ire">Dragons Ire</option>
        <option value="Legerdemain">Legerdemain</option>
        <option value="Metamorphosis">Metamorphosis</option>
        <option value="Naming">Naming</option>   
        <option value="Oneiromancy">Oneiromancy</option>
        <option value="Primal">Primal</option>
        <option value="Pyretics">Pyretics</option>
        <option value="Skycraft">Skycraft</option>
        <option value="Soothsay">Soothsay</option>
        <option value="Sovereign">Sovereign</option>
        <option value="Spring">Spring</option>
        <option value="Summer">Summer</option>
        <option value="Wayfare">Wayfare</option>
        <option value="Winter">Winter</option></select></div></td>
    <td><div class='item'><select name="power8val" size="1" class="form" id="power8val">
          <option selected><?php echo $data['power8val']?><value="<?php echo $data['power8val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power9" size="1" class="form" id="power9">
      <option selected><?php echo $data[power9]?><value="<?php echo $data[power9]?>"></option>
        <option value="Autumn">Autumn</option>
        <option value="Chicanery">Chicanery</option>
        <option value="Chronos">Chronos</option>
        <option value="Contract">Contract</option>
        <option value="Dragons Ire">Dragons Ire</option>
        <option value="Legerdemain">Legerdemain</option>
        <option value="Metamorphosis">Metamorphosis</option>
        <option value="Naming">Naming</option>   
        <option value="Oneiromancy">Oneiromancy</option>
        <option value="Primal">Primal</option>
        <option value="Pyretics">Pyretics</option>
        <option value="Skycraft">Skycraft</option>
        <option value="Soothsay">Soothsay</option>
        <option value="Sovereign">Sovereign</option>
        <option value="Spring">Spring</option>
        <option value="Summer">Summer</option>
        <option value="Wayfare">Wayfare</option>
        <option value="Winter">Winter</option></select></div></td>
    <td><div class='item'><select name="power9val" size="1" class="form" id="power9val">
          <option selected><?php echo $data['power9val']?><value="<?php echo $data['power9val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><select name="power10" size="1" class="form" id="power10">
      <option selected><?php echo $data[power10]?><value="<?php echo $data[power10]?>"></option>
        <option value="Autumn">Autumn</option>
        <option value="Chicanery">Chicanery</option>
        <option value="Chronos">Chronos</option>
        <option value="Contract">Contract</option>
        <option value="Dragons Ire">Dragons Ire</option>
        <option value="Legerdemain">Legerdemain</option>
        <option value="Metamorphosis">Metamorphosis</option>
        <option value="Naming">Naming</option>   
        <option value="Oneiromancy">Oneiromancy</option>
        <option value="Primal">Primal</option>
        <option value="Pyretics">Pyretics</option>
        <option value="Skycraft">Skycraft</option>
        <option value="Soothsay">Soothsay</option>
        <option value="Sovereign">Sovereign</option>
        <option value="Spring">Spring</option>
        <option value="Summer">Summer</option>
        <option value="Wayfare">Wayfare</option>
        <option value="Winter">Winter</option></select></div></td>
    <td><div class='item'><select name="power10val" size="1" class="form" id="power10val">
          <option selected><?php echo $data['power10val']?><value="<?php echo $data['power10val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power11" size="1" class="form" id="power11">
      <option selected><?php echo $data[power11]?><value="<?php echo $data[power11]?>"></option>
        <option value="Autumn">Autumn</option>
        <option value="Chicanery">Chicanery</option>
        <option value="Chronos">Chronos</option>
        <option value="Contract">Contract</option>
        <option value="Dragons Ire">Dragons Ire</option>
        <option value="Legerdemain">Legerdemain</option>
        <option value="Metamorphosis">Metamorphosis</option>
        <option value="Naming">Naming</option>   
        <option value="Oneiromancy">Oneiromancy</option>
        <option value="Primal">Primal</option>
        <option value="Pyretics">Pyretics</option>
        <option value="Skycraft">Skycraft</option>
        <option value="Soothsay">Soothsay</option>
        <option value="Sovereign">Sovereign</option>
        <option value="Spring">Spring</option>
        <option value="Summer">Summer</option>
        <option value="Wayfare">Wayfare</option>
        <option value="Winter">Winter</option></select></div></td>
    <td><div class='item'><select name="power11val" size="1" class="form" id="power11val">
          <option selected><?php echo $data['power11val']?><value="<?php echo $data['power11val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power12" size="1" class="form" id="power12">
      <option selected><?php echo $data[power12]?><value="<?php echo $data[power12]?>"></option>
        <option value="Autumn">Autumn</option>
        <option value="Chicanery">Chicanery</option>
        <option value="Chronos">Chronos</option>
        <option value="Contract">Contract</option>
        <option value="Dragons Ire">Dragons Ire</option>
        <option value="Legerdemain">Legerdemain</option>
        <option value="Metamorphosis">Metamorphosis</option>
        <option value="Naming">Naming</option>   
        <option value="Oneiromancy">Oneiromancy</option>
        <option value="Primal">Primal</option>
        <option value="Pyretics">Pyretics</option>
        <option value="Skycraft">Skycraft</option>
        <option value="Soothsay">Soothsay</option>
        <option value="Sovereign">Sovereign</option>
        <option value="Spring">Spring</option>
        <option value="Summer">Summer</option>
        <option value="Wayfare">Wayfare</option>
        <option value="Winter">Winter</option></select></div></td>
    <td><div class='item'><select name="power12val" size="1" class="form" id="power12val">
          <option selected><?php echo $data['power12val']?><value="<?php echo $data['power12val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
  </tr>
</table>
<div class="center"><hr width="75%"></div>
<table width="100%" border="0" >
  <tr>
    <td colspan="8"><div class="pagetopic">Realms</div></td>
  </tr>
  <tr>
    <td width="21%"><div class='item'>Actor</div></td>
    <td width="11%"><div class='item'><select name="actor_c" size="1" class="form" id="actor_c">
          <option selected><?php echo $data[actor_c]?><value="<?php echo $data[actor_c]?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'>Fae</div></td>
    <td width="11%"><div class='item'><select name="fae_c" size="1" class="form" id="fae_c">
          <option selected><?php echo $data[fae_c]?><value="<?php echo $data[fae_c]?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'>Prop</div></td>
    <td width="11%"><div class='item'><select name="prop_c" size="1" class="form" id="prop_c">
          <option selected><?php echo $data[prop_c]?><value="<?php echo $data[prop_c]?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'>Nature</div></td>
    <td><div class='item'><select name="nature_c" size="1" class="form" id="nature_c">
          <option selected><?php echo $data[nature_c]?><value="<?php echo $data[nature_c]?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Scene</div></td>
    <td><div class='item'><select name="scene_c" size="1" class="form" id="scene_c">
          <option selected><?php echo $data[scene_c]?><value="<?php echo $data[scene_c]?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Time</div></td>
    <td><div class='item'><select name="time_c" size="1" class="form" id="time_c">
          <option selected><?php echo $data[time_c]?><value="<?php echo $data[time_c]?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
  </tr>
</table>
<div class="center"><hr width="90%"></div>
<table border="0" width="100%">
  <tr> 
    <td width="13%"><div class="item">Glamour</div></td>
    <td width="10%"><div align="center" class="itemsm">Perm</div></td>
    <td width="10%"><div class="item"><select name="glamourperm" size="1" class="form" id="glamourperm">
        <option selected><?php echo $data[glamourperm]?><value="<?php echo $data[glamourperm]?>"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="13%"><div class="item">Banality</div></td>
    <td width="10%"><div align="center" class="itemsm">Perm</div></td>
    <td width="10%"><div class="item"><select name="banalityperm" size="1" class="form" id="banalityperm">
        <option selected><?php echo $data[banalityperm]?><value="<?php echo $data[banalityperm]?>"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="13%"><div class="item">Willpower</div></td>
    <td width="10%"><div align="center" class="itemsm">Perm</div></td>
    <td width="10%"><div class="item"><select name="wpperm" size="1" class="form" id="wpperm">
        <option selected><?php echo $data[wpperm]?><value="<?php echo $data[wpperm]?>"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td><div align="center" class="itemsm">Temp</div></td>
    <td><div class="item"><select name="glamourtemp" size="1" class="form" id="glamourtemp">
        <option selected><?php echo $data[glamourtemp]?><value="<?php echo $data[glamourtemp]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center" class="itemsm">Temp</div></td>
    <td><div class="item"><select name="banalitytemp" size="1" class="form" id="banalitytemp">
        <option selected><?php echo $data[banalitytemp]?><value="<?php echo $data[banalitytemp]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>

    <td><div align="center" class="itemsm">Temp</div></td>
    <td><div class="item"><select name="wptemp" size="1" class="form" id="wptemp">
        <option selected><?php echo $data[wptemp]?><value="<?php echo $data[wptemp]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
  </tr>
</table>
<div class="center"><hr width="75%"></div>

<table width="100%">
  <tr>
    <td width="24%"><div class="item">Other Venue Details:</div></td>
    <td width="76%" colspan="10"><div class="item"><textarea name="fae_venue" cols="56" datas="2" class="form" id="fae_venue">This is where you put information such as: Birthrights, Frailty, Legacies, Details about Chimera, House Boon and Flaw (if member), Thresholds & Antithesis, Fae mein, Derangements. You can use formatting found in the profile guide to help organize this.</textarea></div></td>
  </tr>
</table>