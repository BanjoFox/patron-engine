<div class="center"><hr width="80%" /></div>

<table width="100%">
  <tr>
    <td width="24%"><div class="item">Special Advantages</div></td>
    <td width="76%" colspan="10"><div class="item"><textarea name="bygone_venue" cols="56" datas="2" class="form" id="bygone_venue"><?php echo $data[bygone_venue]?></textarea></div></td>
  </tr>
</table>

<div class="center"><hr width="80%" /></div>

<table width="100%" border="0">
  <tr>
    <td colspan="8"><div class="pagetopic">Numina</div></td>
  </tr>
  <tr>
        <td width="21%"><div class='item'><select name="power1" size="1" class="form" id="power1">
<option selected><?php echo $data[power1]?><value="<?php echo $data[power1]?>"></option>
<option value=""></option>
<option value="Alchemy">Alchemy</option>
<option value="Animal Psychic">Animal Psychic</option>
<option value="Anti-Psychic">Anti-Psychic</option>
<option value="Astral Projection">Astral Projection</option>
<option value="Biocontrol">Biocontrol</option>
<option value="Channeling">Channeling</option>
<option value="Clairvoyance">Clairvoyance</option>
<option value="Conjuration">Conjuration</option>
<option value="Conveyance">Conveyance</option>
<option value="Cyberkinesis">Cyberkinesis</option>
<option value="Cyberpathy">Cyberpathy</option>
<option value="Divination">Divination</option>
<option value="Ectoplasmic Gen">Ectoplasmic Gen</option>
<option value="Ephemera">Ephemera</option>
<option value="Enchantment">Enchantment</option>
<option value="Fascination">Fascination</option>
<option value="Fortune">Fortune</option>
<option value="Gauntlet Manip">Gauntlet Manip</option>
<option value="Healing">Healing</option>
<option value="Hellfire">Hellfire</option>
<option value="Illusion">Illusion</option>
<option value="Mind Shields">Mind Shields</option>
<option value="Mortal Necromancy">Mortal Necromancy</option>
<option value="Mecronics">Mecronics</option>
<option value="Oneiromancy">Oneiromancy</option>
<option value="Precognition">Precognition</option>
<option value="Psychic Healing">Psychic Healing</option>
<option value="Psychic Hypnosis">Psychic Hypnosis</option>
<option value="Psy Invisibility">Psy Invisibility</option>
<option value="Psy Shadow">Psy Shadow</option>
<option value="Psy Vampirism">Psy Vampirism</option>
<option value="Psychokinesis">Psychokinesis</option>
<option value="Psycometry">Psycometry</option>
<option value="Psychoportation">Psychoportation</option>
<option value="Pyrokineses">Pyrokineses</option>
<option value="Quint Manip">Quint Manip</option>
<option value="Shadows">Shadows</option>
<option value="Shapeshifting">Shapeshifting</option>
<option value="Soulstealing">Soulstealing</option>
<option value="Spirit Awakening">Spirit Awakening</option>
<option value="Spirit Chasing">Spirit Chasing</option>
<option value="Summon Ward Bind">Summon Ward Bind</option>
<option value="Synergy">Synergy</option>
<option value="Telepathy">Telepathy</option>
<option value="True Faith">True Faith</option>
<option value="Weather Control">Weather Control</option></select></div></td>
    <td width="11%"><div class='item'><select name="power1val" size="1" class="form" id="power1val">
          <option selected><?php echo $data['power1val']?><value="<?php echo $data['power1val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><select name="power2" size="1" class="form" id="power2">
<option selected><?php echo $data[power2]?><value="<?php echo $data[power2]?>"></option>
<option value=""></option>
<option value="Alchemy">Alchemy</option>
<option value="Animal Psychic">Animal Psychic</option>
<option value="Anti-Psychic">Anti-Psychic</option>
<option value="Astral Projection">Astral Projection</option>
<option value="Biocontrol">Biocontrol</option>
<option value="Channeling">Channeling</option>
<option value="Clairvoyance">Clairvoyance</option>
<option value="Conjuration">Conjuration</option>
<option value="Conveyance">Conveyance</option>
<option value="Cyberkinesis">Cyberkinesis</option>
<option value="Cyberpathy">Cyberpathy</option>
<option value="Divination">Divination</option>
<option value="Ectoplasmic Gen">Ectoplasmic Gen</option>
<option value="Ephemera">Ephemera</option>
<option value="Enchantment">Enchantment</option>
<option value="Fascination">Fascination</option>
<option value="Fortune">Fortune</option>
<option value="Gauntlet Manip">Gauntlet Manip</option>
<option value="Healing">Healing</option>
<option value="Hellfire">Hellfire</option>
<option value="Illusion">Illusion</option>
<option value="Mind Shields">Mind Shields</option>
<option value="Mortal Necromancy">Mortal Necromancy</option>
<option value="Mecronics">Mecronics</option>
<option value="Oneiromancy">Oneiromancy</option>
<option value="Precognition">Precognition</option>
<option value="Psychic Healing">Psychic Healing</option>
<option value="Psychic Hypnosis">Psychic Hypnosis</option>
<option value="Psy Invisibility">Psy Invisibility</option>
<option value="Psy Shadow">Psy Shadow</option>
<option value="Psy Vampirism">Psy Vampirism</option>
<option value="Psychokinesis">Psychokinesis</option>
<option value="Psycometry">Psycometry</option>
<option value="Psychoportation">Psychoportation</option>
<option value="Pyrokineses">Pyrokineses</option>
<option value="Quint Manip">Quint Manip</option>
<option value="Shadows">Shadows</option>
<option value="Shapeshifting">Shapeshifting</option>
<option value="Soulstealing">Soulstealing</option>
<option value="Spirit Awakening">Spirit Awakening</option>
<option value="Spirit Chasing">Spirit Chasing</option>
<option value="Summon Ward Bind">Summon Ward Bind</option>
<option value="Synergy">Synergy</option>
<option value="Telepathy">Telepathy</option>
<option value="True Faith">True Faith</option>
<option value="Weather Control">Weather Control</option></select></div></td>
    <td width="11%"><div class='item'><select name="power2val" size="1" class="form" id="power2val">
          <option selected><?php echo $data['power2val']?><value="<?php echo $data['power2val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><select name="power3" size="1" class="form" id="power3">
<option selected><?php echo $data[power3]?><value="<?php echo $data[power3]?>"></option>
<option value=""></option>
<option value="Alchemy">Alchemy</option>
<option value="Animal Psychic">Animal Psychic</option>
<option value="Anti-Psychic">Anti-Psychic</option>
<option value="Astral Projection">Astral Projection</option>
<option value="Biocontrol">Biocontrol</option>
<option value="Channeling">Channeling</option>
<option value="Clairvoyance">Clairvoyance</option>
<option value="Conjuration">Conjuration</option>
<option value="Conveyance">Conveyance</option>
<option value="Cyberkinesis">Cyberkinesis</option>
<option value="Cyberpathy">Cyberpathy</option>
<option value="Divination">Divination</option>
<option value="Ectoplasmic Gen">Ectoplasmic Gen</option>
<option value="Ephemera">Ephemera</option>
<option value="Enchantment">Enchantment</option>
<option value="Fascination">Fascination</option>
<option value="Fortune">Fortune</option>
<option value="Gauntlet Manip">Gauntlet Manip</option>
<option value="Healing">Healing</option>
<option value="Hellfire">Hellfire</option>
<option value="Illusion">Illusion</option>
<option value="Mind Shields">Mind Shields</option>
<option value="Mortal Necromancy">Mortal Necromancy</option>
<option value="Mecronics">Mecronics</option>
<option value="Oneiromancy">Oneiromancy</option>
<option value="Precognition">Precognition</option>
<option value="Psychic Healing">Psychic Healing</option>
<option value="Psychic Hypnosis">Psychic Hypnosis</option>
<option value="Psy Invisibility">Psy Invisibility</option>
<option value="Psy Shadow">Psy Shadow</option>
<option value="Psy Vampirism">Psy Vampirism</option>
<option value="Psychokinesis">Psychokinesis</option>
<option value="Psycometry">Psycometry</option>
<option value="Psychoportation">Psychoportation</option>
<option value="Pyrokineses">Pyrokineses</option>
<option value="Quint Manip">Quint Manip</option>
<option value="Shadows">Shadows</option>
<option value="Shapeshifting">Shapeshifting</option>
<option value="Soulstealing">Soulstealing</option>
<option value="Spirit Awakening">Spirit Awakening</option>
<option value="Spirit Chasing">Spirit Chasing</option>
<option value="Summon Ward Bind">Summon Ward Bind</option>
<option value="Synergy">Synergy</option>
<option value="Telepathy">Telepathy</option>
<option value="True Faith">True Faith</option>
<option value="Weather Control">Weather Control</option></select></div></td>
    <td width="11%"><div class='item'><select name="power3val" size="1" class="form" id="power3val">
          <option selected><?php echo $data['power3val']?><value="<?php echo $data['power3val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><select name="power4" size="1" class="form" id="power4">
<option selected><?php echo $data[power4]?><value="<?php echo $data[power4]?>"></option>
<option value=""></option>
<option value="Alchemy">Alchemy</option>
<option value="Animal Psychic">Animal Psychic</option>
<option value="Anti-Psychic">Anti-Psychic</option>
<option value="Astral Projection">Astral Projection</option>
<option value="Biocontrol">Biocontrol</option>
<option value="Channeling">Channeling</option>
<option value="Clairvoyance">Clairvoyance</option>
<option value="Conjuration">Conjuration</option>
<option value="Conveyance">Conveyance</option>
<option value="Cyberkinesis">Cyberkinesis</option>
<option value="Cyberpathy">Cyberpathy</option>
<option value="Divination">Divination</option>
<option value="Ectoplasmic Gen">Ectoplasmic Gen</option>
<option value="Ephemera">Ephemera</option>
<option value="Enchantment">Enchantment</option>
<option value="Fascination">Fascination</option>
<option value="Fortune">Fortune</option>
<option value="Gauntlet Manip">Gauntlet Manip</option>
<option value="Healing">Healing</option>
<option value="Hellfire">Hellfire</option>
<option value="Illusion">Illusion</option>
<option value="Mind Shields">Mind Shields</option>
<option value="Mortal Necromancy">Mortal Necromancy</option>
<option value="Mecronics">Mecronics</option>
<option value="Oneiromancy">Oneiromancy</option>
<option value="Precognition">Precognition</option>
<option value="Psychic Healing">Psychic Healing</option>
<option value="Psychic Hypnosis">Psychic Hypnosis</option>
<option value="Psy Invisibility">Psy Invisibility</option>
<option value="Psy Shadow">Psy Shadow</option>
<option value="Psy Vampirism">Psy Vampirism</option>
<option value="Psychokinesis">Psychokinesis</option>
<option value="Psycometry">Psycometry</option>
<option value="Psychoportation">Psychoportation</option>
<option value="Pyrokineses">Pyrokineses</option>
<option value="Quint Manip">Quint Manip</option>
<option value="Shadows">Shadows</option>
<option value="Shapeshifting">Shapeshifting</option>
<option value="Soulstealing">Soulstealing</option>
<option value="Spirit Awakening">Spirit Awakening</option>
<option value="Spirit Chasing">Spirit Chasing</option>
<option value="Summon Ward Bind">Summon Ward Bind</option>
<option value="Synergy">Synergy</option>
<option value="Telepathy">Telepathy</option>
<option value="True Faith">True Faith</option>
<option value="Weather Control">Weather Control</option></select></div></td>
    <td><div class='item'><select name="power4val" size="1" class="form" id="power4val">
          <option selected><?php echo $data['power4val']?><value="<?php echo $data['power4val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power5 size="1" class="form" id="power5">
<option selected><?php echo $data[power5]?><value="<?php echo $data[power5]?>"></option>
<option value=""></option>
<option value="Alchemy">Alchemy</option>
<option value="Animal Psychic">Animal Psychic</option>
<option value="Anti-Psychic">Anti-Psychic</option>
<option value="Astral Projection">Astral Projection</option>
<option value="Biocontrol">Biocontrol</option>
<option value="Channeling">Channeling</option>
<option value="Clairvoyance">Clairvoyance</option>
<option value="Conjuration">Conjuration</option>
<option value="Conveyance">Conveyance</option>
<option value="Cyberkinesis">Cyberkinesis</option>
<option value="Cyberpathy">Cyberpathy</option>
<option value="Divination">Divination</option>
<option value="Ectoplasmic Gen">Ectoplasmic Gen</option>
<option value="Ephemera">Ephemera</option>
<option value="Enchantment">Enchantment</option>
<option value="Fascination">Fascination</option>
<option value="Fortune">Fortune</option>
<option value="Gauntlet Manip">Gauntlet Manip</option>
<option value="Healing">Healing</option>
<option value="Hellfire">Hellfire</option>
<option value="Illusion">Illusion</option>
<option value="Mind Shields">Mind Shields</option>
<option value="Mortal Necromancy">Mortal Necromancy</option>
<option value="Mecronics">Mecronics</option>
<option value="Oneiromancy">Oneiromancy</option>
<option value="Precognition">Precognition</option>
<option value="Psychic Healing">Psychic Healing</option>
<option value="Psychic Hypnosis">Psychic Hypnosis</option>
<option value="Psy Invisibility">Psy Invisibility</option>
<option value="Psy Shadow">Psy Shadow</option>
<option value="Psy Vampirism">Psy Vampirism</option>
<option value="Psychokinesis">Psychokinesis</option>
<option value="Psycometry">Psycometry</option>
<option value="Psychoportation">Psychoportation</option>
<option value="Pyrokineses">Pyrokineses</option>
<option value="Quint Manip">Quint Manip</option>
<option value="Shadows">Shadows</option>
<option value="Shapeshifting">Shapeshifting</option>
<option value="Soulstealing">Soulstealing</option>
<option value="Spirit Awakening">Spirit Awakening</option>
<option value="Spirit Chasing">Spirit Chasing</option>
<option value="Summon Ward Bind">Summon Ward Bind</option>
<option value="Synergy">Synergy</option>
<option value="Telepathy">Telepathy</option>
<option value="True Faith">True Faith</option>
<option value="Weather Control">Weather Control</option></select></div></td>
    <td><div class='item'><select name="power5val" size="1" class="form" id="power5val">
          <option selected><?php echo $data['power5val']?><value="<?php echo $data['power5val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power6" size="1" class="form" id="power6">
<option selected><?php echo $data[power6]?><value="<?php echo $data[power6]?>"></option>
<option value=""></option>
<option value="Alchemy">Alchemy</option>
<option value="Animal Psychic">Animal Psychic</option>
<option value="Anti-Psychic">Anti-Psychic</option>
<option value="Astral Projection">Astral Projection</option>
<option value="Biocontrol">Biocontrol</option>
<option value="Channeling">Channeling</option>
<option value="Clairvoyance">Clairvoyance</option>
<option value="Conjuration">Conjuration</option>
<option value="Conveyance">Conveyance</option>
<option value="Cyberkinesis">Cyberkinesis</option>
<option value="Cyberpathy">Cyberpathy</option>
<option value="Divination">Divination</option>
<option value="Ectoplasmic Gen">Ectoplasmic Gen</option>
<option value="Ephemera">Ephemera</option>
<option value="Enchantment">Enchantment</option>
<option value="Fascination">Fascination</option>
<option value="Fortune">Fortune</option>
<option value="Gauntlet Manip">Gauntlet Manip</option>
<option value="Healing">Healing</option>
<option value="Hellfire">Hellfire</option>
<option value="Illusion">Illusion</option>
<option value="Mind Shields">Mind Shields</option>
<option value="Mortal Necromancy">Mortal Necromancy</option>
<option value="Mecronics">Mecronics</option>
<option value="Oneiromancy">Oneiromancy</option>
<option value="Precognition">Precognition</option>
<option value="Psychic Healing">Psychic Healing</option>
<option value="Psychic Hypnosis">Psychic Hypnosis</option>
<option value="Psy Invisibility">Psy Invisibility</option>
<option value="Psy Shadow">Psy Shadow</option>
<option value="Psy Vampirism">Psy Vampirism</option>
<option value="Psychokinesis">Psychokinesis</option>
<option value="Psycometry">Psycometry</option>
<option value="Psychoportation">Psychoportation</option>
<option value="Pyrokineses">Pyrokineses</option>
<option value="Quint Manip">Quint Manip</option>
<option value="Shadows">Shadows</option>
<option value="Shapeshifting">Shapeshifting</option>
<option value="Soulstealing">Soulstealing</option>
<option value="Spirit Awakening">Spirit Awakening</option>
<option value="Spirit Chasing">Spirit Chasing</option>
<option value="Summon Ward Bind">Summon Ward Bind</option>
<option value="Synergy">Synergy</option>
<option value="Telepathy">Telepathy</option>
<option value="True Faith">True Faith</option>
<option value="Weather Control">Weather Control</option></select></div></td>
    <td><div class='item'><select name="power6val" size="1" class="form" id="power6val">
          <option selected><?php echo $data['power6val']?><value="<?php echo $data['power6val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
  </tr>
  <tr>
    <td><div class='item'><select name="power7" size="1" class="form" id="power7">
<option selected><?php echo $data[power7]?><value="<?php echo $data[power7]?>"></option>
<option value=""></option>
<option value="Alchemy">Alchemy</option>
<option value="Animal Psychic">Animal Psychic</option>
<option value="Anti-Psychic">Anti-Psychic</option>
<option value="Astral Projection">Astral Projection</option>
<option value="Biocontrol">Biocontrol</option>
<option value="Channeling">Channeling</option>
<option value="Clairvoyance">Clairvoyance</option>
<option value="Conjuration">Conjuration</option>
<option value="Conveyance">Conveyance</option>
<option value="Cyberkinesis">Cyberkinesis</option>
<option value="Cyberpathy">Cyberpathy</option>
<option value="Divination">Divination</option>
<option value="Ectoplasmic Gen">Ectoplasmic Gen</option>
<option value="Ephemera">Ephemera</option>
<option value="Enchantment">Enchantment</option>
<option value="Fascination">Fascination</option>
<option value="Fortune">Fortune</option>
<option value="Gauntlet Manip">Gauntlet Manip</option>
<option value="Healing">Healing</option>
<option value="Hellfire">Hellfire</option>
<option value="Illusion">Illusion</option>
<option value="Mind Shields">Mind Shields</option>
<option value="Mortal Necromancy">Mortal Necromancy</option>

<option value="Mecronics">Mecronics</option>
<option value="Oneiromancy">Oneiromancy</option>
<option value="Precognition">Precognition</option>
<option value="Psychic Healing">Psychic Healing</option>
<option value="Psychic Hypnosis">Psychic Hypnosis</option>
<option value="Psy Invisibility">Psy Invisibility</option>
<option value="Psy Shadow">Psy Shadow</option>
<option value="Psy Vampirism">Psy Vampirism</option>
<option value="Psychokinesis">Psychokinesis</option>
<option value="Psycometry">Psycometry</option>
<option value="Psychoportation">Psychoportation</option>
<option value="Pyrokineses">Pyrokineses</option>
<option value="Quint Manip">Quint Manip</option>
<option value="Shadows">Shadows</option>
<option value="Shapeshifting">Shapeshifting</option>
<option value="Soulstealing">Soulstealing</option>
<option value="Spirit Awakening">Spirit Awakening</option>
<option value="Spirit Chasing">Spirit Chasing</option>
<option value="Summon Ward Bind">Summon Ward Bind</option>
<option value="Synergy">Synergy</option>
<option value="Telepathy">Telepathy</option>
<option value="True Faith">True Faith</option>
<option value="Weather Control">Weather Control</option></select></div></td>
    <td><div class='item'><select name="power7val" size="1" class="form" id="power7val">
          <option selected><?php echo $data['power7val']?><value="<?php echo $data['power7val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power8" size="1" class="form" id="power8">
<option selected><?php echo $data[power8]?><value="<?php echo $data[power8]?>"></option>
<option value=""></option>
<option value="Alchemy">Alchemy</option>
<option value="Animal Psychic">Animal Psychic</option>
<option value="Anti-Psychic">Anti-Psychic</option>
<option value="Astral Projection">Astral Projection</option>
<option value="Biocontrol">Biocontrol</option>
<option value="Channeling">Channeling</option>
<option value="Clairvoyance">Clairvoyance</option>
<option value="Conjuration">Conjuration</option>
<option value="Conveyance">Conveyance</option>
<option value="Cyberkinesis">Cyberkinesis</option>
<option value="Cyberpathy">Cyberpathy</option>
<option value="Divination">Divination</option>
<option value="Ectoplasmic Gen">Ectoplasmic Gen</option>
<option value="Ephemera">Ephemera</option>
<option value="Enchantment">Enchantment</option>
<option value="Fascination">Fascination</option>
<option value="Fortune">Fortune</option>
<option value="Gauntlet Manip">Gauntlet Manip</option>
<option value="Healing">Healing</option>
<option value="Hellfire">Hellfire</option>
<option value="Illusion">Illusion</option>
<option value="Mind Shields">Mind Shields</option>
<option value="Mortal Necromancy">Mortal Necromancy</option>
<option value="Mecronics">Mecronics</option>
<option value="Oneiromancy">Oneiromancy</option>
<option value="Precognition">Precognition</option>
<option value="Psychic Healing">Psychic Healing</option>
<option value="Psychic Hypnosis">Psychic Hypnosis</option>
<option value="Psy Invisibility">Psy Invisibility</option>
<option value="Psy Shadow">Psy Shadow</option>
<option value="Psy Vampirism">Psy Vampirism</option>
<option value="Psychokinesis">Psychokinesis</option>
<option value="Psycometry">Psycometry</option>
<option value="Psychoportation">Psychoportation</option>
<option value="Pyrokineses">Pyrokineses</option>
<option value="Quint Manip">Quint Manip</option>
<option value="Shadows">Shadows</option>
<option value="Shapeshifting">Shapeshifting</option>
<option value="Soulstealing">Soulstealing</option>
<option value="Spirit Awakening">Spirit Awakening</option>
<option value="Spirit Chasing">Spirit Chasing</option>
<option value="Summon Ward Bind">Summon Ward Bind</option>
<option value="Synergy">Synergy</option>
<option value="Telepathy">Telepathy</option>
<option value="True Faith">True Faith</option>
<option value="Weather Control">Weather Control</option></select></div></td>
    <td><div class='item'><select name="power8val" size="1" class="form" id="power8val">
          <option selected><?php echo $data['power8val']?><value="<?php echo $data['power8val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><select name="power9" size="1" class="form" id="power9">
<option selected><?php echo $data[power9]?><value="<?php echo $data[power9]?>"></option>
<option value=""></option>
<option value="Alchemy">Alchemy</option>
<option value="Animal Psychic">Animal Psychic</option>
<option value="Anti-Psychic">Anti-Psychic</option>
<option value="Astral Projection">Astral Projection</option>
<option value="Biocontrol">Biocontrol</option>
<option value="Channeling">Channeling</option>
<option value="Clairvoyance">Clairvoyance</option>
<option value="Conjuration">Conjuration</option>
<option value="Conveyance">Conveyance</option>
<option value="Cyberkinesis">Cyberkinesis</option>
<option value="Cyberpathy">Cyberpathy</option>
<option value="Divination">Divination</option>
<option value="Ectoplasmic Gen">Ectoplasmic Gen</option>
<option value="Ephemera">Ephemera</option>
<option value="Enchantment">Enchantment</option>
<option value="Fascination">Fascination</option>
<option value="Fortune">Fortune</option>
<option value="Gauntlet Manip">Gauntlet Manip</option>
<option value="Healing">Healing</option>
<option value="Hellfire">Hellfire</option>
<option value="Illusion">Illusion</option>
<option value="Mind Shields">Mind Shields</option>
<option value="Mortal Necromancy">Mortal Necromancy</option>
<option value="Mecronics">Mecronics</option>
<option value="Oneiromancy">Oneiromancy</option>
<option value="Precognition">Precognition</option>
<option value="Psychic Healing">Psychic Healing</option>
<option value="Psychic Hypnosis">Psychic Hypnosis</option>
<option value="Psy Invisibility">Psy Invisibility</option>
<option value="Psy Shadow">Psy Shadow</option>
<option value="Psy Vampirism">Psy Vampirism</option>
<option value="Psychokinesis">Psychokinesis</option>
<option value="Psycometry">Psycometry</option>
<option value="Psychoportation">Psychoportation</option>
<option value="Pyrokineses">Pyrokineses</option>
<option value="Quint Manip">Quint Manip</option>
<option value="Shadows">Shadows</option>
<option value="Shapeshifting">Shapeshifting</option>
<option value="Soulstealing">Soulstealing</option>
<option value="Spirit Awakening">Spirit Awakening</option>
<option value="Spirit Chasing">Spirit Chasing</option>
<option value="Summon Ward Bind">Summon Ward Bind</option>
<option value="Synergy">Synergy</option>
<option value="Telepathy">Telepathy</option>
<option value="True Faith">True Faith</option>
<option value="Weather Control">Weather Control</option></select></div></td>
    <td><div class='item'><select name="power9val" size="1" class="form" id="power9val">
          <option selected><?php echo $data['power9val']?><value="<?php echo $data['power9val']?>"></option>
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select></div></td>
</td>
  </tr>
</table>

<table width="100%" border="0">
  <tr>
      <td colspan="2" align='center'><hr /></td>
  </tr>
  <tr>
      <td width="13%"><div class="item">Rituals</div></td>
          <td width="87%"><textarea name="ritualsk" cols="50" datas="2" class="form" id="ritualsk"><?php echo $data[ritualsk]?></textarea></td>
  </tr>
    <tr>
      <td colspan="2" align='center'><hr /></td>
  </tr>
</table>



<table width="100%" border="0">
  <tr>
    <td width="13%"><div class="item">Willpower</div></td>
    <td width="10%"><div align="center" class="itemsm">Perm</div></td>
    <td width="10%"><div class="item"><select name="wpperm" size="1" class="form" id="wpperm">
        <option selected><?php echo $data[wpperm]?><value="<?php echo $data[wpperm]?>"></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td width="2%">&nbsp;</td>
    <td width="13%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
    <td width="2%">&nbsp;</td>
    <td width="13%"><div class="item">Quintessence</div></td>
    <td width="10%"><div class="itemsm">Temp</div></td>
    <td width="10%"><div class="item"><select name="quinttemp" size="1" class="form" id="quinttemp">
        <option selected><?php echo $data[quinttemp]?><value="<?php echo $data[quinttemp]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="center" class="itemsm">Temp</div></td>
    <td><div class="item"><select name="wptemp" size="1" class="form" id="wptemp">
        <option selected><?php echo $data[wptemp]?><value="<?php echo $data[wptemp]?>"></option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>