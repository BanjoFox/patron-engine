<form name="View Kinfolk" action="">
<link href="../layout.css" rel="stylesheet" type="text/css">
<table style="width:90%"  align="center">
  <tr>
    <td><div align="center" class="pagetopic">Kinfolk Character Sheet</div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'view/master-top.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/wtabar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'view/wta-kin-top.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/wtabar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'view/master-attribute.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/wtabar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'view/wta-abil.php';
      ?>
    </td>
  </tr>
  <tr>
    <td>
      <?php
      require 'view/master-seconds.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/wtabar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'view/master-language.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/wtabar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'view/master-background.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/wtabar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'view/wta-kin-gifts.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/wtabar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
      require 'view/wta-kin-traits.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/wtabar.gif"></div></td>
  </tr>
  <tr>
    <td>
    <?php
    require 'view/wta-renown-alt.php';
      ?>
    </td>
  </tr>
  <tr>
    <td><div class="center"><img src="sheet_images/wtabar.gif"></div></td>
  </tr>
  <tr>
    <td>
      <?php
            require '/var/www/shadowsofthebayou.com/site-inc/bottom_data-view.php';
      ?>
    </td>
  </tr>
</table>
</form>