<?php
if (isset($_POST['old_pw'], $_POST['password']) && $_POST['old_pw'] != $_POST['password']) {
    $comment1 = "Password from '{$_POST['old_pw']}' to '{$_POST['password']}', ";
} else {
    $comment1 = "";
}

if (isset($_POST['view_pw'], $_POST['viewpw']) && $_POST['view_pw'] != $_POST['viewpw']) {
    $comment10 = "View PW '{$_POST['view_pw']}' to '{$_POST['viewpw']}', ";
} else {
    $comment10 = "";
}

if (isset($_POST['old_gender'], $_POST['gender']) && $_POST['old_gender'] != $_POST['gender']) {
    $comment9 = "Gender '{$_POST['old_gender']}' to '{$_POST['gender']}', ";
} else {
    $comment9 = "";
}

if (isset($_POST['old_age'], $_POST['age']) && $_POST['old_age'] != $_POST['age']) {
    $comment3 = "Age '{$_POST['old_age']}' to '{$_POST['age']}', ";
} else {
    $comment3 = "";
}

if (isset($_POST['app_age'], $_POST['apparentage']) && $_POST['app_age'] != $_POST['apparentage']) {
    $comment4 = "Apparent Age '$_POST[app_age]' to '$_POST[apparentage]', ";
} else {
    $comment4 = "";
}

if (isset($_POST['old_nationality'], $_POST['nationality']) && $_POST['old_nationality'] != $_POST['nationality']) {
    $comment8 = "Nationality '$_POST[old_nationality]' to '$_POST[nationality]', ";
} else {
    $comment8 = "";
}

if (isset($_POST['old_concept'], $_POST['concept']) && $_POST['old_concept'] != $_POST['concept']) {
    $comment5 = "Concept '$_POST[old_concept]' to '$_POST[concept]', ";
} else {
    $comment5 = "";
}

if (isset($_POST['old_str'], $_POST['strength']) && $_POST['old_str'] != $_POST['strength']) {
    $comment11 = "Strength '$_POST[old_str]' to '$_POST[strength]', ";
} else {
    $comment11 = "";
}

if (isset($_POST['old_dex'], $_POST['dexterity']) && $_POST['old_dex'] != $_POST['dexterity']) {
    $comment12 = "Dexterity '$_POST[old_dex]' to '$_POST[dexterity]', ";
} else {
    $comment12 = "";
}

if (isset($_POST['old_sta'], $_POST['stamina']) && $_POST['old_sta'] != $_POST['stamina']) {
    $comment13 = "Stamina '$_POST[old_sta]' to '$_POST[stamina]', ";
} else {
    $comment13 = "";
}

if (isset($_POST['old_cha'], $_POST['charisma']) && $_POST['old_cha'] != $_POST['charisma']) {
    $comment14 = "Charisma '$_POST[old_cha]' to '$_POST[charisma]', ";
} else {
    $comment14 = "";
}

if (isset($_POST['old_man'], $_POST['manipulation']) && $_POST['old_man'] != $_POST['manipulation']) {
    $comment15 = "Manipulation '$_POST[old_man]' to '$_POST[manipulation]', ";
} else {
    $comment15 = "";
}

if (isset($_POST['old_app'], $_POST['appearance']) && $_POST['old_app'] != $_POST['appearance']) {
    $comment16 = "Appearance '$_POST[old_app]' to '$_POST[appearance]', ";
} else {
    $comment16 = "";
}

if (isset($_POST['old_per'], $_POST['perception']) && $_POST['old_per'] != $_POST['perception']) {
    $comment17 = "Perception '$_POST[old_per]' to '$_POST[perception]', ";
} else {
    $comment17 = "";
}

if (isset($_POST['old_intel'], $_POST['intelligence']) && $_POST['old_intel'] != $_POST['intelligence']) {
    $comment18 = "Intelligence '$_POST[old_intel]' to '$_POST[intelligence]', ";
} else {
    $comment18 = "";
}

if (isset($_POST['old_wit'], $_POST['wits']) && $_POST['old_wit'] != $_POST['wits']) {
    $comment19 = "Wits '$_POST[old_wit]' to '$_POST[wits]', ";
} else {
    $comment19 = "";
}

if (isset($_POST['att_special'], $_POST['attspecial']) && $_POST['att_special'] != $_POST['attspecial']) {
    $comment20 = "Attribute Specs '$_POST[att_special]' to '$_POST[attspecial]', ";
} else {
    $comment20 = "";
}

if (isset($_POST['old_academics'], $_POST['academics']) && $_POST['old_academics'] != $_POST['academics']) {
    $comment21 = "Academics '$_POST[old_academics]' to '$_POST[academics]', ";
} else {
    $comment21 = "";
}

if (isset($_POST['old_alertness'], $_POST['alertness']) && $_POST['old_alertness'] != $_POST['alertness']) {
    $comment22 = "Alertness '$_POST[old_alertness]' to '$_POST[alertness]', ";
} else {
    $comment22 = "";
}

if (isset($_POST['old_animalken'], $_POST['animalken']) && $_POST['old_animalken'] != $_POST['animalken']) {
    $comment23 = "AnimalKen '$_POST[old_animalken]' to '$_POST[animalken]', ";
} else {
    $comment23 = "";
}

if (isset($_POST['old_athletics'], $_POST['athletics']) && $_POST['old_athletics'] != $_POST['athletics']) {
    $comment24 = "Athletics '$_POST[old_athletics]' to '$_POST[athletics]', ";
} else {
    $comment24 = "";
}

if (isset($_POST['old_art'], $_POST['arts']) && $_POST['old_art'] != $_POST['arts']) {
    $comment25 = "Arts '$_POST[old_art]' to '$_POST[arts]', ";
} else {
    $comment25 = "";
}

if (isset($_POST['old_awareness'], $_POST['awareness']) && $_POST['old_awareness'] != $_POST['awareness']) {
    $comment26 = "Awareness '$_POST[old_awareness]' to '$_POST[awareness]', ";
} else {
    $comment26 = "";
}

if (isset($_POST['old_brawl'], $_POST['brawl']) && $_POST['old_brawl'] != $_POST['brawl']) {
    $comment27 = "Brawl '$_POST[old_brawl]' to '$_POST[brawl]', ";
} else {
    $comment27 = "";
}

if (isset($_POST['old_bureaucracy'], $_POST['bureaucracy']) && $_POST['old_bureaucracy'] != $_POST['bureaucracy']) {
    $comment28 = "Bureaucracy '$_POST[old_bureaucracy]' to '$_POST[bureaucracy]', ";
} else {
    $comment28 = "";
}

if (isset($_POST['old_computer'], $_POST['computer']) && $_POST['old_computer'] != $_POST['computer']) {
    $comment29 = "Computer '$_POST[old_computer]' to '$_POST[computer]', ";
} else {
    $comment29 = "";
}

if (isset($_POST['old_cosmology'], $_POST['cosmology']) && $_POST['old_cosmology'] != $_POST['cosmology']) {
    $comment30 = "Cosmology '$_POST[old_cosmology]' to '$_POST[cosmology]' ,";
} else {
    $comment30 = "";
}

if (isset($_POST['old_crafts'], $_POST['crafts']) && $_POST['old_crafts'] != $_POST['crafts']) {
    $comment31 = "Crafts '$_POST[old_crafts]' to '$_POST[crafts]', ";
} else {
    $comment31 = "";
}

if (isset($_POST['old_drive'], $_POST['drive']) && $_POST['old_drive'] != $_POST['drive']) {
    $comment32 = "Drive '$_POST[old_drive]' to '$_POST[drive]', ";
} else {
    $comment32 = "";
}

if (isset($_POST['old_empathy'], $_POST['empathy']) && $_POST['old_empathy'] != $_POST['empathy']) {
    $comment33 = "Empathy '$_POST[old_empathy]' to '$_POST[empathy]', ";
} else {
    $comment33 = "";
}

if (isset($_POST['old_enigmas'], $_POST['enigmas']) && $_POST['old_enigmas'] != $_POST['enigmas']) {
    $comment34 = "Enigmas '$_POST[old_enigmas]' to '$_POST[enigmas]', ";
} else {
    $comment34 = "";
}

if (isset($_POST['old_esoterica'], $_POST['esoterica']) && $_POST['old_esoterica'] != $_POST['esoterica']) {
    $comment35 = "Esoterica '$_POST[old_esoterica]' to '$_POST[esoterica]', ";
} else {
    $comment35 = "";
}

if (isset($_POST['old_etiquette'], $_POST['etiquette']) && $_POST['old_etiquette'] != $_POST['etiquette']) {
    $comment36 = "Etiquette '$_POST[old_etiquette]' to '$_POST[etiquette]', ";
} else {
    $comment36 = "";
}

if (isset($_POST['old_expression'], $_POST['expression']) && $_POST['old_expression'] != $_POST['expression']) {
    $comment37 = "Expression '$_POST[old_expression]' to '$_POST[expression]', ";
} else {
    $comment37 = "";
}

if (isset($_POST['old_finance'], $_POST['finance']) && $_POST['old_finance'] != $_POST['finance']) {
    $comment38 = "Finance '$_POST[old_finance]' to '$_POST[finance]', ";
} else {
    $comment38 = "";
}

if (isset($_POST['old_firearms'], $_POST['firearms']) && $_POST['old_firearms'] != $_POST['firearms']) {
    $comment39 = "Firearms '$_POST[old_firearms]' to '$_POST[firearms]', ";
} else {
    $comment39 = "";
}

if (isset($_POST['old_gremayre'], $_POST['gremayre']) && $_POST['old_gremayre'] != $_POST['gremayre']) {
    $comment40 = "Gremayre '$_POST[old_gremayre]' to '$_POST[gremayre]', ";
} else {
    $comment40 = "";
}

if (isset($_POST['old_intimidation'], $_POST['intimidation']) && $_POST['old_intimidation'] != $_POST['intimidation']) {
    $comment41 = "Intimidation '$_POST[old_intimidation]' to '$_POST[intimidation]', ";
} else {
    $comment41 = "";
}

if (isset($_POST['old_investigation'], $_POST['investigation']) && $_POST['old_investigation'] != $_POST['investigation']) {
    $comment42 = "Investigation '$_POST[old_investigation]' to '$_POST[investigation]', ";
} else {
    $comment42 = "";
}

if (isset($_POST['old_kenning'], $_POST['kenning']) && $_POST['old_kenning'] != $_POST['kenning']) {
    $comment43 = "Kenning '$_POST[old_kenning]' to '$_POST[kenning]', ";
} else {
    $comment43 = "";
}

if (isset($_POST['old_larceny'], $_POST['larceny']) && $_POST['old_larceny'] != $_POST['larceny']) {
    $comment44 = "Larceny '$_POST[old_larceny]' to '$_POST[larceny]', ";
} else {
    $comment44 = "";
}

if (isset($_POST['old_law'], $_POST['law']) && $_POST['old_law'] != $_POST['law']) {
    $comment45 = "Law '$_POST[old_law]' to '$_POST[law]', ";
} else {
    $comment45 = "";
}

if (isset($_POST['old_leadership'], $_POST['leadership']) && $_POST['old_leadership'] != $_POST['leadership']) {
    $comment46 = "Leadership '$_POST[old_leadership]' to '$_POST[leadership]', ";
} else {
    $comment46 = "";
}

if (isset($_POST['old_lore'], $_POST['lore']) && $_POST['old_lore'] != $_POST['lore']) {
    $comment47 = "Lore '$_POST[old_lore]' to '$_POST[lore]', ";
} else {
    $comment47 = "";
}

if (isset($_POST['old_martialarts'], $_POST['martialarts']) && $_POST['old_martialarts'] != $_POST['martialarts']) {
    $comment48 = "MartialArts '$_POST[old_martialarts]' to '$_POST[martialarts]', ";
} else {
    $comment48 = "";
}

if (isset($_POST['old_medicine'], $_POST['medicine']) && $_POST['old_medicine'] != $_POST['medicine']) {
    $comment49 = "Medicine '$_POST[old_medicine]' to '$_POST[medicine]', ";
} else {
    $comment49 = "";
}

if (isset($_POST['old_meditation'], $_POST['meditation']) && $_POST['old_meditation'] != $_POST['meditation']) {
    $comment50 = "Meditation '$_POST[old_meditation]' to '$_POST[meditation]', ";
} else {
    $comment50 = "";
}

if (isset($_POST['old_melee'], $_POST['melee']) && $_POST['old_melee'] != $_POST['melee']) {
    $comment51 = "Melee '$_POST[old_melee]' to '$_POST[melee]', ";
} else {
    $comment51 = "";
}

if (isset($_POST['old_occult'], $_POST['occult']) && $_POST['old_occult'] != $_POST['occult']) {
    $comment52 = "Occult '$_POST[old_occult]' to '$_POST[occult]', ";
} else {
    $comment52 = "";
}

if (isset($_POST['old_performance'], $_POST['performance']) && $_POST['old_performance'] != $_POST['performance']) {
    $comment53 = "Performance '$_POST[old_performance]' to '$_POST[performance]', ";
} else {
    $comment53 = "";
}

if (isset($_POST['old_politics'], $_POST['politics']) && $_POST['old_politics'] != $_POST['politics']) {
    $comment54 = "Politics '$_POST[old_politics]' to '$_POST[politics]', ";
} else {
    $comment54 = "";
}

if (isset($_POST['old_primalurge'], $_POST['primalurge']) && $_POST['old_primalurge'] != $_POST['primalurge']) {
    $comment55 = "PrimalUrge '$_POST[old_primalurge]' to '$_POST[primalurge]', ";
} else {
    $comment55 = "";
}

if (isset($_POST['old_research'], $_POST['research']) && $_POST['old_research'] != $_POST['research']) {
    $comment56 = "Research '$_POST[old_research]' to '$_POST[research]', ";
} else {
    $comment56 = "";
}

if (isset($_POST['old_rituals'], $_POST['rituals']) && $_POST['old_rituals'] != $_POST['rituals']) {
    $comment57 = "Rituals '$_POST[old_rituals]' to '$_POST[rituals]', ";
} else {
    $comment57 = "";
}

if (isset($_POST['old_science'], $_POST['science']) && $_POST['old_science'] != $_POST['science']) {
    $comment58 = "Science '$_POST[old_science]' to '$_POST[science]', ";
} else {
    $comment58 = "";
}

if (isset($_POST['old_stealth'], $_POST['stealth']) && $_POST['old_stealth'] != $_POST['stealth']) {
    $comment59 = "Stealth '$_POST[old_stealth]' to '$_POST[stealth]', ";
} else {
    $comment59 = "";
}

if (isset($_POST['old_streetwise'], $_POST['streetwise']) && $_POST['old_streetwise'] != $_POST['streetwise']) {
    $comment60 = "Streetwise '$_POST[old_streetwise]' to '$_POST[streetwise]', ";
} else {
    $comment60 = "";
}

if (isset($_POST['old_subterfuge'], $_POST['subterfuge']) && $_POST['old_subterfuge'] != $_POST['subterfuge']) {
    $comment61 = "Subterfuge '$_POST[old_subterfuge]' to '$_POST[subterfuge]', ";
} else {
    $comment61 = "";
}

if (isset($_POST['old_survival'], $_POST['survival']) && $_POST['old_survival'] != $_POST['survival']) {
    $comment62 = "Survival '$_POST[old_survival]' to '$_POST[survival]', ";
} else {
    $comment62 = "";
}

if (isset($_POST['old_technology'], $_POST['technology']) && $_POST['old_technology'] != $_POST['technology']) {
    $comment63 = "Technology '$_POST[old_technology]' to '$_POST[technology]', ";
} else {
    $comment63 = "";
}

if (isset($_POST['abil_special'], $_POST['abilspecial']) && $_POST['abil_special'] != $_POST['abilspecial']) {
    $comment64 = "Ability Specialties '$_POST[abil_special]' to '$_POST[abilspecial]', ";
} else {
    $comment64 = "";
}

if (isset($_POST['old_alt_name'], $_POST['alt_name']) && $_POST['old_alt_name'] != $_POST['alt_name']) {
    $comment2 = "Alternate Name '$_POST[old_alt_name]' to '$_POST[alt_name]', ";
} else {
    $comment2 = "";
}

if (isset($_POST['old_nature'], $_POST['nature']) && $_POST['old_nature'] != $_POST['nature']) {
    $comment6 = "Nature '$_POST[old_nature]' to '$_POST[nature]', ";
} else {
    $comment6 = "";
}

if (isset($_POST['old_demeanor'], $_POST['demeanor']) && $_POST['old_demeanor'] != $_POST['demeanor']) {
    $comment7 = "Demeanor '$_POST[old_demeanor]' to '$_POST[demeanor]', ";
} else {
    $comment7 = "";
}

if (isset($_POST['old_languages'], $_POST['languages']) && $_POST['old_languages'] != $_POST['languages']) {
    $comment65 = "Languages '$_POST[old_languages]' to '$_POST[languages]', ";
} else {
    $comment65 = "";
}

if (isset($_POST['old_wpperm'], $_POST['wpperm']) && $_POST['old_wpperm'] != $_POST['wpperm']) {
    $comment66 = "Perm Willpower '$_POST[old_wpperm]' to '$_POST[wpperm]', ";
} else {
    $comment66 = "";
}

if (isset($_POST['old_wptemp'], $_POST['wptemp']) && $_POST['old_wptemp'] != $_POST['wptemp']) {
    $comment67 = "Temp Willpower '$_POST[old_wptemp]' to '$_POST[wptemp]', ";
} else {
    $comment67 = "";
}

if (isset($_POST['old_virtue1'], $_POST['virtue1']) && $_POST['old_virtue1'] != $_POST['virtue1']) {
    $comment68 = "$_POST[old_virtue1] changed to '$_POST[virtue1]', ";
} else {
    $comment68 = "";
}

if (isset($_POST['old_virtue1val'], $_POST['virtue1val']) && $_POST['old_virtue1val'] != $_POST['virtue1val']) {
    $comment69 = "$_POST[old_virtue1] from '$_POST[old_virtue1val]' to '$_POST[virtue1val]', ";
} else {
    $comment69 = "";
}

if (isset($_POST['old_virtue2'], $_POST['virtue2']) && $_POST['old_virtue2'] != $_POST['virtue2']) {
    $comment70 = "$_POST[old_virtue2] changed to '$_POST[virtue2]', ";
} else {
    $comment70 = "";
}

if (isset($_POST['old_virtue2val'], $_POST['virtue2val']) && $_POST['old_virtue2val'] != $_POST['virtue2val']) {
    $comment71 = "$_POST[old_virtue2] from '$_POST[old_virtue2val]' to '$_POST[virtue2val]', ";
} else {
    $comment71 = "";
}

if (isset($_POST['old_courage'], $_POST['courage']) && $_POST['old_courage'] != $_POST['courage']) {
    $comment72 = "Courage '$_POST[old_courage]' to '$_POST[courage]', ";
} else {
    $comment72 = "";
}

if (isset($_POST['old_bloodmax'], $_POST['bloodmax']) && $_POST['old_bloodmax'] != $_POST['bloodmax']) {
    $comment73 = "Max Blood Pool '$_POST[old_bloodmax]' to '$_POST[bloodmax]', ";
} else {
    $comment73 = "";
}

if (isset($_POST['old_bloodcurrent'], $_POST['bloodcurrent']) && $_POST['old_bloodcurrent'] != $_POST['bloodcurrent']) {
    $comment74 = "Current Blood Pool '$_POST[old_bloodcurrent]' to '$_POST[bloodcurrent]', ";
} else {
    $comment74 = "";
}

if (isset($_POST['old_bloodturn'], $_POST['bloodturn']) && $_POST['old_bloodturn'] != $_POST['bloodturn']) {
    $comment75 = "Blood per Turn '$_POST[old_bloodturn]' to '$_POST[bloodturn]', ";
} else {
    $comment75 = "";
}

if (isset($_POST['old_gnosisperm'], $_POST['gnosisperm']) && $_POST['old_gnosisperm'] != $_POST['gnosisperm']) {
    $comment76 = "Perm Gnosis '$_POST[old_gnosisperm]' to '$_POST[gnosisperm]', ";
} else {
    $comment76 = "";
}

if (isset($_POST['old_gnosistemp'], $_POST['gnosistemp']) && $_POST['old_gnosistemp'] != $_POST['gnosistemp']) {
    $comment77 = "Temp Gnosis '$_POST[old_gnosistemp]' to '$_POST[gnosistemp]', ";
} else {
    $comment77 = "";
}

if (isset($_POST['old_rage_blood'], $_POST['rage_blood'])) {
    $comment78 = "Rage or Blood set to '$_POST[rage_blood]', ";
} else {
    $comment78 = "";
}

if (isset($_POST['old_rageperm'], $_POST['rageperm']) && $_POST['old_rageperm'] != $_POST['rageperm']) {
    $comment79 = "$_POST[rage_blood] '$_POST[old_rageperm]' to '$_POST[rageperm]', ";
} else {
    $comment79 = "";
}

if (isset($_POST['old_ragetemp'], $_POST['ragetemp']) && $_POST['old_ragetemp'] != $_POST['ragetemp']) {
    $comment80 = "Temp Rage '$_POST[old_ragetemp]' to '$_POST[ragetemp]', ";
} else {
    $comment80 = "";
}

if (isset($_POST['old_glamourperm'], $_POST['glamourperm']) && $_POST['old_glamourperm'] != $_POST['glamourperm']) {
    $comment81 = "Perm Glamour '$_POST[old_glamourperm]' to '$_POST[glamourperm]', ";
} else {
    $comment81 = "";
}

if (isset($_POST['old_glamourtemp'], $_POST['glamourtemp']) && $_POST['old_glamourtemp'] != $_POST['glamourtemp']) {
    $comment82 = "Temp Glamour '$_POST[old_glamourtemp]' to '$_POST[glamourtemp]', ";
} else {
    $comment82 = "";
}

if (isset($_POST['old_banalityperm'], $_POST['banalityperm']) && $_POST['old_banalityperm'] != $_POST['banalityperm']) {
    $comment83 = "Perm Banality '$_POST[old_banalityperm]' to '$_POST[banalityperm]', ";
} else {
    $comment83 = "";
}

if (isset($_POST['old_banalitytemp'], $_POST['banalitytemp']) && $_POST['old_banalitytemp'] != $_POST['banalitytemp']) {
    $comment84 = "Temp Banality '$_POST[old_banalitytemp]' to '$_POST[banalitytemp]', ";
} else {
    $comment84 = "";
}

if (isset($_POST['old_corpusperm'], $_POST['corpusperm']) && $_POST['old_corpusperm'] != $_POST['corpusperm']) {
    $comment85 = "Perm Corpus '$_POST[old_corpusperm]' to '$_POST[corpusperm]', ";
} else {
    $comment85 = "";
}

if (isset($_POST['old_corpustemp'], $_POST['corpustemp']) && $_POST['old_corpustemp'] != $_POST['corpustemp']) {
    $comment86 = "Temp Corpus '$_POST[old_corpustemp]' to '$_POST[corpustemp]', ";
} else {
    $comment86 = "";
}

if (isset($_POST['old_arete'], $_POST['arete']) && $_POST['old_arete'] != $_POST['arete']) {
    $comment87 = "Arete '$_POST[old_arete]' to '$_POST[arete]', ";
} else {
    $comment87 = "";
}

if (isset($_POST['old_quinttemp'], $_POST['quinttemp']) && $_POST['old_quinttemp'] != $_POST['quinttemp']) {
    $comment88 = "Quint/Mana '$_POST[old_quinttemp]' to '$_POST[quinttemp]', ";
} else {
    $comment88 = "";
}

if (isset($_POST['old_paradox'], $_POST['paradox']) && $_POST['old_paradox'] != $_POST['paradox']) {
    $comment89 = "Paradox '$_POST[old_paradox]' to '$_POST[paradox]', ";
} else {
    $comment89 = "";
}

if (isset($_POST['old_pathos'], $_POST['pathos']) && $_POST['old_pathos'] != $_POST['pathos']) {
    $comment90 = "Pathos '$_POST[old_pathos]' to '$_POST[pathos]', ";
} else {
    $comment90 = "";
}

if (isset($_POST['old_guild'], $_POST['guild']) && $_POST['old_guild'] != $_POST['guild']) {
    $comment91 = "Guild '$_POST[old_guild]' to '$_POST[guild]', ";
} else {
    $comment91 = "";
}

if (isset($_POST['old_legion'], $_POST['legion']) && $_POST['old_legion'] != $_POST['legion']) {
    $comment92 = "Legion '$_POST[old_legion]' to '$_POST[legion]', ";
} else {
    $comment92 = "";
}

if (isset($_POST['old_life_w'], $_POST['life_w']) && $_POST['old_life_w'] != $_POST['life_w']) {
    $comment93 = "Life '$_POST[old_life_w]' to '$_POST[life_w]', ";
} else {
    $comment93 = "";
}

if (isset($_POST['old_death'], $_POST['death']) && $_POST['old_death'] != $_POST['death']) {
    $comment94 = "Death '$_POST[old_death]' to '$_POST[death]', ";
} else {
    $comment94 = "";
}

if (isset($_POST['old_regret'], $_POST['regret']) && $_POST['old_regret'] != $_POST['regret']) {
    $comment95 = "Regret '$_POST[old_regret]' to '$_POST[regret]', ";
} else {
    $comment95 = "";
}

if (isset($_POST['old_faction'], $_POST['faction']) && $_POST['old_faction'] != $_POST['faction']) {
    $comment96 = "Faction '$_POST[old_faction]' to '$_POST[faction]', ";
} else {
    $comment96 = "";
}

if (isset($_POST['old_coorespondence'], $_POST['coorespondence']) && $_POST['old_coorespondence'] != $_POST['coorespondence']) {
    $comment97 = "Correspondence '$_POST[old_coorespondence]' to '$_POST[coorespondence]', ";
} else {
    $comment97 = "";
}

if (isset($_POST['old_entropy'], $_POST['entropy']) && $_POST['old_entropy'] != $_POST['entropy']) {
    $comment98 = "Entropy '$_POST[old_entropy]' to '$_POST[entropy]', ";
} else {
    $comment98 = "";
}

if (isset($_POST['old_forces'], $_POST['forces']) && $_POST['old_forces'] != $_POST['forces']) {
    $comment99 = "Forces '$_POST[old_forces]' to '$_POST[forces]', ";
} else {
    $comment99 = "";
}

if (isset($_POST['old_life'], $_POST['life']) && $_POST['old_life'] != $_POST['life']) {
    $comment100 = "Life '$_POST[old_life]' to '$_POST[life]', ";
} else {
    $comment100 = "";
}

if (isset($_POST['old_matter'], $_POST['matter']) && $_POST['old_matter'] != $_POST['matter']) {
    $comment101 = "Matter '$_POST[old_matter]' to '$_POST[matter]', ";
} else {
    $comment101 = "";
}

if (isset($_POST['old_mind'], $_POST['mind']) && $_POST['old_mind'] != $_POST['mind']) {
    $comment102 = "Mind '$_POST[old_mind]' to '$_POST[mind]', ";
} else {
    $comment102 = "";
}

if (isset($_POST['old_prime'], $_POST['prime']) && $_POST['old_prime'] != $_POST['prime']) {
    $comment103 = "Prime '$_POST[old_prime]' to '$_POST[prime]', ";
} else {
    $comment103 = "";
}

if (isset($_POST['old_spirit'], $_POST['spirit']) && $_POST['old_spirit'] != $_POST['spirit']) {
    $comment104 = "Spirit '$_POST[old_spirit]' to '$_POST[spirit]', ";
} else {
    $comment104 = "";
}

if (isset($_POST['old_time'], $_POST['time']) && $_POST['old_time'] != $_POST['time']) {
    $comment105 = "Time '$_POST[old_time]' to '$_POST[time]', ";
} else {
    $comment105 = "";
}

if (isset($_POST['old_passion'], $_POST['passion']) && $_POST['old_passion'] != $_POST['passion']) {
    $comment106 = "Passions '$_POST[old_passion]' to '$_POST[passion]', ";
} else {
    $comment106 = "";
}

if (isset($_POST['old_fetter'], $_POST['fetter']) && $_POST['old_fetter'] != $_POST['fetter']) {
    $comment107 = "Fetters '$_POST[old_fetter]' to '$_POST[fetter]', ";
} else {
    $comment107 = "";
}

if (isset($_POST['old_gift1'], $_POST['gift1']) && $_POST['old_gift1'] != $_POST['gift1']) {
    $comment108 = "Level 1 Gifts '$_POST[old_gift1]' to '$_POST[gift1]', ";
} else {
    $comment108 = "";
}

if ($_POST['old_gift2'] != $_POST['gift2']) {
    $comment109 = "Level 2 Gifts '$_POST[old_gift2]' to '$_POST[gift2]', ";
} else {
    $comment109 = "";
}

if ($_POST['old_gift3'] != $_POST['gift3']) {
    $comment110 = "Level 3 Gifts '$_POST[old_gift3] to'$_POST[gift3]', ";
} else {
    $comment110 = "";
}

if ($_POST['old_gift4'] != $_POST['gift4']) {
    $comment111 = "Level 4 Gifts '$_POST[old_gift4]' to '$_POST[gift4]', ";
} else {
    $comment111 = "";
}

if ($_POST['old_gift5'] != $_POST['gift5']) {
    $comment112 = "Level 5 Gifts '$_POST[old_gift5]' to '$_POST[gift5]', ";
} else {
    $comment112 = "";
}

if ($_POST['old_actor_c'] != $_POST['actor_c']) {
    $comment113 = "Actor '$_POST[old_actor_c]' to '$_POST[actor_c]', ";
} else {
    $comment113 = "";
}

if ($_POST['old_fae_c'] != $_POST['fae_c']) {
    $comment114 = "Fae '$_POST[old_fae_c]' to '$_POST[fae_c]', ";
} else {
    $comment114 = "";
}

if ($_POST['old_nature_c'] != $_POST['nature_c']) {
    $comment115 = "Nature '$_POST[old_nature_c]' to '$_POST[nature_c]', ";
} else {
    $comment115 = "";
}

if ($_POST['old_prop_c'] != $_POST['prop_c']) {
    $comment116 = "Prop '$_POST[old_prop_c]' to '$_POST[prop_c]', ";
} else {
    $comment116 = "";
}

if ($_POST['old_scene_c'] != $_POST['scene_c']) {
    $comment117 = "Scene '$_POST[old_scene_c]' to '$_POST[scene_c]', ";
} else {
    $comment117 = "";
}

if ($_POST['old_time_c'] != $_POST['time_c']) {
    $comment118 = "Time '$_POST[old_time_c]' to '$_POST[time_c]', ";
} else {
    $comment118 = "";
}

if ($_POST['old_path'] != $_POST['path']) {
    $comment119 = "Path '$_POST[old_path]' to '$_POST[path]', ";
} else {
    $comment119 = "";
}

if ($_POST['old_pathval'] != $_POST['pathval']) {
    $comment120 = "$_POST[path] '$_POST[old_pathval]' to '$_POST[pathval]' ,";
} else {
    $comment120 = "";
}

if ($_POST['old_pathbearing'] != $_POST['pathbearing']) {
    $comment217 = "Bearing '$_POST[old_pathbearing]' to '$_POST[pathbearing]', ";
} else {
    $comment217 = "";
}

if ($_POST['old_employer'] != $_POST['employer']) {
    $comment122 = "Employer '$_POST[old_employer]' to '$_POST[employer]', ";
} else {
    $comment122 = "";
}

if ($_POST['old_clan'] != $_POST['clan']) {
    $comment123 = "Clan '$_POST[old_clan]' to '$_POST[clan]', ";
} else {
    $comment123 = "";
}

if ($_POST['old_bloodline'] != $_POST['bloodline']) {
    $comment124 = "Bloodline '$_POST[old_bloodline]' to '$_POST[bloodline]', ";
} else {
    $comment124 = "";
}

if ($_POST['old_sire'] != $_POST['sire']) {
    $comment125 = "Sire '$_POST[old_sire]' to '$_POST[sire]', ";
} else {
    $comment125 = "";
}

if ($_POST['old_sect'] != $_POST['sect']) {
    $comment126 = "Sect '$_POST[old_sect]' to '$_POST[sect]', ";
} else {
    $comment126 = "";
}

if ($_POST['old_breed'] != $_POST['breed']) {
    $comment127 = "Breed '$_POST[old_breed]' to '$_POST[breed]', ";
} else {
    $comment127 = "";
}

if ($_POST['old_tribe'] != $_POST['tribe']) {
    $comment128 = "Tribe '$_POST[old_tribe]' to '$_POST[tribe]', ";
} else {
    $comment128 = "";
}

if ($_POST['old_auspice'] != $_POST['auspice']) {
    $comment129 = "Auspice '$_POST[old_auspice]' to '$_POST[auspice]', ";
} else {
    $comment129 = "";
}

if ($_POST['old_packname'] != $_POST['packname']) {
    $comment130 = "Pack/Pride '$_POST[old_packname]' to '$_POST[packname]', ";
} else {
    $comment130 = "";
}

if ($_POST['old_packtotem'] != $_POST['packtotem']) {
    $comment131 = "Totem '$_POST[old_packtotem]' to '$_POST[packtotem]', ";
} else {
    $comment131 = "";
}

if ($_POST['old_camp'] != $_POST['camp']) {
    $comment132 = "Camp '$_POST[old_camp]' to '$_POST[camp]', ";
} else {
    $comment132 = "";
}

if ($_POST['old_tradition'] != $_POST['tradition']) {
    $comment133 = "Tradition '$_POST[old_tradition]' to '$_POST[tradition]', ";
} else {
    $comment133 = "";
}

if ($_POST['old_spec_sphere'] != $_POST['spec_sphere']) {
    $comment134 = "Specialty Sphere '$_POST[old_spec_sphere]' to '$_POST[spec_sphere]', ";
} else {
    $comment134 = "";
}

if ($_POST['old_essence'] != $_POST['essence']) {
    $comment135 = "Essence '$_POST[old_essence]' to '$_POST[essence]', ";
} else {
    $comment135 = "";
}

if ($_POST['old_affiliation'] != $_POST['affiliation']) {
    $comment136 = "Affiliation '$_POST[old_affiliation]' to '$_POST[affiliation]', ";
} else {
    $comment136 = "";
}

if ($_POST['old_kith'] != $_POST['kith']) {
    $comment137 = "Kith '$_POST[old_kith]' to '$_POST[kith]', ";
} else {
    $comment137 = "";
}

if ($_POST['old_seeming'] != $_POST['seeming']) {
    $comment138 = "Seeming '$_POST[old_seeming]' to '$_POST[seeming]', ";
} else {
    $comment138 = "";
}

if ($_POST['old_court'] != $_POST['court']) {
    $comment139 = "Court '$_POST[old_court]' to '$_POST[court]', ";
} else {
    $comment139 = "";
}

if ($_POST['old_house'] != $_POST['house']) {
    $comment140 = "House '$_POST[old_house]' to '$_POST[house]', ";
} else {
    $comment140 = "";
}

if ($_POST['old_bygone_species'] != $_POST['bygone_species']) {
    $comment141 = "Bygone Species '$_POST[old_bygone_species]' to '$_POST[bygone_species]', ";
} else {
    $comment141 = "";
}

if ($_POST['old_bygone_element'] != $_POST['bygone_element']) {
    $comment142 = "Bygone Element '$_POST[old_bygone_element]' to '$_POST[bygone_element]', ";
} else {
    $comment142 = "";
}

if ($_POST['old_generation'] != $_POST['generation']) {
    $comment143 = "Generation '$_POST[old_generation]' to '$_POST[generation]', ";
} else {
    $comment143 = "";
}

if ($_POST['old_rank'] != $_POST['rank']) {
    $comment144 = "Rank '$_POST[old_rank]' to '$_POST[rank]', ";
} else {
    $comment144 = "";
}

if ($_POST['old_merits'] != $_POST['merits']) {
    $comment145 = "Merits '$_POST[old_merits]' to '$_POST[merits]', ";
} else {
    $comment145 = "";
}

if ($_POST['old_flaws'] != $_POST['flaws']) {
    $comment146 = "Flaws '$_POST[old_flaws]' to '$_POST[flaws]', ";
} else {
    $comment146 = "";
}

if ($_POST['old_sec1val'] != $_POST['sec1val']) {
    $comment147 = "$_POST[sec1] '$_POST[old_sec1val]' to '$_POST[sec1val]', ";
} else {
    $comment147 = "";
}

if ($_POST['old_sec2val'] != $_POST['sec2val']) {
    $comment148 = "$_POST[sec2] '$_POST[old_sec2val]' to '$_POST[sec2val]', ";
} else {
    $comment148 = "";
}

if ($_POST['old_sec3val'] != $_POST['sec3val']) {
    $comment149 = "$_POST[sec3] '$_POST[old_sec3val]' to '$_POST[sec3val]', ";
} else {
    $comment149 = "";
}

if ($_POST['old_sec4val'] != $_POST['sec4val']) {
    $comment150 = "$_POST[sec4] '$_POST[old_sec4val]' to '$_POST[sec4val]', ";
} else {
    $comment150 = "";
}

if ($_POST['old_sec5val'] != $_POST['sec5val']) {
    $comment151 = "$_POST[sec5] '$_POST[old_sec5val]' to '$_POST[sec5val]', ";
} else {
    $comment151 = "";
}

if ($_POST['old_sec6val'] != $_POST['sec6val']) {
    $comment152 = "$_POST[sec6] '$_POST[old_sec6val]' to '$_POST[sec6val]', ";
} else {
    $comment152 = "";
}

if ($_POST['old_sec7val'] != $_POST['sec7val']) {
    $comment153 = "$_POST[sec7] '$_POST[old_sec7val]' to '$_POST[sec7val]', ";
} else {
    $comment153 = "";
}

if ($_POST['old_sec8val'] != $_POST['sec8val']) {
    $comment154 = "$_POST[sec8] '$_POST[old_sec8val]' to '$_POST[sec8val]', ";
} else {
    $comment154 = "";
}

if ($_POST['old_sec9val'] != $_POST['sec9val']) {
    $comment155 = "$_POST[sec9] '$_POST[old_sec9val]' to '$_POST[sec9val]', ";
} else {
    $comment155 = "";
}

if ($_POST['old_sec10val'] != $_POST['sec10val']) {
    $comment156 = "$_POST[sec10] '$_POST[old_sec10val]' to '$_POST[sec10val]', ";
} else {
    $comment156 = "";
}

if ($_POST['old_sec11val'] != $_POST['sec11val']) {
    $comment157 = "$_POST[sec11] '$_POST[old_sec11val]' to '$_POST[sec11val]', ";
} else {
    $comment157 = "";
}

if ($_POST['old_sec12val'] != $_POST['sec12val']) {
    $comment158 = "$_POST[sec12] '$_POST[old_sec12val]' to '$_POST[sec12val]', ";
} else {
    $comment158 = "";
}

if ($_POST['old_sec13val'] != $_POST['sec13val']) {
    $comment159 = "$_POST[sec13] '$_POST[old_sec13val]' to '$_POST[sec13val]' , ";
} else {
    $comment159 = "";
}

if ($_POST['old_sec14val'] != $_POST['sec14val']) {
    $comment160 = "$_POST[sec14] '$_POST[old_sec14val]' to '$_POST[sec14val]' , ";
} else {
    $comment160 = "";
}

if ($_POST['old_sec15val'] != $_POST['sec15val']) {
    $comment161 = "$_POST[sec15] '$_POST[old_sec15val]' to '$_POST[sec15val]' , ";
} else {
    $comment161 = "";
}

if ($_POST['old_bg1val'] != $_POST['bg1val']) {
    $comment162 = "$_POST[bg1] '$_POST[old_bg1val]' to '$_POST[bg1val]', ";
} else {
    $comment162 = "";
}

if ($_POST['old_bg2val'] != $_POST['bg2val']) {
    $comment163 = "$_POST[bg2] '$_POST[old_bg2val]' to '$_POST[bg2val]', ";
} else {
    $comment163 = "";
}

if ($_POST['old_bg3val'] != $_POST['bg3val']) {
    $comment164 = "$_POST[bg3] '$_POST[old_bg3val]' to '$_POST[bg3val]', ";
} else {
    $comment164 = "";
}

if ($_POST['old_bg4val'] != $_POST['bg4val']) {
    $comment165 = "$_POST[bg4] '$_POST[old_bg4val]' to '$_POST[bg4val]', ";
} else {
    $comment165 = "";
}

if ($_POST['old_bg5val'] != $_POST['bg5val']) {
    $comment166 = "$_POST[bg5] '$_POST[old_bg5val]' to '$_POST[bg5val]', ";
} else {
    $comment166 = "";
}

if ($_POST['old_bg6val'] != $_POST['bg6val']) {
    $comment167 = "$_POST[bg6] '$_POST[old_bg6val]' to '$_POST[bg6val]', ";
} else {
    $comment167 = "";
}

if ($_POST['old_bg7val'] != $_POST['bg7val']) {
    $comment168 = "$_POST[bg7] '$_POST[old_bg7val]' to '$_POST[bg7val]', ";
} else {
    $comment168 = "";
}

if ($_POST['old_bg8val'] != $_POST['bg8val']) {
    $comment169 = "$_POST[bg8] '$_POST[old_bg8val]' to '$_POST[bg8val]', ";
} else {
    $comment169 = "";
}

if ($_POST['old_bg9val'] != $_POST['bg9val']) {
    $comment170 = "$_POST[bg9] '$_POST[old_bg9val]' to '$_POST[bg9val]', ";
} else {
    $comment170 = "";
}

if ($_POST['old_bg10val'] != $_POST['bg10val']) {
    $comment171 = "$_POST[bg10] '$_POST[old_bg10val]' to '$_POST[bg10val]', ";
} else {
    $comment171 = "";
}

if ($_POST['old_bg11val'] != $_POST['bg11val']) {
    $comment172 = "$_POST[bg11] '$_POST[old_bg11val]' to '$_POST[bg11val]', ";
} else {
    $comment172 = "";
}

if ($_POST['old_bg12val'] != $_POST['bg12val']) {
    $comment173 = "$_POST[bg12] '$_POST[old_bg12val]' to '$_POST[bg12val]', ";
} else {
    $comment173 = "";
}

if ($_POST['old_bg13val'] != $_POST['bg13val']) {
    $comment174 = "$_POST[bg13] '$_POST[old_bg13val]' to '$_POST[bg13val]' , ";
} else {
    $comment174 = "";
}

if ($_POST['old_bg14val'] != $_POST['bg14val']) {
    $comment175 = "$_POST[bg14] '$_POST[old_bg14val]' to '$_POST[bg14val]', ";
} else {
    $comment175 = "";
}

if ($_POST['old_bg15val'] != $_POST['bg15val']) {
    $comment176 = "$_POST[bg15] '$_POST[old_bg15val]' to '$_POST[bg15val]', ";
} else {
    $comment176 = "";
}

if ($_POST['old_power1val'] != $_POST['power1val']) {
    $comment177 = "$_POST[power1] '$_POST[old_power1val]' to '$_POST[power1val]', ";
} else {
    $comment177 = "";
}

if ($_POST['old_power2val'] != $_POST['power2val']) {
    $comment178 = "$_POST[power2] '$_POST[old_power2val]' to '$_POST[power2val]', ";
} else {
    $comment178 = "";
}

if ($_POST['old_power3val'] != $_POST['power3val']) {
    $comment179 = "$_POST[power3] '$_POST[old_power3val]' to '$_POST[power3val]', ";
} else {
    $comment179 = "";
}

if ($_POST['old_power4val'] != $_POST['power4val']) {
    $comment180 = "$_POST[power4] '$_POST[old_power4val]' to '$_POST[power4val]', ";
} else {
    $comment180 = "";
}

if ($_POST['old_power5val'] != $_POST['power5val']) {
    $comment181 = "$_POST[power5] '$_POST[old_power5val]' to '$_POST[power5val]', ";
} else {
    $comment181 = "";
}

if ($_POST['old_power6val'] != $_POST['power6val']) {
    $comment182 = "$_POST[power6] '$_POST[old_power6val]' to '$_POST[power6val]', ";
} else {
    $comment182 = "";
}

if ($_POST['old_power7val'] != $_POST['power7val']) {
    $comment183 = "$_POST[power7] '$_POST[old_power7val]' to '$_POST[power7val]', ";
} else {
    $comment183 = "";
}

if ($_POST['old_power8val'] != $_POST['power8val']) {
    $comment184 = "$_POST[power8] '$_POST[old_power8val]' to '$_POST[power8val]', ";
} else {
    $comment184 = "";
}

if ($_POST['old_power9val'] != $_POST['power9val']) {
    $comment185 = "$_POST[power9] '$_POST[old_power9val]' to '$_POST[power9val]', ";
} else {
    $comment185 = "";
}

if ($_POST['old_power10val'] != $_POST['power10val']) {
    $comment186 = "$_POST[power10] '$_POST[old_power10val]' to '$_POST[power10val]', ";
} else {
    $comment186 = "";
}

if ($_POST['old_power11val'] != $_POST['power11val']) {
    $comment187 = "$_POST[power11] '$_POST[old_power11val]' to '$_POST[power11val]', ";
} else {
    $comment187 = "";
}

if ($_POST['old_power12val'] != $_POST['power12val']) {
    $comment188 = "$_POST[power12] '$_POST[old_power12val]' to '$_POST[power12val]', ";
} else {
    $comment188 = "";
}

if ($_POST['old_power13val'] != $_POST['power13val']) {
    $comment189 = "$_POST[power13] '$_POST[old_power13val]' to '$_POST[power13val]', ";
} else {
    $comment189 = "";
}

if ($_POST['old_power14val'] != $_POST['power14val']) {
    $comment190 = "$_POST[power14] '$_POST[old_power14val]' to '$_POST[power14val]', ";
} else {
    $comment190 = "";
}

if ($_POST['old_power15val'] != $_POST['power15val']) {
    $comment191 = "$_POST[power15] '$_POST[old_power15val]' to '$_POST[power15val]', ";
} else {
    $comment191 = "";
}

if ($_POST['old_power16val'] != $_POST['power16val']) {
    $comment192 = "$_POST[power16] '$_POST[old_power16val]' to '$_POST[power16val]', ";
} else {
    $comment192 = "";
}

if ($_POST['old_power17val'] != $_POST['power17val']) {
    $comment193 = "$_POST[power17] '$_POST[old_power17val]' to '$_POST[power17val]', ";
} else {
    $comment193 = "";
}

if ($_POST['old_power18val'] != $_POST['power18val']) {
    $comment194 = "$_POST[power18] '$_POST[old_power18val]' to '$_POST[power18val]', ";
} else {
    $comment194 = "";
}

if ($_POST['old_renown1perm'] != $_POST['renown1perm']) {
    $comment202 = "$_POST[renown1] perm from '$_POST[old_renown1perm]' to '$_POST[renown1perm]', ";
} else {
    $comment202 = "";
}

if (isset($_POST['old_renown1temp']) && isset($_POST['renown1temp']) && $_POST['old_renown1temp'] != $_POST['renown1temp']) {
    $comment203 = "$_POST[renown1] temp from '$_POST[old_renown1temp]' to '$_POST[renown1temp]', ";
} else {
    $comment203 = "";
}

if (isset($_POST['old_renown2perm']) && isset($_POST['renown2perm']) && $_POST['old_renown2perm'] != $_POST['renown2perm']) {
    $comment204 = "$_POST[renown2] perm from'$_POST[old_renown2perm]' to '$_POST[renown2perm]', ";
} else {
    $comment204 = "";
}

if (isset($_POST['old_renown2temp']) && isset($_POST['renown2temp']) && $_POST['old_renown2temp'] != $_POST['renown2temp']) {
    $comment205 = "$_POST[renown2] temp from '$_POST[old_renown2temp]' to '$_POST[renown2temp]', ";
} else {
    $comment205 = "";
}

if (isset($_POST['old_renown3perm']) && isset($_POST['renown3perm']) && $_POST['old_renown3perm'] != $_POST['renown3perm']) {
    $comment206 = "$_POST[renown3] perm from'$_POST[old_renown3perm]' to '$_POST[renown3perm]', ";
} else {
    $comment206 = "";
}

if (isset($_POST['old_renown3temp']) && isset($_POST['renown3temp']) && $_POST['old_renown3temp'] != $_POST['renown3temp']) {
    $comment207 = "$_POST[renown3] temp from '$_POST[old_renown3temp]' to '$_POST[renown3temp]', ";
} else {
    $comment207 = "";
}

if (isset($_POST['old_sfa']) && isset($_POST['sfa']) && $_POST['old_sfa'] != $_POST['sfa']) {
    $comment214 = "Ready for Approval '$_POST[old_sfa]' to '$_POST[sfa]', ";
} else {
    $comment214 = "";
}

if (isset($_POST['old_sanctioned']) && isset($_POST['sanctioned']) && $_POST['old_sanctioned'] != $_POST['sanctioned']) {
    $comment215 = "Sanctioned '$_POST[old_sanctioned]' to '$_POST[sanctioned]', ";
} else {
    $comment215 = "";
}

if (isset($_POST['old_char_type']) && isset($_POST['char_type']) && $_POST['old_char_type'] != $_POST['char_type']) {
    $comment216 = "Character Type: '$_POST[old_char_type]' to '$_POST[char_type]' ";
} else {
    $comment216 = "";
}

if (isset($_POST['old_retainer_of']) && isset($_POST['retainer_of']) && $_POST['old_retainer_of'] != $_POST['retainer_of']) {
    $comment218 = "Retainer of: '$_POST[old_retainer_of]' to '$_POST[retainer_of]' ";
} else {
    $comment218 = "";
}

if (isset($_POST['old_avatar']) && isset($_POST['avatar']) && $_POST['old_avatar'] != $_POST['avatar']) {
    $comment218 = "Avatar/Mana: '$_POST[old_avatar]' to '$_POST[avatar]' ";
} else {
    $comment219 = "";
}

if (isset($_POST['takexp']) && $_POST['takexp'] != '0') {
    $comment212 = "<span color='ff0000'>Spent '$_POST[takexp]' XP </span>";
} else {
    $comment212 = "";
}

if (isset($_POST['givexp']) && $_POST['givexp'] != '0') {
    $comment213 = "<span color='33ff00'>Added '$_POST[givexp]' XP </span>";
} else {
    $comment213 = "";
}
