<?php
if($_COOKIE['logname']=='') {
    header("location: http://shadowsofthebayou.com/login.php");
    exit();
}
?>
<?php require "../site-inc/gamengdb.php"; ?>
<title></title>
<link rel=stylesheet href="../layout.css" TYPE="text/css">
<div id="pagewrapper">
  <title>Create A Bygone</title>
  <table width="500" align="center">
    <tr>
      <td>
        <form name="Create Bygone" action="create_character.php" method="POST">
          <input name="char_venue" type="hidden" value="Mage">
          <input name="sub_venue" type="hidden" value="Bygone">
          <input name="char_type" type="hidden" value="PC">
          <table width="100%" align="center">
            <tr>
              <td>
                <div align="center" class="pagetopic">Bygone Character Sheet</div>
              </td>
            </tr>
            <tr>
              <td>
                <?php
                include 'creedit/master-top-create.php';
                ?>
              </td>
            </tr>
            <tr>
              <td>
                <div class="center"><img src="sheet_images/bygbar.gif"></div>
              </td>
            </tr>
            <tr>
              <td>
                <?php
                require 'creedit/wod-bygone-top.php';
                ?>
              </td>
            </tr>
            <tr>
              <td>
                <div class="center"><img src="sheet_images/bygbar.gif"></div>
              </td>
            </tr>
            <tr>
              <td>
                <?php
                require 'creedit/master-attribute.php';
                ?>
              </td>
            </tr>
            <tr>
              <td>
                <div class="center"><img src="sheet_images/bygbar.gif"></div>
              </td>
            </tr>
            <tr>
              <td>
                <?php
                require 'creedit/wod-bygone-abil.php';
                ?>
              </td>
            </tr>
            <tr>
              <td>
                <?php
                require 'creedit/master-seconds.php';
                ?>
              </td>
            </tr>
            <tr>
              <td>
                <div class="center"><img src="sheet_images/bygbar.gif"></div>
              </td>
            </tr>
            <tr>
              <td>
                <?php
                require 'creedit/master-language-n.php';
                ?>
              </td>
            </tr>
            <tr>
              <td>
                <div class="center"><img src="sheet_images/bygbar.gif"></div>
              </td>
            </tr>
            <tr>
              <td>
                <?php
                require 'creedit/background-byg.php';
                ?>
              </td>
            </tr>
            <tr>
              <td>
                <div class="center"><img src="sheet_images/bygbar.gif"></div>
              </td>
            </tr>
            <tr>
              <td>
                <?php
                require 'creedit/wod-bygone-traits-n.php';
                ?>
              </td>
            </tr>
            <tr>
              <td>
                <div class="center"><img src="sheet_images/bygbar.gif"></div>
              </td>
            </tr>
            <tr>
              <td>
                <?php
                require 'creedit/master-botdata-p-n.php';
                ?>
              </td>
            </tr>
            <tr>
              <td>
                <?php
                require 'footer.php';
                ?>
              </td>
            </tr>
          </table>
        </form>
</div>