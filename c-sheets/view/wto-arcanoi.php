<div align="center" class="item"><hr width="80%" /></div>
<table width="100%">
  <tr>
    <td width="13%"><div class="item">Passions</div></td>
    <td width="90%" colspan="10"><div class="item"><?php echo $row[passion]?></div></td>
  </tr>
</table>
<div align="center" class="item"><hr width="80%" /></div>
<table width="100%">
  <tr>
    <td width="13%"><div class="item">Fetters</div></td>
    <td width="90%" colspan="10"><div class="item"><?php echo $row[fetter]?></div></td>
  </tr>
</table>
<div align="center" class="item"><hr width="80%" /></div>
<table width="100%" border="0" >
  <tr>
    <td colspan="8"><div class="center">
        <div class="pageitem">Arcanoi</div></td>
  </tr>
  <tr>
    <td width="21%"><div class='item'><?php echo $row['power1']?></div></td>
    <td width="11%"><div class='item'><?php echo $row['power1val']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><?php echo $row['power2']?></div></td>
    <td width="11%"><div class='item'><?php echo $row['power2val']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><?php echo $row['power3']?></div></td>
    <td width="11%"><div class='item'><?php echo $row['power3val']?></div></td>
  </tr>
  <tr>
    <td><div class='item'><?php echo $row['power4']?></div></td>
    <td><div class='item'><?php echo $row['power4val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power5']?></div></td>
    <td><div class='item'><?php echo $row['power5val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power6']?></div></td>
    <td><div class='item'><?php echo $row['power6val']?></div></td>
  </tr>
  <tr>
    <td><div class='item'><?php echo $row['power7']?></div></td>
    <td><div class='item'><?php echo $row['power7val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power8']?></div></td>
    <td><div class='item'><?php echo $row['power8val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power9']?></div></td>
    <td><div class='item'><?php echo $row['power9val']?></div></td>
  </tr>
  <tr>
    <td><div class='item'><?php echo $row['power10']?></div></td>
    <td><div class='item'><?php echo $row['power10val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power11']?></div></td>
    <td><div class='item'><?php echo $row['power11val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power12']?></div></td>
    <td><div class='item'><?php echo $row['power12val']?></div></td>
  </tr>
  <tr>
    <td><div class='item'><?php echo $row['power13']?></div></td>
    <td><div class='item'><?php echo $row['power13val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power14']?></div></td>
    <td><div class='item'><?php echo $row['power14val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power15']?></div></td>
    <td><div class='item'><?php echo $row['power15val']?></div></td>
  </tr>
</table>
<div class="center"><hr width="80%" /></div>
<table width="100%" border="0" >
  <tr> 
    <td width="13%"><div class="item">Corpus</div></td>
    <td width="10%"><div align="center" class="itemsm">Perm</div></td>
    <td width="10%"><div class='item'><?php echo $row[corpusperm]?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="13%"><div class="item">Pathos</div></td>
    <td width="10%"><div align="center" class="itemsm"></div></td>
    <td width="10%"><div class='item'><?php echo $row[pathos]?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="13%"><div class="item">Willpower</div></td>
    <td width="10%"><div align="center" class="itemsm">Perm</div></td>
    <td width="10%"><div class='item'><?php echo $row['wpperm']?></div></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td><div align="center" class="itemsm">Temp</div></td>
    <td><div class='item'><?php echo $row[corpustemp]?></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center" class="itemsm">Temp</div></td>
    <td><div class='item'><?php echo $row['wptemp']?></div></td>
  </tr>
</table>
<div align="center" class="item"><hr width="80%" /></div>
<table width="100%">
  <tr>
    <td width="24%"><div class="item">Other Venue Details:</div></td>
    <td width="76%" colspan="10"><div class="item"><?php echo $row[wraith_venue]?></div></td>
  </tr>
</table>
