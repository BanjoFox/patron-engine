<table width="100%" border="0" >
    <tr>
            <td width="21%"><div class='item'>Merits</div></td>
            <td width="79%"><div class="item"><?php echo $row['merits']?></div></td>
      </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
      <tr>
            <td><div class='item'>Flaws</div></td>
            <td><div class="item"><?php echo $row['flaws']?></div></td>
      </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
      <tr>
            <td><div class='item'>Creation Notes</div></td>
            <td><div class="item"><?php echo $row['cre_notes']?></div></td>
      </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
      <tr>
            <td><div class='item'>History</div></td>
            <td><div class='item'><?php echo $row['player_notes']?></div></td>
      </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
      <tr>
            <td><div class='item'>Gear Carried</div></td>
            <td><div class="item"><?php echo $row['equipment']?></div></td>
      </tr>
    <tr>
    <td colspan="2"><hr align="center" width="75%"></td>
  </tr>
      <tr>
            <td><div class='item'>Possessions</div></td>
            <td><div class="item"><?php echo $row['othereq']?></div></td>
      </tr>
      <tr>
            <td colspan="2"><hr></td>
      </tr>
</table>

<table style="width:100%">
    <tr>
        <td width="21%"><div class='item'>Sanctioned?</td>
        <td width="24%"><div class="item"><?php echo $row['sanctioned']?></div></td>
        <td width="10%">&nbsp;</td>
        <td width="21%"><div class='item'>Character Type</div></td>
        <td width="24%"><div class="item"><?php echo $row['char_type']?></div></td>
    </tr>
</table>


<table style="width:100%"> 
    <tr>
        <td colspan="2"><hr></td>
    </tr>
   <tr>
        <td><div class='item'>Previous ST Notes</div></td>
        <td><div class='item'><?php echo $row['st_notes']?></div></td>
    </tr>
    <tr>
        <td colspan="2"><hr></td>
    </tr>
</table>