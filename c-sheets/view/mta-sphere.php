<table width="100%" border="0" >
  <tr>
    <td colspan="8"><div class="pageitem">Spheres</div></td>
  </tr>
  <tr>
    <td width="25%"><div class='item'>Correspondence</div></td>
    <td width="8%"><div class='item'><?php echo $row['correspondence']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="25%"><div class='item'>Life</div></td>
    <td width="8%"><div class='item'><?php echo $row['life']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="25%"><div class='item'>Prime</div></td>
    <td width="8%"><div class='item'><?php echo $row['prime']?></div></td>
  </tr>
  <tr>
    <td><div class='item'>Entropy</div></td>
    <td><div class='item'><?php echo $row['entropy']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Mind</div></td>
    <td><div class='item'><?php echo $row['mind']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Spirit</div></td>
    <td><div class='item'><?php echo $row['spirit']?></div></td>
  </tr>
  <tr>
    <td><div class='item'>Forces</div></td>
    <td><div class='item'><?php echo $row['forces']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Matter</div></td>
    <td><div class='item'><?php echo $row['matter']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Time</div></td>
    <td><div class='item'><?php echo $row['time']?></div></td>
  </tr>
     <tr>
        <td colspan="8"><div align='center'><hr width='75%'></div></td>
  </tr> 
  <tr>
    <td width="32%"><div class="item">Rotes:</div></td>
    <td width="68%" colspan="7"><div class="item"><?php echo $row['ritualsk']?></div></td>
  </tr>
  </table>
  <table width="100%">
  <tr>
    <td colspan="8"><div align='center'><hr /></div></td>
  </tr>
  <tr>
    <td width="24%"><div class="item">Willpower</div></td>
    <td width="5%"><div class="itemsm">Perm</div></td>
    <td width="5%"><div class="item"><?php echo $row['wpperm']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="25%"><div class="item">Arete</div></td>
    <td width="6%"><div class="item"><?php echo $row['arete']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="25%"><div class="item">Quintessence</div></td>
    <td width="6%"><div class="item"><?php echo $row['quinttemp']?></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div class="itemsm">Temp</div></td>
    <td><div class="item"><?php echo $row['wptemp']?></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td width="33%"><div class="item">Paradox</div></td>
    <td><div class="item"><?php echo $row['paradox']?></div></td>
  </tr>
  </table>
<table width="100%">
  <tr>
    <td colspan="2"><div align='center'><hr width='75%'></div></td>
  </tr> 
  <tr>
    <td width="32%"><div class="item">Other Details:</div></td>
    <td width="68%"><div class="item"><?php echo $row['mage_venue']?></div></td>
  </tr>
</table>