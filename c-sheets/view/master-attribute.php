<table width="100%" border="0" >
  <tr>
    <td colspan="8"><div class="pagetopic">Attributes</div></td>
  </tr>
  <tr> 
    <td width="21%"><div class='item'>Strength</div></td>
    <td width="11%"><div class="item"><?php echo $row['strength']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'>Charisma</div></td>
    <td width="11%"><div class="item"><?php echo $row['charisma']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'>Perception</div></td>
    <td width="11%"><div class="item"><?php echo $row['perception']?></div></td>
  </tr>
  <tr> 
    <td><div class='item'>Dexterity</div></td>
    <td><div class="item"><?php echo $row['dexterity']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Manipulation</div></td>
    <td><div class="item"><?php echo $row['manipulation']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Intelligence</div></td>
    <td><div class="item"><?php echo $row['intelligence']?></div></td>
  </tr>
  <tr> 
    <td><div class='item'>Stamina</div></td>
    <td><div class="item"><?php echo $row['stamina']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Appearance</div></td>
    <td><div class="item"><?php echo $row['appearance']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Wits</div></td>
    <td><div class="item"><?php echo $row['wits']?></div></td>
  </tr>
  <tr> 
    <td><div class='item'>Specialties</div></td>
    <td colspan="8"><div class='item'><?php echo $row['attspecial']?></td>
  </tr>
</table>
