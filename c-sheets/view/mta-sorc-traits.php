<table width="100%" border="0">
  <tr>
    <td colspan="8"><div class="pagetopic">Numina</div></td>
  </tr>
  <tr>
    <td width="21%"><div class='item'><?php echo $row['power1']?></div></td>
    <td width="11%"><div class='item'><?php echo $row['power1val']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><?php echo $row['power2']?></div></td>
    <td width="11%"><div class='item'><?php echo $row['power2val']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><?php echo $row['power3']?></div></td>
    <td width="11%"><div class='item'><?php echo $row['power3val']?></div></td>
  </tr>
  <tr>
    <td><div class='item'><?php echo $row['power4']?></div></td>
    <td><div class='item'><?php echo $row['power4val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power5']?></div></td>
    <td><div class='item'><?php echo $row['power5val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power6']?></div></td>
    <td><div class='item'><?php echo $row['power6val']?></div></td>
  </tr>
  <tr>
    <td><div class='item'><?php echo $row['power7']?></div></td>
    <td><div class='item'><?php echo $row['power7val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power8']?></div></td>
    <td><div class='item'><?php echo $row['power8val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power9']?></div></td>
    <td><div class='item'><?php echo $row['power9val']?></div></td>
  </tr>
  <tr>
    <td><div class='item'><?php echo $row['power10']?></div></td>
    <td><div class='item'><?php echo $row['power10val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power11']?></div></td>
    <td><div class='item'><?php echo $row['power11val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power12']?></div></td>
    <td><div class='item'><?php echo $row['power12val']?></div></td>
  </tr>
  <tr>
    <td><div class='item'><?php echo $row['power13']?></div></td>
    <td><div class='item'><?php echo $row['power13val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power14']?></div></td>
    <td><div class='item'><?php echo $row['power14val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power15']?></div></td>
    <td><div class='item'><?php echo $row['power15val']?></div></td>
  </tr>
</table>

<table width="100%" border="0">
  <tr>
      <td colspan="2" align='center'><hr /></td>
  </tr>
  <tr>
      <td width="13%"><div class="item">Rituals</div></td>
          <td width="87%"><div class="item"><?php echo $row[ritualsk]?></div></td>
  </tr>
    <tr>
      <td colspan="2"><div align='center'><hr /></div></td>
  </tr>
</table>
<table width="100%" border="0">
  <tr>
    <td width="13%"><div class="item">Willpower</div></td>
    <td width="10%"><div align="center" class="itemsm">Perm</div></td>
    <td width="10%"><div class="item"><?php echo $row['wpperm']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="13%">&nbsp;</td>
    <td width="8%">&nbsp;</td>
    <td width="8%">&nbsp;</td>
    <td width="2%">&nbsp;</td>
    <td width="13%"><div class="item">Quintessence</div></td>
    <td width="10%"><div class="itemsm">Temp</div></td>
    <td width="10%"><div class="item"><?php echo $row[quinttemp]?></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="center" class="itemsm">Temp</div></td>
    <td><div class="item"><?php echo $row['wptemp']?></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
 <table width="100%">
  <tr>
        <td colspan="2"><div align='center'><hr width='75%'></div></td>
  </tr> 
  <tr>
    <td width="32%"><div class="item">Other Details:</div></td>
    <td width="68%" colspan="7"><div class="item"><?php echo $row[mage_venue]?></div></td>
  </tr>
</table>