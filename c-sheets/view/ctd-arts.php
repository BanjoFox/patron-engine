<table width="100%" border="0" >
  <tr>
    <td colspan="8"><div class="pagetopic">Arts</div></td>
  </tr>
  <tr>
    <td width="21%"><div class='item'><?php echo $row['power1']?></div></td>
    <td width="11%"><div class='item'><?php echo $row['power1val']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><?php echo $row['power2']?></div></td>
    <td width="11%"><div class='item'><?php echo $row['power2val']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'><?php echo $row['power3']?></div></td>
    <td width="11%"><div class='item'><?php echo $row['power3val']?></div></td>
  </tr>
  <tr>
    <td><div class='item'><?php echo $row['power4']?></div></td>
    <td><div class='item'><?php echo $row['power4val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power5']?></div></td>
    <td><div class='item'><?php echo $row['power5val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power6']?></div></td>
    <td><div class='item'><?php echo $row['power6val']?></div></td>
  </tr>
  <tr>
    <td><div class='item'><?php echo $row['power7']?></div></td>
    <td><div class='item'><?php echo $row['power7val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power8']?></div></td>
    <td><div class='item'><?php echo $row['power8val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power9']?></div></td>
    <td><div class='item'><?php echo $row['power9val']?></div></td>
  </tr>
  <tr>
    <td><div class='item'><?php echo $row['power10']?></div></td>
    <td><div class='item'><?php echo $row['power10val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power11']?></div></td>
    <td><div class='item'><?php echo $row['power11val']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'><?php echo $row['power12']?></div></td>
    <td><div class='item'><?php echo $row['power12val']?></div></td>
  </tr>
</table>
<div class="center"><hr width="75%"></div>
<table width="100%" border="0">
  <tr>
    <td colspan="8"><div class="pagetopic">Realms</div></td>
  </tr>
  <tr>
    <td width="21%"><div class='item'>Actor</div></td>
    <td width="11%"><div class='item'><?php echo $row['actor_c']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'>Fae</div></td>
    <td width="11%"><div class='item'><?php echo $row['fae_c']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="21%"><div class='item'>Prop</div></td>
    <td width="11%"><div class='item'><?php echo $row['prop_c']?></div></td>
  </tr>
  <tr>
    <td><div class='item'>Nature</div></td>
    <td><div class='item'><?php echo $row['nature_c']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Scene</div></td>
    <td><div class='item'><?php echo $row['scene_c']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Time</div></td>
    <td><div class='item'><?php echo $row['time_c']?></div></td>
  </tr>
</table>
<div class="center"><hr width="90%"></div>
<table border="0" width="100%">
  <tr> 
    <td width="13%"><div class="item">Glamour</div></td>
    <td width="10%"><div align="center" class="itemsm">Perm</div></td>
    <td width="10%"><div class="item"><?php echo $row['glamourperm']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="13%"><div class="item">Banality</div></td>
    <td width="10%"><div align="center" class="itemsm">Perm</div></td>
    <td width="10%"><div class="item"><?php echo $row['banalityperm']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="13%"><div class="item">Willpower</div></td>
    <td width="10%"><div align="center" class="itemsm">Perm</div></td>
    <td width="10%"><div class="item"><?php echo $row['wpperm']?></div></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td><div align="center" class="itemsm">Temp</div></td>
    <td><div class="item"><?php echo $row['glamourtemp']?></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center" class="itemsm">Temp</div></td>
    <td><div class="item"><?php echo $row['banalitytemp']?></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>

    <td><div align="center" class="itemsm">Temp</div></td>
    <td><div class="item"><?php echo $row['wptemp']?></div></td>
  </tr>
</table>
<div class="center"><hr width="75%"></div>

<table width="100%">
  <tr>
    <td width="24%"><div class="item">Other Venue Details:</div></td>
    <td width="76%" colspan="10"><div class="item"><?php echo $row['fae_venue']?></div></td>
  </tr>
</table>