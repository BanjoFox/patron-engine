<table width="100%">
  <tr>
    <td width="24%"><div class='item'>Generation</div></td>
    <td width="25%"><div class='item'><?php echo $row[generation]?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="24%"><div class='item'>Sect</div></td>
    <td width="25%"><div class='item'><?php echo $row[sect]?></div></td>
  </tr>
  <tr>
    <td><div class='item'>Clan</div></td>
    <td><div class='item'><?php echo $row[clan]?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Sire</div></td>
    <td><div class='item'><?php echo $row[sire]?></div></td>
  </tr>
  <tr>
    <td><div class='item'>Bloodline</div></td>
    <td><div class='item'><?php echo $row[bloodline]?></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div class='item'>Nature</div></td>
    <td><div class="item"><?php echo $row['nature']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Demeanor</div></td>
    <td><div class="item"><?php echo $row['demeanor']?></div></td>
  </tr>
</table>
