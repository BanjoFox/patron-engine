<table width="100%">
  <tr>
    <td width="24%"><div class="pageitem">Name</div></td>
    <td width="76%"><div class="pageitem"><?php echo $row['log_name']?></div></td>
  </tr>
  <tr>
    <td><div class="item">Player</div></td>
    <td><div class="item"><?php echo $row['playername']?></div></td>
  </tr>
  <tr>
    <td colspan="2"><hr /></td>
  </tr>
</table>

<table style="width:100%">
  <tr>
     <td width="24%"><div class='item'>Create Date</div></td>
     <td width="25%"><div class='item'><?php echo $row['create_date']?></div></td>
     <td width="2%">&nbsp;</td>
     <td width="24%"><div class='item'>Sanctioned?</div></td>
     <td width="25%"><div class='item'><?php echo $row['sanctioned']?></div></td>
  </tr>
  <tr>
     <td><div class='item'>Last Login</div></td>
     <td><div class='item'><?php echo $row['last_login']?></div></td>
     <td>&nbsp;</td>
     <td><div class='item'>Earned XP</div></td>
     <td><div class='item'><?php echo $row['xptotal']?></div></td>
  </tr>
  <tr>
     <td><div class='item'>Last Modified By</div></td>
     <td><div class='item'><?php echo $row['last_modified']?></div></td>
     <td>&nbsp;</td>
     <td><div class='item'>Spent XP</div></td>
     <td><div class='item'><?php echo $row['xpspent']?></div></td>
  </tr>
  <tr>
     <td><div class='item'>Last Modified </div></td>
     <td><div class='item'><?php echo $row['date_modified']?></div></td>
     <td>&nbsp;</td>
     <td><div class='item'>Available XP</div></td>
     <td><div class='item'><?php echo $row['xptotal'] - $row['xpspent']?></div></td>
  </tr>
</table>
<table width="100%">
  <tr>
    <td colspan="2"><hr /></td>
  </tr>
</table>


<table width="100%">
      <tr>
        <td width="24%"><div class='item'>Gender</div></td>
        <td width="25%"><div class='item'><?php echo $row['gender']?></div></td>
        <td width="2%">&nbsp;</td>
        <td width="24%"><div class='item'>Nationality</div></td>
        <td width="25%"><div class='item'><?php echo $row['nationality']?></div></td>
      </tr>
      <tr>
        <td><div class='item'>Age</div></td>
        <td><div class='item'><?php echo $row['age']?></div></td>
        <td>&nbsp;</td>
        <td><div class='item'></div></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><div class='item'>Apparent Age</div></td>
        <td><div class='item'><?php echo $row['apparentage']?></div></td>
        <td>&nbsp;</td>
        <td><div class="item">Concept</div></td>
        <td><div class='item'><?php echo $row['concept']?></div></td>
      </tr>
</table>