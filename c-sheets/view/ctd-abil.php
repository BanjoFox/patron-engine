<table width="100%" border="0" >
  <tr>
    <td colspan="8"><div class="pagetopic">Abilities</div></td>
  </tr>
  <tr>
    <td width="130"><div class="item">Alertness</div></td>
    <td width="50"><div class="item"><?php echo $row['alertness']?></td>
    <td width="5">&nbsp;</td>
    <td width="130"><div class="item">Animal Ken</div></td>
    <td width="50"><div class="item"><?php echo $row['animalken']?></div></td>
    <td width="5">&nbsp;</td>
    <td width="130"><div class="item">Academics</div></td>
    <td width="50"><div class="item"><?php echo $row['academics']?></div></td>
  </tr>
  <tr>
    <td><div class="item">Athletics</div></td>
    <td><div class="item"><?php echo $row['athletics']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Crafts</div></td>
    <td><div class="item"><?php echo $row['crafts']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Computer</div></td>
    <td><div class="item"><?php echo $row['computer']?></div></td>
  </tr>
  <tr>
    <td><div class="item">Brawl</div></td>
    <td><div class="item"><?php echo $row['brawl']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Drive</div></td>
    <td><div class="item"><?php echo $row['drive']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Enigmas</div></td>
    <td><div class="item"><?php echo $row['enigmas']?></div></td>
  </tr>
  <tr>
    <td><div class="item">Empathy</div></td>
    <td><div class="item"><?php echo $row['empathy']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Etiquette</div></td>
    <td><div class="item"><?php echo $row['etiquette']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Gremayre</div></td>
    <td><div class="item"><?php echo $row['gremayre']?></div></td>
  </tr>
  <tr>
    <td><div class="item">Expression</div></td>
    <td><div class="item"><?php echo $row['expression']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Firearms</div></td>
    <td><div class="item"><?php echo $row['firearms']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Investigation</div></td>
    <td><div class="item"><?php echo $row['investigation']?></div></td>
  </tr>
  <tr>
    <td><div class="item">Intimidation</div></td>
    <td><div class="item"><?php echo $row['intimidation']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Larceny</div></td>
    <td><div class="item"><?php echo $row['larceny']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Law</div></td>
    <td><div class="item"><?php echo $row['law']?></div></td>
  </tr>
  <tr>
    <td><div class="item">Kenning</div></td>
    <td><div class="item"><?php echo $row['kenning']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Melee</div></td>
    <td><div class="item"><?php echo $row['melee']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Medicine</div></td>
    <td><div class="item"><?php echo $row['medicine']?></div></td>
  </tr>
  <tr>
    <td><div class="item">Leadership</div></td>
    <td><div class="item"><?php echo $row['leadership']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Performance</div></td>
    <td><div class="item"><?php echo $row['performance']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Politics</div></td>
    <td><div class="item"><?php echo $row['politics']?></div></td>
  </tr>
  <tr>
    <td><div class="item">Streetwise</div></td>
    <td><div class="item"><?php echo $row['streetwise']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Stealth</div></td>
    <td><div class="item"><?php echo $row['stealth']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Science</div></td>
    <td><div class="item"><?php echo $row['science']?></div></td>
  </tr>
  <tr>
    <td><div class="item">Subterfuge</div></td>
    <td><div class="item"><?php echo $row['subterfuge']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Survival</div></td>
    <td><div class="item"><?php echo $row['survival']?></div></td>
    <td>&nbsp;</td>
    <td><div class="item">Technology</div></td>
    <td><div class="item"><?php echo $row['technology']?></div></td>
  </tr>
</table>