<table width="100%">
  <tr>
    <td width="24%"><div class='item'>Affiliation</div></td>
    <td width="25%"><div class='item'><?php echo $row['affiliation']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="24%"><div class='item'>Essence</div></td>
    <td width="25%"><div class='item'><?php echo $row['essence']?></div></td>
  </tr>
  <tr>
    <td width="24%"><div class='item'>Tradition</div></td>
    <td width="25%"><div class='item'><?php echo $row['tradition']?></div></td>
    <td width="2%">&nbsp;</td>
    <td width="24%"><div class='item'>Nature</div></td>
    <td width="25%"><div class='item'><?php echo $row['nature']?></div></td>
  </tr>
  <tr>
    <td><div class='item'>Primary Path</div></td>
    <td><div class='item'><?php echo $row['spec_sphere']?></div></td>
    <td>&nbsp;</td>
    <td><div class='item'>Demeanor</div></td>
    <td><div class='item'><?php echo $row['demeanor']?></div></td>
  </tr>
</table>
