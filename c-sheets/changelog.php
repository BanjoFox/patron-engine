<?php

// New way of doing things
$change_notes = [];
$skills = [
    'password', 'viewpw', 'gender', 'age', 'apparentage', 'nationality', 'concept', 'strength', 
    'dexterity', 'stamina', 'charisma', 'manipulation', 'appearance', 'perception', 'intelligence', 
    'wits', 'attspecial', 'academics', 'alertness', 'animalken', 'athletics', 'arts', 'awareness', 
    'brawl', 'bureaucracy', 'computer', 'cosmology', 'crafts', 'drive', 'empathy', 'enigmas', 'esoterica', 
    'etiquette', 'expression', 'finance', 'firearms', 'gremayre', 'intimidation', 'investigation', 'kenning', 
    'larceny', 'law', 'leadership', 'lore', 'martialarts', 'medicine', 'meditation', 'melee', 'occult', 
    'performance', 'politics', 'primalurge', 'research', 'rituals', 'science', 'stealth', 'streetwise', 
    'subterfuge', 'survival', 'technology', 'abil_special', 'abilspecial', 'alt_name', 'nature', 'demeanor', 
    'languages', 'wpperm', 'wptemp', 'virtue1', 'virtue1val', 'virtue2', 'virtue2val', 'courage', 'bloodmax', 
    'bloodcurrent', 'bloodturn', 'gnosisperm', 'gnosistemp', 'rage_blood', 'rageperm', 'ragetemp', 'glamourperm', 
    'glamourtemp', 'banalityperm', 'banalitytemp', 'corpusperm', 'corpustemp', 'arete', 'quinttemp', 'paradox', 
    'pathos', 'guild', 'legion', 'life_w', 'death', 'regret', 'faction', 'coorespondence', 'entropy', 'forces', 
    'life', 'matter',
];

foreach ($skills as $skill) {
    $old_value = $_POST["old_{$skill}"] ?? null;
    $new_value = $_POST[$skill] ?? null;

    if ($old_value !== $new_value) {
        $change_notes[] = "{$skill} '{$old_value}' to '{$new_value}'";
    }
}
// End of new way of doing things
$field_mapping = [
    'old_mind' => 'mind',    'old_prime' => 'prime',
    'old_spirit' => 'spirit',
    'old_time' => 'time',
    'old_passion' => 'passion',
    'old_fetter' => 'fetter',
    'old_gift1' => 'gift1',
    'old_gift2' => 'gift2',
    'old_gift3' => 'gift3',
    'old_gift4' => 'gift4',
    'old_gift5' => 'gift5',
    'old_actor_c' => 'actor_c',
    'old_fae_c' => 'fae_c',
    'old_nature_c' => 'nature_c',
    'old_prop_c' => 'prop_c',
    'old_scene_c' => 'scene_c',
    'old_time_c' => 'time_c',
    'old_path' => 'path',
    'old_pathval' => 'pathval',
    'old_pathbearing' => 'pathbearing',
    'old_employer' => 'employer',
    'old_clan' => 'clan',
    'old_bloodline' => 'bloodline',
    'old_sire' => 'sire',
    'old_sect' => 'sect',
    'old_breed' => 'breed',
    'old_tribe' => 'tribe',
    'old_auspice' => 'auspice',
    'old_packname' => 'packname',
    'old_packtotem' => 'packtotem',
    'old_camp' => 'camp',
    'old_tradition' => 'tradition',
    'old_spec_sphere' => 'spec_sphere',
    'old_essence' => 'essence',
    'old_affiliation' => 'affiliation',
    'old_kith' => 'kith',
    'old_seeming' => 'seeming',
    'old_court' => 'court',
    'old_house' => 'house',
    'old_bygone_species' => 'bygone_species',
];

$comments = [];
foreach ($field_mapping as $old_field => $field) {
    if (isset($_POST[$old_field], $_POST[$field]) && $_POST[$old_field] != $_POST[$field]) {
        $comments[] = "{$field} '{$_POST[$old_field]}' to '{$_POST[$field]}', ";
    }
}
$comments = [];
$fields = [
    'bygone_element', 'generation', 'rank', 'merits', 'flaws', 
    'sec1', 'sec2', 'sec3', 'sec4', 'sec5', 'sec6', 'sec7', 'sec8', 'sec9', 'sec10', 'sec11', 'sec12', 'sec13', 'sec14', 'sec15', 
    'bg1', 'bg2', 'bg3', 'bg4', 'bg5', 'bg6', 'bg7', 'bg8', 'bg9', 'bg10', 'bg11', 'bg12', 'bg13', 'bg14', 'bg15', 'power1', 
    'power2', 'power3', 'power4', 'power5', 'power6', 'power7', 'power8', 'power9', 'power10', 'power11', 'power12', 'power13', 
    'power14', 'power15', 'power16', 'power17', 'power18', 
    'renown1'
];

foreach ($fields as $field) {
    $oldValue = $_POST["old_{$field}"];
    $newValue = $_POST["{$field}val"];

    if ($oldValue !== $newValue) {
        $comments[] = "{$field} '{$oldValue}' to '{$newValue}', ";
    }
}

$oldRenown1Temp = $_POST['old_renown1temp'] ?? null;
$renown1Temp = $_POST['renown1temp'] ?? null;
$renown1Comment = $oldRenown1Temp !== $renown1Temp ? "$_POST[renown1] temp from '$oldRenown1Temp' to '$renown1Temp', " : "";

$oldRenown2Perm = $_POST['old_renown2perm'] ?? null;
$renown2Perm = $_POST['renown2perm'] ?? null;
$renown2Comment = $oldRenown2Perm !== $renown2Perm ? "$_POST[renown2] perm from '$oldRenown2Perm' to '$renown2Perm', " : "";

$oldRenown2Temp = $_POST['old_renown2temp'] ?? null;
$renown2Temp = $_POST['renown2temp'] ?? null;
$renown2TempComment = $oldRenown2Temp !== $renown2Temp ? "$_POST[renown2] temp from '$oldRenown2Temp' to '$renown2Temp', " : "";

$oldRenown3Perm = $_POST['old_renown3perm'] ?? null;
$renown3Perm = $_POST['renown3perm'] ?? null;
$renown3Comment = $oldRenown3Perm !== $renown3Perm ? "$_POST[renown3] perm from '$oldRenown3Perm' to '$renown3Perm', " : "";

$oldRenown3Temp = $_POST['old_renown3temp'] ?? null;
$renown3Temp = $_POST['renown3temp'] ?? null;
$renown3TempComment = $oldRenown3Temp !== $renown3Temp ? "$_POST[renown3] temp from '$oldRenown3Temp' to '$renown3Temp', " : "";

$oldSfa = $_POST['old_sfa'] ?? null;
$sfa = $_POST['sfa'] ?? null;
$sfaComment = $oldSfa !== $sfa ? "Ready for Approval '$oldSfa' to '$sfa', " : "";

$oldSanctioned = $_POST['old_sanctioned'] ?? null;
$sanctioned = $_POST['sanctioned'] ?? null;
$sanctionedComment = $oldSanctioned !== $sanctioned ? "Sanctioned '$oldSanctioned' to '$sanctioned', " : "";

$oldCharType = $_POST['old_char_type'] ?? null;
$charType = $_POST['char_type'] ?? null;
$charTypeComment = $oldCharType !== $charType ? "Character Type: '$oldCharType' to '$charType' " : "";

$oldRetainerOf = $_POST['old_retainer_of'] ?? null;
$retainerOf = $_POST['retainer_of'] ?? null;
$retainerOfComment = $oldRetainerOf !== $retainerOf ? "Retainer of: '$oldRetainerOf' to '$retainerOf' " : "";

$oldAvatar = $_POST['old_avatar'] ?? null;
$avatar = $_POST['avatar'] ?? null;
$avatarComment = $oldAvatar !== $avatar ? "Avatar/Mana: '$oldAvatar' to '$avatar' " : "";

$takeXp = (int) $_POST['takexp'];
$takeXpComment = $takeXp !== 0 ? "<span color='ff0000'>Spent '$takeXp' XP </span>" : "";

$giveXp = (int) $_POST['givexp'];
$giveXpComment = $giveXp !== 0 ? "<span color='33ff00'>Added '$giveXp' XP </span>" : "";


