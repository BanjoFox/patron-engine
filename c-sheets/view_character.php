<?php require "/var/www/shadowsofthebayou.com/site-inc/header.php"; ?>
<?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
<div id="pagewrapper">
    <?php
    $query = "SELECT * FROM `game_data` WHERE (id='$_POST[id]')";
    $result = mysqli_query($connection, $query)
        or die("<div  class='error'>Couldn't get character data.</div>");
    $row = mysqli_fetch_array($result);
    ?>
    <table width="60%" border='0' cellspacing='0'>
        <tr>
            <td>
                <?php
                if ($row['sub_venue'] == 'Vampire') {
                    include "view_vampire.php";
                } elseif ($row['sub_venue'] == 'Ghoul') {
                    include "view_ghoul.php";
                } elseif ($row['sub_venue'] == 'Werewolf') {
                    include "view_werewolf.php";
                } elseif ($row['sub_venue'] == 'Fera') {
                    include "view_fera.php";
                } elseif ($row['sub_venue'] == 'Kinfolk') {
                    include "view_kinfolk.php";
                } elseif ($row['sub_venue'] == 'Mage') {
                    include "view_mage.php";
                } elseif ($row['sub_venue'] == 'Sorcerer') {
                    include "view_sorcerer.php";
                } elseif ($row['sub_venue'] == 'Changeling') {
                    include "view_changeling.php";
                } elseif ($row['sub_venue'] == 'Kinian') {
                    include "view_kinian.php";
                } elseif ($row['sub_venue'] == 'Hsien') {
                    include "view_hsien.php";
                } elseif ($row['sub_venue'] == 'Wraith') {
                    include "view_wraith.php";
                } elseif ($row['sub_venue'] == 'Medium') {
                    include "view_medium.php";
                } elseif ($row['sub_venue'] == 'Hunter') {
                    include "view_hunter.php";
                } elseif ($row['sub_venue'] == 'Pre-Imbued') {
                    include "view_preimb.php";
                } elseif ($row['sub_venue'] == 'Demon') {
                    include "view_demon.php";
                } elseif ($row['sub_venue'] == 'Thrall') {
                    include "view_thrall.php";
                } elseif ($row['sub_venue'] == 'Mummy') {
                    include "view_mummy.php";
                } elseif ($row['sub_venue'] == 'Gypsy') {
                    include "view_gypsy.php";
                } elseif ($row['sub_venue'] == 'Possessed') {
                    include "view_possessed.php";
                } elseif ($row['sub_venue'] == 'Orpheus') {
                    include "view_orpheus.php";
                } elseif ($row['sub_venue'] == 'KotE') {
                    include "view_kote.php";
                } elseif ($row['sub_venue'] == 'Bygone') {
                    include "view_bygone.php";
                } elseif ($row['sub_venue'] == 'Mortal') {
                    include "view_mortal.php";
                }
                ?>
            </td>
        </tr>
    </table>
</div>