<?php include '/var/www/shadowsofthebayou.com/site-inc/header.php'; ?>

<div id="wrapper">
    <div id="title"></div><!-- title close -->

    <div class="container">

        <div class="column-nav">
            <?php require '/var/www/shadowsofthebayou.com/site-inc/check-access.php'; ?>

        </div><!-- menu close -->

        <div class="column-main">
            <?php
            if ($_COOKIE['privilege'] >= "3") {
                echo "You Do not have rights to do this";
            } else {
                ?>
                <?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
                <?php
                $query = "SELECT log_name FROM `game_data` WHERE (account_type='Character' AND deleted<>'Yes') ORDER BY log_name";
                $result = mysqli_query($connection, $query)
                    or die("<div  class='error'>Couldn't not execute query.<br \>" . mysqli_error($connection) . "<br \></div>");
                $option = "<select name=\"log_name\" class='form'><option value=\"\" class='form'></option>";
                while ($row = mysqli_fetch_array($result)) {
                    $option = "$option <option value=\"$row[log_name]\">$row[log_name]</option>";
                }
                $option = "$option </select>";
                ?>

                <form name="form1" method="post" action="<?php echo $PHP_SELF ?>">
                    <table style="width:90%" border="0" align="center">
                        <caption>
                            <div align="center" class='pagetopic'>Select Character To Update</div>
                        </caption>
                        <tr>
                            <td>
                                <div class="center">
                                    <?php echo $option ?> <input name="Select" type="submit" id="Select2"
                                        value="Select Character" class="form">
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>


                <?php
                if ($_POST['Select'] == "Select Character" && $_POST['log_name'] <> "") {
                    ?>
                    <?php
                    $query = "SELECT * FROM `game_data` WHERE (log_name=\"$_POST[log_name]\")";
                    $results = mysqli_query($connection, $query)

                        or die("<div  class='error'>Couldn't execute query..<br \>" . mysqli_error($connection) . "<br \></div>");
                    $data = @mysqli_fetch_array($results);
                    ?>
                    <?php {
                        $query = "SELECT * FROM `game_data` WHERE (log_name=\"$data[log_name]\")";
                        $results = mysqli_query($connection, $query)

                            or die("<div  class='error'>Couldn't execute query..<br \>" . mysqli_error($connection) . "<br \></div>");
                        $data = @mysqli_fetch_array($results);
                        ?>
                        <table width="100%" border="0" align="center">
                            <tr>
                                <td>
                                    <?php
                                        $char_name = $data['log_name'];
                        $now = date('m/d/y');
                        $oldstnotes = $data['stnotes'];
                        ?>

                                    <form name="form2" method="post" action="<?php echo $PHP_SELF ?>">
                                        <input name="oldstnotes" type="hidden" id="oldnotes"
                                            value="<?php echo $data['st_notes'] ?>">
                                        <input name="now" type="hidden" id="now" value="<?php echo date('m/d/y') ?>">
                                        <input name="oldtotalxp" type="hidden" id="oldtotalxp"
                                            value="<?php echo $data['xptotal'] ?>">
                                        <input name="oldspentxp" type="hidden" id="oldspentxp"
                                            value="<?php echo $data['xpspent'] ?>">
                                        <input name="char_name" type="hidden" id="char_name"
                                            value="<?php echo $data['log_name'] ?>">

                                        <?php
                            if ($data['sub_venue'] == 'Vampire') {
                                include "edit_vampire.php";
                            } elseif ($data['sub_venue'] == 'Ghoul') {
                                include "edit_ghoul.php";
                            } elseif ($data['sub_venue'] == 'Werewolf') {
                                include "edit_werewolf.php";
                            } elseif ($data['sub_venue'] == 'Fera') {
                                include "edit_fera.php";
                            } elseif ($data['sub_venue'] == 'Kinfolk') {
                                include "edit_kinfolk.php";
                            } elseif ($data['sub_venue'] == 'Mage') {
                                include "edit_mage.php";
                            } elseif ($data['sub_venue'] == 'Sorcerer') {
                                include "edit_sorcerer.php";
                            } elseif ($data['sub_venue'] == 'Changeling') {
                                include "edit_changeling.php";
                            } elseif ($data['sub_venue'] == 'Kinian') {
                                include "edit_kinian.php";
                            } elseif ($data['sub_venue'] == 'Wraith') {
                                include "edit_wraith.php";
                            } elseif ($data['sub_venue'] == 'Bygone') {
                                include "edit_bygone.php";
                            } elseif ($data['sub_venue'] == 'Mortal') {
                                include "edit_mortal.php";
                            }
                    }
                    ?>
                                </form>
                                </p>
                <?php } elseif ($_POST['Select2'] == "Update") {
                    $description2 = mysqli_real_escape_string($connection, $_POST['description']);
                    $query = "UPDATE game_data SET password=\"$_POST[password]\",alt_name=\"$_POST[alt_name]\",age=\"$_POST[age]\",apparentage=\"$_POST[apparentage]\",concept=\"$_POST[concept]\",nature=\"$_POST[nature]\",demeanor=\"$_POST[demeanor]\",nationality=\"$_POST[nationality]\",gender=\"$_POST[gender]\",viewpw=\"$_POST[viewpw]\",strength=\"$_POST[strength]\",dexterity=\"$_POST[dexterity]\",stamina=\"$_POST[stamina]\",charisma=\"$_POST[charisma]\",manipulation=\"$_POST[manipulation]\",appearance=\"$_POST[appearance]\",perception=\"$_POST[perception]\",intelligence=\"$_POST[intelligence]\",wits=\"$_POST[wits]\",attspecial=\"$_POST[attspecial]\",academics=\"$_POST[academics]\",alertness=\"$_POST[alertness]\",animalken=\"$_POST[animalken]\",athletics=\"$_POST[athletics]\",arts=\"$_POST[arts]\",awareness=\"$_POST[awareness]\",brawl=\"$_POST[brawl]\",bureaucracy=\"$_POST[bureaucracy]\",computer=\"$_POST[computer]\",cosmology=\"$_POST[cosmology]\",crafts=\"$_POST[crafts]\",drive=\"$_POST[drive]\",empathy=\"$_POST[empathy]\",enigmas=\"$_POST[enigmas]\",esoterica=\"$_POST[esoterica]\",etiquette=\"$_POST[etiquette]\",expression=\"$_POST[expression]\",finance=\"$_POST[finance]\",firearms=\"$_POST[firearms]\",gremayre=\"$_POST[gremayre]\",intimidation=\"$_POST[intimidation]\",investigation=\"$_POST[investigation]\",kenning=\"$_POST[kenning]\",larceny=\"$_POST[larceny]\",law=\"$_POST[law]\",leadership=\"$_POST[leadership]\",lore=\"$_POST[lore]\",martialarts=\"$_POST[martialarts]\",medicine=\"$_POST[medicine]\",
                                meditation=\"$_POST[meditation]\",melee=\"$_POST[melee]\",occult=\"$_POST[occult]\",performance=\"$_POST[performance]\",politics=\"$_POST[politics]\",primalurge=\"$_POST[primalurge]\",research=\"$_POST[research]\",rituals=\"$_POST[rituals]\",science=\"$_POST[science]\",stealth=\"$_POST[stealth]\",streetwise=\"$_POST[streetwise]\",subterfuge=\"$_POST[subterfuge]\",survival=\"$_POST[survival]\",technology=\"$_POST[technology]\",sec1=\"$_POST[sec1]\",sec1val=\"$_POST[sec1val]\",sec2=\"$_POST[sec2]\",sec2val=\"$_POST[sec2val]\",sec3=\"$_POST[sec3]\",sec3val=\"$_POST[sec3val]\",sec4=\"$_POST[sec4]\",sec4val=\"$_POST[sec4val]\",sec5=\"$_POST[sec5]\",sec5val=\"$_POST[sec5val]\",sec6=\"$_POST[sec6]\",sec6val=\"$_POST[sec6val]\",sec7=\"$_POST[sec7]\",sec7val=\"$_POST[sec7val]\",sec8=\"$_POST[sec8]\",sec8val=\"$_POST[sec8val]\",sec9=\"$_POST[sec9]\",sec9val=\"$_POST[sec9val]\",sec10=\"$_POST[sec10]\",sec10val=\"$_POST[sec10val]\",sec11=\"$_POST[sec11]\",sec11val=\"$_POST[sec11val]\",sec12=\"$_POST[sec12]\",sec12val=\"$_POST[sec12val]\",sec13=\"$_POST[sec13]\",sec13val=\"$_POST[sec13val]\",sec14=\"$_POST[sec14]\",sec14val=\"$_POST[sec14val]\",sec15=\"$_POST[sec15]\",sec15val=\"$_POST[sec15val]\",abilspecial=\"$_POST[abilspecial]\",bg1=\"$_POST[bg1]\",bg1val=\"$_POST[bg1val]\",bg2=\"$_POST[bg2]\",bg2val=\"$_POST[bg2val]\",bg3=\"$_POST[bg3]\",bg3val=\"$_POST[bg3val]\",bg4=\"$_POST[bg4]\",bg4val=\"$_POST[bg4val]\",bg5=\"$_POST[bg5]\",bg5val=\"$_POST[bg5val]\",bg6=\"$_POST[bg6]\",bg6val=\"$_POST[bg6val]\",bg7=\"$_POST[bg7]\",bg7val=\"$_POST[bg7val]\",bg8=\"$_POST[bg8]\",bg8val=\"$_POST[bg8val]\",bg9=\"$_POST[bg9]\",bg9val=\"$_POST[bg9val]\",bg10=\"$_POST[bg10]\",bg10val=\"$_POST[bg10val]\",bg11=\"$_POST[bg11]\",bg11val=\"$_POST[bg11val]\",bg12=\"$_POST[bg12]\",bg12val=\"$_POST[bg12val]\",bg13=\"$_POST[bg13]\",bg13val=\"$_POST[bg13val]\",bg14=\"$_POST[bg14]\",bg14val=\"$_POST[bg14val]\",bg15=\"$_POST[bg15]\",bg15val=\"$_POST[bg15val]\",avatar=\"$_POST[avatar]\",languages=\"$_POST[languages]\",wpperm=\"$_POST[wpperm]\",wptemp=\"$_POST[wptemp]\",virtue1=\"$_POST[virtue1]\",virtue1val=\"$_POST[virtue1val]\",virtue2=\"$_POST[virtue2]\",virtue2val=\"$_POST[virtue2val]\",courage=\"$_POST[courage]\",bloodmax=\"$_POST[bloodmax]\",bloodcurrent=\"$_POST[bloodcurrent]\",bloodturn=\"$_POST[bloodturn]\",gnosisperm=\"$_POST[gnosisperm]\",gnosistemp=\"$_POST[gnosistemp]\",rage_blood=\"$_POST[rage_blood]\",rageperm=\"$_POST[rageperm]\",ragetemp=\"$_POST[ragetemp]\",glamourperm=\"$_POST[glamourperm]\",glamourtemp=\"$_POST[glamourtemp]\",banalityperm=\"$_POST[banalityperm]\",banalitytemp=\"$_POST[banalitytemp]\",corpusperm=\"$_POST[corpusperm]\",corpustemp=\"$_POST[corpustemp]\",arete=\"$_POST[arete]\",quintperm=\"$_POST[avatar]\",quinttemp=\"$_POST[quinttemp]\",paradox=\"$_POST[paradox]\",pathos=\"$_POST[pathos]\",guild=\"$_POST[guild]\",legion=\"$_POST[legion]\",life_w=\"$_POST[life_w]\",death=\"$_POST[death]\",regret=\"$_POST[regret]\",faction=\"$_POST[faction]\",renown1=\"$_POST[renown1]\",renown1perm=\"$_POST[renown1perm]\",renown1temp=\"$_POST[renown1temp]\",renown2=\"$_POST[renown2]\",renown2perm=\"$_POST[renown2perm]\",renown2temp=\"$_POST[renown2temp]\",renown3=\"$_POST[renown3]\",renown3perm=\"$_POST[renown3perm]\",renown3temp=\"$_POST[renown3temp]\",power1=\"$_POST[power1]\",power1val=\"$_POST[power1val]\",power2=\"$_POST[power2]\",power2val=\"$_POST[power2val]\",power3=\"$_POST[power3]\",power3val=\"$_POST[power3val]\",power4=\"$_POST[power4]\",power4val=\"$_POST[power4val]\",power5=\"$_POST[power5]\",power5val=\"$_POST[power5val]\",power6=\"$_POST[power6]\",power6val=\"$_POST[power6val]\",power7=\"$_POST[power7]\",power7val=\"$_POST[power7val]\",power8=\"$_POST[power8]\",power8val=\"$_POST[power8val]\",power9=\"$_POST[power9]\",power9val=\"$_POST[power9val]\",power10=\"$_POST[power10]\",power10val=\"$_POST[power10val]\",power11=\"$_POST[power11]\",power11val=\"$_POST[power11val]\",power12=\"$_POST[power12]\",power12val=\"$_POST[power12val]\",power13=\"$_POST[power13]\",power13val=\"$_POST[power13val]\",power14=\"$_POST[power14]\",power14val=\"$_POST[power14val]\",power15=\"$_POST[power15]\",power15val=\"$_POST[power15val]\",power16=\"$_POST[power16]\",power16val=\"$_POST[power16val]\",power17=\"$_POST[power17]\",power17val=\"$_POST[power17val]\",power18=\"$_POST[power18]\",power18val=\"$_POST[power18val]\",coorespondence=\"$_POST[coorespondence]\",entropy=\"$_POST[entropy]\",forces=\"$_POST[forces]\",life=\"$_POST[life]\",matter=\"$_POST[matter]\",mind=\"$_POST[mind]\",prime=\"$_POST[prime]\",spirit=\"$_POST[spirit]\",time=\"$_POST[time]\",passion=\"$_POST[passion]\",fetter=\"$_POST[fetter]\",gift1=\"$_POST[gift1]\",gift2=\"$_POST[gift2]\",gift3=\"$_POST[gift3]\",gift4=\"$_POST[gift4]\",gift5=\"$_POST[gift5]\",actor_c=\"$_POST[actor_c]\",fae_c=\"$_POST[fae_c]\",nature_c=\"$_POST[nature_c]\",prop_c=\"$_POST[prop_c]\",scene_c=\"$_POST[scene_c]\",time_c=\"$_POST[time_c]\",path=\"$_POST[path]\",pathval=\"$_POST[pathval]\",pathbearing=\"$_POST[pathbearing]\",ritualsk=\"$_POST[ritualsk]\",employer=\"$_POST[employer]\",clan=\"$_POST[clan]\",bloodline=\"$_POST[bloodline]\",sire=\"$_POST[sire]\",sect=\"$_POST[sect]\",breed=\"$_POST[breed]\",tribe=\"$_POST[tribe]\",auspice=\"$_POST[auspice]\",packname=\"$_POST[packname]\",packtotem=\"$_POST[packtotem]\",camp=\"$_POST[camp]\",tradition=\"$_POST[tradition]\",spec_sphere=\"$_POST[spec_sphere]\",essence=\"$_POST[essence]\",affiliation=\"$_POST[affiliation]\",kith=\"$_POST[kith]\",seeming=\"$_POST[seeming]\",court=\"$_POST[court]\",house=\"$_POST[house]\",bygone_species=\"$_POST[bygone_species]\",bygone_element=\"$_POST[bygone_element]\",vampire_venue=\"$_POST[vampire_venue]\",shifter_venue=\"$_POST[shifter_venue]\",mage_venue=\"$_POST[mage_venue]\",fae_venue=\"$_POST[fae_venue]\",wraith_venue=\"$_POST[wraith_venue]\",bygone_venue=\"$_POST[bygone_venue]\",mortal_venue=\"$_POST[mortal_venue]\",generation=\"$_POST[generation]\",rank=\"$_POST[rank]\",merits=\"$_POST[merits]\",flaws=\"$_POST[flaws]\",retainer_of=\"$_POST[retainer_of]\",cre_notes=\"$_POST[cre_notes]\",equipment=\"$_POST[equipment]\",othereq=\"$_POST[othereq]\",sfa=\"$_POST[sfa]\",char_type=\"$_POST[char_type]\",player_notes=\"$_POST[player_notes]\",sanctioned=\"$_POST[sanctioned]\",xptotal=($_POST[oldtotalxp]+\"$_POST[givexp]\"),xpspent=($_POST[oldspentxp]+\"$_POST[takexp]\"),xpavail=(($_POST[oldtotalxp]+\"$_POST[givexp]\")-($_POST[oldspentxp]+\"$_POST[takexp]\")) WHERE (log_name=\"$_POST[char_name]\")";
                    $result = mysqli_query($connection, $query)
                        or die("Could not execute query.<br \>" . mysqli_error($connection) . "<br \>");
                    echo "<div  class='success'>The character '$_POST[char_name]' has been updated.</div>";
                }
            }
?>
                    </td>
                </tr>
            </table>
        </div><!-- content close -->
    </div><!-- container close -->
</div><!-- wrapper close -->