# Patron Engine

The Patron Engine is a web-app for building a Classic World of Darkness Roleplaying website.

## Installation

Right now its just a copy/paste to webserver.
At some point I will need to export the SQL to build the DB (MariaDB)

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[AGPL](https://www.gnu.org/licenses/#AGPL)