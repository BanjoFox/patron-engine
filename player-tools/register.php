<?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>$_POST['log_name']
<link rel="stylesheet" type="text/css" href="../layout.css">
<div id="pagewrapper">
<?php
if ($_POST['submit'] == "Register") {
    /* Check for name */
    if ($_POST['log_name'] == "") {
        echo "<strong><div  class='error'>You must enter a user name.</strong></div><div ><a href='javascript:history.back()'><br \>Try Again</a></div>";
        $_POST = null;
        exit();
    }
    /* Check that username is at 4 characters long, change if you desire more or less, max for field is 32 */
    if (strlen(trim($_POST['log_name'])) < 4) {
        echo "<strong><div  class='error'>Username must be at least four characters.</div></strong><br \><div ><a href='javascript:history.back()'>Try Again</a></div>";
        $_POST = null;
        exit();
    }
    /* Check for duplicate name, this includes Player and Character names. */
    $query = "SELECT  log_name FROM game_data WHERE log_name='$_POST[log_name]'";
    $result = mysqli_query($connection, $query) or die("Unable to get member names.<br \>'.mysqli_error($connection).'<br \>");
    $num = mysqli_num_rows($result);
    if ($num > 0) {
        echo "<strong><div  class='error'>Name already in use please try another.</div></strong><br \><div ><a href='javascript:history.back()'>Try Again</a></div>";
        $_POST = null;
        exit();
    }
    /* Check for password */
    if ($_POST['fpassword'] == "") {
        echo "<strong><div  class='error'>You must enter a password.</div></strong><br \><div ><a href='javascript:history.back()'>Try Again</a></div>";
        $_POST = null;
        exit();
    }
    /* Check that password is at least 5 characters long, change if you desire more or less, max for field is 32  */
    if (strlen(trim($_POST['fpassword'])) < 5) {
        echo "<strong><div  class='error'>Password must be at least five characters.</div></strong><br \><div ><a href='javascript:history.back()'>Try Again</a></div>";
        $_POST = null;
        exit();
    }
    /*  Check that password and confirmed password match */
    if ($_POST['fpassword'] != $_POST['confirmpw']) {
        echo "<strong><div  class='error'>Password and retype password do not match.</div></strong><br \><div ><a href='javascript:history.back()'>Try Again</a></div>";
        $_POST = null;
        exit();
    }
    /* Check for email address */
    if ($_POST['email'] == "") {
        echo "<strong><div  class='error'>You must enter an e-mail address.</div></strong><br \><div ><a href='javascript:history.back()'>Try Again</a></div>";
        $_POST = null;
        exit();
    }

    /* Check for min age, set to 18 years old, if you desire younger change. */
    if ($_POST['user_age'] != "Yes") {
        echo "<strong><div  class='error'>You must be at least 18 to join.</div><br \><div ><a href='javascript:history.back()'>Try Again</a></div>";
        $_POST = null;
        exit();
    }
    /* Check for acceptance */
    if ($_POST['accept_tos'] != "Yes") {
        echo "<strong><div  class='error'>You must agree to the terms and conditions of use.</div></strong><br \><div ><a href='javascript:history.back()'>Try Again</a></div>";
        $_POST = null;
        exit();
    } else {
        $query = "INSERT INTO game_data (log_name,date_joined,password,email,show_email,account_type) VALUE (\"$_POST[log_name]\",NOW(),\"$_POST[fpassword]\",\"$_POST[email]\",\"$_POST[show_email]\",\"Player\")";
    }
    if ($query && $connection) {
        $result = mysqli_query($connection, $query)
           or die("<div  class='error'><strong>Couldn't add user. Error: " . mysqli_error($connection) . "</strong></div><br \>");
    } else {
        die("<div  class='error'><strong>Couldn't add user. Error: Connection or query is null</strong></div><br \>");
    }
    echo "<br \><center><div  class='success'>Thank you for registering $_POST[log_name],<br \>Please save your username and password for referance.</p></center>";

    /* Send Email to new member. Be sure to correct all the information or customize it to your liking. If you want to remove it delete this section down to but not including the }, if you with to temporaryly disable the sending place a # infront of $mailsend */
    $emess .= "We appreciate your interest in the Patron Game Engine.\n\n";
    $emess .= "Your new member account has been setup with the following information:\n";
    $emess .= "Username: $log_name \n";
    $emess .= "Password: $_POST[fpassword]\n\n";
    $emess .= "If you have any questions or problems,";
    $emess .= " email chicagobcg@gmail.com";
    $ehead = "From: chicagobcg@gmail.com\r\n";
    $subj = "Your new member account for Black City Games: Chicago The Second City Chronicles\r\n";
    $mailsend = mail("$email", "$subj", "$emess", "$ehead");
} else {
    ?>
<form action="" method="POST"> 
<table width="75%" border="0" align="center">
   <tr>
         <td colspan="3"><div align="center" class="pagetopic">New Member Registration</div></td>
   </tr>
   <tr>
      <td align="right" width="33%"><div class="pageitem">User Name</div></td>
      <td width="33%"><input type="text" name="log_name" id="log_name" size="32" maxlength="32" class="formreq"></td>
      <td width="34%"><div class="warning">(Min4/Max32 No Special Characters)</div></td>
   </tr>
   <tr> 
      <td align="right" ><div class="pageitem">Password</div></td>
      <td><input type="password" name="fpassword" id="fpassword" size="32" maxlength="32" class="formreq"></td>
      <td><div class="warning">(Min5/Max32 No Special Characters)</div></td>
   </tr>
   <tr>
       <td align="right"><div class="pageitem">Retype Password</div></td>
      <td><input type="password" name="confirmpw" id="confirmpw" size="32" maxlength="32" class="formreq"></td>
      <td>&nbsp;</td>
   </tr>
   <tr>
      <td align="right"><div class="pageitem">E-Mail</div></td>
      <td><input type="text" name="email" id="email" size="32" maxlength="128" class="formreq"></td>
      <td><div class="warnsm"></div></td>
   </tr>
   <tr>
      <td align="right"><div class="pageitem">Show E-Mail</div></td>
      <td><select name="show_email" id="show_email" class="form">
            <option value="Yes" selected>Yes</option>
            <option value="No">No</option>
            </select></td>
      <td><div class="warning">(Visible to non-staff?)</div></td>
   </tr>
   <tr>
      <td align="right"><div class="pageitem">At Least 18?</div></td>
      <td><select name="user_age" id="user_age" class="form">
             <option value="Yes">Yes</option>
             <option value="No" selected>No</option>
             </select></td></td>
   </tr>
   <tr>
      <td colspan="3"><div class="center"><hr width="75%"></div></td>
      </tr>
   <tr>
     <td align="right">&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
   </tr>
   <tr>
     <td colspan="3" align="justify"><div class="item">Shadows on the Bayou, is a moderated RPG chat offering venues from Classic World of Darkness with some elements and rules from the 20th Anniversary Editions of those games. <br \><br \>
This is not 'Free Form', again to make it clear it is 'Moderated' meaning there are rules and staff members in the form of Administrators and Storytellers to implement, adjudicate and enforce those rules. <br \>
<br \>
It is your responsibility familiarize yourself with the setting and house rules, we are not a straight out of a book game. We have spent many hours creating the setting based on occurrences from our role-play in the past. <br \>
<br \>
Do not assume you can just jump in with knowledge of the standard game setting and run, many assumptions are incorrect and some might be shockingly so. <br \>
<br \>
This is the game you are registering to play in, if this isn't your thing or your preferred style please don't waste your time or the time of others, there are other games around you might enjoy better.</div></td>
     </tr>
   <tr>
     <td align="right">&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
   </tr>
   <tr>
      <td colspan="3"><div class="center"><hr width="75%"></div></td>
   </tr>
   <tr>
      <td colspan="2"><div class="pagetopic">Do you accept and agree to follow the Code of Conduct?<a href="http://shadowsofthebayou.com/shadows/wiki/index.php?title=Code_of_Conduct" target="_blank"> (Click Here To Read)</a></div></td>
      <td><select name="accept_tos" id="accept_tos" class="formreq">
            <option value="Yes">Yes</option>
            <option value="No" selected>No</option>
            </select></td>
  </tr>
  <tr>
      <td colspan="3"><div class="center"><hr width="75%"></div></td>
  </tr>
  <tr>
      <td>&nbsp;</td>
      <td align="center"><input type="submit" name="submit" value="Register" class="form"></td>
      <td align="center">&nbsp;</td>
  </tr>
</table>
</form>

<?php } ?>
</div>
