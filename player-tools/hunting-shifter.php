<title></title>
<link rel=stylesheet href="../layout.css" TYPE="text/css">
<div id="pagewrapper">
  <?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
  <div align="center" class='pagetopic'>Gnosis Replenish</div><br \>
  <br \>
  <?php
    $query = "SELECT * FROM `game_data` WHERE (id='$_POST[id]')";
  $result = mysqli_query($connection, $query) or die("<div align='center' class='error'>Couldn't get character data.</div>");
  $data = mysqli_fetch_array($result);

  {
      ?>
    <?php
      if ($data['char_venue'] != "Werewolf") {
          echo "<div align='center' class='warning'>Character is not a werewolf, fera or kinfolk!</div><br \><br \>";
      } else {
          if ($data['gnosisperm'] == "0") {
              echo "<div align='center' class='warning'>Your kinfolk does not have the Gnosis Merit</div><br \><br \>";
          } else {
              ?>
            <?php
              $today = date("Y-m-d");
              if (($data['last_hunt'] == "$today")) {
                  echo "<div align='center' class='warning'>You already replenished today.</div><br \><br \>";
              } else {
                  ?>
          <div class="center">
                <?php
                  if ($_POST['Submit'] == "Replenish" && $_POST['style'] <> null && $data['last_hunt'] <> "$today" && $_POST['pool'] <> null && $_POST['target'] <> "0") {
                      // set and reset values
                      $result = "";
                      $successes = 0;
                      $botches = 0;
                      $target = 9;
                      if ($_POST['target'] == "") {
                          $target = 6;
                      } else {
                          $target = "$_POST[target]";
                      }
                      if ($_POST['target'] > 10) {
                          $_POST['target'] = 10;
                      }

                      // generate roll and make viewable
                      for ($i = 0; $i < $_POST['pool']; $i++) {
                          $roll = mt_rand(1, 10);
                          if ($roll >= $target) {
                              $successes++;
                          }
                          if ($roll == 1) {
                              $botches++;
                          }
                          $result = $result . ", " . $roll;
                      }


                      // results
                      $now = date('m/d/y H:i:s');
                      $result = substr($result, 2);
                      $sux = ($successes - $botches);
                      if (($successes <= 0) and ($botches > 0)) {
                          $net = "<dice-result class=\"botch\">BOTCH!</dice-result>";
                      } elseif (($successes >= 1) and ($botches >= $successes)) {
                          $net = "<dice-result class=\"failed\">Failed</dice-result>";
                      } elseif ($sux == "0") {
                          $net = "<dice-result class=\"failed\">Failed</dice-result>";
                      } else {
                          $net = "<span color=#99FF00>gained $sux gnosis.</span>";
                      }


                      $result = substr($result, 2);
                      $sux = ($successes - $botches);
                      if (($successes <= 0) and ($botches > 0)) {
                          $net = "<dice-result class=\"botch\">BOTCH!</dice-result>";
                      } elseif (($successes >= 1) and ($botches >= $successes)) {
                          $net = "<dice-result class=\"failed\">Failed</dice-result>";
                      } elseif ($sux == "0") {
                          $net = "<dice-result class=\"failed\">Failed</dice-result>";
                      }
                      if ($successes > "$_POST[howlong]") {
                          $gain = "$_POST[howlong]";
                      } else {
                          $gain = "$sux";
                      }



                      $display = "Regained Gnosis by $_POST[style] for $_POST[howlong] hours ($_POST[pool] dice at diff $_POST[target]) $net";
                      $newroll = 1;
                  }
                  // add results to database
                  if (($_POST['Submit'] == "Replenish") && $newroll == 1) {
                      $query = "INSERT INTO huntinglog (date,charactername,playername,results,venue) VALUES (NOW(),\"$data[log_name]\",\"$data[playername]\",\"$display\",\"$_POST[venue]\")";
                      $result = mysqli_query($connection, $query)
                      or die("<br \><div align='center' class='error'>Could not execute query.<br \>" . mysqli_error($connection) . "<br \></div>");
                  }

                  if ($gain > "0") {
                      $query = "UPDATE `game_data` SET gnosistemp=($_POST[oldgnosistotal]+$gain),last_hunt=NOW() WHERE (log_name=\"$data[log_name]\")";
                      $result = mysqli_query($connection, $query) or die("<div class='error'>Could not execute query for gnosis add.<br \>" . mysqli_error($connection) . "<br \></div>");
                      echo "";
                  } else {
                      echo "";
                  }

                  if (($_POST['oldgnosistotal'] + $gain) > "$data[gnosisperm]") {
                      $query = "UPDATE `game_data` SET gnosistemp=\"$data[gnosisperm]\" WHERE (log_name=\"$data[log_name]\")";
                      $result = mysqli_query($connection, $query) or die("<div class='error'>Could not execute query for gnosis add.<br \>" . mysqli_error($connection) . "<br \></div>");
                      echo "";
                  } else {
                      echo "";
                  }

                  if (($net == "<dice-result class=\"botch\">BOTCH!</dice-result>") && $_POST['style'] == "spirit negotiation") {
                      $query = "INSERT INTO char_reqs (char_name,reqtype,req_date,justi,req_stat,player_name,char_venue) VALUES (\"$_POST[char_name]\",\"Botched Gnosis\",NOW(),\"$display\",\"Reviewing\",\"$_COOKIE[logname]\",\"$data[sub_venue]\")";
                      $result = mysqli_query($connection, $query) or die("<div class='error'>Could not execute query for staff alert.<br \>" . mysqli_error($connection) . "<br \></div>");
                      echo "";
                  } else {
                      echo "";
                  }
                  ?>
            <form name="form1" method="post" action="">

              <input name="char_name" type="hidden" id="char_name" value="<?php echo $data['log_name'] ?>">
              <input name="id" type="hidden" id="id" value="<?php echo $data['id'] ?>">
              <input name="oldgnosistotal" type="hidden" id="oldgnosistotal" value="<?php echo $data['gnosistemp'] ?>">
              <input name="venue" type="hidden" id="venue" value="<?php echo $data['char_venue'] ?>">

              <table width="100%" border="0">
                <tr>
                  <td width="45%" valign="top">
                    <table width="80%" align="center">
                      <tr>
                        <td width="35%">
                          <div class="pageitem">Character</div>
                        </td>
                        <td width="65%">
                          <div class="pageitem"><?php echo $data['log_name'] ?></div>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          <div class="item">
                            <hr width="50%">
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="item">Current Gnosis</div>
                        </td>
                        <td>
                          <div class="item"><?php echo $data['gnosistemp'] ?></div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="item">Max Gnosis</div>
                        </td>
                        <td>
                          <div class="item"><?php echo $data['gnosisperm'] ?></div>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          <div class="item">
                            <hr width="50%">
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="item">Last Replenish Roll</div>
                        </td>
                        <td>
                          <div class="item"><?php echo $data['last_hunt'] ?></div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class='item'>Method</div>
                        </td>
                        <td>
                          <div class='item'><select name="style" size="1" id="style" class="form">
                              <option value=""></option>
                              <option value="meditating">Meditating (8)</option>
                              <option value="spirit negotiation">Spirit Negotiation (6)</option>
                            </select>
                          </div>
                        </td>
                      </tr>

                      <tr>
                        <td colspan="2">
                          <hr width="50%" />
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class='item'>Pool</div>
                        </td>
                        <td>
                          <div class='itemsm'><input type="text" name="pool" id="pool" class="form" size="1" maxlength="2"
                              value="0"> (Based on Method)</div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class='item'>Diffuculty</div>
                        </td>
                        <td>
                          <div class='itemsm'><select name="target" size="1" id="target" class="form">
                              <option value="8">8</option>
                              <option value="6">6</option>
                            </select> (Based on Method)</div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class='item'>How Long</div>
                        </td>
                        <td>
                          <div class='itemsm'><input type="text" name="howlong" id="howlong" class="form" size="1" maxlength="2"
                              value="0"> (In Hours)</div>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          <hr width="50%" />
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          <div class="center"><input type="submit" name="submit" value="Replenish" id="submit" class="form">
                          </div>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td width="55%" valign="top">
                    <table style="width:100%" border="0" align="center">
                      <tr>
                        <td>
                          <div class="pageitem" align="center">How to Use</div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="warning">You can only roll once a day.</div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="item">1. Select Method. This will tell you the pool to use.</div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="item">2. For Pool, add the two traits based on method then put that number here.</div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="item">3. For Difficulty, select according to method.</div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="item">4. For How Long, input the number of hours they will spend at the task. This is the
                            max number of gnosis you can regain no matter the roll.</div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="item">5. Click Replenish</div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <hr width="50%" align="center">
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="warning">If the result is a Botch and you were negotiating with spirits, it will notify
                            the staff in the form of a character request.</div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <hr width="50%" align="center">
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="item">Meditation = Wits (<?php echo $data['wits'] ?>) + Enigmas
                            (<?php echo $data['enigmas'] ?>)<br \> Spirit Negotiation = Charisma
                            (<?php echo $data['charisma'] ?>) +
                            Expression (<?php echo $data['expression'] ?>) <span color=#ffff00>Requires Spirit Speech</span>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </form>
          </div>
                <?php
              }
          }
      }
  }
  ?>
  <?php
  $query = "SELECT * FROM huntinglog WHERE venue='Werewolf' ORDER BY recordid DESC limit 50";
  $result = mysqli_query($connection, $query) or die("Couldn't execute query.");
  $nrows = mysqli_num_rows($result);

  // Display results table
  echo "<table width='700' align='center' cellspacing='0' cellpadding='0'>";
  echo "<tr><td colspan='5' align='center'><div class='pagetopic'>Results</div></td></tr>";
  echo "<tr><td colspan='5'><hr></td></tr>";
  for ($i = 0; $i < $nrows; $i++) {
      $n = $i + 1;
      $row = mysqli_fetch_array($result);
      extract($row);
      echo "<tr>\n
           <td width='8%'><div class='item'>$date</div></td>\n
           <td width='2%'><div class='item'></div></td>\n
           <td width='20%'><div class='item'>$charactername</div></td>\n
           <td width='2%'></td>\n
           <td width='68%'><div class='item'>$results</div></td>\n
           </tr>\n";
      echo "<tr><td colspan='5'><hr></td></tr>\n";
  }
  echo "</table>\n";
  ?>
</div>