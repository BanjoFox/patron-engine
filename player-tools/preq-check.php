<br \>
<table width='90%' border='1' cellspacing='1' align="center">
    <caption>
        <div class='pagetopic'>Your Open Character Requests</div>
    </caption>
    <tr>
        <td width='20%'>
            <div class='item'>Character</div>
        </td>
        <td width='15%'>
            <div class='item'>Venue</div>
        </td>
        <td width='19%'>
            <div class='item'>Type</div>
        </td>
        <td width='15%'>
            <div class='item'>Date Added</div>
        </td>
        <td width='9%'>
            <div class='item'>Status</div>
        </td>
        <td width='14%'>
            <div class='item'>Pend Until</div>
        </td>
        <td width='9%'>
            <div class='item'></div>
        </td>
    </tr>

    <?php
    $query = "SELECT  char_name,char_venue,reqtype,req_date,req_stat,player_name,pend_til,id FROM char_reqs WHERE ((req_stat='new' OR req_stat='reviewing' OR req_stat='followup' OR req_stat='pending') AND (player_name='$_COOKIE[logname]')) ORDER BY pend_til ASC, char_name";
    $rs = mysqli_query($connection, $query);
    //     echo "(search: $sql)";
    while ($row = mysqli_fetch_array($rs)) {
        if ($row['req_stat'] == "new" or $row['req_stat'] == "followup") {
            ?>
            <table style="width:90%" border="1" cellspacing="0" align="center">
                <?php
                echo("<tr><td width='20%'><div class='itemsm'>$row[char_name]</div></td>
		   <td width='15%'><div class='itemsm'>$row[char_venue]</div></td>
		   <td width='20%' ><div class='itemsm'>$row[reqtype]</div></td>
		   <td width='15%' ><div class='itemsm'>$row[req_date]</div></td>
		   <td width='9%' ><div class='itemsm'>$row[req_stat]</div></td>
		   <td width='15%' ><div class='itemsm'>$row[pend_til]</div></td>
                 <td width='9%'  valign='middle'><div class='item'><input type='hidden' name='id' value='$row[id]'>
<form id='form1' name='form1' method='post' action='../player-tools/pwork_req.php'>
<input type='hidden' name='id' value='$row[id]'>
<input type='submit' name='click' class='form' target='_blank' id='click' value='Work'></form></div></td>
	      </tr>
");
        } else {
            ?>
                <table style="width:90%" border="1" cellspacing="0" align="center">
                    <?php
                    echo("<tr><td width='20%'><div class='itemsm'>$row[char_name]</div></td>
		   <td width='15%'><div class='itemsm'>$row[char_venue]</div></td>
		   <td width='20%' ><div class='itemsm'>$row[reqtype]</div></td>
		   <td width='15%' ><div class='itemsm'>$row[req_date]</div></td>
		   <td width='9%' ><div class='itemsm'>$row[req_stat]</div></td>
		   <td width='15%' ><div class='itemsm'>$row[pend_til]</div></td>
                 <td width='9%'  valign='middle'><div class='item'><input type='hidden' name='id' value='$row[id]'>
<form id='form1' name='form1' method='post' target='_blank' action='../player-tools/pview_req.php'>
<input type='hidden' name='id' value='$row[id]'>
<input type='submit' name='click' class='form' target='_blank' id='click' value='View'></form></div>

</div></td></tr>");

        }
    }
    ?>
        </table>
        <br \>
        <table width='85%' cellspacing='1'>
            <caption>
                <div class='pageitem'>Status Meaning:</div>
            </caption>
            <tr>
                <td>
                    <div class="item">
                        <strong>Followup:</strong> Staff requesting info/clarification from player.<br \>
                        <strong>New:</strong> Staff has not yet reviewed request.<br \>
                        <strong>Pending:</strong> Awating time must elapse or other situation.<br \>
                        <strong>Reviewing:</strong> Staff is looking over request<br \>
                        <br \>
                        If you see a <strong>Work</strong> box it means your request is new, or additional information /
                        clarification was requested by staff.
                    </div>
                </td>
            </tr>
        </table>
        <br \>