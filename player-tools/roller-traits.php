<?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
<hr width="75%" align="center">
<?php
$query = "SELECT log_name,wptemp,wpperm,sub_venue,bloodmax,bloodcurrent,gnosisperm,gnosistemp,quinttemp,glamourperm,glamourtemp FROM game_data WHERE (account_type='Character' AND deleted<>'Yes' AND playername='$_COOKIE[logname]' AND sanctioned<>'Hold' AND sanctioned<>'No' AND char_type<>'RET') ORDER BY log_name ASC";
$rs = mysqli_query($connection, $query);

// Table header
echo "<table width='70%' border='1' cellspacing='0' align='center'>";
echo "<tr class='item'><th>Name</th><th>Sub-Venue</th><th>Perm WP</th><th>Temp WP</th><th>Trait 1</th><th>Trait 2</th></tr>";

// Use a while loop to iterate over each row
while ($row = mysqli_fetch_array($rs)) {
    echo "<tr><td><div class='itemsm'>" . $row['log_name'] . "</div></td>";
    echo "<td><div class='itemsm'>" . $row['sub_venue'] . "</div></td>";
    echo "<td><div class='itemsm'>Perm WP: " . $row['wpperm'] . "</div></td>";
    echo "<td><div class='itemsm'>Temp WP: " . $row['wptemp'] . "</div></td>";

    // Generate the last two rows dynamically based on sub_venue
    echo generateLastTwoRows($row['sub_venue'], $row);
}

echo "</table>";

// Function to generate last two rows based on sub_venue
function generateLastTwoRows($sub_venue, $row)
{
    switch ($sub_venue) {
        case 'Vampire':
            return "<td><div class='itemsm'>Max Blood: {$row['bloodmax']}</div></td>
                    <td><div class='itemsm'>Current Blood: {$row['bloodcurrent']}</div></td></tr>";
        case 'Ghoul':
            return "<td><div class='itemsm'>Vitae: {$row['bloodcurrent']}</div></td>
                    <td><div class='itemsm'></div></td></tr>";
        case 'Werewolf':
        case 'Fera':
        case 'Kinfolk':
            return "<td><div class='itemsm'>Perm Gnosis: {$row['gnosisperm']}</div></td>
                    <td><div class='itemsm'>Temp Gnosis: {$row['gnosistemp']}</div></td></tr>";
        case 'Mage':
        case 'Sorcerer':
            return "<td><div class='itemsm'>Quintessence: {$row['quinttemp']}</div></td>
                    <td><div class='itemsm'></div></td></tr>";
        case 'Changeling':
        case 'Kinain':
            return "<td><div class='itemsm'>Perm Glamour: {$row['glamourperm']}</div></td>
                    <td><div class='itemsm'>Temp Glamour: {$row['glamourtemp']}</div></td></tr>";
        case 'Bygone':
            return "<td><div class='itemsm'>Mana: {$row['quinttemp']}</div></td>
                    <td><div class='itemsm'></div></td></tr>";
        case 'Mortal':
            return "<td><div class='itemsm'>Quintessence: {$row['quinttemp']}</div></td>
                    <td><div class='itemsm'></div></td></tr>";
        default:
            return "<td>Default Data 1</td>
                    <td>Default Data 2</td></tr>";
    }
}
?>
<hr width="75%" align="center">
