<?php include '/var/www/shadowsofthebayou.com/site-inc/header.php'; ?>
<?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>

<div id="pagewrapper">
  <?php
  $query = "SELECT log_name FROM `game_data` WHERE (account_type='Character' AND deleted<>'Yes' AND playername='$_COOKIE[logname]' AND sanctioned<>'Hold') ORDER BY log_name";
  $result = mysqli_query($connection, $query)
    or die("Couldn't not execute query.<br \>" . mysqli_error($connection) . "<br \>");
  $option = "<select name=\"name\" class='form'><option value=\"\" class='form'></option>";
  while ($row = mysqli_fetch_array($result)) {
    $option = "$option <option value=\"$row[log_name]\" class='form'>$row[log_name]</option>";
  }
  $option = "$option </select>";

  if ($_POST['submit'] == "Roll Dice" && ($_POST['name'] <> null or $_POST['name2'] <> null) && $_POST['action'] <> null && $_POST['pool'] <> null) {
    // set and reset values
    $result = "";
    $target = "10";
    $successes = 0;
    $botches = 0;
    if ($_POST['target'] == "") {
      $target = 6;
    } else {
      $target = "$_POST[target]";
    }
    if ($_POST['pool'] > 20) {
      $_POST['pool'] = 20;
    }
    if ($_POST['target'] > 10) {
      $_POST['target'] = 10;
    }
    // generate roll and make viewable
    for ($i = 0; $i < $_POST['pool']; $i++) {
      $roll = mt_rand(1, 10);
      if ($roll >= $target) {
        $successes++;
      }
      if ($roll == 1) {
        $botches++;
      }
      if ($roll == 10 and $_POST['rr10'] == "Y") {
        $successes++;
        $note = "(Specialty)";
      }
      $result = $result . ", " . $roll;
    }
    // results
    if (isset($_POST['spendwp']) && $_POST['spendwp'] == "Y") {
      $query2 = "SELECT * FROM `game_data` WHERE (log_name='$_POST[name]')";
      $result2 = mysqli_query($connection, $query2) or die("Could not get character data for use XP.<br \>" . mysqli_error($connection) . "<br \>");
      $data = mysqli_fetch_array($result2);
      if ($data['wptemp'] != "0") {
        $character_name = "$data[log_name]";
        $currentwp = "$data[wptemp]";
        $minuswp = 1;
        $newwp = $currentwp - $minuswp;
        $successes++;
        $swp = ",<span class=willpower>W</span>";
        $query3 = "UPDATE game_data SET wptemp=($data[wptemp]-$minuswp) WHERE (log_name='$character_name')";
        $result3 = mysqli_query($connection, $query3) or die("Couldn't update user wp in roll.<br \>" . mysqli_error($connection) . "<br \>");
      } else {
      }
    }
    $now = date('m/d/y H:i:s');
    $name = "<strong>" . $name . "</strong>";
    $name2 = "<strong>" . $name2 . "</strong>";
    $result = substr($result, 2);
    $sux = ($successes - $botches);
    if (($successes <= 0) and ($botches > 0)) {
      $net = "<span class='botch'>BOTCH!</span>";
    } elseif (($successes >= 1) and ($botches >= $successes)) {
      $net = "<span class='failure'>Failed</span>";
    } elseif ($sux == "0") {
      $net = "<span class='failure'>Failed</span>";
    } else {
      $net = "<span class='success'>$sux Successes</span>";
    }
    $display = "<div class='item'>$now $_POST[name] $_POST[name2] rolls $_POST[pool] dice vs. diff of $target to $_POST[action]: $result $swp: $net $note</div>";
    $newroll = 1;
  } elseif ($_POST['submit2'] == "Roll Inits" && ($_POST['name'] <> null or $_POST['name2'] <> null)) {
    // set and reset values
    $result = "";
    // generate roll and make viewable
    $roll = mt_rand(1, 10);
    $now = date('m/d/y H:i:s');
    $name = "<strong>" . $_POST['name'] . "</strong>";
    $name2 = "<strong>" . $_POST['name2'] . "</strong>";
    $display = "<div class='item'>$now $name $name2 rolls initative: $roll</div>";
    $newroll = 1;
    // fate die
  } elseif ($_POST['submit8'] == "Roll Fate" && ($_POST['name'] <> null or $_POST['name2'] <> null)) {
    // set and reset values
    $result = "";
    // generate roll and make viewable
    $roll = mt_rand(1, 10);
    $now = date('m/d/y H:i:s');
    $name = "<strong>" . $_POST['name'] . "</strong>";
    $name2 = "<strong>" . $_POST['name2'] . "</strong>";
    $display = "<div class='item'>$now $name $name2 rolls for Fate: $roll</div>";
    $newroll = 1;

    // spend wp
  } elseif ($_POST['submit3'] == "Spend WP" && ($_POST['name'] <> null) && $_POST['action'] <> null) {
    $query3 = "SELECT * FROM `game_data` WHERE (log_name='$_POST[name]')";
    $result3 = mysqli_query($connection, $query3) or die("Could not get character data for use WP.<br \>" . mysqli_error($connection) . "<br \>");
    $data = mysqli_fetch_array($result3);
    if ($data['wptemp'] != "0") {
      $character_name = "$data[log_name]";
      $currentwp = "$data[wptemp]";
      $minuswp = 1;
      $newwp = $currentwp - $minuswp;
      $query4 = "UPDATE game_data SET wptemp=($data[wptemp]-$minuswp) WHERE (log_name='$character_name')";
      $result4 = mysqli_query($connection, $query4) or die("Couldn't update spend WP.<br \>" . mysqli_error($connection) . "<br \>");
    } else {
    }
    $now = date('m/d/y H:i:s');
    $name = "<strong>" . $name . "</strong>";
    $name2 = "<strong>" . $name2 . "</strong>";
    $display = "<div class='spend'>$now $_POST[name] $_POST[name2] spends a willpower to $_POST[action]</div>";
    $newroll = 1;
    // spend blood
  } elseif ($_POST['submit4'] == "Spend Blood" && ($_POST['name'] <> null) && $_POST['action'] <> null && $_POST['pool'] <> null) {
    $query5 = "SELECT * FROM `game_data` WHERE (log_name='$_POST[name]')";
    $result5 = mysqli_query($connection, $query5) or die("Could not get character data for spend blood.<br \>" . mysqli_error($connection) . "<br \>");
    $data = mysqli_fetch_array($result5);
    if ($data['bloodcurrent'] != "0") {
      $character_name = "$data[log_name]";
      $currentbp = "$data[bloodcurrent]";
      $minusbp = "$_POST[pool]";
      $newbp = $currentbp - $minusbp;
      $query6 = "UPDATE game_data SET bloodcurrent=($data[bloodcurrent]-$minusbp) WHERE (log_name='$character_name')";
      $result6 = mysqli_query($connection, $query6) or die("Couldn't update spend blood.<br \>" . mysqli_error($connection) . "<br \>");
    } else {
    }
    $now = date('m/d/y H:i:s');
    $name = "<strong>" . $name . "</strong>";
    $name2 = "<strong>" . $name2 . "</strong>";
    $display = "<div class='spend'>$now $_POST[name] $_POST[name2] spends $_POST[pool] blood to $_POST[action]</div>";
    $newroll = 1;

    //spend gnosis
  } elseif ($_POST['submit5'] == "Spend Gnosis" && ($_POST['name'] <> null) && $_POST['action'] <> null && $_POST['pool'] <> null) {
    $query7 = "SELECT * FROM `game_data` WHERE (log_name='$_POST[name]')";
    $result7 = mysqli_query($connection, $query7) or die("Could not get character data for spend gnosis.<br \>" . mysqli_error($connection) . "<br \>");
    $data = mysqli_fetch_array($result7);
    if ($data['gnosistemp'] != "0") {
      $character_name = "$data[log_name]";
      $currentgnosis = "$data[gnosistemp]";
      $minusgnosis = "$_POST[pool]";
      $newgnosis = $currentgnosis - $minusgnosis;
      $query8 = "UPDATE game_data SET gnosistemp=($data[gnosistemp]-$minusgnosis) WHERE (log_name='$character_name')";
      $result8 = mysqli_query($connection, $query8) or die("Couldn't update spend gnosis.<br \>" . mysqli_error($connection) . "<br \>");
    } else {
    }
    $now = date('m/d/y H:i:s');
    $name = "<strong>" . $name . "</strong>";
    $name2 = "<strong>" . $name2 . "</strong>";
    $display = "<div class='spend'>$now $_POST[name] $_POST[name2] spends $_POST[pool] gnosis to $_POST[action]</div>";
    $newroll = 1;

    // spend quint
  } elseif ($_POST['submit6'] == "Spend Quint" && ($_POST['name'] <> null) && $_POST['action'] <> null && $_POST['pool'] <> null) {
    $query9 = "SELECT * FROM `game_data` WHERE (log_name='$_POST[name]')";
    $result9 = mysqli_query($connection, $query9) or die("Could not get character data for spend quint.<br \>" . mysqli_error($connection) . "<br \>");
    $data = mysqli_fetch_array($result9);
    if ($data['quinttemp'] != "0") {
      $character_name = "$data[log_name]";
      $currentquint = "$data[quinttemp]";
      $minusquint = "$_POST[pool]";
      $newquint = $currentquint - $minusquint;
      $query10 = "UPDATE game_data SET quinttemp=($data[quinttemp]-$minusquint) WHERE (log_name='$character_name')";
      $result10 = mysqli_query($connection, $query10) or die("Couldn't update spend quint.<br \>" . mysqli_error($connection) . "<br \>");
    } else {
    }
    $now = date('m/d/y H:i:s');
    $name = "<strong>" . $name . "</strong>";
    $name2 = "<strong>" . $name2 . "</strong>";
    $display = "<div class='spend'>$now $_POST[name] $_POST[name2] spends $_POST[pool] quint to $_POST[action]</div>";
    $newroll = 1;

    // Spend Glamour
  } elseif ($_POST['submit7'] == "Spend Glamour" && ($_POST['name'] <> null) && $_POST['action'] <> null && $_POST['pool'] <> null) {
    $query11 = "SELECT * FROM `game_data` WHERE (log_name='$_POST[name]')";
    $result11 = mysqli_query($connection, $query11) or die("Could not get character data for spend glamour.<br \>" . mysqli_error($connection) . "<br \>");
    $data = mysqli_fetch_array($result11);
    if ($data['glamourtemp'] != "0") {
      $character_name = "$data[log_name]";
      $currentglamour = "$data[glamourtemp]";
      $minusglamour = "$_POST[pool]";
      $newglamour = $currentglamour - $minusglamour;
      $query12 = "UPDATE game_data SET glamourtemp=($data[glamourtemp]-$minusglamour) WHERE (log_name='$character_name')";
      $result12 = mysqli_query($connection, $query12) or die("Couldn't update spend glamour.<br \>" . mysqli_error($connection) . "<br \>");
    } else {
    }
    $now = date('m/d/y H:i:s');
    $name = "<strong>" . $name . "</strong>";
    $name2 = "<strong>" . $name2 . "</strong>";
    $display = "<div class='spend'>$now $_POST[name] $_POST[name2] spends $_POST[pool] glamour to $_POST[action]</div>";
    $newroll = 1;

  }
  // add results to database
  if (($_POST['submit'] == "Roll Dice" or $_POST['submit2'] == "Roll Inits" or $_POST['submit3'] == "Spend WP" or $_POST['submit4'] == "Spend Blood" or $_POST['submit5'] == "Spend Gnosis" or $_POST['submit6'] == "Spend Quint" or $_POST['submit7'] == "Spend Glamour" or $_POST['submit8'] == "Roll Fate") && $newroll == 1) {
    $sql = "Select recordid from diceroller";
    $count = @mysqli_query($connection, $sql);
    $i = 0;
    while ($results = @mysqli_fetch_array($count)) {
      $currec[$i] = $results['recordid'];
      $i++;
    }

    //delete old rolls
    array_multisort($currec, SORT_DESC);
    if ($currec[0] > 50) {//<
      $delrec = $currec[0] - 50;
      $sql = <<<end
delete from diceroller where recordid="$delrec";
end;
      @mysqli_query($connection, $sql);
    }
    //end delete old rolls
  
    $sql = <<<end
insert into diceroller set results="$display";
end;
    @mysqli_query($connection, $sql);
  }
  ?>
  <form name="form1" method="post" action="<?php echo $PHP_SELF ?>">
    <table width="700" border="0" align="center">
      <caption>
        <div align='center'><img src="../site-img/dieroller.gif"></div>
      </caption>
      <tr>
        <td valign="top">
          <table width="350" border="0" align="center" cellpadding="0">
            <?php
            if ($_COOKIE['privilege'] <= "3") {
              echo ("<tr><td><div class='pageitem' align='right'>Name:</div></td><td width='10'></td><td><input type='text' name='name2' size='24' maxlength='32' class='form'></td></tr>");
            } else {
              ("");
            }
            ?>
            <div class="pageitem">Items in <span style="color:red;">RED</span> are required</div>
            <tr>
              <td>
                <div class="pageitem" align="right" style="color:red;">Name:</div>
              </td>
              <td width="10">&nbsp;</td>
              <td>
                <?php echo $option ?>
              </td>
            </tr>
            <tr>
              <td valign="top">
                <div class="pageitem" align="right" style="color:red;">Action:</div>
              </td>
              <td width="10">&nbsp;</td>
              <td><textarea name="action" cols="46" rows="3" id="action" class="form"></textarea></td>
            </tr>
            <tr>
              <td>
                <div class="pageitem" align="right" style="color:red;">Pool:</div>
              </td>
              <td width="10">&nbsp;</td>
              <td><input type="text" name="pool" size="2" maxlength="2" id="pool" class="form"></td>
            </tr>
            <tr>
              <td>
                <div class="pageitem" align="right">Diff:</div>
              </td>
              <td width="10">&nbsp;</td>
              <td><input type="text" name="target" size="2" maxlength="2" id="target" class="form"></td>
            </tr>
            <tr>
              <td>
                <div class="pageitem" align="right">Specialty:</div>
              </td>
              <td width="10">&nbsp;</td>
              <td><input type="checkbox" name="rr10" value="Y" class="form"></td>
            </tr>
            <tr>
              <td>
                <div class="pageitem" align="right">Use WP?</div>
              </td>
              <td width="10">&nbsp;</td>
              <td><input type="checkbox" name="spendwp" value="Y" id="spendwp" class="form"></td>
            </tr>
            <tr>
              <td colspan="3">
                <div class="center">
                  <input type="submit" name="submit" value="Roll Dice" id="submit" class="form">
                  <input type="submit" name="refresh" value="Refresh" id="refresh" class="form">
                  <input type="submit" name="submit2" value="Roll Inits" id="submit2" class="form">
                  <input type="submit" name="submit3" value="Spend WP" id="submit3" class="form">
                  <input type="submit" name="submit8" value="Roll Fate" id="submit8" class="formreq">
                </div>
              </td>
            </tr>
            <tr>
              <td colspan="3">
                <div class="center">
                  <hr width="50%></div></td>
        </tr>
          <td colspan=" 3">
                  <div class="center">
                    <input type="submit" name="submit4" value="Spend Blood" id="submit4" class="form">
                    <input type="submit" name="submit5" value="Spend Gnosis" id="submit5" class="form">
                    <input type="submit" name="submit6" value="Spend Quint" id="submit6" class="form">
                    <input type="submit" name="submit7" value="Spend Glamour" id="submit7" class="form">
                  </div>
              </td>
            </tr>
          </table>
        </td>
        <td width="10">&nbsp;</td>
        <td valign="top">
          <table width="350" border="0" align="center" cellpadding="0">

            <tr>
              <td>
                <div class="pageitem" align="center">How to use</div>
              </td>
            </tr>
            <tr>
              <td>
                <div class="item">1. To roll a pool: Select character name, type in action, enter number of dice in
                  pool. Click &quot;Roll Dice.</div>
              </td>
            </tr>
            <tr>
              <td>
                <div class="item">2. If you have a specialty that applies (check with ST), click the Specialty box.
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <div class="item">3. If you want to spend a willpower on a roll select the Use WP box, if you have
                  willpower to spend a blue W will appear on the roll results, if you do not have the willpower it will
                  not show.</div>
              </td>
            </tr>
            <tr>
              <td>
                <div class="item">4. If you want to roll just initative, select the character name then click &quot;Roll
                  Inits&quot;</div>
              </td>
            </tr>
            <tr>
              <td>
                <div class="item">5. If you want to spend a willpower for a reason other than a roll: Select character
                  name, type the reason in the action box, then click &quot;Spend WP&quot;.</div>
              </td>
            </tr>
            <tr>
              <td>
                <div class="item">6. If you want to spend a trait like blood or other: Select character name, type the
                  reason in the action box, put the amount you want to spend in Pool then click the correct
                  &quot;Spend...&quot;</div>
              </td>
            </tr>
            <tr>
              <td>
                <div class="item">7. If you are asked by staff to roll a Fate Die: Select character name then click
                  &quot;Roll Fate&quot;.</div>
              </td>
            </tr>
            <tr>
              <td>
                <div class="item">8. If you want to just refresh the table click &quot;Refresh&quot;.</div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </form>
  <?php
  // Display a table of Characters with their relevant traits
  require "../player-tools/roller-traits.php";
  ?>
  <br \>
  <?php
  $query = "SELECT * FROM diceroller ORDER BY recordid DESC LIMIT 50";
  $result = mysqli_query($connection, $query) or die("Query execution failed: " . mysqli_error($connection));

  if ($result) {
    echo "<table width='700' align='center' cellspacing='0' cellpadding='0'>";
    echo "<tr><td colspan='3' align='center'><div class='pagetopic'>Results</div></td></tr>";
    echo "<tr><td colspan='3'><hr></td></tr>";

    while ($row = mysqli_fetch_assoc($result)) {
      echo "<tr><td>{$row['results']}</td></tr>";
      echo "<tr><td colspan='3'><hr></td></tr>\n";
    }

    echo "</table>\n";
  }
  ?>
</div>