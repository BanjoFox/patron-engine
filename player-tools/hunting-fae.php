<?php
require '/var/www/shadowsofthebayou.com/site-inc/header.php';
require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php";
?>

<div id="pagewrapper">

    <div align="center" class='pagetopic'>Changeling Glamour Replenish</div><br>
    <br>

    <?php
    $query = "SELECT * FROM `game_data` WHERE (id='$_POST[id]')";
    $result = mysqli_query($connection, $query) or die("<div align='center' class='error'>Couldn't get character data.</div>");
    $data = mysqli_fetch_array($result);

    $fame_bg_keys = ['bg1', 'bg2', 'bg3', 'bg4', 'bg5', 'bg6', 'bg7', 'bg8', 'bg9', 'bg10', 'bg11', 'bg12', 'bg13', 'bg14', 'bg15'];
    $fame = array_reduce(
        $fame_bg_keys,
        function ($carry, $key) use ($data) {
            if ($data[$key] == "Fame") {
                return $data["{$key}val"];
            }
            return $carry;
        },
        0
    );

    if ($data['sub_venue'] != "Changeling") {
        echo "<div align='center' class='warning'>Character is not a changeling.</div><br><br>";
    } else {
        $today = date("Y-m-d");
        if (($data['last_hunt'] == "$today")) {
            echo "<div align='center' class='warning'>You already replenished today.</div><br><br>";
        } else {
            ?>

            <div class="center">
                <?php
                if ($_POST['submit'] == "Replenish" && $_POST['style'] !== null && $data['last_hunt'] !== $today && $_POST['pool'] !== null && $_POST['target'] !== "0") {
                    $rolls = [];
                    $successes = 0;
                    $botches = 0;
                    $target = ($_POST['target'] > 10) ? 10 : (int) $_POST['target'];

                    for ($i = 0; $i < $_POST['pool']; $i++) {
                        $rolls[] = mt_rand(1, 10);
                    }

                    foreach ($rolls as $roll) {
                        if ($roll >= $target) {
                            $successes++;
                        }
                        if ($roll == 1) {
                            $botches++;
                        }
                    }

                    $result = implode(', ', $rolls);
                    $sux = ($successes - $botches);

                    $net = $sux > 0 ? "<span color=#99FF00>gained $sux Glamour.</span>" : ($sux < 0 ? "<dice-result class=\"failed\">Failed</dice-result>" : "<dice-result class=\"botch\">BOTCH!</dice-result>");

                    $notes = [($_POST['herd'] !== "0") ? "Dreamers $_POST[herd]" : "", ($_POST['fame'] !== "0") ? "Fame $_POST[fame]" : ""];

                    $display = "Tried a $_POST[style]: ($_POST[pool] dice at diff $_POST[target]) and $net " . implode(' ', array_filter($notes));

                    $gain = max($sux, 0);

                    if ($_POST['submit'] == "Replenish") {
                        $query = "INSERT INTO huntinglog (date, charactername, playername, results, venue) VALUES (NOW(), '$data[log_name]', '$data[playername]', '$display', '$_POST[venue]')";
                        $result = mysqli_query($connection, $query) or die("<br><div align='center' class='error'>Could not execute query.<br>" . mysqli_error($connection) . "<br></div>");

                        $query = "UPDATE `game_data` SET glamourtemp=GREATEST(glamourperm, glamourtemp + $gain), last_hunt=NOW() WHERE (log_name='$data[log_name]')";
                        $result = mysqli_query($connection, $query) or die("<div class='error'>Could not execute query for glamour add.<br>" . mysqli_error($connection) . "<br></div>");

                        if ($net == "<dice-result class=\"botch\">BOTCH!</dice-result>" && $_POST['style'] == "Ravage") {
                            $query = "INSERT INTO char_reqs (charactername, reqtype, req_date, justi, req_stat, player_name, char_venue) VALUES ('$data[charactername]', 'Botched Ravage', NOW(), '$display <br>Add 1 Perm Banality', 'Reviewing', '$_COOKIE[logname]', '$data[sub_venue]')";
                            $result = mysqli_query($connection, $query) or die("<div class='error'>Could not execute query for staff alert.<br>" . mysqli_error($connection) . "<br></div>");
                        }

                        if ($net == "<dice-result class=\"failed\">Failed</dice-result>" && $_POST['style'] == "Ravage") {
                            $query = "INSERT INTO char_reqs (charactername, reqtype, req_date, justi, req_stat, player_name, char_venue) VALUES ('$data[charactername]', 'Failed Ravage', NOW(), '$display <br>Add 1 Temp Banality', 'Reviewing', '$_COOKIE[logname]', '$data[sub_venue]')";
                            $result = mysqli_query($connection, $query) or die("<div class='error'>Could not execute query for staff alert.<br>" . mysqli_error($connection) . "<br></div>");
                        }
                    }
                }
                ?>
                <form name="form1" method="post" action="">
                    <input name="char_name" type="hidden" id="char_name" value="<?php echo $data['log_name'] ?>">
                    <input name="id" type="hidden" id="id" value="<?php echo $data['id'] ?>">
                    <input name="venue" type="hidden" id="venue" value="<?php echo $data['sub_venue'] ?>">
                    <input name="oldglamourtotal" type="hidden" id="oldglamourtotal" value="<?php echo $data['glamourtemp'] ?>">

                    <table width="100%" border="2">
                        <tr>
                            <td width="40%" valign="top">
                                <table width="80%" align="center">
                                    <tr>
                                        <td width="35%">
                                            <div class="pageitem">Character</div>
                                        </td>
                                        <td width="65%">
                                            <div class="pageitem">
                                                <?php echo $data['log_name'] ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class='item'>Replenish Method</div>
                                        </td>
                                        <td>
                                            <div class='item'>
                                                <select name="style" size="1" id="style" class="form">
                                                    <option value=""></option>
                                                    <option value="Musing">Musing (8)</option>
                                                    <option value="Ravage">Ravage (8)</option>
                                                    <option value="Rapture">Rapture (Temp G - Perm G +6)</option>
                                                    <option value="Sanctuary">Sanctuary (7)</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class='item'>Dreamers</div>
                                        </td>
                                        <td>
                                            <div class='itemsm'>
                                                <input type="text" name="herd" id="herd" class="form" size="1" maxlength="1"
                                                    value="0"> Add to Pool
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class='item'>Fame</div>
                                        </td>
                                        <td>
                                            <div class='itemsm'>
                                                <input type="text" name="fame" id="fame" class="form" size="1" maxlength="1"
                                                    value="<?php echo $fame ?>"> Subtract from Diff
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <hr width="50%" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class='item'>Pool</div>
                                        </td>
                                        <td>
                                            <div class='itemsm'>
                                                <input type="text" name="pool" id="pool" class="form" size="1" maxlength="2"
                                                    value="0">(Based on
                                                Method + Dreamers)
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class='item'>Difficulty</div>
                                        </td>
                                        <td>
                                            <div class='itemsm'>
                                                <input type="text" name="target" id="target" class="form" size="1" maxlength="2"
                                                    value="0">(Based on
                                                Method - Fame)
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <hr width="50%" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="center">
                                                <input type="submit" name="submit" value="Replenish" id="submit" class="form">
                                            </div>
                                        </td>
                                    </tr>

                                </table>
                            </td>
                            <td width="60%" valign="top">
                                <table style="width:100%" border="0" align="center">
                                    <tr>
                                        <td>
                                            <div class="pageitem" align="center">How to Use</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="warning">You can only roll once a day. Recommended after Requesting XP
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="item">1. Select Replenishment Method. This will tell you the base
                                                difficulty and what pool
                                                to use.</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="item">2. Put your Dreamer rating in the box (Unless Rapture or
                                                Sanctuary), if you have a
                                                pooled Dreamers, use the total rating.</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="item">3. Fame, if you possess Fame it will show in the box for you.
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="item">4. For Pool, add the two traits based on method and add Dreamers
                                                then put that
                                                number here.</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="item">5. For Difficulty, add the base for your method. Note the lowest
                                                Diff Allowed is 2.
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="item">6. Make sure the Method, Pool and Difficulty has been entered then
                                                Click Replenish
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="item">It will automatically add the result to your temp Glamour up to
                                                the max rating, all
                                                exess is ignored/lost.</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <hr width="50%" align="center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="warning">If the result is a Fail or a Botch on a Ravaging it will notify
                                                the staff in the
                                                form of a character request.</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <hr width="50%" align="center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="item"><strong>Musing</strong> = Wits (<?php echo $data['wits'] ?>) + Ability
                                                Selected At
                                                Careation<br><strong>Ravage</strong> = Attribute + Ability Selected At
                                                Creation<br><strong>Rapture</strong> = Higher of
                                                Perm Banality (<?php echo $data['banalityperm'] ?>) or Temp Banality
                                                (<?php echo $data['banalitytemp'] ?>) - Perm Glamour
                                                (<?php echo $data['glamourperm'] ?>) if number
                                                is a negative use 0, add 6 this is the difficulty. Do not Add Dreamers to
                                                Pool.<br><strong>Sanctuary</strong>
                                                = Perception (<?php echo $data['perception'] ?>) + Gremayre
                                                (<?php echo $data['gremayre'] ?>) Do not
                                                Add Dreamers to Pool.</div>
                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <?php
        }
    }
    ?>

    <!-- Display Results Table -->
    <?php
    $query = "SELECT * FROM huntinglog WHERE (playername='$_COOKIE[logname]' && venue='Changeling') ORDER BY recordid DESC limit 50";
    $result = mysqli_query($connection, $query) or die("Couldn't execute query.");
    $nrows = mysqli_num_rows($result);

    // Display results table
    echo "<table width='700' align='center' cellspacing='0' cellpadding='0'>";
    echo "<tr><td colspan='5' align='center'><div class='pagetopic'>Results</div></td></tr>";
    echo "<tr><td colspan='5'><hr></td></tr>";
    for ($i = 0; $i < $nrows; $i++) {
        $n = $i + 1;
        $row = mysqli_fetch_array($result);
        extract($row);
        echo "<tr>\n";
        echo "<td width='8%'><div class='item'>$date</div></td>\n";
        echo "<td width='2%'><div class='item'></div></td>\n";
        echo "<td width='20%'><div class='item'>$charactername</div></td>\n";
        echo "<td width='2%'></td>\n";
        echo "<td width='65%'><div class='item'>$results</div></td>\n";
        echo "</tr>\n";
    }
    echo "</table>";
    ?>
</div>