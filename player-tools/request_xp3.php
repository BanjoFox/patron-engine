<?php
if ($_COOKIE['logname'] == '') {
    header("location: http://shadowsofthebayou.com/login.php");
    exit();
}
?>

<head>
</head>
<link rel=stylesheet href="../layout.css" TYPE="text/css">
<div id="pagewrapper">
    <?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
    <div class="pagetopic">Request XP For A Character</div>
    <br \>
    <?php
    if ($_POST['Select'] == "Request XP" && $_POST['log_name'] <> "" && $_POST['Verify'] == "Yes") {
    }
    ?>
    <?php
    $query = "SELECT * FROM `game_data` WHERE (log_name=\"$_POST[log_name]\")";
    $results = mysqli_query($connection, $query)
        or die("Couldn't execute query..<br \>" . mysqli_error($connection) . "<br \>");
    $data = @mysqli_fetch_array($results);
    ?>
    <br />
    <form name="form3" method="post" action="">
        <?php
        $query = "SELECT * FROM `game_data` WHERE (log_name=\"$_POST[log_name]\")";
        $result = mysqli_query($connection, $query)
            or die("Couldn't get character data.");
        $data = mysqli_fetch_array($result);
        ?>
        <?php
        $today = date("Y-m-d");
        $oldxptotal = $data['xptotal'];
        $oldspentxp = $data['xpspent'];
        $maxwp = $data['wpperm'];
        $oldwptemp = $data['wptemp'];
        $xpaward = ".5";
        $retxpaward = ".1";
        $wpadd = "1";
        $oldbloodtotal = $data['bloodcurrent'];
        $blooduse = "1";
        $bloodgain = "1";
        $ghoulmax = "10";
        $character_name = $data['log_name'];
        $id = $data['id'];
        ?>
        <?php
        if ($data['sanctioned'] == "Yes") {
            if ($data['last_login'] <> $today) {
                $query = "UPDATE `game_data` SET xptotal=($oldxptotal+$xpaward),xpavail=(($oldxptotal+$xpaward)-($oldspentxp)) WHERE (log_name=\"$character_name\")";
                $result = mysqli_query($connection, $query)
                    or die("<div  class='error'>Could not execute query for XP award.<br \>" . mysqli_error($connection) . "<br \></div>");

                $query = "UPDATE `game_data` SET xptotal=(xptotal+.1) WHERE (retainer_of=\"$character_name\" AND sanctioned=\"Yes\" AND char_type=\"RET\")";
                $result = mysqli_query($connection, $query)
                    or die("<div  class='error'>Could not execute query for retainer XP award.<br \>" . mysqli_error($connection) . "<br \></div>");
                echo "<div  class='success'>XP awarded.</div>";

                $query = "INSERT INTO character_logins (character_name,player_name) VALUES (\"$character_name\",\"$_COOKIE[logname]\")";
                $result = mysqli_query($connection, $query) or die("<div class='error'>Could not update character login instance.<br \>" . mysqli_error($connection) . "<br \></div>");

            } else {
                echo "<div  class='warning'>XP already given for today.<br \>Willpower already gained or at max.<br \></div><br \>";
            }

            if ($data['last_login'] <> $today) {
                if ($data['wptemp'] <> $data['wpperm']) {
                    $query = "UPDATE `game_data` SET wptemp=($oldwptemp+$wpadd) WHERE (log_name=\"$character_name\")";
                    $result = mysqli_query($connection, $query) or die("<div  class='error'>Could not execute query for WP restore.<br \></div>");
                    echo "<div  class='success'>One willpower restored.</div><br \>";
                } else {
                    echo "<div  class='warning'>Willpower already gained or at max.</div><br \>";
                }
            }
        } else {
            echo "<div  class='warning'>Character not sanctioned</div><br \>";
        }

        if ($data['sanctioned'] == "Yes" and $data['sub_venue'] == "Vampire") {
            if ($data['last_login'] <> $today) {
                $query = "UPDATE `game_data` SET bloodcurrent=($oldbloodtotal-$blooduse) WHERE (log_name=\"$character_name\")";
                $result = mysqli_query($connection, $query) or die("<div class='error'>Could not execute query for blood spend.<br \></div>");
                echo "<div class='success'>Blood Point spent to awaken.</div><br \>";
            } else {
                echo "<div class='warning'>Blood Point already spent to awaken.</div><br \>";
            }
        } else {
            echo "";
        }

        if (
            $data['sanctioned'] == "Yes" and ($data['clan'] == "Anushin-Rawan" or $data['clan'] == "Bratovich" or $data['clan'] == "Ducheski" or $data['clan'] == "Grimaldi" or
                $data['clan'] == "Kairouan Brother" or $data['clan'] == "Obertus" or $data['clan'] == "Oprinchniki" or $data['clan'] == "Rossellini" or $data['clan'] == "Zantosa" or $data['clan'] == "Dhampire")
        ) {
            if ($data['bloodcurrent'] <> $ghoulmax) {
                $query = "UPDATE `game_data` SET bloodcurrent=($oldbloodtotal+$bloodgain) WHERE (log_name=\"$character_name\")";
                $result = mysqli_query($connection, $query) or die("<div class='error'>Could not execute query for blood add.<br \></div>");
                echo "<div class='success'>Dhampire / Revenant Vitae regained.</div><br \>";
            } else {
                echo "<div  class='warning'>Dhampire / Revenant Vitae already at max.<br \></div><br \>";
            }
        }

        ?>
        <?php
        $query = "UPDATE game_data SET last_login=\"$today\" WHERE(log_name=\"$character_name\")";
        $result = mysqli_query($connection, $query)
            or die("<div  class='error'>Could not update last login.<br \>" . mysqli_error($connection) . "</div><br \>");
        ?>
        <?php
        $threemonthago = date('Y-m-d', strtotime("3 month ago"));
        $fourmonthago = date('Y-m-d', strtotime("4 month ago"));
        $fivemonthago = date('Y-m-d', strtotime("5 month ago"));
        $sixmonthago = date('Y-m-d', strtotime("6 month ago"));
        ?>
        <?php
        $query = "UPDATE `game_data` SET sanctioned=\"Hold\",last_modified=\"Shadows System\",date_modified=NOW() WHERE (sanctioned='Yes' AND char_type='PC' AND last_login<'$fourmonthago' AND last_login!='0000-00-00')";
        $result = mysqli_query($connection, $query) or die("<div class='error'>Could not execute move to Hold Status.<br \>" . mysqli_error($connection) . "<br \></div>");
        ?>
        <?php
        $query = "UPDATE `game_data` SET deleted='Yes',deleted_date=NOW(),deleted_by=\"Shadows System\",date_modified=NOW() WHERE (sanctioned='Hold' AND char_type='PC' AND last_login<'$sixmonthago' AND last_login!='0000-00-00')";
        $result = mysqli_query($connection, $query) or die("<div class='error'>Could not execute move to Hold Status.<br \>" . mysqli_error($connection) . "<br \></div>");
        ?>
    </form>