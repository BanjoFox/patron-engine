<title></title>
<link rel=stylesheet href="../layout.css" TYPE="text/css">
<div id="pagewrapper">
  <?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
  <div align="center" class='pagetopic'>Vampire Hunting</div><br \>
  <br \>
  <?php
  $query = "SELECT * FROM `game_data` WHERE (id='$_POST[id]')";
  $result = mysqli_query($connection, $query) or die("<div align='center' class='error'>Couldn't get character data.</div>");
  $data = mysqli_fetch_array($result);

  {
    ?>
    <?php
    if ($data['bg1'] == "Fame") {
      $fame = "$data[bg1val]";
    } elseif ($data['bg2'] == "Fame") {
      $fame = "$data[bg2val]";
    } elseif ($data['bg3'] == "Fame") {
      $fame = "$data[bg3val]";
    } elseif ($data['bg4'] == "Fame") {
      $fame = "$data[bg4val]";
    } elseif ($data['bg5'] == "Fame") {
      $fame = "$data[bg5val]";
    } elseif ($data['bg6'] == "Fame") {
      $fame = "$data[bg6val]";
    } elseif ($data['bg7'] == "Fame") {
      $fame = "$data[bg7val]";
    } elseif ($data['bg8'] == "Fame") {
      $fame = "$data[bg8val]";
    } elseif ($data['bg9'] == "Fame") {
      $fame = "$data[bg9val]";
    } elseif ($data['bg10'] == "Fame") {
      $fame = "$data[bg10val]";
    } elseif ($data['bg11'] == "Fame") {
      $fame = "$data[bg11val]";
    } elseif ($data['bg12'] == "Fame") {
      $fame = "$data[bg12val]";
    } elseif ($data['bg13'] == "Fame") {
      $fame = "$data[bg13val]";
    } elseif ($data['bg14'] == "Fame") {
      $fame = "$data[bg14val]";
    } elseif ($data['bg15'] == "Fame") {
      $fame = "$data[bg15val]";
    } else {
      $fame = "0";
    }

    if ($data['sub_venue'] != "Vampire") {
      echo "<div align='center' class='warning'>Character is not a vampire!</div><br \><br \>";
    } else {
      ?>
      <?php
      $today = date("Y-m-d");
      if (($data['last_hunt'] == "$today")) {
        echo "<div align='center' class='warning'>You already hunted today.</div><br \><br \>";
      } else {
        ?>
        <div class="center">
          <?php
          if (
            $_POST['Submit'] == "Hunt" && $_POST['area'] <> null && $data['last_hunt'] <> "$today"
            && $_POST['pool'] <> null && $_POST['target'] <> "0"
          ) {
            // set and reset values
            $result = "";
            $successes = 0;
            $botches = 0;
            $target = 9;
            if ($_POST['target'] == "") {
              $target = 6;
            } else {
              $target = "$_POST[target]";
            }
            if ($_POST['target'] > 10) {
              $_POST['target'] = 10;
            }

            // generate roll and make viewable
            for ($i = 0; $i < $_POST['pool']; $i++) {
              $roll = mt_rand(1, 10);
              if ($roll >= $target) {
                $successes++;
              }
              if ($roll == 1) {
                $botches++;
              }
              $result = $result . ", " . $roll;
            }

            //other notes
            if ($_POST['preyx'] == "Yes") {
              $note1 = "<span color=#00CCFF>PreyEx </span>";
            }
            if ($_POST['herd'] <> "0") {
              $note2 = "<span color=#00CCFF>Herd $_POST[herd] </span>";
            }
            if ($_POST['fame'] <> "0") {
              $note3 = "<span color=#00CCFF>Fame $_POST[fame]</span>";
            } else {
              "";
            }


            // results
            $now = date('m/d/y H:i:s');
            $result = substr($result, 2);
            $sux = ($successes - $botches);
            if (($successes <= 0) and ($botches > 0)) {
              $net = "<dice-result class=\"botch\">BOTCH!</dice-result>";
            } elseif (($successes >= 1) and ($botches >= $successes)) {
              $net = "<dice-result class=\"failed\">Failed</dice-result>";
            } elseif ($sux == "0") {
              $net = "<dice-result class=\"failed\">Failed</dice-result>";
            } else {
              $net = "<span color=#99FF00>gained $sux blood.</span>";
            }


            $result = substr($result, 2);
            $sux = ($successes - $botches);
            if (($successes <= 0) and ($botches > 0)) {
              $net = "<dice-result class=\"botch\">BOTCH!</dice-result>";
            } elseif (($successes >= 1) and ($botches >= $successes)) {
              $net = "<dice-result class=\"failed\">Failed</dice-result>";
            } elseif ($sux == "0") {
              $net = "<dice-result class=\"failed\">Failed</dice-result>";
            } else {
              $gain = "$sux";
            }


            $display = "Went hunting in the $_POST[area]: ($_POST[pool] dice at diff $_POST[target]) using $_POST[style] tactics and $net $note1 $note2 $note3";
            $newroll = 1;
          }
          // add results to database
          if (($_POST['Submit'] == "Hunt") && $newroll == 1) {
            $query = "INSERT INTO huntinglog (date,charactername,playername,results,venue) VALUES (NOW(),\"$data[log_name]\",\"$data[playername]\",\"$display\",\"$_POST[venue]\")";
            $result = mysqli_query($connection, $query)
              or die("<br \><div align='center' class='error'>Could not execute query.<br \>" . mysqli_error($connection) . "<br \></div>");
          }

          if ($gain > "0") {
            $query = "UPDATE `game_data` SET bloodcurrent=($_POST[oldbloodtotal]+$gain),last_hunt=NOW() WHERE (log_name=\"$data[log_name]\")";
            $result = mysqli_query($connection, $query) or die("<div class='error'>Could not execute query for bloodadd.<br \>" . mysqli_error($connection) . "<br \></div>");
            echo "";
          } else {
            echo "";
          }

          if (($_POST['oldbloodtotal'] + $gain) > "$data[bloodmax]") {
            $query = "UPDATE `game_data` SET bloodcurrent=\"$data[bloodmax]\" WHERE (log_name=\"$data[log_name]\")";
            $result = mysqli_query($connection, $query) or die("<div class='error'>Could not execute query for bloodadd.<br \>" . mysqli_error($connection) . "<br \></div>");
            echo "";
          } else {
            echo "";
          }

          if ($net == "<dice-result class=\"botch\">BOTCH!</dice-result>") {
            $query = "INSERT INTO char_reqs (char_name,reqtype,req_date,justi,req_stat,player_name,char_venue) VALUES (\"$_POST[char_name]\",\"Botched Feeding\",NOW(),\"$display\",\"Reviewing\",\"$_COOKIE[logname]\",\"$data[sub_venue]\")";
            $result = mysqli_query($connection, $query) or die("<div class='error'>Could not execute query for staff alert.<br \>" . mysqli_error($connection) . "<br \></div>");
            echo "";
          } else {
            echo "";
          }


          //echo ("<div class='item'>Name: $_POST[char_name]<br \>");
          //echo ("Target: $_POST[target]<br \>");
          //echo ("Pool: $_POST[pool]<br \>");
          //echo ("area: $_POST[area]<br \>");
          //echo ("Diff: $target<br \>");
          //echo ("fame: $_POST[fame]<br \>");
          //echo ("preyx: $_POST[preyx]<br \>");
          //echo ("herd: $_POST[herd]<br \>");
          //echo ("Results: $display<br \>");
          //echo ("Style: $_POST[style]<br \>");
          //echo ("gain: $gain<br \>");
          //echo ("oldblood: $_POST[oldbloodtotal] <br \>");
          //echo ("max: $data[bloodmax] <br \>");
          //echo ("newtotal: $_POST[oldbloodtotal]+$gain </div>");
    
          ?>
          <form name="form1" method="post" action="">

            <input name="char_name" type="hidden" id="char_name" value="<?php echo $data['log_name'] ?>">
            <input name="id" type="hidden" id="id" value="<?php echo $data['id'] ?>">
            <input name="venue" type="hidden" id="venue" value="<?php echo $data['sub_venue'] ?>">
            <input name="oldbloodtotal" type="hidden" id="oldbloodtotal" value="<?php echo $data['bloodcurrent'] ?>">

            <table width="100%" border="0">
              <tr>
                <td width="55%" valign="top">
                  <table width="80%" align="center">
                    <tr>
                      <td width="35%">
                        <div class="pageitem">Character</div>
                      </td>
                      <td width="65%">
                        <div class="pageitem"><?php echo $data['log_name'] ?></div>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <div class="item">
                          <hr width="50%">
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="item">Current Blood Pool</div>
                      </td>
                      <td>
                        <div class="item"><?php echo $data['bloodcurrent'] ?></div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="item">Max Blood Pool</div>
                      </td>
                      <td>
                        <div class="item"><?php echo $data['bloodmax'] ?></div>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <div class="item">
                          <hr width="50%">
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="item">Last Feed Roll</div>
                      </td>
                      <td>
                        <div class="item"><?php echo $data['last_hunt'] ?></div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class='item'>Hunting Where?</div>
                      </td>
                      <td>
                        <div class='item'><select name="area" size="1" id="area" class="form">
                            <option value=""></option>
                            <option value="Hollygrove">Hollygrove (4)</option>
                            <option value="Lower 9th">Lower 9th (4)</option>
                            <option value="Gentilly">Gentilly (5)</option>
                            <option value="Lakeview">Lakeview (5)</option>
                            <option value="Business District">Business District (6)</option>
                            <option value="French Quarter">French Quarter (6)</option>
                            <option value="Black Pearl">Black Pearl (7)</option>
                            <option value="Touro">Touro (7)</option>
                            <option value="Audubon">Audubon (8)</option>
                            <option value="Garden District">Garden District (8)</option>
                          </select>
                        </div>
                      </td>
                    </tr>

                    <tr>
                      <td>
                        <div class='item'>Hunting Style</div>
                      </td>
                      <td>
                        <div class='item'><select name="style" size="1" id="style" class="form">
                            <option value=""></option>
                            <option value="Cat-and-Mouse">Cat-and-Mouse</option>
                            <option value="Harrying">Harrying</option>
                            <option value="Sandman">Sandman</option>
                            <option value="Seduction">Seduction</option>
                          </select>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class='item'>Herd</div>
                      </td>
                      <td>
                        <div class='itemsm'><input type="text" name="herd" id="herd" class="form" size="1" maxlength="1"
                            value="0"> Add to Pool</div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class='item'>Fame</div>
                      </td>
                      <td>
                        <div class='itemsm'><input type="text" name="fame" id="fame" class="form" size="1" maxlength="1"
                            value="<?php echo $fame ?>"> Subtract from Diff</div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class='item'>Prey Exclusion / Ventrue?</div>
                      </td>
                      <td>
                        <div class='itemsm'><select name="preyx" size="1" id="preyx" class="form">
                            <option value="No" selected>No</option>
                            <option value="Yes">Yes</option>
                          </select> (If Yes add 1 to Diff)</div>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <hr width="50%" />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class='item'>Pool</div>
                      </td>
                      <td>
                        <div class='itemsm'><input type="text" name="pool" id="pool" class="form" size="1" maxlength="2"
                            value="0">(Based on Style + Herd)</div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class='item'>Diffuculty</div>
                      </td>
                      <td>
                        <div class='itemsm'><input type="text" name="target" id="target" class="form" size="1" maxlength="2"
                            value="0">(Based on Where - Fame +1 for Prey Exclusion)</div>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <hr width="50%" />
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <div class="center"><input type="submit" name="submit" value="Hunt" id="submit" class="form"></div>
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="45%" valign="top">
                  <table style="width:100%" border="0" align="center">
                    <tr>
                      <td>
                        <div class="pageitem" align="center">How to Use</div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="warning">You can only roll once a day. Recommended after Requesting XP</div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="item">1. Select Hunting Where? This will tell you the base difficulty.</div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="item">2. Select Hunting Style, this will tell you what base pool to use.</div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="item">3. Put your Herd rating in the box, if you have a pooled Herd, use the total rating.
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="item">4. Fame, if you possess Fame it will show in the box.</div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="item">5. For Prey Exlusion, if you have the flaw or are a Ventrue select Yes.</div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="item">6. For Pool, add the two traits based on style and add Herd then put that number
                          here.</div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="item">7. For Difficulty, add the base from location and subtract Fame but add 1 if Prey
                          Exclusion is Yes. Note the lowest Diff Allowed is 2.</div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="item">8. Make sure the Pool and Difficulty has been entered then Click Hunt</div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="item">It will automatically add the result to your blood pool up to the max rating, all
                          exess is ignored/lost.</div>
                      </td>
                    </tr>

                    <tr>
                      <td>
                        <hr width="50%" align="center">
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="warning">If the result is a Botch it will notify the staff in the form of a character
                          request.</div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <hr width="50%" align="center">
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="item">Cat and Mouse = Wits (<?php echo $data['wits'] ?>) + Streetwise
                          (<?php echo $data['streetwise'] ?>)<br \> Harrying = Stam (<?php echo $data['stamina'] ?>) + Athletics
                          (<?php echo $data['athletics'] ?>)<br \> Sandman = Perception (<?php echo $data['perception'] ?>) +
                          Stealth (<?php echo $data['stealth'] ?>)<br \>Seduction = App (<?php echo $data['appearance'] ?>) +
                          Subterfuge (<?php echo $data['subterfuge'] ?>)</div>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </form>
        </div>
        <?php
      }
    }
  }
  ?>
  <?php
    $query = "SELECT * FROM huntinglog WHERE (playername='$_COOKIE[logname]' && venue='Vampire' && charactername='$data[log_name]') ORDER BY recordid DESC limit 50";
    $result = mysqli_query($connection, $query) or die("Couldn't execute query.");
    $nrows = mysqli_num_rows($result);

    // Display results table
    echo "<table width='700' align='center' cellspacing='0' cellpadding='0'>";
    echo "<tr><td colspan='5' align='center'><div class='pagetopic'>Results</div></td></tr>";
    echo "<tr><td colspan='5'><hr></td></tr>";
    for ($i = 0; $i < $nrows; $i++) {
      $n = $i + 1;
      $row = mysqli_fetch_array($result);
      extract($row);
      echo "<tr>\n
           <td width='8%'><div class='item'>$date</div></td>\n
           <td width='2%'><div class='item'></div></td>\n
           <td width='20%'><div class='item'>$charactername</div></td>\n
           <td width='2%'></td>\n
           <td width='68%'><div class='item'>$results</div></td>\n
           </tr>\n";
      echo "<tr><td colspan='5'><hr></td></tr>\n";
    }
    echo "</table>\n";
  ?>
</div>