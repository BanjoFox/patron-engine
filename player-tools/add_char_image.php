<?php include '/var/www/shadowsofthebayou.com/site-inc/header.php'; ?>

<div id="wrapper">
    <div id="title"></div><!-- title close -->

    <div class="container">

        <div class="column-nav">
            <?php require '/var/www/shadowsofthebayou.com/site-inc/check-access.php'; ?>
        </div><!-- menu close -->

        <div class="column-main">
            <?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
            <?php
            $query = "SELECT log_name FROM `game_data` WHERE (account_type='Character' AND deleted<>'Yes' AND playername='$_COOKIE[logname]' AND char_type<>'RET') ORDER BY log_name";
$result = mysqli_query($connection, $query)
    or die("Couldn't not execute query.<br \>" . mysqli_error($connection) . "<br \>");
$option = "<select name=\"char_name\" class='form'><option value=\"\" class='form'></option>";
while ($row = mysqli_fetch_array($result)) {
    $option = "$option <option value=\"$row[log_name]\">$row[log_name]</option>";
}
$option = "$option </select>";
?>
            <table>
                <caption>
                    <div><img src="../site-img/addcharpic.gif"></div>
                </caption>
                <tr>
                    <td>
                        <div class="itemsm">Choose you character from the drop down then use the browse button to search
                            for your image. You may only upload an image in GIF, JPG or JPEG formats and the image must
                            be smaller then 1mb in size or it will fail, the image will then show in your characters
                            profile. Please upload only one image for your character, and use <a href="guide.jpg"
                                target="_blank">this</a> template for size so it will look correct with the rest of the
                            profile. If you use a different size it WILL resize your image. If you have problems or
                            questions contact Poobah via fpm for assistance.</div>
                    </td>
                </tr>
            </table>
            <?php
//define a maxim size for the uploaded images in Kb
define("MAX_SIZE", "1024");
//This function reads the extension of the file. It is used to determine if the file is an image by checking the extension.
function getExtension($str)
{
    $i = strrpos($str, ".");
    if (!$i) {
        return "";
    }
    $l = strlen($str) - $i;
    $ext = substr($str, $i + 1, $l);
    return $ext;
}
/**
* This variable is used as a flag. The value is initialized with 0 (meaning no error found)
and it will be changed to 1 if an error occures.
If the error occures the file will not be uploaded.
**/
$errors = '0';
//checks if the form has been submitted
if (isset($_POST['Submit'])) {
    //reads the name of the file the user submitted for uploading
    $image = $_FILES['image']['name'];
    //if it is not empty
    if ($image) {
        //get the original name of the file from the clients machine
        $filename = stripslashes($_FILES['image']['name']);
        //get the extension of the file in a lower case format
        $extension = getExtension($filename);
        $extension = strtolower($extension);
        //if it is not a known extension, we will suppose it is an error and will not upload the file,
        //otherwise we will do more tests
        if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "gif") && ($extension != "png")) {
            //print error message
            echo '<div class="error" align="center">Unallowed extension!</div>';
            $errors = '1';
        } else {
            //get the size of the image in bytes
            //$_FILES['image']['tmp_name'] is the temporary filename of the file
            //in which the uploaded file was stored on the server
            $size = filesize($_FILES['image']['tmp_name']);
            //compare the size with the maxim size we defined and print error if bigger
            if ($size > MAX_SIZE * 1024) {
                echo '<div class="error" align="center">You have exceeded the size limit!</div>';
                $errors = '1';
            } else {
                //we will give an unique name, for example the time in unix time format
                $image_name = time() . '.' . $extension;
                //the new name will be containing the full path where will be stored (images folder)
                $newname = "char_images/" . $image_name;
                //we verify if the image has been uploaded, and print error instead
                $copied = copy($_FILES['image']['tmp_name'], $newname);
                if (!$copied) {
                    echo '<div class="error" align="center">Image Uploaded Unsuccessfull!</div>';
                    $errors = 1;
                }
            }
        }
    }
    //If no errors registred, print the success message
    if (isset($_POST['Submit']) && !$errors && $_POST['char_name'] <> "") {
        $query = "UPDATE game_data SET char_image='$newname' WHERE (log_name='$_POST[char_name]')";
        $result = mysqli_query($connection, $query)
            or die('<p>Could not link image to character.<br \>' . mysqli_error($connection) . '</p><br \>');
        echo "<br \><div class='success' >Picture uploaded successfully for '$_POST[char_name]'.</div>";
    }
}
?>
            <form name="newadd" method="post" enctype="multipart/form-data" action="">
                <table width="75%" align="center">
                    <tr>
                        <td>
                            <div class='pageitem'>Choose Character:</div>
                        </td>
                        <td>
                            <div class='item'><?php echo $option ?></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class='pageitem'>Choose Image:</div>
                        </td>
                        <td>
                            <div class='item'><input type="file" name="image" class="form"></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="center"><input name="Submit" type="submit" value="Upload Image" class="form">
                            </div>
                        </td>
                    </tr>
                </table>
            </form>

        </div><!-- content close -->


    </div><!-- container close -->
</div><!-- wrapper close -->