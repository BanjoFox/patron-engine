<?php include '/var/www/shadowsofthebayou.com/site-inc/header.php'; ?>

<div id="wrapper">
<div id="title"></div><!-- title close -->

<div class="container">

       <div class="column-nav"><?php require '/var/www/shadowsofthebayou.com/site-inc/check-access.php'; ?>

       </div><!-- menu close -->

       <div class="column-main">

<?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
<?php
/* Check for password */
if ($_POST['fpassword'] == "") {
    echo "<div  class='error'>You must enter a password.</div><br \><div ><a href='javascript:history.back()'>Try Again</a></div>";
    $_POST = null;
    exit();
}
/* Check that password is at least 5 characters long, change if you desire more or less, max for field is 32  */
if (strlen(trim($_POST['fpassword'])) < 5) {
    echo "<div  class='error'>Password must be at least five characters.</div><br \><div ><a href='javascript:history.back()'>Try Again</a></div>";
    $_POST = null;
    exit();
}
/*  Check that password and confirmed password match */
if ($_POST['fpassword'] != $_POST['confirmpw']) {
    echo "<div  class='error'>Password and retype password do not match.</div><br \><div ><a href='javascript:history.back()'>Try Again</a></div>";
    $_POST = null;
    exit();
}
/* Check for email address */
if ($_POST['email'] == "") {
    echo "<div  class='error'>You must enter an e-mail address.</div><br \><div ><a href='javascript:history.back()'>Try Again</a></div>";
    $_POST = null;
    exit();
}
/* Check for correct email format, correct format is ?????@???.??? or ?????@???.?? */
if (!preg_match('/^[A-z0-9_\-]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z.]{2,4}$/', $_POST['email'])) {
    echo "<div  class='error'>E-mail address format is not valid.</div><br \><div ><a href='javascript:history.back()'>Try Again</a></div>";
    $_POST = null;
    exit();
}
$query = "UPDATE game_data SET password=\"$_POST[fpassword]\",email=\"$_POST[email]\",show_email=\"$_POST[show_email]\" WHERE (log_name=\"$_COOKIE[logname]\")";
$result = mysqli_query($connection, $query)
 or die("Could not update profile.<br \>".mysqli_error($connection)."<br \>");

//$query = "UPDATE flashchat_users SET password=\"$_POST['fpassword']\" WHERE (login=\"$_COOKIE[logname]\")";
//$result = mysqli_query($connection, $query)
// or die ("Could not update flashchat password.<br \>".mysqli_error($connection)."<br \>");

echo "<div  class='pageitem'>Your member profile information has been updated.<br \>Please make a note of your new password if you changed it.</div>";
?>


              </div><!-- content close -->


</div><!-- container close -->
</div><!-- wrapper close -->
