<title></title>
<link rel=stylesheet href="../layout.css" TYPE="text/css">
<div id="pagewrapper">
  <?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
  <div><img src="../site-img/editcharprof.gif"></div>
  <br \>
  <?php
  $query = "SELECT * FROM `game_data` WHERE (id='$_POST[id]')";
  $result = mysqli_query($connection, $query)

    or die("<div  class='error'>Couldn't get character data.</div>");
  $data = mysqli_fetch_array($result);

  {
    ?>
    <div class="center">
      <form name="form1" method="post" action="edit-prof.php">
        <input name="char_name" type="hidden" id="char_name" value="<?php echo $data['log_name'] ?>">
        <input name="id" type="hidden" id="id" value="<?php echo $data['id'] ?>">

        <table width="75%">
          <caption><span align="center" class='pagetopic'>Update Character Profile Below</span></caption>
          <tr>
            <td width="24%">
              <div class="pageitem">Name</div>
            </td>
            <td width="76%">
              <div class="pageitem"><?php echo $data['log_name'] ?></div>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <hr />
            </td>
          </tr>
          <tr>
            <td>
              <div class='item'>Apparent Age</div>
            </td>
            <td>
              <div class='item'><?php echo $data['apparentage'] ?></div>
            </td>
          </tr>
          <tr>
            <td>
              <div class='item'>Appearance</div>
            </td>
            <td>
              <div class='item'><?php echo $data['appearance'] ?></div>
            </td>
          </tr>
          <tr>
            <td>
              <div class='item'>Gender</div>
            </td>
            <td>
              <div class='item'><?php echo $data['gender'] ?></div>
            </td>
          </tr>
          <tr>
            <td>
              <div class='item'>Sanctioned</div>
            </td>
            <td>
              <div class='item'><?php echo $data['sanctioned'] ?></div>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <hr />
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>
              <center>
                <div class='warning'>When creating you profile do not use any double quotes ( " ) use single ( ' ). If you
                  do the profile will be ignored and not added, if you are editing an existing profile, be sure to swap
                  out all double quotes ( " ) or single ( ' ) or you will lose the profile.</div>
              </center>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <hr />
            </td>
          </tr>
          <tr>
            <td>
              <div class='item'>Profile</div>
            </td>
            <td><textarea name="profile" cols="70" rows="30" class="form"
                id="profile"><?php echo $data['profile'] ?></textarea></td>
          </tr>
          <tr>
            <td colspan="2">
              <hr width="75%" />
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <div class="center"><input name="Select" type="submit" id="Select" value="Update" class="form" /></div>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <hr />
            </td>
          </tr>
        </table>
      </form>
    </div>
    <?php
  }
  ?>
</div>