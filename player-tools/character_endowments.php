<?php
include '/var/www/shadowsofthebayou.com/site-inc/header.php';
$currentUser = $_COOKIE['logname'];
?>

<div id="wrapper">
    <div id="title"></div><!-- title close -->

    <div class="container">

        <div class="column-nav">
            <?php require '/var/www/shadowsofthebayou.com/site-inc/check-access.php'; ?>
        </div><!-- menu close -->

        <div class="column-main">
            <div style="padding-bottom: 2em;">
                <h2 style="text-align: center;">Endowment Generator</h2>
                <p>How to Use:
                    <ul>
                        <li>Select character type</li>
                        <li>Click Generate Endowment</li>
                        <li>View Endowments</li>
                    </ul>
                    <strong>NOTE:</strong> You may only generate one endowment per character type per day!<br /><br />
                    <strong class="error" style="text-align: center;">SPECIAL NOTE FOR CRU:</strong><br />
                    Refreshing the page button may have HIGHLY UNEXPECTED results.
                </p>
                <?php
                include '/var/www/shadowsofthebayou.com/endowments/endowment_generator.php';
                echo "Your new endowment is: <span id='endowment'>" . $endowmentResult . "</span>";
                ?>
            </div>
            <div>
                <?php
                include '/var/www/shadowsofthebayou.com/endowments/endowment_tables.php';
                ?>
            </div>
        </div><!-- content close -->
    </div><!-- container close -->
</div><!-- wrapper close -->