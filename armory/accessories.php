<link rel=stylesheet href="../layout.css" TYPE="text/css">

<div id="pagewrapper">

<center>
<table>
  <caption><div class='pagetopic'>The Armory</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='item'><a href="melee.php">Melee Weapons</a></div></td>
              <td><div class='item'><a href="firearms.php">Firearms</a></div></td>
              <td><div class='item'><a href="armor.php">Armor</div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="projectile.php">Projectile Weapons</a></div></td>
              <td><div class='item'><a href="accessories.php">Firearm Accessories</a></div></td>
              <td><div class='item'><a href="explosives.php">Explosives</a></div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="ammo.php">Special Ammo</a></div></td>
              <td><div class='item'></div></td>
              <td><div class='item'><a href="special.php">Special Weapons</a></div></td>
            </tr>
        </table>
    </td>
  </tr>
</table>
<br \>
<hr width='75%'>
<br \>

<table>
  <caption><div class='pagetopic'>Firearm Accessories</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='itemb'>Type:</div></td>
              <td><div class='itemb'>Diff Reduction:</div></td>
              <td><div class='itemb'>Notes:</div></td>
            </tr>

           <tr>
              <td><div class='item'>Collapsing Stock</div></td>
              <td><div class='item'>-</div></td>
              <td><div class='itemsm'>Reduce Conceal 1 step for Rifles, SMG (Large), Assault Rifles and Shotguns (normal and semi-auto)</div></td>
            </tr>

           <tr>
              <td><div class='item'>Laser Sight</div></td>
              <td><div class='item'>-1</div></td>
              <td><div class='itemsm'>After 1 turn aiming</div></td>
            </tr>

           <tr>
              <td><div class='item'>Night Vision Scope</div></td>
              <td><div class='item'>-1</div></td>
              <td><div class='itemsm'>After 1 turn aiming, otherwise no pens for darkness</div></td>
            </tr>

           <tr>
              <td><div class='item'>Sniper Scope</div></td>
              <td><div class='item'>-2</div></td>
              <td><div class='itemsm'>After 2 turns aiming, otherwise doubles range</div></td>
            </tr>

           <tr>
              <td><div class='item'>Silencer</div></td>
              <td><div class='item'>0</div></td>
              <td><div class='itemsm'>Increases noticing diff +1 per range</div></td>
            </tr>

           <tr>
              <td><div class='item'>Thermal Scope</div></td>
              <td><div class='item'>-1</div></td>
              <td><div class='itemsm'>After 1 turns aiming, otherwise no pens for darkness, useless against vampires</div></td>
            </tr>

        </table>
    </td>
  </tr>
</table>
</center>
<br \>
</div>