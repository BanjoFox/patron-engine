<link rel=stylesheet href="../layout.css" TYPE="text/css">

<div id="pagewrapper">

<center>

<table>
  <caption><div class='pagetopic'>The Armory</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='item'><a href="melee.php">Melee Weapons</a></div></td>
              <td><div class='item'><a href="firearms.php">Firearms</a></div></td>
              <td><div class='item'><a href="armor.php">Armor</div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="projectile.php">Projectile Weapons</a></div></td>
              <td><div class='item'><a href="accessories.php">Firearm Accessories</a></div></td>
              <td><div class='item'><a href="explosives.php">Explosives</a></div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="ammo.php">Special Ammo</a></div></td>
              <td><div class='item'></div></td>
              <td><div class='item'><a href="special.php">Special Weapons</a></div></td>
            </tr>
        </table>
    </td>
  </tr>
</table>
<br \>
<hr width='75%'>
<br \>

<table>
  <caption><div class='pagetopic'>Bows</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='itemb'>Type:</div></td>
              <td><div class='itemb'>Damage:</div></td>
              <td><div class='itemb'>Range:</div></td>
              <td><div class='itemb'>Rate:</div></td>
              <td><div class='itemb'>Mag:</div></td>
              <td><div class='itemb'>Conceal:</div></td>
              <td><div class='itemb'>Notes:</div></td>
            </tr>
       
           <tr>
              <td><div class='item'>Short Bow</div></td>
              <td><div class='item'>4</div></td>
              <td><div class='item'>60</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'>#1</div></td>
            </tr>

           <tr>
              <td><div class='item'>Hunting Bow</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>100</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'>#1</div></td>
            </tr>

           <tr>
              <td><div class='item'>Long Bow</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>120</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'>#1</div></td>
            </tr>

           <tr>
              <td><div class='item'>Crossbow</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>90</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>T</div></td>
              <td><div class='itemsm'>#2</div></td>
            </tr>

           <tr>
              <td><div class='item'>Crossbow (Commando)</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>60</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>#2, #3</div></td>
            </tr>

           <tr>
              <td><div class='item'>Crossbow (Hand)</div></td>
              <td><div class='item'>4</div></td>
              <td><div class='item'>50</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>T</div></td>
              <td><div class='itemsm'>#4</div></td>
            </tr>

           <tr>
              <td><div class='item'>Crossbow (Heavy)</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>100</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'>#2</div></td>
            </tr>

        </table>
    </td>
  </tr>
</table>
<br \><br \><br \>
<table>
  <caption><div class='pagetopic'>Thrown Weapons</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='itemb'>Type:</div></td>
              <td><div class='itemb'>Diff:</div></td>
              <td><div class='itemb'>Damage:</div></td>
              <td><div class='itemb'>Conceal:</div></td>
              <td><div class='itemb'>Notes:</div></td>
            </tr>
       
            <tr>
              <td><div class='item'>Knife (when thrown)</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>Strength +1/L</div></td>
              <td><div class='item'>P</div></td>
              <td><div class='itemsm'></div></td>
            </tr>

           <tr>
              <td><div class='item'>Shuriken</div></td>
              <td><div class='item'>7</div></td>
              <td><div class='item'>3/L</div></td>
              <td><div class='item'>P</div></td>
              <td><div class='itemsm'></div></td>
            </tr>

           <tr>
              <td><div class='item'>Spear</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +1/L</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'></div></td>
            </tr>

           <tr>
              <td><div class='item'>Stone (large, thrown)</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +2/B</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>Fist sized, adjust damange for smaller or larger</div></td>
            </tr>

            <tr>
              <td><div class='item'>Tomahawk (when thrown)</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +1/L</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'></div></td>
            </tr>

        </table>
    </td>
  </tr>
</table>
<br \><br \>
<table width='70%'>
<caption><div class='pagetopic'>Projectile Weapon Notes:</div></caption>
  <tr>
    <td><div class="itemsm">
#1 A Character with Archery 3+ can draw, notch and loose in one action, otherwise move action must be used to draw and notch<br \>
#2 Requires two actions to reload, then another to load and ready, split actions cannot be used for this<br \>
#3 Collapsible, takes one turn to deploy, then another to load and ready, split actions cannot be used for this<br \>
#4 Requires one action to reload, then another to ready, split actions cannot be used for this<br \>
    </div></td>
  </tr>
</table>
</center>
<br \>
</div>