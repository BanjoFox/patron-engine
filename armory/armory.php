<link rel=stylesheet href="../layout.css" TYPE="text/css">

<div id="pagewrapper">
<center>

<table>
  <caption><div class='pagetopic'>The Armory</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='item'><a href="melee.php">Melee Weapons</a></div></td>
              <td><div class='item'><a href="firearms.php">Firearms</a></div></td>
              <td><div class='item'><a href="armor.php">Armor</div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="projectile.php">Projectile Weapons</a></div></td>
              <td><div class='item'><a href="accessories.php">Firearm Accessories</a></div></td>
              <td><div class='item'><a href="explosives.php">Explosives</a></div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="ammo.php">Special Ammo</a></div></td>
              <td><div class='item'></div></td>
              <td><div class='item'><a href="special.php">Special Weapons</a></div></td>
            </tr>
        </table>
    </td>
  </tr>
</table>
<br \>
<hr width='75%'>

<table border='0' cellspacing='1' cellpadding='1' width='50%'>
   <tr>
      <td><div class='item'>
Select one of the categories above to see a list of items that might be found in the city. Some have example images.<br \><br \>

Use this source for weapons and stats, venue special items such as Klaives are not listed.<br \><br \>

Not everything listed will be accessible, some are here just for cStaff referance, many items will require substantial cash and or Influence to acquire if it can be had at all. </div>
      </td>
   </tr>
</table>


<hr width='75%'>

</center>
</div>
