<link rel=stylesheet href="../layout.css" TYPE="text/css">

<div id="pagewrapper">


<center>

<table>
  <caption><div class='pagetopic'>The Armory</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='item'><a href="melee.php">Melee Weapons</a></div></td>
              <td><div class='item'><a href="firearms.php">Firearms</a></div></td>
              <td><div class='item'><a href="armor.php">Armor</div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="projectile.php">Projectile Weapons</a></div></td>
              <td><div class='item'><a href="accessories.php">Firearm Accessories</a></div></td>
              <td><div class='item'><a href="explosives.php">Explosives</a></div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="ammo.php">Special Ammo</a></div></td>
              <td><div class='item'></div></td>
              <td><div class='item'><a href="special.php">Special Weapons</a></div></td>
            </tr>
        </table>
    </td>
  </tr>
</table>
<br \>
<hr width='75%'>
<br \>

<table>
  <caption><div class='pagetopic'>Explosives</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='itemb'>Type:</div></td>
              <td><div class='itemb'>Blast Area:</div></td>
              <td><div class='itemb'>Blast Power:</div></td>
              <td><div class='itemb'>Burn:</div></td>
              <td><div class='itemb'>Notes:</div></td>
            </tr>

           <tr>
              <td><div class='item'>Blasting Powder</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>Yes</div></td>
              <td><div class='itemsm'>#1</div></td>
            </tr>

           <tr>
              <td><div class='item'>Concussion Grenade</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>8</div></td>
              <td><div class='item'>No</div></td>
              <td><div class='itemsm'>#2</div></td>
            </tr>

           <tr>
              <td><div class='item'>Dynamite</div></td>
              <td><div class='item'>4</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Yes</div></td>
              <td><div class='itemsm'>#3</div></td>
            </tr>

           <tr>
              <td><div class='item'>Exploding Car</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>12</div></td>
              <td><div class='item'>Maybe</div></td>
              <td><div class='itemsm'>#4</div></td>
            </tr>

           <tr>
              <td><div class='item'>IED</div></td>
              <td><div class='item'>3-5</div></td>
              <td><div class='item'>8-15</div></td>
              <td><div class='item'>Maybe</div></td>
              <td><div class='itemsm'>#5</div></td>
            </tr>

           <tr>
              <td><div class='item'>Fragmentation Grenade</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>12</div></td>
              <td><div class='item'>No</div></td>
              <td><div class='itemsm'></div></td>
            </tr>

           <tr>
              <td><div class='item'>Gasoline (1 gal)</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>2</div></td>
              <td><div class='item'>Yes</div></td>
              <td><div class='itemsm'>#4</div></td>
            </tr>

           <tr>
              <td><div class='item'>Molotov Cocktail</div></td>
              <td><div class='item'>2</div></td>
              <td><div class='item'>8</div></td>
              <td><div class='item'>Yes</div></td>
              <td><div class='itemsm'>#4</div></td>
            </tr>

           <tr>
              <td><div class='item'>Plastique</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>10</div></td>
              <td><div class='item'>No</div></td>
              <td><div class='itemsm'>#6</div></td>
            </tr>

           <tr>
              <td><div class='item'>Suicide Bomb</div></td>
              <td><div class='item'>4-6</div></td>
              <td><div class='item'>10-15</div></td>
              <td><div class='item'>Maybe</div></td>
              <td><div class='itemsm'>#7</div></td>
            </tr>

           <tr>
              <td><div class='item'>Tear Gas</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>4</div></td>
              <td><div class='item'>No</div></td>
              <td><div class='itemsm'>#8</div></td>
            </tr>

           <tr>
              <td><div class='item'>White Phosperous</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>12</div></td>
              <td><div class='item'>Yes</div></td>
              <td><div class='itemsm'>#4, #9</div></td>
            </tr>

        </table>
    </td>
  </tr>
</table>
<br \><br \>
<table width='65%'>
<caption><div class='pagetopic'>Explosives Notes:</div></caption>
  <tr>
    <td><div class="itemsm">
#1: Loose Powder, creates blinding flash as well, includes black powder<br \>
#2: Bashing Damage<br \>
#3: Per Stick<br \>
#4: Aggravated Damage<br \>
#5: Might be incindiary if so make Agg as well, might be armor piercing (1/2 armor)<br \>
#6: Per 1/2 Kilo<br \>
#7: Might be incindiary if so make Agg as well. Kills wearer<br \>
#8: Bashing Damage only, no roll over, reduce die pools 2 per 'damage' success, double for acute / heightened senses involving vision or smell<br \>
#9: Illuminates wide area, damage from direct contact<br \>
    </div></td>
  </tr>
</table>
</center>
<br \>
</div>
