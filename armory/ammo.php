<link rel=stylesheet href="../layout.css" TYPE="text/css">

<div id="pagewrapper">


<center>

<table>
  <caption><div class='pagetopic'>The Armory</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='item'><a href="melee.php">Melee Weapons</a></div></td>
              <td><div class='item'><a href="firearms.php">Firearms</a></div></td>
              <td><div class='item'><a href="armor.php">Armor</div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="projectile.php">Projectile Weapons</a></div></td>
              <td><div class='item'><a href="accessories.php">Firearm Accessories</a></div></td>
              <td><div class='item'><a href="explosives.php">Explosives</a></div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="ammo.php">Special Ammo</a></div></td>
              <td><div class='item'></div></td>
              <td><div class='item'><a href="special.php">Special Weapons</a></div></td>
            </tr>
        </table>
    </td>
  </tr>
</table>
<br \>
<hr width='75%'>
<br \>

<table>
  <caption><div class='pagetopic'>Special Ammunition</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='itemb'>Type:</div></td>
              <td><div class='itemb'>Damage:</div></td>
              <td><div class='itemb'>Effects:</div></td>
            </tr>

           <tr>
              <td><div class='item'>AG (Silver)</div></td>
              <td><div class='item'>L*</div></td>
              <td><div class='itemsm'>Normal for weapon, Aggravated and not normally soakable by Shifters</div></td>
           </tr>


           <tr>
              <td><div class='item'>Flechettes</div></td>
              <td><div class='item'>7/L</div></td>
              <td><div class='itemsm'>Modern Armor rating -2, Archaic armor rating doubled, shotguns only</div></td>
           </tr>

           <tr>
              <td><div class='item'>Incendiary</div></td>
              <td><div class='item'>4/A</div></td>
              <td><div class='itemsm'>2/A for one turn after hit, ignites flammable objects or materials</div></td>
            </tr>

           <tr>
              <td><div class='item'>Non Lethal (Rubber)</div></td>
              <td><div class='item'>B</div></td>
              <td><div class='itemsm'>Changes firearm damage to bashing except for head shots.</div></td>
            </tr>

           <tr>
              <td><div class='item'>Tear Gas Round</div></td>
              <td><div class='item'>3/B</div></td>
              <td><div class='itemsm'>3 meter area, see Tear Gas in explosives for effects</div></td>
            </tr>

           <tr>
              <td><div class='item'>Armor Piercing</div></td>
              <td><div class='item'>L</div></td>
              <td><div class='itemsm'>Does normal damage for weapon but Armor Rating is -2</div></td>
            </tr>

        </table>
    </td>
  </tr>
</table>
</center>
<br \>
</div>