<link rel=stylesheet href="../layout.css" TYPE="text/css">

<div id="pagewrapper">

<center>

<table>
  <caption><div class='pagetopic'>The Armory</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='item'><a href="melee.php">Melee Weapons</a></div></td>
              <td><div class='item'><a href="firearms.php">Firearms</a></div></td>
              <td><div class='item'><a href="armor.php">Armor</div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="projectile.php">Projectile Weapons</a></div></td>
              <td><div class='item'><a href="accessories.php">Firearm Accessories</a></div></td>
              <td><div class='item'><a href="explosives.php">Explosives</a></div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="ammo.php">Special Ammo</a></div></td>
              <td><div class='item'></div></td>
              <td><div class='item'><a href="special.php">Special Weapons</a></div></td>
            </tr>
        </table>
    </td>
  </tr>
</table>
<br \>
<hr width='75%'>
<br \>

<table>
  <caption><div class='pagetopic'>Special Weapons</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='itemb'>Type:</div></td>
              <td><div class='itemb'>Damage:</div></td>
              <td><div class='itemb'>Range:</div></td>
              <td><div class='itemb'>Rate:</div></td>
              <td><div class='itemb'>Mag:</div></td>
              <td><div class='itemb'>Conceal:</div></td>
              <td><div class='itemb'>Notes:</div></td>
            </tr>

           <tr>
              <td><div class='item'>Stun Gun / Taser</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>0/2</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>2/5</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>#1, Stun Guns require melee attack to deploy and have 2 charges<br \>
                                      Tazers use a firearm attack and can apply charge multiple times if leads are not removed<br \>
                                      Vampires are immune to Stun Guns / Tasers</div></td>
            </tr>

           <tr>
              <td><div class='item'>Pepper / Bear Spray</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>2/5</div></td>
              <td><div class='item'>P/J</div></td>
              <td><div class='itemsm'>#1, Pepper Spray are small self defence measures, require firearms attack and have 2 charges<br \>
                                      Bear Spray is a larger can, requies firearm attack and have 5 charges</div></td>

            </tr>
        </table>
    </td>
  </tr>
</table>
<br \><br \>
<table width='65%'>
<caption><div class='pagetopic'>Special Notes:</div></caption>
  <tr>
    <td><div class="itemsm">
#1: Bashing Damage only, no roll over, reduce die pools 2 per 'damage' success, double for acute / heightened senses involving vision or smell<br \>
    </div></td>
  </tr>
</table>
</center>


<br \>


</center>
</div>