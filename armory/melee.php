<link rel=stylesheet href="../layout.css" TYPE="text/css">

<div id="pagewrapper">

<center>

<table>
  <caption><div class='pagetopic'>The Armory</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='item'><a href="melee.php">Melee Weapons</a></div></td>
              <td><div class='item'><a href="firearms.php">Firearms</a></div></td>
              <td><div class='item'><a href="armor.php">Armor</div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="projectile.php">Projectile Weapons</a></div></td>
              <td><div class='item'><a href="accessories.php">Firearm Accessories</a></div></td>
              <td><div class='item'><a href="explosives.php">Explosives</a></div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="ammo.php">Special Ammo</a></div></td>
              <td><div class='item'></div></td>
              <td><div class='item'><a href="special.php">Special Weapons</a></div></td>
            </tr>
        </table>
    </td>
  </tr>
</table>
<br \>
<hr width='75%'>
<br \>

<table>
  <caption><div class='pagetopic'>Axes</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='itemb'>Type:</div></td>
              <td><div class='itemb'>Difficulty:</div></td>
              <td><div class='itemb'>Damage/Type:</div></td>
              <td><div class='itemb'>Conceal:</div></td>
              <td><div class='itemb'>Notes:</div></td>
            </tr>

           <tr>
              <td><div class='item'>Axe</div></td>
              <td><div class='item'>7</div></td>
              <td><div class='item'>Strength +2/L</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'></div></td>
            </tr>

           <tr>
              <td><div class='item'>Great Axe</div></td>
              <td><div class='item'>7</div></td>
              <td><div class='item'>Strength +6/L</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'>#1</div></td>
            </tr>

           <tr>
              <td><div class='item'>Hatchet</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +1/L</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'></div></td>
            </tr>

           <tr>
              <td><div class='item'>Pole Arm</div></td>
              <td><div class='item'>7</div></td>
              <td><div class='item'>Strength +3/L</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'>#2</div></td>
            </tr>

           <tr>
              <td><div class='item'>Tomahawk</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +2/L</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>Thrown is Str+1/L Dam</div></td>
            </tr>

        </table>
    </td>
  </tr>
</table>
<br \><br \><br \>
<table>
  <caption><div class='pagetopic'>Blades</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='itemb'>Type:</div></td>
              <td><div class='itemb'>Difficulty:</div></td>
              <td><div class='itemb'>Damage/Type:</div></td>
              <td><div class='itemb'>Conceal:</div></td>
              <td><div class='itemb'>Notes:</div></td>
            </tr>

           <tr>
              <td><div class='item'>Great Sword</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>Strength +6/L</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'>#1</div></td>
            </tr>

           <tr>
              <td><div class='item'>Hook Sword(s)</div></td>
              <td><div class='item'>7</div></td>
              <td><div class='item'>Strength +3/L</div></td>
              <td><div class='item'>T</div></td>
              <td><div class='itemsm'>Used in pairs, #2, #5, #6</div></td>
            </tr>

           <tr>
              <td><div class='item'>Katana</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +2/L</div></td>
              <td><div class='item'>T</div></td>
              <td><div class='itemsm'>#4</div></td>
            </tr>

           <tr>
              <td><div class='item'>Knife</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>Strength +1/L</div></td>
              <td><div class='item'>P</div></td>
              <td><div class='itemsm'>Thrown is Str/L Dam</div></td>
            </tr>

           <tr>
              <td><div class='item'>Sai</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>Strength +1/L</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>#5, #6</div></td>
            </tr>

           <tr>
              <td><div class='item'>Stiletto</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>Strength +1/L</div></td>
              <td><div class='item'>P</div></td>
              <td><div class='itemsm'>#3</div></td>
            </tr>

           <tr>
              <td><div class='item'>Short Sword</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>Strength +2/L</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>Includes Machete</div></td>
            </tr>

           <tr>
              <td><div class='item'>Sword</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +2/L</div></td>
              <td><div class='item'>T</div></td>
              <td><div class='itemsm'>Long or Broad</div></td>
            </tr>
        </table>
    </td>
  </tr>
</table>
<br \><br \><br \>
<table>
  <caption><div class='pagetopic'>Clubbing</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='itemb'>Type:</div></td>
              <td><div class='itemb'>Difficulty:</div></td>
              <td><div class='itemb'>Damage/Type:</div></td>
              <td><div class='itemb'>Conceal:</div></td>
              <td><div class='itemb'>Notes:</div></td>
            </tr>

           <tr>
              <td><div class='item'>Baseball Bat</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>Strength +2/B</div></td>
              <td><div class='item'>T</div></td>
              <td><div class='itemsm'></div></td>
            </tr>

           <tr>
              <td><div class='item'>Crowbar</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +1/L</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'></div></td>
            </tr>

           <tr>
              <td><div class='item'>Huge Spiked Club</div></td>
              <td><div class='item'>7</div></td>
              <td><div class='item'>Strength +4/L</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'>#1</div></td>
            </tr>

           <tr>
              <td><div class='item'>Iron Staff</div></td>
              <td><div class='item'>7</div></td>
              <td><div class='item'>Strength +3/L</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'>#1</div></td>
            </tr>

           <tr>
              <td><div class='item'>Mace</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +2/L</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'></div></td>
            </tr>

           <tr>
              <td><div class='item'>Nunchaku</div></td>
              <td><div class='item'>7</div></td>
              <td><div class='item'>Strength +2/B</div></td>
              <td><div class='item'>T</div></td>
              <td><div class='itemsm'>#5, #7, #9</div></td>
            </tr>

           <tr>
              <td><div class='item'>Riot Baton</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>Strength +1/B</div></td>
              <td><div class='item'>T</div></td>
              <td><div class='itemsm'>Includes improvised or average bludgeoning weapons</div></td>
            </tr>

           <tr>
              <td><div class='item'>Spiked Club</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +2/L</div></td>
              <td><div class='item'>T</div></td>
              <td><div class='itemsm'>Includes Morning Stars</div></td>
            </tr>

           <tr>
              <td><div class='item'>Stone (large)</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +2/B</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>Fist sized, adjust damange for smaller or larger</div></td>
            </tr>

           <tr>
              <td><div class='item'>Staff</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +1/B</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'></div></td>
            </tr>

        </table>
    </td>
  </tr>
</table>
<br \><br \><br \>
<table>
  <caption><div class='pagetopic'>Fist-Extention</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='itemb'>Type:</div></td>
              <td><div class='itemb'>Difficulty:</div></td>
              <td><div class='itemb'>Damage/Type:</div></td>
              <td><div class='itemb'>Conceal:</div></td>
              <td><div class='itemb'>Notes:</div></td>
            </tr>

           <tr>
              <td><div class='item'>Brass Knuckles</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength /L</div></td>
              <td><div class='item'>P</div></td>
              <td><div class='itemsm'></div></td>
            </tr>

            <tr>
              <td><div class='item'>Hand Claws (Small)</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +1/L</div></td>
              <td><div class='item'>P</div></td>
              <td><div class='itemsm'></div></td>
            </tr>

           <tr>
              <td><div class='item'>Hand Claws (Large)</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +2/L</div></td>
              <td><div class='item'>P</div></td>
              <td><div class='itemsm'></div></td>
            </tr>

           <tr>
              <td><div class='item'>Katar</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +2/L</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>#3, #5</div></td>
            </tr>

           <tr>
              <td><div class='item'>Sap</div></td>
              <td><div class='item'>4</div></td>
              <td><div class='item'>Strength +1/B</div></td>
              <td><div class='item'>P</div></td>
              <td><div class='itemsm'></div></td>
            </tr>

           <tr>
              <td><div class='item'>Spiked Gauntlet</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +1/L</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'></div></td>
            </tr>

           <tr>
              <td><div class='item'>War Fan</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>Strength +2/L</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>#5, #7</div></td>
            </tr>

           <tr>
              <td><div class='item'>Wind and Fire Wheel</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +3/L</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'>#6</div></td>
            </tr>

         </table>
    </td>
  </tr>
</table>
<br \><br \><br \>
<table>
  <caption><div class='pagetopic'>Whips and Chains</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='itemb'>Type:</div></td>
              <td><div class='itemb'>Difficulty:</div></td>
              <td><div class='itemb'>Damage/Type:</div></td>
              <td><div class='itemb'>Conceal:</div></td>
              <td><div class='itemb'>Notes:</div></td>
            </tr>

           <tr>
              <td><div class='item'>Barbed Cat</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength /L</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>#10</div></td>
            </tr>

           <tr>
              <td><div class='item'>Bullwhip</div></td>
              <td><div class='item'>7</div></td>
              <td><div class='item'>Strength +1/L</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>#10</div></td>
            </tr>

           <tr>
              <td><div class='item'>Chain</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>Strength /B</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>#10</div></td>
            </tr>

           <tr>
              <td><div class='item'>Chain Whip</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +1/L</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>#10</div></td>
            </tr>

           <tr>
              <td><div class='item'>Flogger</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +1/B</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>#10</div></td>
            </tr>

           <tr>
              <td><div class='item'>Kusarigama</div></td>
              <td><div class='item'>7</div></td>
              <td><div class='item'>Strength +3/B</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>#2, #6, #10</div></td>
            </tr>

           <tr>
              <td><div class='item'>Manriki-Gusari</div></td>
              <td><div class='item'>7</div></td>
              <td><div class='item'>Strength +2/L</div></td>
              <td><div class='item'>T</div></td>
              <td><div class='itemsm'>#2, #6, #10</div></td>
            </tr>

           <tr>
              <td><div class='item'>Whip</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength /L</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>#10</div></td>
            </tr>

      </table>
    </td>
  </tr>
</table>
<br \><br \><br \>
<table>
  <caption><div class='pagetopic'>Improvised</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='itemb'>Type:</div></td>
              <td><div class='itemb'>Difficulty:</div></td>
              <td><div class='itemb'>Damage/Type:</div></td>
              <td><div class='itemb'>Conceal:</div></td>
              <td><div class='itemb'>Notes:</div></td>
            </tr>

            <tr>
              <td><div class='item'>Brick</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +1/B</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>Includes sizable stones, thrown for Str/B dam</div></td>
            </tr>

           <tr>
              <td><div class='item'>Broken Bottle</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>Strength +1/L</div></td>
              <td><div class='item'>P</div></td>
              <td><div class='itemsm'>#8</div></td>
            </tr>

            <tr>
              <td><div class='item'>Chainsaw</div></td>
              <td><div class='item'>8</div></td>
              <td><div class='item'>Strength +7/L</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'>#9</div></td>
            </tr>

           <tr>
              <td><div class='item'>Chair</div></td>
              <td><div class='item'>7</div></td>
              <td><div class='item'>Strength +2/B</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'></div></td>
            </tr>

           <tr>
              <td><div class='item'>Table</div></td>
              <td><div class='item'>8</div></td>
              <td><div class='item'>Strength +3/B</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'>Including sofas, whole item not parts, #1</div></td>
            </tr>

         </table>
    </td>
  </tr>
</table>
<br \><br \>
<table width='50%'>
<caption><div class='pagetopic'>Melee Weapon Notes:</div></caption>
  <tr>
    <td><div class="itemsm">
#1: Two-Handed, heavy, requires Strength 3 to use<br \>
#2: Two-Handed, but can be used 1 handed with +2 Diff<br \>
#3: Penetrates up to 3 points of armor (not Fort, Lunas Armor and the like)<br \>
#4: May be used two-handed for +1 Damage<br \>
#5: +1 to die pool when used to block<br \>
#6: +3 to die pool for disarm attempts<br \>
#7: +1 difficulty to opponents block attempts<br \>
#8: Breaks after 3 uses.<br \>
#9: On a botch, user does damage to self<br \>
#10: May be used to entagle limb at +1 Diff<br \>
    </div></td>
  </tr>
</table>
</center>
<br \>
</div>