<link rel=stylesheet href="../layout.css" TYPE="text/css">

<div id="pagewrapper">

<center>

<table>
  <caption><div class='pagetopic'>The Armory</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='item'><a href="melee.php">Melee Weapons</a></div></td>
              <td><div class='item'><a href="firearms.php">Firearms</a></div></td>
              <td><div class='item'><a href="armor.php">Armor</div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="projectile.php">Projectile Weapons</a></div></td>
              <td><div class='item'><a href="accessories.php">Firearm Accessories</a></div></td>
              <td><div class='item'><a href="explosives.php">Explosives</a></div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="ammo.php">Special Ammo</a></div></td>
              <td><div class='item'></div></td>
              <td><div class='item'><a href="special.php">Special Weapons</a></div></td>
            </tr>
        </table>
    </td>
  </tr>
</table>
<br \>
<hr width='75%'>
<br \>

<table>
<caption><div class='pagetopic'>Armor</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='itemb'>Type:</div></td>
              <td><div class='itemb'>Rating:</div></td>
              <td><div class='itemb'>Dex Pen:</div></td>
            <tr>

           <tr>
              <td><div class='item'>Biker Jacket</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>-1</div></td>
            <tr>

           <tr>
              <td><div class='item'>Chain Mail</div></td>
              <td><div class='item'>4</div></td>
              <td><div class='item'>-2</div></td>
            <tr>

           <tr>
              <td><div class='item'>Concealed Kevlar Vest</div></td>
              <td><div class='item'>2</div></td>
              <td><div class='item'>0</div></td>
            <tr>

           <tr>
              <td><div class='item'>Cosplay Mail</div></td>
              <td><div class='item'>2</div></td>
              <td><div class='item'>-1</div></td>
            <tr>


           <tr>
              <td><div class='item'>Flack Vest</div></td>
              <td><div class='item'>4</div></td>
              <td><div class='item'>-2</div></td>
            <tr>

           <tr>
              <td><div class='item'>Full Plate</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>-2</div></td>
            <tr>

           <tr>
              <td><div class='item'>Kevlar Vest</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>-1</div></td>
            <tr>

           <tr>
              <td><div class='item'>Leather Armor</div></td>
              <td><div class='item'>2</div></td>
              <td><div class='item'>-1</div></td>
            <tr>

           <tr>
              <td><div class='item'>Leather Duster</div></td>
              <td><div class='item'>2</div></td>
              <td><div class='item'>-2</div></td>
            <tr>

           <tr>
              <td><div class='item'>Military Armor</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>-2</div></td>
            <tr>

           <tr>
              <td><div class='item'>Reinforced Clothing</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>0</div></td>
            <tr>

           <tr>
              <td><div class='item'>Riot Suit</div></td>
              <td><div class='item'>4</div></td>
              <td><div class='item'>-2</div></td>
            <tr>

           <tr>
              <td><div class='item'>Steel Breastplate</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>-2</div></td>
            <tr>

        </table>
    </td>
  </tr>
</table>
<br \><br \>
<table width='58%'>
<caption><div class='pagetopic'>Armor Notes:</div></caption>
  <tr>
    <td><div class="itemsm">
Biker Jacket: heavy think full sleeved jacket intended to protect during spills and slides on pavement<br \>
Chain Mail: Battle worthy steel medieval chain armor, with sleeves<br \>
Concealed Kevlar Vest: Light body armor concealable under a shirt, no sleeves<br \>
Cosplay Mail: Modern costume mail armor, aluminum or thinner steal rings, has sleeves<br \>
Flack Vest: Heavy older style bukly military and surplus body armor, no sleeves<br \>
Full Plate: Articulated steel knights armor total body protection<br \>
Kevlar Vest: Modern body armor can be concealed under jacket, no sleeves excellent mobility<br \>
Leather Armor: Battle worthy medieval thick and stiffened leather armor<br \>
Leather Duster: Full length to ankles, with sleeves. Hot and heavy during summer<br \>
Military Armor: Full chest, side and pelvis armor, heavy but decent range of motion<br \>
Reinforced Clothing: Custom (expensive) protective fabrics, includes good quality leather or boned corsets<br \>
Riot Suit: Police full protective suit typically paired with Riot shield<br \>
Steel Breastplate: Battle worthy steel medieval armor, no sleeves<br \>
    </div></td>
  </tr>
</table>
<br \><br \>
<table>
<caption><div class='pagetopic'>Shields</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='itemb'>Type:</div></td>
              <td><div class='itemb'>Rating:</div></td>
              <td><div class='itemb'>Dex Pen:</div></td>
              <td><div class='itemb'>Notes:</div></td>
            <tr>

           <tr>
              <td><div class='item'>Cloak</div></td>
              <td><div class='item'>1/+1</div></td>
              <td><div class='item'>0</div></td>
              <td><div class='itemsm'>Cannot be used to bash</div></td>
            <tr>

           <tr>
              <td><div class='item'>Metal Shield</div></td>
              <td><div class='item'>4/+2</div></td>
              <td><div class='item'>-2</div></td>
              <td><div class='itemsm'></div></td>
            <tr>

           <tr>
              <td><div class='item'>Riot Shield</div></td>
              <td><div class='item'>5/+2</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='itemsm'></div></td>
            <tr>

           <tr>
              <td><div class='item'>Trashcan Lid</div></td>
              <td><div class='item'>3/+1</div></td>
              <td><div class='item'>0</div></td>
              <td><div class='itemsm'></div></td>
            <tr>

           <tr>
              <td><div class='item'>Wooden Shield</div></td>
              <td><div class='item'>2/+2</div></td>
              <td><div class='item'>-1</div></td>
              <td><div class='itemsm'></div></td>
            <tr>

           <tr>
              <td><div class='item'>Buckler / Vambrace</div></td>
              <td><div class='item'>0</div></td>
              <td><div class='item'>0</div></td>
              <td><div class='itemsm'>Allows parrying one-handed melee weapons</div></td>
            <tr>
        </table>
    </td>
  </tr>
</table>
<br \><br \>
<table width='60%'>
<caption><div class='pagetopic'>Shield Notes:</div></caption>
  <tr>
    <td><div class="itemsm">
The shield rating is as follows: The first number is what is added to soaks if the shield is deployed and not avoided with a called shot (if possible), and the second number is added to the attackers difficulty when the shield is employed again unless avoided with a called shot (if possible)<br \><br \>
You can bash your opponent with a shield, doing Str + 1 Bashing<br \><br \>
Cloak: Thick heavy cloth used to discract and redirect, protection rating useless against bullets<br \>
Metal Shield: Battle worthy metal medieval shield, large and tend to be heavy<br \>
Riot Shield: Large plexi or metal shield with viewport, very tough and can be used for full cover<br \>
Trashcan Lid: Good old improvised metal trashcan lid, <br \>
Wooden Shield: Battle worthy wooden medieval shield, medium size meant to protect torso.<br \>
    </div></td>
  </tr>
</table>
<br \><br \><br \>
<caption><div class='pagetopic'>Helmets</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='itemb'>Type:</div></td>
              <td><div class='itemb'>Rating:</div></td>
              <td><div class='itemb'>Per Pen:</div></td>
            <tr>

           <tr>
              <td><div class='item'>Military Kevlar Helmet</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>-1</div></td>
            <tr>

           <tr>
              <td><div class='item'>Sport Helmet</div></td>
              <td><div class='item'>2</div></td>
              <td><div class='item'>-1</div></td>
            <tr>

         </table>
    </td>
  </tr>
</table>
<br \><br \>
<table width='60%'>
<caption><div class='pagetopic'>Helmet Notes:</div></caption>
  <tr>
    <td><div class="itemsm">
Military Kevlar Helmet: Military armor helmet<br \>
Sport: Includes motorcycle helmets, mostly designed to protect against impacts, half value against guns<br \>
    </div></td>
  </tr>
</table>
<br \>
</center>

</div>