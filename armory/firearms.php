<link rel=stylesheet href="../layout.css" TYPE="text/css">

<div id="pagewrapper">

<center>

<table>
  <caption><div class='pagetopic'>The Armory</div></caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='item'><a href="melee.php">Melee Weapons</a></div></td>
              <td><div class='item'><a href="firearms.php">Firearms</a></div></td>
              <td><div class='item'><a href="armor.php">Armor</div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="projectile.php">Projectile Weapons</a></div></td>
              <td><div class='item'><a href="accessories.php">Firearm Accessories</a></div></td>
              <td><div class='item'><a href="explosives.php">Explosives</a></div></td>
            </tr>

           <tr>
              <td><div class='item'><a href="ammo.php">Special Ammo</a></div></td>
              <td><div class='item'></div></td>
              <td><div class='item'><a href="special.php">Special Weapons</a></div></td>
            </tr>
        </table>
    </td>
  </tr>
</table>
<br \>
<hr width='75%'>
<br \>

<table width='75%'>
  <caption><div class='pagetopic'>Firearms</caption>
  <tr>
    <td>
        <table border='1' cellspacing='1' cellpadding='1'>
           <tr>
              <td><div class='itemb'>Type:</div></td>
              <td><div class='itemb'>Damage:</div></td>
              <td><div class='itemb'>Range:</div></td>
              <td><div class='itemb'>Rate:</div></td>
              <td><div class='itemb'>Mag:</div></td>
              <td><div class='itemb'>Conceal:</div></td>
              <td><div class='itemb'>Notes:</div></td>
              <td><div class='itemb'></div></td>
            </tr>
       
           <tr>
              <td><div class='item'>Revolver (Light)</div></td>
              <td><div class='item'>4</div></td>
              <td><div class='item'>12</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>P</div></td>
              <td><div class='itemsm'></div></td>
              <td><div class='item'><a href='img/lightrevolver.jpg' alt='Light Revolvers' target='_blank'>Sample</a></div></td>
            </tr>

           <tr>
              <td><div class='item'>Revolver (Heavy)</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>35</div></td>
              <td><div class='item'>2</div></td>
              <td><div class='item'>6</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'></div></td>
              <td><div class='item'><a href='img/heavyrevolver.jpg' alt='Heavy Revolvers' target='_blank'>Sample</a></div></td>
            </tr>

           <tr>
              <td><div class='item'>Semi-Auto Pistol (Light)</div></td>
              <td><div class='item'>4</div></td>
              <td><div class='item'>20</div></td>
              <td><div class='item'>4</div></td>
              <td><div class='item'>17+1</div></td>
              <td><div class='item'>P</div></td>
              <td><div class='itemsm'></div></td>
              <td><div class='item'><a href='img/semiautolight.jpg' alt='Light Semi-Auto Pistols' target='_blank'>Sample</a></div></td>
            </tr>

           <tr>
              <td><div class='item'>Semi-Auto Pistol (Heavy)</div></td>
              <td><div class='item'>5</div></td>
              <td><div class='item'>30</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>7+1</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'></div></td>
              <td><div class='item'><a href='img/semiautohvy.jpg' alt='Heavy Semi-Auto Pistols' target='_blank'>Sample</a></div></td>
            </tr>

           <tr>
              <td><div class='item'>Rifle</div></td>
              <td><div class='item'>8</div></td>
              <td><div class='item'>200</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>5+1</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'></div></td>
              <td><div class='item'><a href='img/rifle.jpg' alt='Rifles' target='_blank'>Sample</a></div></td>
            </tr>

           <tr>
              <td><div class='item'>Submachine Gun (Small)</div></td>
              <td><div class='item'>4</div></td>
              <td><div class='item'>25</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>30+1</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>#1</div></td>
              <td><div class='item'><a href='img/smgsmall.jpg' alt='Small Submachine Guns' target='_blank'>Sample</a></div></td>
            </tr>

           <tr>
              <td><div class='item'>Submachine Gun (Large)</div></td>
              <td><div class='item'>4</div></td>
              <td><div class='item'>50</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>30+1</div></td>
              <td><div class='item'>T</div></td>
              <td><div class='itemsm'>#1</div></td>
              <td><div class='item'><a href='img/smglrg.jpg' alt='Large Submachine Guns' target='_blank'>Sample</a></div></td>
            </tr>

           <tr>
              <td><div class='item'>Assault Rifle</div></td>
              <td><div class='item'>7</div></td>
              <td><div class='item'>150</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>42+1</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'>#1</div></td>
              <td><div class='item'><a href='img/assaultrifle.jpg' alt='Assault Rifles' target='_blank'>Sample</a></div></td>
            </tr>

           <tr>
              <td><div class='item'>Shotgun (Sawed Off)</div></td>
              <td><div class='item'>8</div></td>
              <td><div class='item'>10</div></td>
              <td><div class='item'>2</div></td>
              <td><div class='item'>2</div></td>
              <td><div class='item'>J</div></td>
              <td><div class='itemsm'>Example is double barrel, change rate and clip to 1 for single</div></td>
              <td><div class='item'><a href='img/sawoff.jpg' alt='Sawed Off Shotguns' target='_blank'>Sample</a></div></td>
            </tr>
 
          <tr>
              <td><div class='item'>Shotgun</div></td>
              <td><div class='item'>8</div></td>
              <td><div class='item'>25</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>6+1</div></td>
              <td><div class='item'>T</div></td>
              <td><div class='itemsm'>Typical Pump</div></td>
              <td><div class='item'><a href='img/shotguns.jpg' alt='Shotguns' target='_blank'>Sample</a></div></td>
            </tr>

           <tr>
              <td><div class='item'>Shotgun (Semi-Automatic)</div></td>
              <td><div class='item'>8</div></td>
              <td><div class='item'>25</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>6+1</div></td>
              <td><div class='item'>T</div></td>
              <td><div class='itemsm'></div></td>
              <td><div class='item'><a href='img/shotgunsemi.jpg' alt='Semi-Auto Shotguns' target='_blank'>Sample</a></div></td>
            </tr>

           <tr>
              <td><div class='item'>Shotgun (Assault)</div></td>
              <td><div class='item'>8</div></td>
              <td><div class='item'>50</div></td>
              <td><div class='item'>3</div></td>
              <td><div class='item'>32+1</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'>#1, Military Weapon</div></td>
              <td><div class='item'><a href='img/shotgunassl.jpg' alt='Assault Shotguns' target='_blank'>Sample</a></div></td>
            </tr>

           <tr>
              <td><div class='item'>Anti-Material Rifle</div></td>
              <td><div class='item'>16</div></td>
              <td><div class='item'>400</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>10+1</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'>#2, Military Weapon</div></td>
              <td><div class='item'><a href='img/antimaterial.jpg' alt='Anti-Material Rifles' target='_blank'>Sample</a></div></td>
            </tr>

            <tr>
              <td><div class='item'>Black Powder (Muzzle-loader)</div></td>
              <td><div class='item'>7</div></td>
              <td><div class='item'>9</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>1</div></td>
              <td><div class='item'>N</div></td>
              <td><div class='itemsm'>Takes 3 Actions to Reload</div></td>
              <td><div class='item'><a href='img/blackpowder.jpg' alt='Black Powder Rifles' target='_blank'>Sample</a></div></td>
            </tr>

        </table>
    </td>
  </tr>
</table>

<br \><br \>
<table width='75%'>
<caption><div class='pagetopic'>Firearm Weapon Notes:</div></caption>
  <tr>
    <td><div class="itemsm">
#1 May fire full-auto, 3rd bursts, and strafing sprays<br \>
#2 Very Large and Heavy, Str 3+ required, cannot be silenced or suppressed<br \><br \>
Assault Rifles above are military grade and illegal, they can be had in legal civilian models that lack full-auto, burst and spray ability
    </div></td>
  </tr>
</table>
<br \>
</center>

</div>