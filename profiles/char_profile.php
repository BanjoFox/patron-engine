<title></title>
<link rel="stylesheet" type="text/css" href="../layout.css">
</head>
<div id="pagewrapper">
  <table width="750" border="0" cellspacing="0" align="center">
    <tr>
      <td width="120">
        <div class="pagetopic">Name</div>
      </td>
      <td width="630">
        <div class="pagetopic"><?php echo $row['log_name'] ?></div>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <hr>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <table width="100%" border="0">
          <tr>
            <td width="215" valign="top">
              <table width="215" border="0">
                <tr>
                  <td width="50%">
                    <div class="item">Age</div>
                  </td>
                  <td width="50%">
                    <div class="item"><?php echo $row['apparentage'] ?></div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="item">Appearance</div>
                  </td>
                  <td>
                    <div class="item"><?php echo $row['appearance'] ?></div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="item">Gender</div>
                  </td>
                  <td>
                    <div class="item"><?php echo $row['gender'] ?></div>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <hr width='50%'>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="item">Last login</div>
                  </td>
                  <td>
                    <div class="item"><?php echo $row['last_login'] ?></div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="item">Player</div>
                  </td>
                  <td>
                    <div class="item"><?php echo $row['playername'] ?></div>
                  </td>
                </tr>
              </table>
            </td>
            <td width="535">
              <div class="center"><img src="http://shadowsofthebayou.com/player-tools/<?php echo $row['char_image'] ?>"
                  alt="<?php echo $row['log_name'] ?>" height="357" border="0"></div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <hr>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <div class="item"><?php echo $row[profile] ?></div>
      </td>
    </tr>
  </table>
</div>

</html>