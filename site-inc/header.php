<!DOCTYPE html>
<html lang="en">
    
<?php
if (!isset($_COOKIE['logname']) || $_COOKIE['logname'] == '') {
    header("location: http://shadowsofthebayou.com/login.php");
    exit();
}
?>

<head>
    <meta charset="UTF-8">
    <link rel=stylesheet href="../layout.css" TYPE="text/css">
</head>
