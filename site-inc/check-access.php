<?php
if ($_COOKIE['logname'] == '') {
    header("location: http://shadowsofthebayou.com/login.php");
    exit();
} elseif ($_COOKIE['privilege'] == "0") {
    $privlevel = "Site Owner";
} elseif ($_COOKIE['privilege'] == "1") {
    $privlevel = "Admin";
} elseif ($_COOKIE['privilege'] == "2") {
    $privlevel = "Storyteller";
} elseif ($_COOKIE['privilege'] == "4") {
    $privlevel = "Player";
} else {
    $privlevel = "Banned";
}
?>

<div style="text-align: center;">
    <img src='../site-img/loggin.gif' /><br />
    <div class="item"><strong><?php echo $_COOKIE['logname'] ?></strong><br />
        <?php echo $privlevel ?>
    </div>

    <p><img src="../site-img/access-tools.gif" alt="access tools" align="center"><br />
        <a href="http://shadowsofthebayou.com">Return Home</a><br />
        <a href="http://shadowsofthebayou.com/forum" target="_blank">Access Forums</a><br />
        <a href="http://shadowsofthebayou.com/player-tools/ooclogin.php" target="_blank">Chat OOC Login</a>
        <hr style="height:1px; width:60px;" />
        <a href="http://shadowsofthebayou.com/logout.php">Log Out</a>
    </p>

    <?php
    $navFiles = [
        "0" => ["nav-owner_tools.php", "nav-admin_tools.php", "nav-storyteller_tools.php", "nav-player_tools.php"],
        "1" => ["nav-admin_tools.php", "nav-storyteller_tools.php", "nav-player_tools.php"],
        "2" => ["nav-storyteller_tools.php", "nav-player_tools.php"],
        "4" => ["nav-player_tools.php"]
    ];
    $navFilesToInclude = $navFiles[$_COOKIE['privilege']] ?? [];
    foreach ($navFilesToInclude as $file) {
        include "/var/www/shadowsofthebayou.com/site-inc/$file";
    }
    if ($privlevel == "Banned") {
        print "<p class='error'>You are banned<br />Contact Admin<br /></a>";
    }
    ?><br />
</div>
<div class="small" align="center">
    <hr align="center" width="80%">Powered by Patron v6.0<br \>Last Revised: Oct 2021<br \>Code &copy; Poobah
</div>
<br />