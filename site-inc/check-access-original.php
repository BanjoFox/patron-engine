<?php if ($_COOKIE['logname'] == '') {
    header("location: http://shadowsofthebayou.com/login.php");
    exit();
}
?>

<?php if ($_COOKIE['privilege'] == "1") {
    $privlevel = "Admin";
} elseif ($_COOKIE['privilege'] == "2") {
    $privlevel = "Storyteller";
} elseif ($_COOKIE['privilege'] == "99") {
    $privlevel = "Banned";
} else {
    $privlevel = "Player";
}
?>

<div class="center">
    <img src='../site-img/loggin.gif' /><br />
    <div class="item"><strong><?php echo $_COOKIE['logname'] ?></strong><br /><?php echo $privlevel ?></div>

    <p><img src="../site-img/access-tools.gif" alt="access tools" align="center"><br />
        <a href="http://shadowsofthebayou.com">Return Home</a><br />
        <a href="http://shadowsofthebayou.com/forum" target="_blank">Access Forums</a><br />
        <a href="http://shadowsofthebayou.com/player-tools/ooclogin.php" target="_blank">Chat OOC Login</a>
        <hr style="height:1px; width:60px;" />
        <a href="http://shadowsofthebayou.com/logout.php">Log Out</a>
    </p>

    <?php if ($_COOKIE['logname'] == "Shadow Admin") {
        print("<p><img src='../site-img/owner-tools.gif' alt='owner tools'  /><br />
<a href='http://shadowsofthebayou.com/staff-tools/edit_chartype.php'>Allow Character Types</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/change_hold.php'>Change Hold Status</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/pc_login_check.php'>Check PC Logins</a><br />
<a href='http://shadowsofthebayou.com/c-sheets/edit_sheets_special.php'>Edit Sheets S</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/change_player_name.php'>Edit Player Name</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/set_priv.php'>Set User to Admin</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/delete_user.php'>Delete Player</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/reset-pw.php'>Reset Users Password</a><br />
</p>");
    } else {
        print("");
    }
?>

    <?php if ($_COOKIE['logname'] == "DevilishAngel") {
        print("<p><img src='../site-img/owner-tools.gif' alt='owner tools'  /><br />
<a href='http://shadowsofthebayou.com/staff-tools/change_hold.php'>Change Hold Status</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/pc_login_check.php'>Check PC Logins</a><br />
<a href='http://shadowsofthebayou.com/c-sheets/edit_sheets_special.php'>Edit Sheets S</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/reset-pw.php'>Reset Users Password</a><br />
</p>");
    } else {
        print("");
    }
?>
    <?php if ($_COOKIE['privilege'] <= "1") {
        print("<p><img src='../site-img/admin-tools.gif' alt='owner tools'  /><br />
<a href='http://shadowsofthebayou.com/staff-tools/set_privilege.php'>Set Member Privileges</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/list_deleted_char.php'>List Deleted Characters</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/perm_delete_char.php'>Perm Delete Character</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/restore_character.php'>Restore Character</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/change_owner.php'>Change Character Owner</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/change_char_type.php'>Change Character Type</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/change_char_name.php'>Change Character Name</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/add-room.php'>Create Room Name</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/edit_create_rules.php' target='_blank'>Edit Creation Rules</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/unlink_endowments.php'>Unlink Endowment</a><br />
<a href='http://shadowsofthebayou.com/player-tools/da_request_xp.php' target='_blank'>Grant XP</a><br />
</p>");
    } else {
        print("");
    }
?>
    <?php if ($_COOKIE['privilege'] <= "2") {
        print("<p><img src='../site-img/st-tools.gif' alt='st tools'  /><br />
<a href='http://shadowsofthebayou.com/staff-tools/delete_character.php'>Delete Character</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/edit_room_d.php' target='_blank'>Edit Room Description</a><br />
<a href='http://shadowsofthebayou.com/c-sheets/edit_sheets.php'>Edit Sheets</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/list_hold_char.php'>List Characters on Hold</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/search_characters.php'>Search Characters</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/search_players.php'>Search Players</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/see_endowments.php'>Show Endowments</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/st-view-reqs.php'>View Character Reqs</a><br />
<a href='http://shadowsofthebayou.com/c-sheets/st_view_sheets.php'>View Sheets</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/view_replenishments.php'>View Replenishments</a><br />
<br />
<a href='http://shadowsofthebayou.com/site-inc/char_breakdown.php' target='_blank'>View Character Breakdown</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/view_create_rules.php' target='_blank'>View Creation Rules</a><br />
<br />
<a href='http://shadowsofthebayou.com/c-sheets/edit_retainers.php'>Edit Retainers</a><br />
<a href='http://shadowsofthebayou.com/c-sheets/st_view_retainers.php'>View Retainers</a><br />
<a href='http://shadowsofthebayou.com/staff-tools/search_retainer.php'>Search Retainers</a><br />
</p>");
    } else {
        print("");
    }
?>
    <?php if ($_COOKIE['privilege'] != "99") {
        print("<p><img src='../site-img/player-tools.gif' alt='player tools'  /><br />
<a href='http://shadowsofthebayou.com/player-tools/player_update_profile.php'>Update Your Profile</a><br />
<a href='http://shadowsofthebayou.com/player-tools/getuserinfo.php'>Get Member Infomation</a><br />
<a href='http://shadowsofthebayou.com/player-tools/mycharacters.php'>My Character Panel</a><br />
<a href='http://shadowsofthebayou.com/player-tools/show-endowments.php'>Show Your Endowments</a><br />
<a href='http://shadowsofthebayou.com/c-sheets/player_view_sheets.php'>View All Sheets</a><br />
<a href='http://shadowsofthebayou.com/player-tools/view-all-profile.php' target='_blank'>View All Profiles</a><br />
<a href='http://shadowsofthebayou.com/site-inc/char_breakdown.php' target='_blank'>View Character Breakdown</a><br />
<a href='http://shadowsofthebayou.com/player-tools/diceroller.php' target='_blank'>Dice Roller</a><br /></p>");
        include 'char_check.php';
        //    include 'endowment_check.php';
    } else {
        print("<p class='error'>You are banned<br />Contact Admin<br /></a>");
    }
?><br />
</div>
<div class="small" align="center">
    <hr align="center" width="80%">Powered by Patron v6.0<br \>Last Revised: Oct 2021<br \>Code &copy; Poobah
</div>
<br />