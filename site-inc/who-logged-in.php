<?php
require "site-inc/gamengdb.php";
$today = date('Y-m-d');
?>

<div class="pagetopic">Characters Who Logged in Today<br \>
    <hr width="75%">
</div>

<div class='item'>
    <?php
    $query = "SELECT log_name FROM game_data WHERE (last_login = ? AND account_type = 'Character' AND sanctioned = 'Yes') ORDER BY log_name";
    $stmt = mysqli_prepare($connection, $query);
    mysqli_stmt_bind_param($stmt, 's', $today);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);

    while ($row = mysqli_fetch_assoc($result)) {
        echo "\"$row[log_name]\",";
    }
    ?>
</div>