<?php require "gamengdb.php"; ?>
<div class="pagetopic">Count of Approved Characters: 
<?php
$query = "SELECT account_type,sanctioned,deleted,sub_venue, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC')";
$result = mysqli_query($connection, $query) or die("Couldn't execute query");
while ($row = mysqli_fetch_assoc($result)) {
    echo("{$row['count']}");
} ?>
</div>
<div class="pagetopic">Player Count: 
<?php
$query = "SELECT account_type, count(*)  'count' FROM game_data WHERE (account_type='Player')";
$result = mysqli_query($connection, $query) or die("Couldn't execute query");
while ($row = mysqli_fetch_assoc($result)) {
    echo("{$row['count']}");
} ?>
<br \><hr width="75%"></div>
<table width='69%' align='center'>
<tr>
<td>
<div class="createtxtb" align="center">
<?php
// check for vampire ######
    $query = "SELECT DISTINCT character_type FROM `character_type_allowed` WHERE status='Open'";
$result = mysqli_query($connection, $query)
or die("Couldn't execute query for character type.");
while ($row = mysqli_fetch_array($result)) {
    if ($row['character_type'] == 'Vampire') {
        $query = "SELECT account_type,sanctioned,deleted,sub_venue, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Vampire')";
        $result = mysqli_query($connection, $query) or die("Couldn't execute query");
        while ($row = mysqli_fetch_assoc($result)) {
            echo("{$row['count']} Vampires, ");
        }
    } else {
        print("");
    }
}
// check for ghoul ######
$query = "SELECT DISTINCT character_type FROM `character_type_allowed` WHERE status='Open'";
$result = mysqli_query($connection, $query)

or die("Couldn't execute query for character type.");
while ($row = mysqli_fetch_array($result)) {
    if ($row['character_type'] == 'Ghoul') {
        $query = "SELECT account_type,sanctioned,deleted,sub_venue, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Ghoul')";
        $result = mysqli_query($connection, $query) or die("Couldn't execute query");
        while ($row = mysqli_fetch_assoc($result)) {
            echo("{$row['count']} Ghouls, ");
        }
    } else {
        print("");
    }
}
// check for werewolf ######
$query = "SELECT DISTINCT character_type FROM `character_type_allowed` WHERE status='Open'";
$result = mysqli_query($connection, $query)

or die("Couldn't execute query for character type.");
while ($row = mysqli_fetch_array($result)) {
    if ($row['character_type'] == 'Werewolf') {
        $query = "SELECT account_type,sanctioned,sub_venue, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Werewolf')";
        $result = mysqli_query($connection, $query) or die("Couldn't execute query");
        while ($row = mysqli_fetch_assoc($result)) {
            echo("{$row['count']} Werewolf, ");
        }
    } else {
        print("");
    }
}
// check for fera ######
$query = "SELECT DISTINCT character_type FROM `character_type_allowed` WHERE status='Open'";
$result = mysqli_query($connection, $query)

or die("Couldn't execute query for character type.");
while ($row = mysqli_fetch_array($result)) {
    if ($row['character_type'] == 'Fera') {
        $query = "SELECT account_type,sanctioned,sub_venue, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Fera')";
        $result = mysqli_query($connection, $query) or die("Couldn't execute query");
        while ($row = mysqli_fetch_assoc($result)) {
            echo("{$row['count']} Fera, ");
        }
    } else {
        print("");
    }
}
// check for kinfolk ######
$query = "SELECT DISTINCT character_type FROM `character_type_allowed` WHERE status='Open'";
$result = mysqli_query($connection, $query)

or die("Couldn't execute query for character type.");
while ($row = mysqli_fetch_array($result)) {
    if ($row['character_type'] == 'Kinfolk') {
        $query = "SELECT account_type,sanctioned,sub_venue, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Kinfolk')";
        $result = mysqli_query($connection, $query) or die("Couldn't execute query");
        while ($row = mysqli_fetch_assoc($result)) {
            echo("{$row['count']} Kinfolk, ");
        }
    } else {
        print("");
    }
}
// check for mage ######
$query = "SELECT DISTINCT character_type FROM `character_type_allowed` WHERE status='Open'";
$result = mysqli_query($connection, $query)

or die("Couldn't execute query for character type.");
while ($row = mysqli_fetch_array($result)) {
    if ($row['character_type'] == 'Mage') {
        $query = "SELECT account_type,sanctioned,deleted,sub_venue, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Mage')";
        $result = mysqli_query($connection, $query) or die("Couldn't execute query");
        while ($row = mysqli_fetch_assoc($result)) {
            echo("{$row['count']} Mages, ");
        }
    } else {
        print("");
    }
}
// check for sorcerer ######
$query = "SELECT DISTINCT character_type FROM `character_type_allowed` WHERE status='Open'";
$result = mysqli_query($connection, $query)

or die("Couldn't execute query for character type.");
while ($row = mysqli_fetch_array($result)) {
    if ($row['character_type'] == 'Sorcerer') {
        $query = "SELECT account_type,sanctioned,deleted,sub_venue, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Sorcerer')";
        $result = mysqli_query($connection, $query) or die("Couldn't execute query");
        while ($row = mysqli_fetch_assoc($result)) {
            echo("{$row['count']} Sorcerer / Psychics,  ");
        }
    } else {
        print("");
    }
}
// check for changeling ######
$query = "SELECT DISTINCT character_type FROM `character_type_allowed` WHERE status='Open'";
$result = mysqli_query($connection, $query)

or die("Couldn't execute query for character type.");
while ($row = mysqli_fetch_array($result)) {
    if ($row['character_type'] == 'Changeling') {
        $query = "SELECT account_type,sanctioned,deleted,sub_venue, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Changeling')";
        $result = mysqli_query($connection, $query) or die("Couldn't execute query");
        while ($row = mysqli_fetch_assoc($result)) {
            echo("{$row['count']} Changelings, ");
        }
    } else {
        print("");
    }
}
// check for kinain ######
$query = "SELECT DISTINCT character_type FROM `character_type_allowed` WHERE status='Open'";
$result = mysqli_query($connection, $query)

or die("Couldn't execute query for character type.");
while ($row = mysqli_fetch_array($result)) {
    if ($row['character_type'] == 'Kinain') {
        $query = "SELECT account_type,sanctioned,deleted,sub_venue, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Kinian')";
        $result = mysqli_query($connection, $query) or die("Couldn't execute query");
        while ($row = mysqli_fetch_assoc($result)) {
            echo("{$row['count']} Kinain, ");
        }

    } else {
        print("");
    }
}
// check for wraith ######
$query = "SELECT DISTINCT character_type FROM `character_type_allowed` WHERE status='Open'";
$result = mysqli_query($connection, $query)

or die("Couldn't execute query for character type.");
while ($row = mysqli_fetch_array($result)) {
    if ($row['character_type'] == 'Wraith') {
        $query = "SELECT account_type,sanctioned,deleted,sub_venue, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Wraith')";
        $result = mysqli_query($connection, $query) or die("Couldn't execute query");
        while ($row = mysqli_fetch_assoc($result)) {
            echo("{$row['count']} Wraiths, ");
        }
    } else {
        print("");
    }
}
// check for bygone ######
$query = "SELECT DISTINCT character_type FROM `character_type_allowed` WHERE status='Open'";
$result = mysqli_query($connection, $query)

or die("Couldn't execute query for character type.");
while ($row = mysqli_fetch_array($result)) {
    if ($row['character_type'] == 'Bygone') {
        $query = "SELECT account_type,sanctioned,deleted,sub_venue, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Bygone')";
        $result = mysqli_query($connection, $query) or die("Couldn't execute query");
        while ($row = mysqli_fetch_assoc($result)) {
            echo("{$row['count']} Bygones, ");
        }
    } else {
        print("");
    }
}
// check for mortal ######
$query = "SELECT DISTINCT character_type FROM `character_type_allowed` WHERE status='Open'";
$result = mysqli_query($connection, $query)

or die("Couldn't execute query for character type.");
while ($row = mysqli_fetch_array($result)) {
    if ($row['character_type'] == 'Mortal') {
        $query = "SELECT account_type,sanctioned,deleted,sub_venue, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Mortal')";
        $result = mysqli_query($connection, $query) or die("Couldn't execute query");
        while ($row = mysqli_fetch_assoc($result)) {
            echo("{$row['count']} Mortals <br \>");
        }
    } else {
        print("");
    }
}
?>
</div>
</td>
</tr>
</table>
