<br \>
<?php
require "site-inc/gamengdb.php";

$sevendays = date('Y-m-d', strtotime("7 days ago"));
$sixdays = date('Y-m-d', strtotime("6 days ago"));
$fivedays = date('Y-m-d', strtotime("5 days ago"));
$fourdays = date('Y-m-d', strtotime("4 days ago"));
$threedays = date('Y-m-d', strtotime("3 days ago"));
$twodays = date('Y-m-d', strtotime("2 days ago"));
$yesterday = date('Y-m-d', strtotime("1 days ago"));
$today = date('Y-m-d', strtotime('NOW'));
?>
<div class="pagetopic">Welcome to our newest members!<br \>
    <hr width="75%">
</div>
<div style="text-align: center; font-size: 12px;">
    <?php
    $newMemberDays = [
        date('Y-m-d', strtotime('7 days ago')),
        date('Y-m-d', strtotime('6 days ago')),
        date('Y-m-d', strtotime('5 days ago')),
        date('Y-m-d', strtotime('4 days ago')),
        date('Y-m-d', strtotime('3 days ago')),
        date('Y-m-d', strtotime('2 days ago')),
        date('Y-m-d', strtotime('1 days ago')),
        date('Y-m-d', strtotime('NOW'))
    ];

    $newMemberQuery = "SELECT log_name FROM game_data WHERE date_joined IN ('" . implode("', '", $newMemberDays) . "') ORDER BY log_name";
    $newMemberResult = mysqli_query($connection, $newMemberQuery) or die(mysqli_error($connection));

    while ($newMember = mysqli_fetch_assoc($newMemberResult)) {
        echo $newMember['log_name'] . ', ';
    }
    ?>
</div>
<br />