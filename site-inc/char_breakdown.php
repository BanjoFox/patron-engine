<?php if ($_COOKIE['logname'] == '') {
    header("location: http://shadowsofthebayou.com/login.php");
    exit();
}
?>
<head>
<link rel=stylesheet href="../layout.css" TYPE="text/css">
<div id="pagewrapper">
<title></title>
</head>
<?php require "gamengdb.php"; ?>

<div class="pagetopic">
<br \>
<div align='center'><img src="../site-img/charbreakdown.gif"></div>
<br \>
<br \>
<table align="center" width="50%" border="1" cellspacing="0">
<capiton><div class="pagetopic">Vampires</div></caption>
<tr><td><div class="item">
<div class="pagetopic">By Sect and Clan</div>
<div class="pagetopic">Camarilla</div>

<?php
$query = "SELECT account_type,sanctioned,sub_venue,clan,sect, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Vampire' and sect='Camarilla') GROUP BY clan";
$result = mysqli_query($connection, $query) or die("Couldn't execute query");
while ($row = mysqli_fetch_assoc($result)) {
    echo("  {$row['clan']} {$row['count']}, ");
}
?>

<br \><br \>

<div class="pagetopic">Sabbat</div>
<?php
$query = "SELECT account_type,sanctioned,sub_venue,clan,sect, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Vampire' and sect='Sabbat') GROUP BY clan";
$result = mysqli_query($connection, $query) or die("Couldn't execute query");
while ($row = mysqli_fetch_assoc($result)) {
    echo("  {$row['clan']} {$row['count']}, ");
}
?>

<br \><br \>


<div class="pagetopic">Independent</div>
<?php
$query = "SELECT account_type,sanctioned,sub_venue,clan,sect, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Vampire' and sect='Independent') GROUP BY clan";
$result = mysqli_query($connection, $query) or die("Couldn't execute query");
while ($row = mysqli_fetch_assoc($result)) {
    echo("  {$row['clan']} {$row['count']}, ");
}
?>
</div> 
</td></tr>


</table>
<br \>
<br \>


<table align="center" width="50%" border="1" cellspacing="0">
<capiton><div class="pagetopic">Ghouls by Clan</div></caption>
<tr><td><div class="item">
<?php
$query = "SELECT account_type,sub_venue,sanctioned,clan, count(*)  'count' FROM game_data WHERE (account_type='Character' and sub_venue='Ghoul' and deleted='No' and char_type='PC' and sanctioned='Yes') GROUP BY clan";
$result = mysqli_query($connection, $query) or die("Couldn't execute query");
while ($row = mysqli_fetch_assoc($result)) {
    echo("  {$row['clan']} {$row['count']}, ");
}
?> 
</div> 
</td></tr>
</table>
<br \><br \>


<table align="center" width="50%" border="1" cellspacing="0">
<div class="pagetopic">Werewolves</div>
<tr><td><div class="item">
<div class="pagetopic">Werewolves by Tribe</div>
<?php
$query = "SELECT account_type,sanctioned,sub_venue,tribe, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Werewolf') GROUP BY tribe";
$result = mysqli_query($connection, $query) or die("Couldn't execute query");
while ($row = mysqli_fetch_assoc($result)) {
    echo("  {$row['tribe']} {$row['count']}, ");
}
?>
<br \><br \>
<div class="pagetopic">Werewolves by Auspice</div>
<?php
$query = "SELECT account_type,sanctioned,sub_venue,auspice, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Werewolf') GROUP BY auspice";
$result = mysqli_query($connection, $query) or die("Couldn't execute query");
while ($row = mysqli_fetch_assoc($result)) {
    echo(" {$row['auspice']} {$row['count']}, ");
}
?>
</div> 
</td></tr>
</table>
<br \>
<br \>


<table align="center" width="50%" border="1" cellspacing="0">
<capiton><div class="pagetopic">Fera by Tribe</div></caption>
<tr><td><div class="item">
<?php
$query = "SELECT account_type,sub_venue,sanctioned,tribe, count(*)  'count' FROM game_data WHERE (account_type='Character' and sub_venue='Fera' and deleted='No' and char_type='PC' and sanctioned='Yes') GROUP BY tribe";
$result = mysqli_query($connection, $query) or die("Couldn't execute query");
while ($row = mysqli_fetch_assoc($result)) {
    echo("  {$row['tribe']} {$row['count']}, ");
}
?> 
</div> 
</td></tr>
</table>
<br \>
<br \>


<table align="center" width="50%" border="1" cellspacing="0">
<capiton><div class="pagetopic">Kinfolk by Tribe</div></caption>
<tr><td><div class="item">
<?php
$query = "SELECT account_type,sub_venue,sanctioned,tribe, count(*)  'count' FROM game_data WHERE (account_type='Character' and sub_venue='Kinfolk' and deleted='No' and char_type='PC' and sanctioned='Yes') GROUP BY tribe";
$result = mysqli_query($connection, $query) or die("Couldn't execute query");
while ($row = mysqli_fetch_assoc($result)) {
    echo("  {$row['tribe']} {$row['count']}, ");
}
?>
</div> 
</td></tr>
</table>
<br \><br \>

<table align="center" width="50%" border="1" cellspacing="0">
<capiton><div class="pagetopic">Mage by Tradition</div></caption>
<tr><td><div class="item">
<?php
$query = "SELECT account_type,sanctioned,sub_venue,tradition, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Mage') GROUP BY tradition";
$result = mysqli_query($connection, $query) or die("Couldn't execute query");
while ($row = mysqli_fetch_assoc($result)) {
    echo("  {$row['tradition']} {$row['count']}, ");
}
?>  
</div> 
</td></tr>
</table>
<br \><br \>

<table align="center" width="50%" border="1" cellspacing="0">
<capiton><div class="pagetopic">Sorcerer / Psychic by Tradition</div></caption>
<tr><td><div class="item">
<?php
$query = "SELECT account_type,sanctioned,sub_venue,tradition, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Sorcerer') GROUP BY tradition";
$result = mysqli_query($connection, $query) or die("Couldn't execute query");
while ($row = mysqli_fetch_assoc($result)) {
    echo("  {$row['tradition']} {$row['count']}, ");
}
?> 
</div> 
</td></tr>
</table>
<br \><br \>


<table align="center" width="50%" border="1" cellspacing="0">
<capiton><div class="pagetopic">Changelings by Kith</div></caption>
<tr><td><div class="item">
<?php
$query = "SELECT account_type,sanctioned,sub_venue,kith, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Changeling') GROUP BY kith";
$result = mysqli_query($connection, $query) or die("Couldn't execute query");
while ($row = mysqli_fetch_assoc($result)) {
    echo("  {$row['kith']} {$row['count']}, ");
}
?>
</div> 
</td></tr>
</table>
<br \><br \>


<table align="center" width="50%" border="1" cellspacing="0">
<capiton><div class="pagetopic">Kinain by Kith</div></caption>
<tr><td><div class="item">
<?php
$query = "SELECT account_type,sanctioned,sub_venue,kith, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Kinian') GROUP BY kith";
$result = mysqli_query($connection, $query) or die("Couldn't execute query");
while ($row = mysqli_fetch_assoc($result)) {
    echo("  {$row['kith']} {$row['count']}, ");
}
?>  
</div> 
</td></tr>
</table>
<br \><br \>

<table align="center" width="50%" border="1" cellspacing="0">
<capiton><div class="pagetopic">Bygones by Type</div></caption>
<tr><td><div class="item">
<?php
$query = "SELECT account_type,sanctioned,sub_venue,bygone_species, count(*)  'count' FROM game_data WHERE (account_type='Character' and sanctioned='Yes' and deleted='No' and char_type='PC' and sub_venue='Bygone') GROUP BY bygone_species";
$result = mysqli_query($connection, $query) or die("Couldn't execute query");
while ($row = mysqli_fetch_assoc($result)) {
    echo("  {$row['bygone_species']} {$row['count']}, ");
}
?>
</div> 
</td></tr>
</table>
<br \><br \>
</div>
</div>