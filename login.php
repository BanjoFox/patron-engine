<?php
ini_set('display_errors', 'On');
/* Program: login.php (login)
 * Desc: Login for database, duh.
 */
session_start();
require "site-inc/gamengdb.php";
switch (@$_GET['do']) {
    case "login":
        $fusername = addslashes(trim($_POST['fusername']));
        $fpassword = addslashes(trim($_POST['fpassword']));
        $sql = "SELECT log_name FROM game_data WHERE log_name='$fusername'";
        $result = mysqli_query($connection, $sql) or
        die("Could not execute query.<br \>" . mysqli_error($connection) . "<br \>");
        $num = mysqli_num_rows($result);
        if ($num == 1)    // login name was found
        {
            $sql = "SELECT log_name,password,user_privilege FROM game_data WHERE log_name='$fusername' AND password='$fpassword'";
            $result2 = mysqli_query($connection, $sql) or die("Could not execute query.<br \>" . mysqli_error($connection) . "<br \>");
            $num2 = mysqli_num_rows($result2);
            if ($num2 > 0)    // password is correct
            {
                $row = mysqli_fetch_array($result2);
                $logname = $fusername;
                $privilege = $row['user_privilege'];
                $st_flag = $row['st_flag'];
                setcookie('logname', $logname, time() + (60 * 60 * 24 * 1), '/', 'shadowsofthebayou.com');
                setcookie('privilege', $privilege, time() + (60 * 60 * 24 * 1), '/', 'shadowsofthebayou.com');
                if ($st_flag == "yes") {
                    setcookie('st_flag', $st_flag, time() + (60 * 60 * 24 * 1), '/', 'shadowsofthebayou.com');
                } else {
                }
                $query = "UPDATE game_data SET last_login=NOW() WHERE (log_name='$fusername')";
                $result = mysqli_query($connection, $query) or die("Could not execute query.<br \>" . mysqli_error($connection) . "<br \>");
                header("Location: index.php");
            } else        // password is not correct
            {
                unset($do);
                $message = "<p class='error'>Incorrect password.</p>";
                include "login_form.inc";
            }
        } elseif ($num == 0)    // login name not found
        {
            unset($do);
            $message = "<p class='error'>User does not exist.</p>";
            include "login_form.inc";
        }
        break;
    default:
        include "login_form.inc";
}