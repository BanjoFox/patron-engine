<title>View Creation Riles</title>
<link rel="stylesheet" type="text/css" href="../layout.css">
</head>
<div id="pagewrapper">
<?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
<br \>
<div class="center"><div align="center" class="pagetopic">View Creation Rules</div>
<br \>
<?php
    $query = "SELECT DISTINCT char_type_name FROM `create_rules` ORDER BY id";
$result = mysqli_query($connection, $query)

    or die("Could not execute query.<br \>".mysqli_error($connect)."<br \>");
$option = "<select name=\"char_type_name\" class='form'><option value=\"\" class='form'></option>";
while ($row = mysqli_fetch_array($result)) {
    $option = "$option <option value=\"$row[char_type_name]\" class='form'>$row[char_type_name]</option>";
}
$option = "$option </select>";
?>
<?php $sqldata['char_type_name']?>
<form name="form1" method="post" action="">
    <table style="width:90%" border="0" cellspacing="0" align="center">
    <caption><div align="center" class='pagetopic'>Select Character Type To Edit Creation Rules</div><caption>
              <tr>
                <td width="50%"><div class='pageitem' >
                  <div align="right">Character Type: </div>
                </div></td>
                <td width="50%"><div class='pageitem'> <?php echo $option?></div></td>
              </tr>
              
              <tr>
                <td colspan="2"><div class="center"><input name="Select" type="submit" id="Select" value="View Creation Rules" class="form"></div></td>
              </tr>
        </table>
<br \>
</form>
<form name="form2" method="post" action="">
<input name="char_type_name" type="hidden" id="char_type_name" value="<?php echo $_POST['char_type_name']?>">
<?php
    $query = "SELECT * FROM `create_rules` WHERE (char_type_name=\"$_POST[char_type_name]\")";
$result = mysqli_query($connection, $query)
 or die("Couldn't execute query.");
$row = mysqli_fetch_array($result);
?>
<div id="pagewrapper">

<table width="750" border="0" cellspacing="0"  align="center">
  <tr>
    <td><div class="pagetopic">Character Type: <?php echo $row['char_type_name']?></div></td>
  </tr>
    <tr>
    <td><hr></td>
  </tr>
 <tr>
    <td><div class="pagetopic">Allowed Sources:</div></td>
  </tr>
  <tr>
    <td><div class="item">Current: <?php echo $row['books']?></div></td>
  </tr>
  <tr>
    <td><hr></td>
  </tr>
  <tr>
    <td><div class="pagetopic">Starting Dots:</div></td>
  </tr>
  <tr>
    <td><div class="item">Current: <?php echo $row['start_dots']?></div></td>
  </tr>
   <tr>
    <td><hr></td>
  </tr>
  <tr>
    <td><div class="pagetopic">Venue Allowances:</div></td>
  </tr>
  <tr>
    <td><div class="item">Current:</div></td>
  </tr>
  <tr>
    <td><div class="item"><?php echo $row['venue_1']?></div></td>
  </tr>
   <tr>
    <td><hr></td>
  </tr>
  <tr>
    <td><div class="pagetopic">Backgrounds:</div></td>
  </tr>
    <tr>
    <td><div class="item">Current:</div></td>
  </tr>
  <tr>
    <td><div class="item"><?php echo $row['backgrounds']?></div></td>
  </tr>
  <tr>
    <td><hr></td>
  </tr>
  <tr>
    <td><div class="pagetopic">Venue Allowances 2:</div></td>
  </tr>
  <tr>
    <td><div class="item">Current:</div></td>
  </tr>
  <tr>
    <td><div class="item"><?php echo $row['venue_2']?></div></td>
  </tr>
  <tr>
    <td><hr></td>
  </tr>
  <tr>
    <td><div class="pagetopic">Merits & Flaws:</div></td>
  </tr>
  <tr>
    <td><div class="item">Current:</div></td>
  </tr>
  <tr>
    <td><div class="item"><?php echo $row['merit_flaws']?></div></td>
  </tr>
   <tr>
    <td><hr></td>
  </tr>
</table>
</form>
</div>
</div>
