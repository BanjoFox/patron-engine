<?php include '/var/www/shadowsofthebayou.com/site-inc/header.php'; ?>

<div id="wrapper">
    <div id="title"></div><!-- title close -->

    <div class="container">

        <div class="column-nav"><?php require '/var/www/shadowsofthebayou.com/site-inc/check-access.php'; ?>

        </div><!-- menu close -->

        <div class="column-main">
            <?php
            if ($_COOKIE['privilege'] >= "2") {
                echo "You Do not have rights to do this";
            } else {
                ?>
                <?php include "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
                <?php
                $query = "SELECT DISTINCT log_name FROM game_data WHERE (account_type='Player' AND user_privilege>'0') ORDER BY log_name";
                $result = mysqli_query($connection, $query)
                    or die("Could not execute query.<br \>" . mysqli_error($connection) . "<br \>");
                $option = "<select name=\"log_name\" class='form'><option value=\"\" class='form'></option>";
                while ($row = mysqli_fetch_array($result)) {
                    $option = "$option <option value=\"$row[log_name]\">$row[log_name]</option>";
                }
                $option = "$option </select>";
                ?>
                <?php echo $sqldata['log_name'] ?>

                <form name="form1" method="post" action="">
                    <table width="80%" align="center">
                        <caption>
                            <div align="center" class="pagetopic">Select Member To Change Privileges For</div>
                        </caption>
                        <tr>
                            <td>
                                <div class="center"><?php echo $option ?> <input name="Select" type="submit" id="Select"
                                        value="Select User" class="form"></div>
                            </td>
                        </tr>
                    </table>
                </form>

                <?php
                if ($_POST['Select'] == "Select User" && $_POST['log_name'] <> "") {
                    ?>
                    <?php
                    $query = "SELECT * FROM `game_data` WHERE (log_name=\"$_POST[log_name]\")";
                    $results = mysqli_query($connection, $query)

                        or die("Couldn't execute query..<br \>" . mysqli_error($connection) . "<br \>");
                    $data = @mysqli_fetch_array($results);
                    ?>
                    <?php
                    if ($_COOKIE['privilege'] == "0") {
                        $privlevel = "Site Owner";
                    } elseif ($_COOKIE['privilege'] == "1") {
                        $privlevel = "Admin";
                    } elseif ($_COOKIE['privilege'] == "2") {
                        $privlevel = "Storyteller";
                    } elseif ($_COOKIE['privilege'] == "4") {
                        $privlevel = "Player";
                    } else {
                        $privlevel = "Banned";
                    }
                    ?>
                    <form name="form2" method="post" action="">
                        <input name="log_name" type="hidden" id="log_name" value="<?php echo $_POST['log_name'] ?>">
                        <input name="user_privilege" type="hidden" id="user_privilege"
                            value="<?php echo $_POST['user_privilege'] ?>">
                        <hr width="80%">
                        <table width="80%" border="0" align="center" cellspacing="0">
                            <caption>
                                <div align="center" class="pagetopic">Select New Member Level</div>
                            </caption>
                            <tr>
                                <td>
                                    <div class="pagetopic">User Name</div>
                                </td>
                                <td>
                                    <div class="pagetopic">Current Level</div>
                                </td>
                                <td>
                                    <div class="pagetopic">Change To:</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="center" class="item"><?php echo $_POST['log_name'] ?></div>
                                </td>
                                <td>
                                    <div align="center" class="item"><?php echo $privlevel ?></div>
                                </td>
                                <td>
                                    <div class="center"><select name="user_privilege" id="user_privilege" class="form">
                                            <option value="99">Banned</option>
                                            <option value="4" selected>Player</option>
                                            <option value="2">Storyteller</option>
                                            <option value="1">Administrator</option>
                                        </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="center"><input name="Select" type="submit" id="Select" value="Change"
                                            class="form"></div>
                                </td>
                            </tr>
                        </table>
                    </form>

                    <?php
                } elseif ($_POST['Select'] == "Change" && $_POST['log_name'] <> "") {
                    $query = "UPDATE `game_data` SET user_privilege=\"$_POST[user_privilege]\" WHERE (log_name=\"$_POST[log_name]\")";
                    $result = mysqli_query($connection, $query)
                        or die("Could not execute query.<br \>" . mysqli_error($connection) . "<br \>");
                    echo "<p>Member privlidges have been updated.</p><br \>";
                }
            }
            ?>


        </div><!-- content close -->


    </div><!-- container close -->
</div><!-- wrapper close -->