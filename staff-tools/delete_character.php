<?php include '/var/www/shadowsofthebayou.com/site-inc/header.php'; ?>

<div id="wrapper">
    <div id="title"></div><!-- title close -->

    <div class="container">

        <div class="column-nav">
            <?php require '/var/www/shadowsofthebayou.com/site-inc/check-access.php'; ?>

        </div><!-- menu close -->

        <div class="column-main">
            <?php
            if ($_COOKIE['privilege'] >= "3") {
                echo "You Do not have rights to do this";
            } else {
                ?>
                <?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
                <?php
                if ($_POST['Select'] == "Delete Character" && $_POST['log_name'] <> "" && $_POST['Verify'] == "Yes") {
                    $query = "UPDATE game_data SET deleted='Yes',deleted_date=NOW(),deleted_by='$_COOKIE[logname]',st_notes=\"$_POST[now] $_COOKIE[logname]: Deleted Character<br \>$_POST[oldstnotes]\" WHERE (log_name=\"$_POST[log_name]\")";
                    $result = mysqli_query($connection, $query)
                        or die("Could not execute query.<br \>" . mysqli_error($connection) . "<br \>");
                    echo "<div  class='success'>The character '$_POST[log_name]' has been deleted.</div>";
                }
                ?>
                <?php
                $query = "SELECT DISTINCT log_name FROM `game_data` WHERE (deleted<>'Yes' AND account_type='Character') ORDER BY log_name";
                $result = mysqli_query($connection, $query)
                    or die("Could not execute query.<br \>" . mysqli_error($connection) . "<br \>");
                $option = "<select name=\"log_name\" class='form'><option value=\"\" class='form'></option>";
                while ($row = mysqli_fetch_array($result)) {
                    $option = "$option <option value=\"$row[log_name]\" class='form'>$row[log_name]</option>";
                }
                $option = "$option </select>";
                ?>
                <?php echo $sqldata['log_name'] ?>
                <form name="form1" method="post" action="<?php echo $PHP_SELF ?>">
                    <table style="width:90%" align="center">
                        <caption>
                            <div class='pagetopic' align="center">Select A Character To Delete</div>
                        </caption>
                        <tr>
                            <td>
                                <div class="center">
                                    <?php echo $option ?> <input name="Select" type="submit" class="form" id="Select"
                                        value="Select Character">
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
                <?php
                if ($_POST['Select'] == "Select Character" && $_POST['log_name'] <> "") {
                    ?>
                    <?php
                    $query = "SELECT * FROM `game_data` WHERE (log_name=\"$_POST[log_name]\")";
                    $results = mysqli_query($connection, $query)
                    or die("Couldn't execute query..<br \>" . mysqli_error($connection) . "<br \>");
                    $data = @mysqli_fetch_array($results);
                    ?>
                    <br />
                    <form name="form2" method="post" action="<?php echo $PHP_SELF ?>">
                        <input name="log_name" type="hidden" id="log_name" value="<?php echo $_POST['log_name'] ?>">
                        <input name="oldstnotes" type="hidden" id="oldnotes" value="<?php echo $data['st_notes'] ?>">
                        <input name="now" type="hidden" id="now" value="<?php echo date('m/d/y') ?>">
                        <table style="width:90%" border="0" align="center">
                            <caption>
                                <div class="pagetopic" align="center">Delete Character Below</div>
                            </caption>
                            <tr>
                                <td>
                                    <div class="pageitem" align="center">Character Name</div>
                                </td>
                                <td>
                                    <div class="warning" align="center">Notice</div>
                                </td>
                                <td>
                                    <div class="pageitem" align="center">Sure?</div>
                                </td>
                            </tr>
                            <tr>
                                <td width="33%">
                                    <div class="item" align="center">
                                    <?php echo $_POST['log_name'] ?>
                                </td>
                                <td width="47%">
                                    <div class="warnsm" align="center">Before you submit make sure you understand that this
                                        process may not be reversible.</div>
                                </td>
                                <td width="20%">
                                    <div class="item" align="center">
                                        <select name="Verify" class="form" id="Verify" class="form">
                                            <option selected>No</option>
                                            <option>Yes</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="center">
                                        <hr>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="center"><input name="Select" type="submit" class="form" id="Select"
                                            value="Delete Character" class="form"></div>
                                </td>
                            </tr>
                        </table>
                    </form>
                <?php }
                } ?>
        </div><!-- content close -->


    </div><!-- container close -->
</div><!-- wrapper close -->
