<title>Edit Room Desriptions</title>
<link rel="stylesheet" type="text/css" href="../layout.css">
</head>
<div id="pagewrapper">
<?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
<?php
if ($_COOKIE['privilege'] >= "3") {
    echo "You Do not have rights to do this";
} else {
    ?>
<br \>
<div class="center"><div align="center" class="pagetopic">Edit Room Description</div>
<br \>
    <?php
    $query = "SELECT DISTINCT room_name FROM `chat_room_d` ORDER BY room_name";
    $result = mysqli_query($connection, $query)

        or die("Could not execute query.<br \>".mysqli_error($connect)."<br \>");
    $option = "<select name=\"room_name\" class='form'><option value=\"\" class='form'></option>";
    while ($row = mysqli_fetch_array($result)) {
        $option = "$option <option value=\"$row[room_name]\" class='form'>$row[room_name]</option>";
    }
    $option = "$option </select>";
    ?>
    <?php $sqldata[room_name]?>
<form name="form1" method="post" action="">
    <table style="width:90%" border="0" cellspacing="0" align="center">
    <caption><div align="center" class='pagetopic'>Select Room To Edit Description</div><caption>
              <tr>
                <td width="50%"><div class='pageitem' >
                  <div align="right">Room Name: </div>
                </div></td>
                <td width="50%"><div class='pageitem'> <?php echo $option?></div></td>
              </tr>
              
              <tr>
                <td colspan="2"><div class="center"><input name="Select" type="submit" id="Select" value="Select Room" class="form"></div></td>
              </tr>
        </table>
<br \>
</form>
<form name="form2" method="post" action="edit-room.php">
<input name="room_name" type="hidden" id="room_name" value="<?php echo $_POST[room_name]?>">
    <?php
    $query = "SELECT * FROM `chat_room_d` WHERE (room_name=\"$_POST[room_name]\")";
    $result = mysqli_query($connection, $query)
    or die("Couldn't execute query.");
    $row = mysqli_fetch_array($result);
    ?>
<div id="pagewrapper">

<table width="750" border="0" cellspacing="0"  align="center">
  <tr>
    <td><div class="pagetopic">Room Name: <?php echo $row[room_name]?></div></td>
  </tr>
    <tr>
    <td><hr></td>
  </tr>
  <tr>
    <td><div class="pagetopic">Where is it in the city?</div></td>
  </tr>
  <tr>
    <td><div class="item">Current: <?php echo $row[city_location]?></div></td>
  </tr>
  <tr>
    <td><div class="center"><hr width="50%"></div></td>
  </tr>
  <tr>
    <td><center><div class='warning'>Where is the city is it? Perhaps the neighborhood, which ward it's located, maybe even nearest cross-streets or landmarks. Something to give players an idea of where the location is.</div></center></td>
  </tr>
  <tr>
    <td><div class="item"><textarea name="city_location" cols="120" datas="10" class="form" id="city_location"><?php echo $row[city_location]?></textarea></div></td>
  </tr>
   <tr>
    <td><hr></td>
  </tr>
  <tr>
    <td><div class="pagetopic">Main Description:</div></td>
  </tr>
  <tr>
    <td><div class="item">Current:</div></td>
  </tr>
  <tr>
    <td><div class="item"><?php echo $row[main_description]?></div></td>
  </tr>
  <tr>
    <td><div class="center"><hr width="50%"></div></td>
  </tr>
  <tr>
    <td><center><div class='warning'>You can create the decription just like a character profile. Send Poobah the image links or the images so he can upload them. When creating the main description do not use any double quotes ( " ) use single ( ' ). If you do the description will be ignored and not added, if you are editing an existing description, be sure to swap out all double quotes ( " ) or single ( ' ) or you will lose it.</div></center></td>
  </tr>
  <tr>
    <td><div class="item"><textarea name="main_description" cols="120" datas="30" class="form" id="main_description"><?php echo $row[main_description]?></textarea></div></td>
  </tr>
   <tr>
    <td><hr></td>
  </tr>
  <tr>
    <td><div class="pagetopic">Notable NPCs and Phenomenon:</div></td>
  </tr>
    <tr>
    <td><div class="item">Current:</div></td>
  </tr>
  <tr>
    <td><div class="item"><?php echo $row[room_notables]?></div></td>
  </tr>
  <tr>
    <td><div class="center"><hr width="50%"></div></td>
  </tr>
  <tr>
    <td><center><div class='warning'>Notables are for names of persistant NPCs or phenomenon like a shallowing. It can include known territory claims as well as things like the gauntlet rating or anything else that would assist role-play.</div></center></td>
  </tr>
  <tr>
    <td><div class="item"><textarea name="room_notables" cols="120" datas="2" class="form" id="room_notables"><?php echo $row[room_notables]?></textarea></div></td>
  </tr>
   <tr>
    <td><hr></td>
  </tr>
  <tr>
    <td><div class="center"><input name="Select" type="submit" id="Select" value="Update" class="form" /><br \><br \></div></td>
    </tr>
</table>
</form>
<?php } ?>
</div>
</div>
