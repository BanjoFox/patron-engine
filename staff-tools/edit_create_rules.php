<title>Edit Creation Rules</title>
<link rel="stylesheet" type="text/css" href="../layout.css">
</head>
<div id="pagewrapper">
  <?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
  <?php
  if ($_COOKIE['privilege'] >= "3") {
    echo "You Do not have rights to do this";
  } else {
    ?>
    <br \>
    <div class="center">
      <div align="center" class="pagetopic">Edit Creation Rules</div>
      <br \>
      <?php
      $query = "SELECT DISTINCT char_type_name FROM `create_rules` ORDER BY id";
      $result = mysqli_query($connection, $query)

        or die("Could not execute query.<br \>" . mysqli_error($connect) . "<br \>");
      $option = "<select name=\"char_type_name\" class='form'><option value=\"\" class='form'></option>";
      while ($row = mysqli_fetch_array($result)) {
        $option = "$option <option value=\"$row[char_type_name]\" class='form'>$row[char_type_name]</option>";
      }
      $option = "$option </select>";
  }
  ?>
    <?php $sqldata['char_type_name'] ?>
    <form name="form1" method="post" action="">
      <table style="width:90%" border="0" cellspacing="0" align="center">
        <caption>
          <div align="center" class='pagetopic'>Select Character Type To Edit Creation Rules</div>
          <caption>
          <tr>
            <td width="50%">
              <div class='pageitem'>
                <div align="right">Character Type: </div>
              </div>
            </td>
            <td width="50%">
              <div class='pageitem'> <?php echo $option ?></div>
            </td>
          </tr>

        <tr>
          <td colspan="2">
            <div class="center"><input name="Select" type="submit" id="Select" value="Edit Creation Rules" class="form">
            </div>
          </td>
        </tr>
      </table>
      <br \>
    </form>
    <form name="form2" method="post" action="edit_create_rules_e.php">
      <input name="char_type_name" type="hidden" id="char_type_name" value="<?php echo $_POST['char_type_name'] ?>">
      <?php
      $query = "SELECT * FROM `create_rules` WHERE (char_type_name=\"$_POST[char_type_name]\")";
      $result = mysqli_query($connection, $query)
        or die("Couldn't execute query.");
      $row = mysqli_fetch_array($result);
      ?>
      <div id="pagewrapper">

        <table width="750" border="0" cellspacing="0" align="center">
          <tr>
            <td>
              <div class="pagetopic">Character Type: <?php echo $row['char_type_name'] ?></div>
            </td>
          </tr>
          <tr>
            <td>
              <hr>
            </td>
          </tr>
          <tr>
            <td>
              <div class="pagetopic">Allowed Sources:</div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="item">Current: <?php echo $row['books'] ?></div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="center">
                <hr width="50%">
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <center>
                <div class='warning'>This is where you list the allowed books for the character type</div>
              </center>
            </td>
          </tr>
          <tr>
            <td>
              <div class="item"><textarea name="books" cols="120" datas="10" class="form"
                  id="books"><?php echo $row['books'] ?></textarea></div>
            </td>
          </tr>

          <tr>
            <td>
              <hr>
            </td>
          </tr>
          <tr>
            <td>
              <div class="pagetopic">Starting Dots:</div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="item">Current: <?php echo $row['start_dots'] ?></div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="center">
                <hr width="50%">
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <center>
                <div class='warning'>This is where you list the starting dots for the character type</div>
              </center>
            </td>
          </tr>
          <tr>
            <td>
              <div class="item"><textarea name="start_dots" cols="120" datas="10" class="form"
                  id="start_dots"><?php echo $row['start_dots'] ?></textarea></div>
            </td>
          </tr>
          <tr>
            <td>
              <hr>
            </td>
          </tr>
          <tr>
            <td>
              <div class="pagetopic">Venue Allowances:</div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="item">Current:</div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="item"><?php echo $row['venue_1'] ?></div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="center">
                <hr width="50%">
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <center>
                <div class='warning'>This is where you put information like allowed Clans/Tribes/Etc</div>
              </center>
            </td>
          </tr>
          <tr>
            <td>
              <div class="item"><textarea name="venue_1" cols="120" datas="30" class="form"
                  id="venue_1"><?php echo $row['venue_1'] ?></textarea></div>
            </td>
          </tr>
          <tr>
            <td>
              <hr>
            </td>
          </tr>
          <tr>
            <td>
              <div class="pagetopic">Backgrounds:</div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="item">Current:</div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="item"><?php echo $row['backgrounds'] ?></div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="center">
                <hr width="50%">
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <center>
                <div class='warning'>This where you list limits for backgrounds, list those not allowed or what you need
                  to see to justify any of them.</div>
              </center>
            </td>
          </tr>
          <tr>
            <td>
              <div class="item"><textarea name="backgrounds" cols="120" datas="2" class="form"
                  id="backgrounds"><?php echo $row['backgrounds'] ?></textarea></div>
            </td>
          </tr>
          <tr>
            <td>
              <hr>
            </td>
          </tr>
          <tr>
            <td>
              <div class="pagetopic">Venue Allowances 2:</div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="item">Current:</div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="item"><?php echo $row['venue_2'] ?></div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="center">
                <hr width="50%">
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <center>
                <div class='warning'>This is where you put information like limits on powers, or other expected
                  information that players should include about the character for the venue.</div>
              </center>
            </td>
          </tr>
          <tr>
            <td>
              <div class="item"><textarea name="venue_2" cols="120" datas="30" class="form"
                  id="venue_2"><?php echo $row['venue_2'] ?></textarea></div>
            </td>
          </tr>
          <tr>
            <td>
              <hr>
            </td>
          </tr>
          <tr>
            <td>
              <div class="pagetopic">Merits & Flaws:</div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="item">Current:</div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="item"><?php echo $row['merit_flaws'] ?></div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="center">
                <hr width="50%">
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <center>
                <div class='warning'>This is where you put information for banned or capped merits and flaws.</div>
              </center>
            </td>
          </tr>
          <tr>
            <td>
              <div class="item"><textarea name="merit_flaws" cols="120" datas="30" class="form"
                  id="merit_flaws"><?php echo $row['merit_flaws'] ?></textarea></div>
            </td>
          </tr>
          <tr>
            <td>
              <hr>
            </td>
          </tr>
          <tr>
            <td>
              <div class="center"><input name="Select" type="submit" id="Select" value="Update" class="form" /><br \><br
                  \></div>
            </td>
          </tr>
        </table>
    </form>
  </div>
</div>