<div style="padding: 2em;">
    <table width='90%' border='1' cellspacing='1' align="center">
        <caption>
            <div class='pagetopic'>Open Character Requests</div>
        </caption>
        <tr>
            <td width='19%'>
                <div class='item'>Character</div>
            </td>
            <td width='15%'>
                <div class='item'>Venue</div>
            </td>
            <td width='19%'>
                <div class='item'>Type</div>
            </td>
            <td width='15%'>
                <div class='item'>Date Added</div>
            </td>
            <td width='9%'>
                <div class='item'>Status</div>
            </td>
            <td width='15%'>
                <div class='item'>Pend Date</div>
            </td>
            <td width='9%'>
                <div class='item'></div>
            </td>
        </tr>

        <?php
        require '/var/www/shadowsofthebayou.com/site-inc/gamengdb.php';
        $query = "SELECT char_name,char_venue,reqtype,req_date,req_stat,pend_til,id FROM char_reqs WHERE (req_stat='new' OR req_stat='reviewing' OR req_stat='pending' OR req_stat='followup')  ORDER BY pend_til ASC, req_date, char_name";
        $rs = mysqli_query($connection, $query);
        // echo "(search: $query)";
        while ($row = mysqli_fetch_array($rs)) {
            if ($row['req_stat'] == "new" or $row['req_stat'] == "reviewing" or $row['req_stat'] == "followup") {
                ?>
                <table style="width:90%" border="1" cellspacing="0" align="center">
                    <?php
                    echo ("<tr><td width='20%'><div class='itemsm'>$row[char_name]</div></td>
		   <td width='15%'><div class='itemsm'>$row[char_venue]</div></td>
		   <td width='20%' ><div class='itemsm'>$row[reqtype]</div></td>
		   <td width='15%' ><div class='itemsm'>$row[req_date]</div></td>
		   <td width='9%' ><div class='itemsm'>$row[req_stat]</div></td>
		   <td width='15%' ><div class='itemsm'>$row[pend_til]</div></td>
                 <td width='9%'  valign='middle'><div class='item'><input type='hidden' name='id' value='$row[id]'>
<form id='form1' name='form1' method='post' action='../staff-tools/work_req.php'>
<input type='hidden' name='id' value='$row[id]'>
<input type='submit' name='click' class='form' target='_blank' id='click' value='Work'></form></div></td>
	      </tr>
");
            } else {
                ?>
                    <table style="width:90%" border="1" cellspacing="0" align="center">
                        <?php
                        echo ("<tr><td width='20%'><div class='itemsm'>$row[char_name]</div></td>
		   <td width='15%'><div class='itemsm'>$row[char_venue]</div></td>
		   <td width='20%' ><div class='itemsm'>$row[reqtype]</div></td>
		   <td width='15%' ><div class='itemsm'>$row[req_date]</div></td>
		   <td width='9%' ><div class='itemsm'>$row[req_stat]</div></td>
		   <td width='15%' ><div class='itemsm'>$row[pend_til]</div></td>
                 <td width='10%'  valign='middle'><div class='item'><input type='hidden' name='id' value='$row[id]'>
<form id='form1' name='form1' method='post' action='../staff-tools/work_req.php'>
<input type='hidden' name='id' value='$row[id]'>
<input type='submit' name='click' class='form' target='_blank' id='click' value='Work'></form></div></td></tr>");

            }
        }
        ?>
            </table>
            <div style="width: 90%;">
                <ul class="item">
                    <li><strong>Followup:</strong> Staff requested info/clarification from player.</li>
                    <li><strong>New:</strong> Staff has not yet reviewed request.</li>
                    <li><strong>Pending:</strong> Awating time must elapse or other situation.</li>
                    <li><strong>Reviewing:</strong> Staff is looking over request</li>
                </ul>
            </div>
            <br \>
            <hr align="center" width="75%">
</div>