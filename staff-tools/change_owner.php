<?php include '/var/www/shadowsofthebayou.com/site-inc/header.php'; ?>

<div id="wrapper">
    <div id="title"></div><!-- title close -->

    <div class="container">

        <div class="column-nav"><?php require '/var/www/shadowsofthebayou.com/site-inc/check-access.php'; ?>

        </div><!-- menu close -->

        <div class="column-main">
            <?php
            if ($_COOKIE['privilege'] >= "2") {
                echo "You Do not have rights to do this";
            } else {
                ?>

                <?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
                <?php
                $query = "SELECT DISTINCT log_name FROM game_data WHERE account_type='Character' ORDER BY log_name";
                $result = mysqli_query($connection, $query)

                    or die("<div class='error' >Could not execute query.<br \>" . mysqli_error($connection) . "<br \></div>");
                $option = "<select name=\"character_name\"><option value=\"\" class='form'></option>";
                while ($row = mysqli_fetch_array($result)) {
                    $option = "$option <option value=\"$row[log_name]\" class='form'>$row[log_name]</option>";
                }
                $option = "$option </select>";
                ?>
                <?php
                $query2 = "SELECT DISTINCT log_name FROM game_data WHERE account_type='Player' ORDER BY log_name";
                $result2 = mysqli_query($connection, $query2)
                    or die("<div class='error' >Could not execute query 2.<br \>" . mysqli_error($connection) . "<br \></div>");
                $option2 = "<select name=\"player_name\"><option value=\"\" class='form'></option>";
                while ($row = mysqli_fetch_array($result2)) {
                    $option2 = "$option2 <option value=\"$row[log_name]\" class='form'>$row[log_name]</option>";
                }
                $option2 = "$option2 </select>";
                ?>

                <form name="form1" method="post" action="">
                    <table style="width:90%" border="0" align="center">
                        <caption>
                            <div class='pagetopic' align="center">Select A Character To Change It's Owner</div>
                        </caption>
                        <tr>
                            <td>
                                <div class='pageitem' align="center" class="form"><?php echo $option ?> <input name="Select"
                                        type="submit" id="Select" value="Select Character" class="form"></div>
                            </td>
                        </tr>
                    </table>
                </form>
                <?php
                if ($_POST['Select'] == "Select Character" && $_POST['character_name'] <> "") {
                    ?>
                    <?php
                    $query = "SELECT * FROM `game_data` WHERE (log_name=\"$_POST[character_name]\")";
                    $results = mysqli_query($connection, $query)

                        or die("<div class='pagetopic' >Couldn't execute query..<br \>" . mysqli_error($connection) . "<br \></div>");
                    $data = @mysqli_fetch_array($results);
                    ?>
                    <form name="form2" method="post" action="">
                        <input name="character_name" type="hidden" id="character_name"
                            value="<?php echo $_POST['character_name'] ?>">
                        <input name="player_name" type="hidden" id="player_name" value="<?php echo $_POST['player_name'] ?>">
                        <input name="oldstnotes" type="hidden" id="oldnotes" value="<?php echo $data['st_notes'] ?>">
                        <input name="now" type="hidden" id="now" value="<?php echo date('m/d/y') ?>">
                        <input name="old_owner" type="hidden" id="old_owner" value="<?php echo $data['playername'] ?>">
                        <table style="width:90%" border="0" align="center" cellspacing="0">
                            <caption>
                                <div class="pagetopic" align="center">Set New Owner Below</div>
                            </caption>
                            <tr>
                                <td>
                                    <div class='pageitem'>Character</div>
                                </td>
                                <td>
                                    <div class='pageitem'>Current Owner</div>
                                </td>
                                <td>
                                    <div class='pageitem'>Change Owner to:</div>
                                </td>
                            </tr>
                            <tr>
                                <td width="34%">
                                    <div class='item'><?php echo $_POST['character_name'] ?></div>
                                </td>
                                <td width="33%">
                                    <div class='item'><?php echo $data['playername'] ?></div>
                                </td>
                                <td width="33%">
                                    <div class='item'><?php echo $option2 ?></div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center"><input name="Select" type="submit" id="Select"
                                        value="Change Owner" class="form"></td>
                            </tr>
                        </table>
                    </form>
                    <?php
                } elseif ($_POST['Select'] == "Change Owner" && $_POST['character_name'] <> "") { {
                        $query = "UPDATE `game_data` SET playername=\"$_POST[player_name]\",st_notes=\"$_POST[now] $_COOKIE[logname]: Changed owner from '$_POST[old_owner]' to '$_POST[player_name]'<br \>$_POST[oldstnotes]\" WHERE (log_name=\"$_POST[character_name]\")";
                        $result = mysqli_query($connection, $query)
                            or die("Could not execute query.<br \>" . mysqli_error($connection) . "<br \>");
                        echo "<div class='success' >Character ownership has been changed to '$_POST[player_name]'.<br \></div>";
                    }
                }
            }
            ?>


        </div><!-- content close -->


    </div><!-- container close -->
</div><!-- wrapper close -->