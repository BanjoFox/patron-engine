<?php include '/var/www/shadowsofthebayou.com/site-inc/header.php'; ?>

<div id="wrapper">
  <div id="title"></div><!-- title close -->

  <div class="container">

    <div class="column-nav">
      <?php require '/var/www/shadowsofthebayou.com/site-inc/check-access.php'; ?>

    </div><!-- menu close -->

    <div class="column-main">
      <?php
        if ($_COOKIE['privilege'] >= "2") {
            echo "You Do not have rights to do this";
        } else {
            ?>

            <?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
            <?php
            $query = "SELECT DISTINCT log_name FROM game_data WHERE account_type='Player' ORDER BY log_name";
            $result = mysqli_query($connection, $query)

            or die("<div class='error' >Could not execute query.<br \>" . mysqli_error($connection) . "<br \></div>");
            $option = "<select name=\"player_name\" class='form'><option value=\"\" class='form'></option>";
            while ($row = mysqli_fetch_array($result)) {
                $option = "$option <option value=\"$row[log_name]\" class='form'>$row[log_name]</option>";
            }
            $option = "$option </select>";
            ?>
            <?php echo $sqldata['log_name'] ?>

        <form name="form1" method="post" action="<?php echo $PHP_SELF ?>">
          <table style="width:90%" border="0" align="center">
            <caption>
              <div class='pagetopic' align="center">Select A Player To Change It's Name</div>
            </caption>
            <tr>
              <td>
                <div class='pageitem' align="center">
                  <?php echo $option ?> <input name="Select" type="submit" id="Select" value="Select Player" class="form">
                </div>
              </td>
            </tr>
          </table>
        </form>
            <?php
            if ($_POST['Select'] == "Select Player" && $_POST['player_name'] <> "") {
                ?>
                <?php
                $query = "SELECT * FROM `game_data` WHERE (log_name=\"$_POST[player_name]\")";
                $results = mysqli_query($connection, $query)

                or die("<div class='pagetopic' >Couldn't get player name..<br \>" . mysqli_error($connection) . "<br \></div>");
                $data = @mysqli_fetch_array($results);
                ?>
          <form name="form2" method="post" action="<?php echo $PHP_SELF ?>">
            <input name="player_name" type="hidden" id="player_name" value="<?php echo $_POST['player_name'] ?>">
            <input name="id" type="hidden" id="id" value="<?php echo $data['id'] ?>">
            <table style="width:90%" border="0" align="center" cellspacing="0">
              <caption>
                <div class="pagetopic" align="center">Change Name Below</div>
              </caption>
              <tr>
                <td>
                  <?php
                    // echo ("ID: '$data[id]'<br \>Player Name:'$data[log_name]'");
                ?>
                </td>
              </tr>
              <tr>
                <td>
                  <div class='pageitem'>Current Name</div>
                </td>
                <td>
                  <div class='pageitem'>Change Name To:</div>
                </td>
              </tr>
              <tr>
                <td width="34%">
                  <div class='item'>
                    <?php echo $_POST['player_name'] ?>
                  </div>
                </td>
                <td width="33%"><input name="new_name" type="text" class="form" id="new_name" /></td>
              </tr>
              <tr>
                <td colspan="2" align="center">
                  <div class="warning">Be aware this does not check for duplicate names.</div>
                </td>
              </tr>
              <tr>
                <td colspan="2" align="center">
                  <hr>
                </td>
              </tr>
              <tr>
                <td colspan="2" align="center"><input name="Select" type="submit" id="Select" value="Change Name"
                    class="form"></td>
              </tr>
            </table>
          </form>
                <?php
            } elseif ($_POST['Select'] == "Change Name" && $_POST['new_name'] <> "") {
                {
                    $query = "UPDATE `game_data` SET log_name=\"$_POST[new_name]\" WHERE (log_name=\"$_POST[player_name]\")";
                    $result = mysqli_query($connection, $query)
                    or die("Could not change name.<br \>" . mysqli_error($connection) . "<br \>");
                    echo "<div class='success' >Player name has been changed to '$_POST[new_name]'.</div>";
                }
            }
            ?>
            <?php
        }
?>



    </div><!-- content close -->


  </div><!-- container close -->
</div><!-- wrapper close -->
