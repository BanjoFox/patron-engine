<?php include '/var/www/shadowsofthebayou.com/site-inc/header.php'; ?>

<div id="wrapper">
    <div id="title"></div><!-- title close -->

    <div class="container">

        <div class="column-nav"><?php require '/var/www/shadowsofthebayou.com/site-inc/check-access.php'; ?>

        </div><!-- menu close -->

        <div class="column-main">
            <?php
            if ($_COOKIE['privilege'] >= "3") {
                echo "You Do not have rights to do this";
            } else {
                ?>

                <?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
                <?php
                $query = "SELECT DISTINCT log_name FROM game_data WHERE (account_type='Player') ORDER BY log_name";
                $result = mysqli_query($connection, $query)

                    or die("Could not execute query.<br \>" . mysqli_error($connection) . "<br \>");
                $option = "<select name=\"log_name\" class='form'><option value=\"\" class='form'></option>
                                <option value=playername!='' selected class='form'>Any</option>";
                while ($row = mysqli_fetch_array($result)) {
                    $option = "$option <option value=\"playername='$row[log_name]'\" class='form'>$row[log_name]</option>";
                }
                $option = "$option </select>";
                ?>
                <form name="form1" method="post" action="">
                    <div class="pagetopic" align="center">Character Search</div>
                    <table border="1" cellspacing="0" celladding="0" align="center" width="50%">
                        <caption>
                            <div class='pagetopic' align="center">Choose Criteria</div>
                        </caption>
                        <tr>
                            <td>
                                <table border="0" cellspacing="0" celladding="0" align="center" width="100%">
                                    <td width="16%">
                                        <div class="pageitem">Venue</div>
                                    </td>
                                    <td width="16%">
                                        <div class="pageitem">Sub-venue</div>
                                    </td>
                                    <td width="19%">
                                        <div class="pageitem">Player</div>
                                    </td>
                                    <td width="16%">
                                        <div class="pageitem">Sanct?</div>
                                    </td>
                                    <td width="16%">
                                        <div class="pageitem">Type</div>
                                    </td>
                        </tr>
                        <td>
                            <select name="select_venue" id="select_venue" class="form">
                                <option value="char_venue!=''">Any</option>
                                <option value="char_venue='Vampire'">Vampire</option>
                                <option value="char_venue='Werewolf'">Werewolf</option>
                                <option value="char_venue='Mage'">Mage</option>
                                <option value="char_venue='Changeling'">Changeling</option>
                                <option value="char_venue='Wraith'">Wraith</option>
                                <option value="char_venue='Bygone'">Bygone</option>
                                <option value="char_venue='Mortal'">Mortal</option>
                            </select>
                        <td>
                            <select name="select_subvenue" id="select_subvenue" class="form">
                                <option value="sub_venue!=''">Any</option>
                                <option value="sub_venue='Vampire'">Vampire</option>
                                <option value="sub_venue='Ghoul'">Ghoul</option>
                                <option value="sub_venue='Werewolf'">Werewolf</option>
                                <option value="sub_venue='Fera'">Fera</option>
                                <option value="sub_venue='Kinfolk'">Kinfolk</option>
                                <option value="sub_venue='Mage'">Mage</option>
                                <option value="sub_venue='Sorcerer'">Sorcerer</option>
                                <option value="sub_venue='Changeling'">Changeling</option>
                                <option value="sub_venue='Kinian'">Kinain</option>
                                <option value="sub_venue='Wraith'">Wraith</option>
                                <option value="sub_venue='Bygone'">Bygone</option>
                                <option value="sub_venue='Mortal'">Mortal</option>
                            </select>
                        </td>
                        <td>
                            <?php echo $option ?>
                        </td>
                        <td align="center">
                            <select name="sanctioned" id="sanctioned" class="form">
                                <option value="sanctioned!=''">Any</option>
                                <option value="sanctioned='Yes'">Yes</option>
                                <option value="sanctioned='No'">No</option>
                                <option value="sanctioned='Hold'">Hold</option>
                            </select>
                        </td>
                        <td align="center">
                            <select name="char_type" id="char_type" class="form">
                                <option value="char_type!=''">Any</option>
                                <option value="char_type='PC'">PC</option>
                                <option value="char_type='NPC'">NPC</option>
                                <option value="char_type='RET'">RET</option>
                            </select>
                        </td>
                        </tr>
                        <tr>
                            <td colspan='6'>
                                <hr width="75%">
                            </td>
                        </tr>
                        <tr>
                            <td colspan='6'>
                                <div class="center"><input name="Select" type="submit" id="Select" value="Search"
                                        class="form"></div>
                            </td>
                    </table>
                    </td>
                    </tr>
                    </table>
                    <br \>
                    <hr width="75%">
                </form>
                <?php
                if ($_POST['Select'] == "Search") {
                    ?>
                    <?php
                    $type = "account_type='Character' AND";
                    $char_venue = (stripslashes("$_POST[select_venue] AND"));
                    $sub_venue = (stripslashes("$_POST[select_subvenue] AND"));
                    $player = (stripslashes("$_POST[log_name] AND"));
                    $sanctioned = (stripslashes("$_POST[sanctioned] AND"));
                    $last_log = (stripslashes("$_POST[last_login] AND"));
                    $chartype = (stripslashes("$_POST[char_type] AND"));
                    $last = "deleted!='Yes'";
                    $find = ("($char_venue  $sub_venue $player $sanctioned $chartype $type $last)");
                    //echo ("Search : $find <br \>");
                    //echo ("date : $onemonthago <br \>");
                    ?>
                    <table border="1" style="width:90%" cellspacing="0" align="center">
                        <caption>
                            <div class="pagetopic" align="center">Results</div>
                        </caption>
                        <tr>
                            <td width='22%'>
                                <div class='item'>Character Name</div>
                            </td>
                            <td width='14%'>
                                <div class='item'>Venue</div>
                            </td>
                            <td width='14%'>
                                <div class='item'>Sub-Venue</div>
                            </td>
                            <td width='20%'>
                                <div class='item'>Player</div>
                            </td>
                            <td width='9%'>
                                <div class='itemsm'>Last Log</div>
                            </td>
                            <td width='8%'>
                                <div class='itemsm'>Sant?</div>
                            </td>
                            <td width='6%'>
                                <div class='itemsm'>Type</div>
                            </td>
                            <td width='7%'>
                                <div class='item'></div>
                            </td>
                        </tr>
                    </table>
                    <?php
                    $query = "SELECT  log_name,char_venue,sub_venue,playername,last_login,sanctioned,char_type,id FROM game_data WHERE $find ORDER BY log_name ASC";
                    $rs = mysqli_query($connection, $query);
                    while ($row = mysqli_fetch_array($rs)) {
                        ?>
                        <table border="1" cellspacing="0" style="width:90%" align="center">
                            <?php
                            echo("<td width='22%'><div class='itemsm'>$row[log_name]</div></td><td width='14%'><div class='itemsm'>$row[char_venue]</div></td><td width='14%'><div class='itemsm'>$row[sub_venue]</div></td><td width='20%'><div class='itemsm'>$row[playername]</div></td><td width='9%'><div class='itemsm'>$row[last_login]</div></td><td width='8%'><div  class='itemsm'>$row[sanctioned]</div></td><td width='6%'><div class='itemsm' >$row[char_type]</div></td><td width='7%'><div class='itemsm' ></div>
<input type='hidden' name='id' value='$row[id]'>
<form id='form1' name='form1' method='post' target='_blank' action='../c-sheets/view_character.php'>
<input type='hidden' name='id' value='$row[id]'>
<input type='submit' name='click' id='click' value='View' class='form'> 
</form></p></td></tr>");
                    }
                }
            }
?>
            </table>


        </div><!-- content close -->


    </div><!-- container close -->
</div><!-- wrapper close -->