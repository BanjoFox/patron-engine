<?php include '/var/www/shadowsofthebayou.com/site-inc/header.php'; ?>

<div id="wrapper">
    <div id="title"></div><!-- title close -->

    <div class="container">

        <div class="column-nav"><?php require '/var/www/shadowsofthebayou.com/site-inc/check-access.php'; ?>

        </div><!-- menu close -->

        <div class="column-main">
            <?php
            require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php";

            if ($_COOKIE['privilege'] >= "3") {
                echo "You Do not have rights to do this";
            } elseif ($_POST['Select'] == "Delete Character" && $_POST['log_name'] <> "" && $_POST['Verify'] == "Yes") {

                    $query = "DELETE FROM `game_data` WHERE (log_name=\"$_POST[log_name]\")";
                    $result = mysqli_query($connection, $query)
                        or die("<div  class='error'>Could not delete from Patron.<br \>" . mysqli_error($connection) . "<br \></div>");

                    $query = "DELETE FROM `char_reqs` WHERE (char_name=\"$_POST[log_name]\")";
                    $result = mysqli_query($connection, $query)
                        or die("<div  class='error'>Could not delete records from character requests.<br \>" . mysqli_error($connection) . "<br \></div>");

                    $query = "DELETE FROM `character_logins` WHERE (character_name=\"$_POST[log_name]\")";
                    $result = mysqli_query($connection, $query)
                        or die("<div  class='error'>Could not delete records from character logins.<br \>" . mysqli_error($connection) . "<br \></div>");

                    $query = "DELETE FROM `endowments` WHERE (used_on=\"$_POST[log_name]\")";
                    $result = mysqli_query($connection, $query)
                        or die("<div  class='error'>Could not delete records from influence requests.<br \>" . mysqli_error($connection) . "<br \></div>");

                    $query = "DELETE FROM `game_data` WHERE (employer=\"$_POST[log_name]\")";
                    $result = mysqli_query($connection, $query)
                        or die("<div  class='error'>Could not delete retainers.<br \>" . mysqli_error($connection) . "<br \></div>");

                    $query = "DELETE FROM `huntinglog` WHERE (charactername=\"$_POST[log_name]\")";
                    $result = mysqli_query($connection, $query)
                        or die("<div  class='error'>Could not delete replenihses<br \>" . mysqli_error($connection) . "<br \></div>");

                    echo "<div  class='success'>The character sheet, requests, endowment, and retainers has been deleted for the character '$_POST[log_name]'!<br \><br \><br \></div>";
                }

                $query = "SELECT DISTINCT log_name FROM `game_data` WHERE (deleted='Yes' AND account_type='Character') ORDER BY log_name";
                $result = mysqli_query($connection, $query)
                    or die("<div  class='error'>Could not execute query.<br \>" . mysqli_error($connection) . "<br \></div>");
                $option = "<select name=\"log_name\" class='form'><option value=\"\" class='form'></option>";
                while ($row = mysqli_fetch_array($result)) {
                    $option = "$option <option value=\"$row[log_name]\" class='form'>$row[log_name]</option>";
                }
                $option = "$option </select>";

                ?>
                <form name="form1" method="post" action="">
                    <table style="width:90%" align="center">
                        <caption>
                            <div align="center" class='pagetopic'>Select Character To Permanently Delete</div>
                        </caption>
                        <tr>
                            <td>
                                <div class="center"><?php echo $option ?> <input name="Select" type="submit" class="form"
                                        id="Select" value="Select Character"></div>
                            </td>
                        </tr>
                    </table>
                </form>
                <?php
                if ($_POST['Select'] == "Select Character" && $_POST['log_name'] <> "") {
                    ?>
                    <?php
                    $query = "SELECT * FROM `game_data` WHERE (log_name=\"$_POST[log_name]\")";
                    $results = mysqli_query($connection, $query)
                        or die("<div  class='error'>Couldn't execute query..<br \>" . mysqli_error($connection) . "<br \>");
                    $data = @mysqli_fetch_array($results);
                }
                    ?>
                    <br />
                    <form name="form2" method="post" action="">
                        <input name="log_name" type="hidden" id="log_name" value="<?php echo $_POST['log_name'] ?>">
                        <table style="width:90%" border="0" align="center">
                            <caption>
                                <div align="center" class='pagetopic'>Permanently Delete Character Below</div>
                            </caption>
                            <tr>
                                <td>
                                    <div align="center" class='pageitem'>Character Name</div>
                                </td>
                                <td>
                                    <div align="center" class='warning'>Notice</div>
                                </td>
                                <td>
                                    <div align="center" class='pageitem'>Sure?</div>
                                </td>
                            </tr>
                            <tr>
                                <td width="33%">
                                    <div align="center" class='item'><?php echo $_POST['log_name'] ?></div>
                                </td>
                                <td width="47%">
                                    <div align="center" class='warning'>Before you submit make sure you understand that this NOT
                                        reversible.</div>
                                </td>
                                <td width="20%">
                                    <div class="center">
                                        <select name="Verify" class="form" id="Verify">
                                            <option selected>No</option>
                                            <option>Yes</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="center">
                                        <hr>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="center"><input name="Select" type="submit" class="form" id="Select"
                                            value="Delete Character"></div>
                                </td>
                            </tr>
                        </table>
                    </form>

        </div><!-- content close -->
    </div><!-- container close -->
</div><!-- wrapper close -->