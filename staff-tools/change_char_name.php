<?php include '/var/www/shadowsofthebayou.com/site-inc/header.php'; ?>

<div id="wrapper">
<div id="title"></div><!-- title close -->

<div class="container">

       <div class="column-nav"><?php require '/var/www/shadowsofthebayou.com/site-inc/check-access.php'; ?>

       </div><!-- menu close -->

       <div class="column-main">
<?php
if ($_COOKIE['privilege'] >= "2") {
    echo "You Do not have rights to do this";
} else {
    ?>

    <?php require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; ?>
      <?php
        $query = "SELECT DISTINCT log_name FROM game_data WHERE account_type='Character' ORDER BY log_name";
    $result = mysqli_query($connection, $query)

                 or die("<div class='error' >Could not execute query.<br \>".mysqli_error($connection)."<br \></div>");
    $option = "<select name=\"character_name\" class='form'><option value=\"\" class='form'></option>";
    while ($row = mysqli_fetch_array($result)) {
        $option = "$option <option value=\"$row[log_name]\" class='form'>$row[log_name]</option>";
    }
    $option = "$option </select>";
    ?>
    <?php echo $sqldata['log_name']?>

<form name="form1" method="post" action="">
<table style="width:90%" border="0" align="center" >
<caption><div class='pagetopic' align="center">Select A Character To Change It's Name</div></caption>
  <tr>
    <td><div class='pageitem' align="center"><?php echo $option?>  <input name="Select" type="submit" id="Select" value="Select Character" class="form"></div></td>
  </tr>
</table>
</form>
    <?php
    if ($_POST['Select'] == "Select Character" && $_POST['character_name'] <> "") {
        ?>
        <?php
        $query = "SELECT * FROM `game_data` WHERE (log_name=\"$_POST[character_name]\")";
        $results = mysqli_query($connection, $query)

         or die("<div class='pagetopic' >Couldn't get character name..<br \>".mysqli_error($connection)."<br \></div>");
        $data = @mysqli_fetch_array($results);
        ?>
<form name="form2" method="post" action="">
<input name="character_name" type="hidden" id="character_name" value="<?php echo $_POST['character_name']?>">
<input name="id" type="hidden" id="id" value="<?php echo $data['id']?>">

<input name="oldstnotes" type="hidden" id="oldnotes" value="<?php echo $data['st_notes']?>">
<input name="now" type="hidden" id="now" value="<?php echo date('m/d/y')?>">
<input name="old_name" type="hidden" id="old_name" value="<?php echo $_POST['character_name']?>">

    <table style="width:90%"  border="0" align="center"  cellspacing="0">
    <caption><div class="pagetopic" align="center">Change Name Below</div></caption>
<tr>
<td>
        <?php
        // echo ("ID: '$data[id]'<br \>Character Name:'$data[log_name]'");
        ?>
</td>
</tr>
        <tr>
            <td><div class='pageitem'>Current Name</div></td>
              <td><div class='pageitem'>Change Name To:</div></td>
        </tr>
        <tr>
            <td width="34%"><div class='item'><?php echo $_POST['character_name']?></div></td>
              <td width="33%"><input name="new_name" type="text" class="form" id="new_name" /></td>
          </tr>
        <tr>
            <td colspan="2" align="center"><div class="warning">Be aware this does not check for duplicate names.</div></td>
          </tr>
        <tr>
            <td colspan="2" align="center"><hr></td>
          </tr>
        <tr>
            <td colspan="2" align="center"><input name="Select" type="submit" id="Select" value="Change Name" class="form"></td>
          </tr>
    </table>
</form>
        <?php
    } elseif ($_POST['Select'] == "Change Name" && $_POST['new_name'] <> "") {
        {
            $query = "UPDATE `game_data` SET log_name=\"$_POST[new_name]\",st_notes=\"$_POST[now] $_COOKIE[logname]: Changed character name from '$_POST[old_name]' to '$_POST[new_name]'<br \>$_POST[oldstnotes]\" WHERE (id=\"$_POST[id]\")";
            $result = mysqli_query($connection, $query)
            or die("Could not change name.<br \>".mysqli_error($connection)."<br \>");
            // echo "'$query'<br \>";
            $query = "UPDATE `char_reqs` SET char_name=\"$_POST[new_name]\" WHERE (char_name=\"$_POST[character_name]\")";
            $result = mysqli_query($connection, $query)
            or die("Could not change character requests name.<br \>".mysqli_error($connection)."<br \>");
            // echo "'$query'<br \>");

            echo "<div class='success' >Character name has been changed from '$_POST[old_name]' to '$_POST[new_name]'.<br \>
                              Character Logins, and Requests are updated as well.</div>";
        }
    }
    ?>
    <?php
}
?>



              </div><!-- content close -->


</div><!-- container close -->
</div><!-- wrapper close -->
