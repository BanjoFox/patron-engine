<?php include '/var/www/shadowsofthebayou.com/site-inc/header.php'; ?>

<div style="padding: 2em;">
    <table style="width:90%" border="1" align="center">
        <caption class="pagetopic">Characters Ready For Approval</caption>
        <tr>
            <td width='30%'><span class='item'>Character Name</span></td>
            <td width='20%'>
                <div class='item'>Type</div>
            </td>
            <td width='25%'>
                <div class='item'>Player</div>
            </td>
            <td width='15%'>
                <div class='item'>Created</div>
            </td>
            <td width='10%'>
                <div class='item'>&nbsp;
            </td>
        </tr>
        <?php $query = "SELECT log_name,char_venue,sub_venue,playername,create_date,sanctioned,last_login,sanctioned,
            id FROM game_data WHERE (account_type='Character' AND (sanctioned='No' OR sanctioned='Pre')
            AND deleted<>'Yes' AND sfa='Yes') ORDER BY create_date,sub_venue,log_name";
        $rs = mysqli_query($connection, $query);
        while ($data = mysqli_fetch_assoc($rs)) {
            $cells = [
                'Character Name' => $data['log_name'],
                'Type' => $data['sub_venue'],
                'Player' => $data['playername'],
                'Created' => $data['create_date']
            ];
            echo "<tr>";
            foreach ($cells as $label => $value) {
                echo "<td class='item'>$value</td>";
            }
            echo "<td>
                <form method='post' target='_blank' action='../c-sheets/view_character.php'>
                <input type='hidden' name='id' value='$data[id]'>
                <input type='submit' name='click' class='form' target='_blank' id='click' value='View'>
                </form>
                </td>";
            echo "</tr>";
        }
        ?>
    </table>
    <hr align="center" width="75%">
</div>