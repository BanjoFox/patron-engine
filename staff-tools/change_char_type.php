<?php include '/var/www/shadowsofthebayou.com/site-inc/header.php'; ?>

<div id="wrapper">
    <div id="title"></div><!-- title close -->

    <div class="container">

        <div class="column-nav"><?php require '/var/www/shadowsofthebayou.com/site-inc/check-access.php'; ?></div><!-- menu close -->

        <div class="column-main">
            <?php
            if ($_COOKIE['privilege'] >= "2") {
                echo "You do not have rights to do this";
            } else {
                require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php";

                $query = "SELECT DISTINCT log_name FROM game_data WHERE (account_type='Character' AND deleted<>'Yes') ORDER BY log_name";
                $result = mysqli_query($connection, $query) or die("<div class='error'>Could not execute query.<br \>" . mysqli_error($connection) . "</div><br \>");

                $option = "<select name=\"log_name\" class='form'><option value=\"\" class='form'></option>";
                while ($row = mysqli_fetch_array($result)) {
                    $option .= "<option value=\"$row[log_name]\" class='form'>$row[log_name]</option>";
                }
                $option .= "</select>";

                ?>
                <form name="form1" method="post" action="">
                    <table width="85%" border="0" align="center" cellspacing="0">
                        <caption><div align="center" class='pagetopic'>Select A Character Below To Change Its Type</div></caption>
                        <tr>
                            <td><div class="center"><?php echo $option; ?> <input name="Select" type="submit" id="Select" value="Select Character" class="form"></div></td>
                        </tr>
                    </table>
                </form>
                <?php
                if ($_POST['Select'] == "Select Character" && $_POST['log_name'] != "") {
                    $query = "SELECT * FROM `game_data` WHERE (log_name=\"$_POST[log_name]\")";
                    $results = mysqli_query($connection, $query) or die("<div class='error'>Couldn't execute query..<br \>" . mysqli_error($connection) . "</div><br \>");
                    $data = @mysqli_fetch_array($results);
                    ?>
                    <form name="form2" method="post" action="">
                        <input name="log_name" type="hidden" id="log_name" value="<?php echo $_POST['log_name'] ?>">
                        <input name="oldstnotes" type="hidden" id="oldnotes" value="<?php echo $data['st_notes'] ?>">
                        <input name="now" type="hidden" id="now" value="<?php echo date('m/d/y') ?>">
                        <input name="old_type" type="hidden" id="old_type" value="<?php echo $data['sub_venue'] ?>">
                        <hr>
                        <table width="85%" border="0" align="center">
                            <caption><div class="center">Change Character Type Below</div></caption>
                            <tr>
                                <td><div class='pageitem'>Character Name:</div></td>
                                <td><div class='pageitem'>Current Type:</div></td>
                                <td><div class='pageitem'>Change Type To:</div></td>
                            </tr>
                            <tr>
                                <td width="34%"><div class='item'><?php echo $_POST['log_name'] ?></div></td>
                                <td width="33%"><div class='item'><?php echo $data['sub_venue'] ?></div></td>
                                <td width="33%">
                                    <select name="change_to" id="change_to" class="form">
                                        <option value=""></option>
                                        <option value="Vampire">Vampire</option>
                                        <option value="Ghoul">Ghoul</option>
                                        <option value="Werewolf">Werewolf</option>
                                        <option value="Fera">Fera</option>
                                        <option value="Kinfolk">Kinfolk</option>
                                        <option value="Mage">Mage</option>
                                        <option value="Sorcerer">Sorcerer</option>
                                        <option value="Changeling">Changeling</option>
                                        <option value="Kinian">Kinain</option>
                                        <option value="Wraith">Wraith</option>
                                        <option value="Bygone">Bygone</option>
                                        <option value="Mortal">Mortal</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3"><div class='warnsm'>Keep in mind while this can be useful to correct a character type choice for a player who decides they want a different type of character this tool is mostly meant for "upgrading" a character from a sub venue to the main venue, like a mortal or ghoul to a vampire for example for more radical changes (like from a mage to a vampire) care must be taken to ensure that abilities from one venue are noted as they may not appear on the new sheet if they are not already there.</div></td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center"><input name="Select" type="submit" id="Select" value="Change" class="form"></td>
                            </tr>
                        </table>
                    </form>
                    <?php
                } elseif ($_POST['Select'] == "Change" && $_POST['log_name'] != "") {
                    switch ($_POST['change_to']) {
                        case "Vampire":
                        case "Ghoul":
                            $_POST['new_venue'] = "Vampire";
                            break;
                        case "Werewolf":
                        case "Fera":
                        case "Kinfolk":
                            $_POST['new_venue'] = "Werewolf";
                            break;
                        case "Mage":
                        case "Sorcerer":
                            $_POST['new_venue'] = "Mage";
                            break;
                        case "Changeling":
                        case "Kinian":
                            $_POST['new_venue'] = "Changeling";
                            break;
                        case "Wraith":
                            $_POST['new_venue'] = "Wraith";
                            break;
                        case "Bygone":
                            $_POST['new_venue'] = "Bygone";
                            break;
                        case "Mortal":
                            $_POST['new_venue'] = "Mortal";
                            break;
                    }
                    $query = "UPDATE game_data SET sub_venue=\"$_POST[change_to]\",char_venue=\"$_POST[new_venue]\",st_notes=\"$_POST[now] $_COOKIE[logname]: Changed character from a '$_POST[old_type]' to a '$_POST[change_to]' <br \>$_POST[oldstnotes]\" WHERE log_name='$_POST[log_name]'";
                    $result = mysqli_query($connection, $query) or die("<div class='error'>Could not update.<br \>" . mysqli_error($connection) . "<br \>Query: $query</div>");
                    echo "<div class='success'>Character '$_POST[log_name]' has been changed to a '$_POST[change_to]'.</div><br \>";
                }
            }
?>
        </div><!-- content close -->
    </div><!-- container close -->
</div><!-- wrapper close -->
