<?php
include '/var/www/shadowsofthebayou.com/site-inc/header.php';
require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php";
?>

<div id="wrapper">
    <div id="title"></div><!-- title close -->

    <div class="container">

        <div class="column-nav"><?php require '/var/www/shadowsofthebayou.com/site-inc/check-access.php'; ?>

        </div><!-- menu close -->

        <div class="column-main">
            <?php
            if ($_COOKIE['privilege'] >= "3") {
                echo "You Do not have rights to do this";
            } else {
                if (isset($_POST['Select']) && $_POST['Select'] == "Change Hold" && isset($_POST['log_name']) && $_POST['log_name'] <> "" && isset($_POST['Verify']) && $_POST['Verify'] == "Yes") {
                    $newdate = date('Y-m-d', strtotime("2 month ago"));

                    $query = "UPDATE game_data SET sanctioned='Yes',last_modified='$_COOKIE[logname]',last_login='$newdate',st_notes=\"$_POST[now] $_COOKIE[logname]: Character Hold Reset Performed<br \>$_POST[oldstnotes] \" WHERE (log_name=\"$_POST[log_name]\")";
                    $result = mysqli_query($connection, $query) or die("Could not execute query.<br \>" . mysqli_error($connection) . "<br \>");
                    echo "<div  class='success'>Hold status for '$_POST[log_name]' has been altered.</div>";
                }

                $query = "SELECT DISTINCT log_name FROM `game_data` WHERE (deleted<>'Yes' AND account_type='Character' AND sanctioned='Hold') ORDER BY log_name ASC";
                $result = mysqli_query($connection, $query)
                    or die("Could not execute query.<br \>" . mysqli_error($connection) . "<br \>");
                $option = "<select name=\"log_name\" class='form'><option value=\"\" class='form'></option>";
                while ($row = mysqli_fetch_array($result)) {
                    $option = "$option <option value=\"$row[log_name]\" class='form'>$row[log_name]</option>";
                }
                $option = "$option </select>";
                ?>
                <?php echo $sqldata['log_name'] ?>
                <form name="form1" method="post" action="">
                    <table style="width:90%" align="center">
                        <caption>
                            <div class='pagetopic' align="center">Select A Character Change Hold Status</div>
                        </caption>
                        <tr>
                            <td>
                                <div class="center"><?php echo $option ?> <input name="Select" type="submit" class="form"
                                        id="Select" value="Select Character"></div>
                            </td>
                        </tr>
                    </table>
                </form>
                <?php
                if ($_POST['Select'] == "Select Character" && $_POST['log_name'] <> "") {
                    ?>
                    <?php
                    $query = "SELECT * FROM `game_data` WHERE (log_name=\"$_POST[log_name]\")";
                    $results = mysqli_query($connection, $query)
                        or die("Couldn't execute query..<br \>" . mysqli_error($connection) . "<br \>");
                    $data = @mysqli_fetch_array($results);
                    ?>
                    <br />
                    <?php
                    $twomonthsago = date('Y-m-d', strtotime("2 month ago"));
                    ?>
                    <form name="form2" method="post" action="">
                        <input name="log_name" type="hidden" id="log_name" value="<?php echo $_POST['log_name'] ?>">
                        <input name="oldstnotes" type="hidden" id="oldnotes" value="<?php echo $data['st_notes'] ?>">
                        <input name="now" type="hidden" id="now" value="<?php echo date('m/d/y') ?>">
                        <input name="newstnotes" type="hidden" id="newstnotes" value="Character Hold Reset Performed">
                        <input name="newdate" type="hidden" id="newdate" value="date('Y-m-d',strtotime('2 month ago'))">


                        <table style="width:90%" border="0" align="center">
                            <caption>
                                <div class="pagetopic" align="center">Change Hold Status Below</div>
                            </caption>
                            <tr>
                                <td>
                                    <div class="pageitem" align="center">Character Name</div>
                                </td>
                                <td>
                                    <div class="warning" align="center">Notice</div>
                                </td>
                                <td>
                                    <div class="pageitem" align="center">Sure?</div>
                                </td>
                            </tr>
                            <tr>
                                <td width="33%">
                                    <div class="item" align="center"><?php echo $_POST['log_name'] ?>
                                </td>
                                <td width="47%">
                                    <div class="warnsm" align="center">This will set the last log in to 2 months ago today, this
                                        means they have two months to log the character in or it will go back on hold.</div>
                                </td>
                                <td width="20%">
                                    <div class="item" align="center">
                                        <select name="Verify" class="form" id="Verify" class="form">
                                            <option selected>No</option>
                                            <option>Yes</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="center">
                                        <hr>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="center"><input name="Select" type="submit" class="form" id="Select"
                                            value="Change Hold" class="form"></div>
                                </td>
                            </tr>
                        </table>
                        <?php echo ("date: $newdate"); ?>

                    </form>
                <?php }
            } ?>
        </div><!-- content close -->


    </div><!-- container close -->
</div><!-- wrapper close -->