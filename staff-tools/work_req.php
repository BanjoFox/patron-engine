<?php
require '/var/www/shadowsofthebayou.com/site-inc/header.php';
?>

<div id="wrapper">
    <div id="title"></div><!-- title close -->

    <div class="container">

        <div class="column-nav"><?php require '../site-inc/check-access.php'; ?>

        </div><!-- menu close -->

        <div class="column-main">
            <?php
            if ($_COOKIE['privilege'] >= "3") {
                echo "You Do not have rights to do this";
            } else {
                ?>
                <?php include "../site-inc/gamengdb.php"; ?>
                <?php
                $query = "SELECT * FROM `char_reqs` WHERE (id='$_POST[id]')";
                $result = mysqli_query($connection, $query)
                    or die("<div class='error'>Couldn't get request data.<br>" . mysqli_error($connection) . "<br></div>");
                $row = mysqli_fetch_array($result);
                ?>
                <form name="form2" method="post" action="workreq.php">
                    <input name="oldremark" type="hidden" id="oldnotes" value="<?php echo $row['staff_remark'] ?>">
                    <input name="now" type="hidden" id="now" value="<?php echo date('m/d/y') ?>">
                    <input name="id" type="hidden" id="char_name" value="<?php echo $_POST['id'] ?>">
                    <input name="char_name" type="hidden" id="char_name" value="<?php echo $row['char_name'] ?>">
                    <table width='85%' cellspacing='1'>
                        <caption>
                            <div class='pageitem'>Work Character Request</div>
                        </caption>
                        <tr>
                            <td width='40%'>
                                <div class='pageitem'>Character Name</div>
                            </td>
                            <td width='3%'></td>
                            <td width='57%'>
                                <div class='item'><?php echo $row['char_name']; ?></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class='pageitem'>Player Name</div>
                            </td>
                            <td></td>
                            <td>
                                <div class='item'><?php echo $row['player_name'] ?></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='3'>
                                <hr width='75%'>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class='pageitem'>Request Type</div>
                            </td>
                            <td></td>
                            <td>
                                <div class='item'><?php echo $row['reqtype'] ?></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='3'>
                                <hr width='75%'>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class='pageitem'>Justification</div>
                            </td>
                            <td></td>
                            <td>
                                <div class='item'><?php echo $row['justi'] ?></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='3'>
                                <hr width='75%'>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class='pageitem'>Request Date</div>
                            </td>
                            <td></td>
                            <td>
                                <div class='item'><?php echo $row['req_date'] ?></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='3'>
                                <hr width='75%'>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class='pageitem'>ST Remarks</div>
                            </td>
                            <td></td>
                            <td><textarea name="staff_remark" id="staff_remark" cols="40" rows="3" class='form'></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class='pageitem'>Request Status</div>
                            </td>
                            <td></td>
                            <td><select name="req_stat" id="req_stat" class="form">
                                    <option selected>
                                        <?php echo $row['req_stat'] ?>
                                        <value="<?php echo $data['req_stat'] ?>" /option>
                                    <option value="approved">approved</option>
                                    <option value="denied">denied</option>
                                    <option value="followup">followup</option>
                                    <option value="pending">pending</option>
                                    <option value="reviewing">reviewing</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class='pageitem'>Pend Until</div>
                            </td>
                            <td></td>
                            <td>
                                <select name="pend_time" id="pend_time" class="form">
                                    <option selected><?php echo $row['pend_til'] ?>
                                        <value="<?php echo $data['pend_til'] ?>">
                                    </option>
                                    <option value="0000-1-1">0000-1-1</option>
                                    <option value="0000-1-2">0000-1-2</option>
                                    <option value="0000-1-3">0000-1-3</option>
                                    <option value="0000-1-4">0000-1-4</option>
                                    <option value="0000-1-5">0000-1-5</option>
                                    <option value="0000-1-6">0000-1-6</option>
                                    <option value="0000-1-7">0000-1-7</option>
                                    <option value="0000-1-8">0000-1-8</option>
                                    <option value="0000-1-9">0000-1-9</option>
                                    <option value="0000-1-10">0000-1-10</option>
                                    <option value="0000-1-11">0000-1-11</option>
                                    <option value="0000-1-12">0000-1-12</option>
                                    <option value="0000-1-13">0000-1-13</option>
                                    <option value="0000-1-14">0000-1-14</option>
                                    <option value="0000-1-15">0000-1-15</option>
                                    <option value="0000-1-16">0000-1-16</option>
                                    <option value="0000-1-17">0000-1-17</option>
                                    <option value="0000-1-18">0000-1-18</option>
                                    <option value="0000-1-19">0000-1-19</option>
                                    <option value="0000-1-20">0000-1-20</option>
                                    <option value="0000-1-21">0000-1-21</option>
                                    <option value="0000-1-22">0000-1-22</option>
                                    <option value="0000-1-23">0000-1-23</option>
                                    <option value="0000-1-24">0000-1-24</option>
                                    <option value="0000-1-25">0000-1-25</option>
                                    <option value="0000-1-26">0000-1-26</option>
                                    <option value="0000-1-27">0000-1-27</option>
                                    <option value="0000-1-28">0000-1-28</option>
                                    <option value="0000-1-29">0000-1-29</option>
                                    <option value="0000-1-30">0000-1-30</option>
                                    <option value="0000-1-31">0000-1-31</option>
                                    <option value="0000-2-1">0000-2-1</option>
                                    <option value="0000-2-2">0000-2-2</option>
                                    <option value="0000-2-3">0000-2-3</option>
                                    <option value="0000-2-4">0000-2-4</option>
                                    <option value="0000-2-5">0000-2-5</option>
                                    <option value="0000-2-6">0000-2-6</option>
                                    <option value="0000-2-7">0000-2-7</option>
                                    <option value="0000-2-8">0000-2-8</option>
                                    <option value="0000-2-9">0000-2-9</option>
                                    <option value="0000-2-10">0000-2-10</option>
                                    <option value="0000-2-11">0000-2-11</option>
                                    <option value="0000-2-12">0000-2-12</option>
                                    <option value="0000-2-13">0000-2-13</option>
                                    <option value="0000-2-14">0000-2-14</option>
                                    <option value="0000-2-15">0000-2-15</option>
                                    <option value="0000-2-16">0000-2-16</option>
                                    <option value="0000-2-17">0000-2-17</option>
                                    <option value="0000-2-18">0000-2-18</option>
                                    <option value="0000-2-19">0000-2-19</option>
                                    <option value="0000-2-20">0000-2-20</option>
                                    <option value="0000-2-21">0000-2-21</option>
                                    <option value="0000-2-22">0000-2-22</option>
                                    <option value="0000-2-23">0000-2-23</option>
                                    <option value="0000-2-24">0000-2-24</option>
                                    <option value="0000-2-25">0000-2-25</option>
                                    <option value="0000-2-26">0000-2-26</option>
                                    <option value="0000-2-27">0000-2-27</option>
                                    <option value="0000-2-28">0000-2-28</option>
                                    <option value="0000-2-29">0000-2-29</option>
                                    <option value="0000-3-1">0000-3-1</option>
                                    <option value="0000-3-2">0000-3-2</option>
                                    <option value="0000-3-3">0000-3-3</option>
                                    <option value="0000-3-4">0000-3-4</option>
                                    <option value="0000-3-5">0000-3-5</option>
                                    <option value="0000-3-6">0000-3-6</option>
                                    <option value="0000-3-7">0000-3-7</option>
                                    <option value="0000-3-8">0000-3-8</option>
                                    <option value="0000-3-9">0000-3-9</option>
                                    <option value="0000-3-10">0000-3-10</option>
                                    <option value="0000-3-11">0000-3-11</option>
                                    <option value="0000-3-12">0000-3-12</option>
                                    <option value="0000-3-13">0000-3-13</option>
                                    <option value="0000-3-14">0000-3-14</option>
                                    <option value="0000-3-15">0000-3-15</option>
                                    <option value="0000-3-16">0000-3-16</option>
                                    <option value="0000-3-17">0000-3-17</option>
                                    <option value="0000-3-18">0000-3-18</option>
                                    <option value="0000-3-19">0000-3-19</option>
                                    <option value="0000-3-20">0000-3-20</option>
                                    <option value="0000-3-21">0000-3-21</option>
                                    <option value="0000-3-22">0000-3-22</option>
                                    <option value="0000-3-23">0000-3-23</option>
                                    <option value="0000-3-24">0000-3-24</option>
                                    <option value="0000-3-25">0000-3-25</option>
                                    <option value="0000-3-26">0000-3-26</option>
                                    <option value="0000-3-27">0000-3-27</option>
                                    <option value="0000-3-28">0000-3-28</option>
                                    <option value="0000-3-29">0000-3-29</option>
                                    <option value="0000-3-30">0000-3-30</option>
                                    <option value="0000-3-31">0000-3-31</option>
                                    <option value="0000-4-1">0000-4-1</option>
                                    <option value="0000-4-2">0000-4-2</option>
                                    <option value="0000-4-3">0000-4-3</option>
                                    <option value="0000-4-4">0000-4-4</option>
                                    <option value="0000-4-5">0000-4-5</option>
                                    <option value="0000-4-6">0000-4-6</option>
                                    <option value="0000-4-7">0000-4-7</option>
                                    <option value="0000-4-8">0000-4-8</option>
                                    <option value="0000-4-9">0000-4-9</option>
                                    <option value="0000-4-10">0000-4-10</option>
                                    <option value="0000-4-11">0000-4-11</option>
                                    <option value="0000-4-12">0000-4-12</option>
                                    <option value="0000-4-13">0000-4-13</option>
                                    <option value="0000-4-14">0000-4-14</option>
                                    <option value="0000-4-15">0000-4-15</option>
                                    <option value="0000-4-16">0000-4-16</option>
                                    <option value="0000-4-17">0000-4-17</option>
                                    <option value="0000-4-18">0000-4-18</option>
                                    <option value="0000-4-19">0000-4-19</option>
                                    <option value="0000-4-20">0000-4-20</option>
                                    <option value="0000-4-21">0000-4-21</option>
                                    <option value="0000-4-22">0000-4-22</option>
                                    <option value="0000-4-23">0000-4-23</option>
                                    <option value="0000-4-24">0000-4-24</option>
                                    <option value="0000-4-25">0000-4-25</option>
                                    <option value="0000-4-26">0000-4-26</option>
                                    <option value="0000-4-27">0000-4-27</option>
                                    <option value="0000-4-28">0000-4-28</option>
                                    <option value="0000-4-29">0000-4-29</option>
                                    <option value="0000-4-30">0000-4-30</option>
                                    <option value="0000-4-31">0000-4-31</option>
                                    <option value="0000-5-1">0000-5-1</option>
                                    <option value="0000-5-2">0000-5-2</option>
                                    <option value="0000-5-3">0000-5-3</option>
                                    <option value="0000-5-4">0000-5-4</option>
                                    <option value="0000-5-5">0000-5-5</option>
                                    <option value="0000-5-6">0000-5-6</option>
                                    <option value="0000-5-7">0000-5-7</option>
                                    <option value="0000-5-8">0000-5-8</option>
                                    <option value="0000-5-9">0000-5-9</option>
                                    <option value="0000-5-10">0000-5-10</option>
                                    <option value="0000-5-11">0000-5-11</option>
                                    <option value="0000-5-12">0000-5-12</option>
                                    <option value="0000-5-13">0000-5-13</option>
                                    <option value="0000-5-14">0000-5-14</option>
                                    <option value="0000-5-15">0000-5-15</option>
                                    <option value="0000-5-16">0000-5-16</option>
                                    <option value="0000-5-17">0000-5-17</option>
                                    <option value="0000-5-18">0000-5-18</option>
                                    <option value="0000-5-19">0000-5-19</option>
                                    <option value="0000-5-20">0000-5-20</option>
                                    <option value="0000-5-21">0000-5-21</option>
                                    <option value="0000-5-22">0000-5-22</option>
                                    <option value="0000-5-23">0000-5-23</option>
                                    <option value="0000-5-24">0000-5-24</option>
                                    <option value="0000-5-25">0000-5-25</option>
                                    <option value="0000-5-26">0000-5-26</option>
                                    <option value="0000-5-27">0000-5-27</option>
                                    <option value="0000-5-28">0000-5-28</option>
                                    <option value="0000-5-29">0000-5-29</option>
                                    <option value="0000-5-30">0000-5-30</option>
                                    <option value="0000-5-31">0000-5-31</option>
                                    <option value="0000-6-1">0000-6-1</option>
                                    <option value="0000-6-2">0000-6-2</option>
                                    <option value="0000-6-3">0000-6-3</option>
                                    <option value="0000-6-4">0000-6-4</option>
                                    <option value="0000-6-5">0000-6-5</option>
                                    <option value="0000-6-6">0000-6-6</option>
                                    <option value="0000-6-7">0000-6-7</option>
                                    <option value="0000-6-8">0000-6-8</option>
                                    <option value="0000-6-9">0000-6-9</option>
                                    <option value="0000-6-10">0000-6-10</option>
                                    <option value="0000-6-11">0000-6-11</option>
                                    <option value="0000-6-12">0000-6-12</option>
                                    <option value="0000-6-13">0000-6-13</option>
                                    <option value="0000-6-14">0000-6-14</option>
                                    <option value="0000-6-15">0000-6-15</option>
                                    <option value="0000-6-16">0000-6-16</option>
                                    <option value="0000-6-17">0000-6-17</option>
                                    <option value="0000-6-18">0000-6-18</option>
                                    <option value="0000-6-19">0000-6-19</option>
                                    <option value="0000-6-20">0000-6-20</option>
                                    <option value="0000-6-21">0000-6-21</option>
                                    <option value="0000-6-22">0000-6-22</option>
                                    <option value="0000-6-23">0000-6-23</option>
                                    <option value="0000-6-24">0000-6-24</option>
                                    <option value="0000-6-25">0000-6-25</option>
                                    <option value="0000-6-26">0000-6-26</option>
                                    <option value="0000-6-27">0000-6-27</option>
                                    <option value="0000-6-28">0000-6-28</option>
                                    <option value="0000-6-29">0000-6-29</option>
                                    <option value="0000-6-30">0000-6-30</option>
                                    <option value="0000-7-1">0000-7-1</option>
                                    <option value="0000-7-2">0000-7-2</option>
                                    <option value="0000-7-3">0000-7-3</option>
                                    <option value="0000-7-4">0000-7-4</option>
                                    <option value="0000-7-5">0000-7-5</option>
                                    <option value="0000-7-6">0000-7-6</option>
                                    <option value="0000-7-7">0000-7-7</option>
                                    <option value="0000-7-8">0000-7-8</option>
                                    <option value="0000-7-9">0000-7-9</option>
                                    <option value="0000-7-10">0000-7-10</option>
                                    <option value="0000-7-11">0000-7-11</option>
                                    <option value="0000-7-12">0000-7-12</option>
                                    <option value="0000-7-13">0000-7-13</option>
                                    <option value="0000-7-14">0000-7-14</option>
                                    <option value="0000-7-15">0000-7-15</option>
                                    <option value="0000-7-16">0000-7-16</option>
                                    <option value="0000-7-17">0000-7-17</option>
                                    <option value="0000-7-18">0000-7-18</option>
                                    <option value="0000-7-19">0000-7-19</option>
                                    <option value="0000-7-20">0000-7-20</option>
                                    <option value="0000-7-21">0000-7-21</option>
                                    <option value="0000-7-22">0000-7-22</option>
                                    <option value="0000-7-23">0000-7-23</option>
                                    <option value="0000-7-24">0000-7-24</option>
                                    <option value="0000-7-25">0000-7-25</option>
                                    <option value="0000-7-26">0000-7-26</option>
                                    <option value="0000-7-27">0000-7-27</option>
                                    <option value="0000-7-28">0000-7-28</option>
                                    <option value="0000-7-29">0000-7-29</option>
                                    <option value="0000-7-30">0000-7-30</option>
                                    <option value="0000-7-31">0000-7-31</option>
                                    <option value="0000-8-1">0000-8-1</option>
                                    <option value="0000-8-2">0000-8-2</option>
                                    <option value="0000-8-3">0000-8-3</option>
                                    <option value="0000-8-4">0000-8-4</option>
                                    <option value="0000-8-5">0000-8-5</option>
                                    <option value="0000-8-6">0000-8-6</option>
                                    <option value="0000-8-7">0000-8-7</option>
                                    <option value="0000-8-8">0000-8-8</option>
                                    <option value="0000-8-9">0000-8-9</option>
                                    <option value="0000-8-10">0000-8-10</option>
                                    <option value="0000-8-11">0000-8-11</option>
                                    <option value="0000-8-12">0000-8-12</option>
                                    <option value="0000-8-13">0000-8-13</option>
                                    <option value="0000-8-14">0000-8-14</option>
                                    <option value="0000-8-15">0000-8-15</option>
                                    <option value="0000-8-16">0000-8-16</option>
                                    <option value="0000-8-17">0000-8-17</option>
                                    <option value="0000-8-18">0000-8-18</option>
                                    <option value="0000-8-19">0000-8-19</option>
                                    <option value="0000-8-20">0000-8-20</option>
                                    <option value="0000-8-21">0000-8-21</option>
                                    <option value="0000-8-22">0000-8-22</option>
                                    <option value="0000-8-23">0000-8-23</option>
                                    <option value="0000-8-24">0000-8-24</option>
                                    <option value="0000-8-25">0000-8-25</option>
                                    <option value="0000-8-26">0000-8-26</option>
                                    <option value="0000-8-27">0000-8-27</option>
                                    <option value="0000-8-28">0000-8-28</option>
                                    <option value="0000-8-29">0000-8-29</option>
                                    <option value="0000-8-30">0000-8-30</option>
                                    <option value="0000-8-31">0000-8-31</option>
                                    <option value="0000-9-1">0000-9-1</option>
                                    <option value="0000-9-2">0000-9-2</option>
                                    <option value="0000-9-3">0000-9-3</option>
                                    <option value="0000-9-4">0000-9-4</option>
                                    <option value="0000-9-5">0000-9-5</option>
                                    <option value="0000-9-6">0000-9-6</option>
                                    <option value="0000-9-7">0000-9-7</option>
                                    <option value="0000-9-8">0000-9-8</option>
                                    <option value="0000-9-9">0000-9-9</option>
                                    <option value="0000-9-10">0000-9-10</option>
                                    <option value="0000-9-11">0000-9-11</option>
                                    <option value="0000-9-12">0000-9-12</option>
                                    <option value="0000-9-13">0000-9-13</option>
                                    <option value="0000-9-14">0000-9-14</option>
                                    <option value="0000-9-15">0000-9-15</option>
                                    <option value="0000-9-16">0000-9-16</option>
                                    <option value="0000-9-17">0000-9-17</option>
                                    <option value="0000-9-18">0000-9-18</option>
                                    <option value="0000-9-19">0000-9-19</option>
                                    <option value="0000-9-20">0000-9-20</option>
                                    <option value="0000-9-21">0000-9-21</option>
                                    <option value="0000-9-22">0000-9-22</option>
                                    <option value="0000-9-23">0000-9-23</option>
                                    <option value="0000-9-24">0000-9-24</option>
                                    <option value="0000-9-25">0000-9-25</option>
                                    <option value="0000-9-26">0000-9-26</option>
                                    <option value="0000-9-27">0000-9-27</option>
                                    <option value="0000-9-28">0000-9-28</option>
                                    <option value="0000-9-29">0000-9-29</option>
                                    <option value="0000-9-30">0000-9-30</option>
                                    <option value="0000-10-1">0000-10-1</option>
                                    <option value="0000-10-2">0000-10-2</option>
                                    <option value="0000-10-3">0000-10-3</option>
                                    <option value="0000-10-4">0000-10-4</option>
                                    <option value="0000-10-5">0000-10-5</option>
                                    <option value="0000-10-6">0000-10-6</option>
                                    <option value="0000-10-7">0000-10-7</option>
                                    <option value="0000-10-8">0000-10-8</option>
                                    <option value="0000-10-9">0000-10-9</option>
                                    <option value="0000-10-10">0000-10-10</option>
                                    <option value="0000-10-11">0000-10-11</option>
                                    <option value="0000-10-12">0000-10-12</option>
                                    <option value="0000-10-13">0000-10-13</option>
                                    <option value="0000-10-14">0000-10-14</option>
                                    <option value="0000-10-15">0000-10-15</option>
                                    <option value="0000-10-16">0000-10-16</option>
                                    <option value="0000-10-17">0000-10-17</option>
                                    <option value="0000-10-18">0000-10-18</option>
                                    <option value="0000-10-19">0000-10-19</option>
                                    <option value="0000-10-20">0000-10-20</option>
                                    <option value="0000-10-21">0000-10-21</option>
                                    <option value="0000-10-22">0000-10-22</option>
                                    <option value="0000-10-23">0000-10-23</option>
                                    <option value="0000-10-24">0000-10-24</option>
                                    <option value="0000-10-25">0000-10-25</option>
                                    <option value="0000-10-26">0000-10-26</option>
                                    <option value="0000-10-27">0000-10-27</option>
                                    <option value="0000-10-28">0000-10-28</option>
                                    <option value="0000-10-29">0000-10-29</option>
                                    <option value="0000-10-30">0000-10-30</option>
                                    <option value="0000-10-31">0000-10-31</option>
                                    <option value="0000-11-1">0000-11-1</option>
                                    <option value="0000-11-2">0000-11-2</option>
                                    <option value="0000-11-3">0000-11-3</option>
                                    <option value="0000-11-4">0000-11-4</option>
                                    <option value="0000-11-5">0000-11-5</option>
                                    <option value="0000-11-6">0000-11-6</option>
                                    <option value="0000-11-7">0000-11-7</option>
                                    <option value="0000-11-8">0000-11-8</option>
                                    <option value="0000-11-9">0000-11-9</option>
                                    <option value="0000-11-10">0000-11-10</option>
                                    <option value="0000-11-11">0000-11-11</option>
                                    <option value="0000-11-12">0000-11-12</option>
                                    <option value="0000-11-13">0000-11-13</option>
                                    <option value="0000-11-14">0000-11-14</option>
                                    <option value="0000-11-15">0000-11-15</option>
                                    <option value="0000-11-16">0000-11-16</option>
                                    <option value="0000-11-17">0000-11-17</option>
                                    <option value="0000-11-18">0000-11-18</option>
                                    <option value="0000-11-19">0000-11-19</option>
                                    <option value="0000-11-20">0000-11-20</option>
                                    <option value="0000-11-21">0000-11-21</option>
                                    <option value="0000-11-22">0000-11-22</option>
                                    <option value="0000-11-23">0000-11-23</option>
                                    <option value="0000-11-24">0000-11-24</option>
                                    <option value="0000-11-25">0000-11-25</option>
                                    <option value="0000-11-26">0000-11-26</option>
                                    <option value="0000-11-27">0000-11-27</option>
                                    <option value="0000-11-28">0000-11-28</option>
                                    <option value="0000-11-29">0000-11-29</option>
                                    <option value="0000-11-30">0000-11-30</option>
                                    <option value="0000-12-1">0000-12-1</option>
                                    <option value="0000-12-2">0000-12-2</option>
                                    <option value="0000-12-3">0000-12-3</option>
                                    <option value="0000-12-4">0000-12-4</option>
                                    <option value="0000-12-5">0000-12-5</option>
                                    <option value="0000-12-6">0000-12-6</option>
                                    <option value="0000-12-7">0000-12-7</option>
                                    <option value="0000-12-8">0000-12-8</option>
                                    <option value="0000-12-9">0000-12-9</option>
                                    <option value="0000-12-10">0000-12-10</option>
                                    <option value="0000-12-11">0000-12-11</option>
                                    <option value="0000-12-12">0000-12-12</option>
                                    <option value="0000-12-13">0000-12-13</option>
                                    <option value="0000-12-14">0000-12-14</option>
                                    <option value="0000-12-15">0000-12-15</option>
                                    <option value="0000-12-16">0000-12-16</option>
                                    <option value="0000-12-17">0000-12-17</option>
                                    <option value="0000-12-18">0000-12-18</option>
                                    <option value="0000-12-19">0000-12-19</option>
                                    <option value="0000-12-20">0000-12-20</option>
                                    <option value="0000-12-21">0000-12-21</option>
                                    <option value="0000-12-22">0000-12-22</option>
                                    <option value="0000-12-23">0000-12-23</option>
                                    <option value="0000-12-24">0000-12-24</option>
                                    <option value="0000-12-25">0000-12-25</option>
                                    <option value="0000-12-26">0000-12-26</option>
                                    <option value="0000-12-27">0000-12-27</option>
                                    <option value="0000-12-28">0000-12-28</option>
                                    <option value="0000-12-29">0000-12-29</option>
                                    <option value="0000-12-30">0000-12-30</option>
                                    <option value="0000-12-31">0000-12-31</option>

                                </select>
            </div>
            </td>
            </tr>
            <tr>
                <td>
                    <div class='pageitem'>Previous Remarks</div>
                </td>
                <td></td>
                <td>
                    <div class='item'><?php echo $row["staff_remark"] ?></div>
                </td>
            </tr>
            <tr>
                <td colspan='3'>
                    <hr width='75%'>
                </td>
            </tr>
            <tr>
                <td colspan='3' align='center'><input type="submit" name="Update" value="Update" class="form"></td>
            </tr>
            </table>
            </form>
            <br>
            <table width='85%' cellspacing='1'>
                <caption><u>
                        <div class='pageitem'>How to Chose Request Status</div>
                    </u></caption>
                <tr>
                    <td>
                        <div class="item">
                            <strong>Approved</strong> is used when you are satisified and will make the needed change and close the
                            request.<br><br>
                            <strong>Denied</strong> is used to close the request after the request is declined for whatever
                            reason.<br><br>
                            <strong>Followup</strong> is used to alert the player more info is needed or something needs to be
                            clarified, the request will go back so the player can add additional info. This does not allow
                            them to edit any of the request information only to add additional information.<br><br>
                            <strong>Pending</strong> should be used when all is fine but a time must elapse, otherwise it would be
                            approved.<br><br>
                            <strong>Reviewing</strong> should should be used when the storyteller needs to schedule a scene with the
                            player or they need to research something.<br><br>
                        </div>
                    </td>
                </tr>
            </table>
        <?php } ?>
        </td>
        </tr>
        </table>
    </div><!-- content close -->


</div><!-- container close -->
</div><!-- wrapper close -->