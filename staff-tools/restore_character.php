<?php include '/var/www/shadowsofthebayou.com/site-inc/header.php'; ?>

<div id="wrapper">
    <div id="title"></div><!-- title close -->

    <div class="container">

        <div class="column-nav"><?php require '/var/www/shadowsofthebayou.com/site-inc/check-access.php'; ?>

        </div><!-- menu close -->

        <div class="column-main">
            <?php
            require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php"; 

            if ($_COOKIE['privilege'] >= "3") {
                echo "You Do not have rights to do this";
            } elseif ($_POST['Select'] == "Restore Character" && $_POST['log_name'] <> "" && $_POST['Verify'] == "Yes") {
                    $query = "UPDATE game_data SET deleted='No',deleted_date='0000-00-00',deleted_by='',st_notes=\"$_POST[now] $_COOKIE[logname]: Restored character from deleted state.<br \>$_POST[oldstnotes]\" WHERE (log_name=\"$_POST[log_name]\")";
                    $result = mysqli_query($connection, $query)
                        or die("<div  class='error'>Could not execute query.<br \>" . mysqli_error($connection) . "<br \></div>");
                    echo "<div  class='success'>The character '$_POST[log_name]' has been restored.</div>";
                }

                $query = "SELECT DISTINCT log_name FROM `game_data` WHERE deleted<>'No' ORDER BY log_name";
                $result = mysqli_query($connection, $query)

                    or die("<div class='error'>Could not execute query.<br \>" . mysqli_error($connection) . "<br \></div>");
                $option = "<select name=\"log_name\" class='form'><option value=\"\" class='form'></option>";
                while ($row = mysqli_fetch_array($result)) {
                    $option = "$option <option value=\"$row[log_name]\">$row[log_name]</option>";
                }
                $option = "$option </select>";
            ?>
            <form name="form1" method="post" action="">

                <table style="width:90%" align="center">
                    <caption>
                        <div align="center" class='pagetopic'>Select A Character To Restore</div>
                    </caption>
                    <tr>
                        <td>
                            <div class="center"><?php echo $option ?> <input name="Select" type="submit" class="form"
                                    id="Select" value="Select Character"></div>
                        </td>
                    </tr>
                </table>
            </form>

            <?php
            if ($_POST['Select'] == "Select Character" && $_POST['log_name'] <> "") {
                ?>
                <?php
                $query = "SELECT * FROM `game_data` WHERE (log_name=\"$_POST[log_name]\")";
                $results = mysqli_query($connection, $query)

                    or die("<div  class='error'>Couldn't execute query..<br \>" . mysqli_error($connection) . "<br \></div>");
                $data = @mysqli_fetch_array($results);
            }
            ?>
            </span>
            <form name="form2" method="post" action="">
                <input name="log_name" type="hidden" id="log_name" value="<?php echo $_POST['log_name'] ?>">
                <input name="oldstnotes" type="hidden" id="oldnotes" value="<?php echo $data['st_notes'] ?>">
                <input name="now" type="hidden" id="now" value="<?php echo date('m/d/y') ?>">
                <hr width="75%" align="center" />
                <table width="100%" border="1" align="center" cellspacing="0" class="text">
                    <caption>
                        <div align="center" class='pagetopic'>Restore Character Below</div>
                    </caption>
                    <tr>
                        <td>
                            <div class="pagetopic">Character Name</div>
                        </td>
                        <td>
                            <div align="center" class="warning">Notice</div>
                        </td>
                        <td>
                            <div class="pagetopic">Are you sure?</div>
                        </td>
                    </tr>
                    <tr>
                        <td width="25%">
                            <div align="center" class="item"><?php echo $_POST['log_name'] ?></div>
                        </td>
                        <td width="51%">
                            <div align="center" class="warning">When you restore a character it is returned to the exact
                                state it was before deleted.</div>
                        </td>
                        <td width="24%">
                            <div class="center"><select name="Verify" class="form" id="Verify">
                                    <option selected>No</option>
                                    <option>Yes</option>
                                </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="center"><input name="Select" type="submit" class="form" id="Select"
                                    value="Restore Character"></div>
                        </td>
                    </tr>
                </table>
            </form>
        </div><!-- content close -->
    </div><!-- container close -->
</div><!-- wrapper close -->