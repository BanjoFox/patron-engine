<?php
require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php";

if ($_POST['submit'] == "Award Endowment" && $_COOKIE['logname'] !== null) {
    $name = $_COOKIE['logname'];
    $ip = getenv("REMOTE_ADDR");

    $generationResult = generateRank();
    $freebieResult = generateFreebies();
    $specialResult = generateSpecialResult();

    $display = generateDisplay($generationResult, $freebieResult, $specialResult);

    $query = "INSERT INTO endowments (date, name, result, ip_add, char_type) VALUES (NOW(), '$name', '$display', '$ip', 'Ghoul')";
    $result = mysqli_query($connection, $query) or die(mysqli_error($connection));
}

function generateRank()
{
        return 'None';
}
function generateFreebies()
{
    $roll = mt_rand(1, 10);
    switch ($roll) {
        case 1:
        case 2:
            return '+5 Freebies';
        case 3:
        case 4:
            return '+10 Freebies';
        case 5:
        case 6:
            return '+15 Freebies';
        case 7:
        case 8:
            return '+20 Freebies';
        case 9:
        case 10:
            return '+25 Freebies';
        default:
            return '';
    }
}
function generateSpecialResult()
{
    $roll = mt_rand(1, 10);
    switch ($roll) {
        case 7:
            return 'and Free Merit up to 4pt value.';
        case 8:
        case 9:
            return 'and +1 Discipline dot';            
        case 10:
            return 'and +2 Discipline dots';
        default:
            return '';
    }
}
function generateDisplay($generationResult, $freebieResult, $specialResult)
{
    return "$generationResult $freebieResult $specialResult";
}