<?php
    require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php";

    $query = "SELECT result, used_on FROM `endowments` WHERE char_type = '$characterType' AND name = '{$_COOKIE['logname']}' AND (used_on IS NULL OR used_on = '')";
    $endowmentResult = mysqli_query($connection, $query)
        or die("<div class='error'>Couldn't get endowments.</div>");
    $endowment = mysqli_fetch_assoc($endowmentResult);

    if ($endowment['used_on'] !== '') {
        echo "<div align='center' class='warning'>You have no available endowment for a " . $characterType . ".</div>";
    } else {
        echo "<table width='50%' border='0' cellspacing='0' align='center'>
                <tr>
                    <td><div class='success' align='center'>{$endowment['result']}</div></td>
                </tr>
            </table>";
    }

    $query = "SELECT * FROM `create_rules` WHERE char_type_name = '$characterType'";
    $rulesResult = mysqli_query($connection, $query)
        or die("<div class='error'>Couldn't get character data.</div>");
    $rules = mysqli_fetch_assoc($rulesResult);