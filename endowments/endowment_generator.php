<form name="endowment_generator" action="" method="POST">
    <div>
        <label class="label" for="char_type">Character Type:</label>
        <select class="form" name="char_type">
            <option value="">Select Type</option>
            <option value="bygone">Bygone</option>
            <option value="changeling">Changeling</option>
            <option value="fera">Fera</option>
            <option value="ghoul">Ghoul</option>
            <option value="kinain">Kinain</option>
            <option value="kinfolk">Kinfolk</option>
            <option value="mage">Mage</option>
            <option value="mortal">Mortal</option>
            <option value="sorcerer">Sorcerer</option>
            <option value="vampire">Vampire</option>
            <option value="werewolf">Werewolf</option>
            <option value="wraith">Wraith</option>
        </select>
        <input class="form" type="submit" name="genEndowmentBtn" value="Generate Endowment">
    </div>
</form>

<?php
require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php";

// Check if the form has been submitted
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['genEndowmentBtn'])) {
    $typeSelected = $_POST['char_type'];
    $endowmentResult = ''; // Initialize the endowment result variable
    $rankResult = ''; // Initialize the rank result variable

    // Check if a character type has been selected
    // If not, display an error message, and stop the script
    if (empty($typeSelected)) {
        die($endowmentResult = '<span class="error">Please select a character type.</span>');
    }

    // Check if the user has created an endowment for the selected character type today
    // If so, display an error message, and stop the script
    $query = "SELECT * FROM endowments WHERE name = '$_COOKIE[logname]' AND date = CURDATE() AND char_type = '$typeSelected'";
    $result = mysqli_query($connection, $query) or die("Couldn't execute query for character type.<br \>" . mysqli_error($connection) . "<br \>");
    if (mysqli_num_rows($result) > 0) {
        die($endowmentResult = '<span class="error">You have already created an endowment for</span> 
            <span id="endowment">' . $typeSelected . '</span><span class="error"> today.</span>');
    }

    // Get the username from the cookie and the IP address from the server
    $name = $_COOKIE['logname'];
    $ip = getenv("REMOTE_ADDR");

    // Generate the three components of the endowment
    $freebieResult = generateFreebies();
    $specialResult = call_user_func($typeSelected . 'SpecialResult');
    if (in_array($typeSelected, array('fera', 'vampire', 'werewolf'))) {
        $rankResult = call_user_func($typeSelected . 'RankResult');
    }

    // Combine the three components into a display string
    $endowmentResult = generateDisplay($freebieResult, $specialResult, $rankResult);

    // Create a SQL query to insert the endowment into the database
    $query = "INSERT INTO endowments (date, name, result, ip_add, char_type) VALUES (NOW(), '$name', '$endowmentResult', '$ip', '$typeSelected')";

    // Execute the SQL query and display an error if it fails
    $result = mysqli_query($connection, $query) or die(mysqli_error($connection));
}

//-
// Function to Display the final results to the user
//
function generateDisplay($freebieResult, $specialResult, $rankResult)
{
    $resultString = "";
    if (!is_null($rankResult)) {
        $resultString = ", $rankResult";
    }
    return "$freebieResult, $specialResult" . $resultString;
}

//-
// Generate Freebies for any character type
function generateFreebies()
{
    $roll = mt_rand(1, 10);
    $freebieResult = '';
    switch ($roll) {
        case 1:
        case 2:
            $freebieResult = '+5 Freebies';
            break;
        case 3:
        case 4:
            $freebieResult = '+10 Freebies';
            break;
        case 5:
        case 6:
            $freebieResult = '+15 Freebies';
            break;
        case 7:
        case 8:
            $freebieResult = '+20 Freebies';
            break;
        case 9:
        case 10:
            $freebieResult = '+25 Freebies';
            break;
        default:
            return $freebieResult;
    }
    return $freebieResult;
}

//-
// Endowment list for Bygones
function bygoneSpecialResult()
{
    $specialResult = '';
    $roll = mt_rand(1, 10);
    switch ($roll) {
        case 7:
            $specialResult = 'and Free Merit up to 4pt value.';
            break;
        case 8:
        case 9:
        case 10:
            $specialResult = 'and +5 pts Advantages';
            break;
        default:
            return $specialResult;
    }
    return $specialResult;
}

//-
// Endowment list for Changelings
function changelingSpecialResult()
{
    $specialResult = '';
    $roll = mt_rand(1, 10);
    switch ($roll) {
        case 7:
            $specialResult = 'and Free Merit up to 4pt value.';
            break;
        case 8:
        case 9:
            $specialResult = 'and +1 Art or Realm';
            break;
        case 10:
            $specialResult = 'and +5 pts Advantages';
            break;
        default:
            return $specialResult;
    }
    return $specialResult;
}

//-
// Endowment list for Fera
function feraSpecialResult()
{
    $roll = mt_rand(1, 20);
    $specialResult = '';
    switch ($roll) {
        case 13:
        case 14:
            $specialResult = 'Free Merit up to 4pt value';
            break;
        case 15:
            $specialResult = 'and +1 Gift';
            break;
        case 16:
            $specialResult = 'and +1 Perm Renown';
            break;
        case 17:
            $specialResult = 'and +1 Gift +1 Perm Renown';
            break;
        case 18:
            $specialResult = 'and +2 Gift +1 Perm Renown';
            break;
        case 19:
            $specialResult = 'and +1 Gift +1 Perm Renown';
            break;
        case 20:
            $specialResult = 'and +2 Gift +1 Perm Renown';
            break;
        default:
            return $specialResult;
    }
    return $specialResult;
}

function feraRankResult()
{
    $roll = mt_rand(1, 10);
    $rankResult = '';
    switch ($roll) {
        case 8:
        case 9:
        case 10:
            $rankResult = 'and Rank 2';
            break;
        default:
            $rankResult = 'and Rank 1';
    }
    return $rankResult;
}
//-
// Endowment list for Ghoul
function ghoulSpecialResult()
{
    $specialResult = '';
    $roll = mt_rand(1, 10);
    switch ($roll) {
        case 7:
            $specialResult = 'and Free Merit up to 4pt value.';
            break;
        case 8:
        case 9:
            $specialResult = 'and +1 Discipline dot';
            break;
        case 10:
            $specialResult = 'and +2 Discipline dots';
            break;
        default:
            return $specialResult;
    }
}

//-
// Endowment list for Kinain
function kinainSpecialResult()
{
    $specialResult = '';
    $roll = mt_rand(1, 10);
    switch ($roll) {
        case 8:
            $specialResult = 'and Free Merit up to 4pt value.';
            break;
        case 9:
            $specialResult = 'and +1 Art or Realm';
            break;
        case 10:
            $specialResult = 'and +1 Art or Realm';
            break;
        default:
            return $specialResult;
    }
}

//-
// Endowment list for Kinfolk
function kinfolkSpecialResult()
{
    $specialResult = '';
    $roll = mt_rand(1, 10);
    switch ($roll) {
        case 7:
            $specialResult = 'and Free Merit up to 4pt value.';
            break;
        case 8:
        case 9:
            $specialResult = 'and +1 Temp Renown';
            break;
        case 10:
            $specialResult = 'and 1 Gift or Numina Dot +2 Temp Renown';
            break;
        default:
            return $specialResult;
    }
}

//-
// Endowment list for Mage
function mageSpecialResult()
{
    $specialResult = '';
    $roll = mt_rand(1, 10);
    switch ($roll) {
        case 8:
            $specialResult = 'and Free Merit up to 4pt value.';
            break;
        case 9:
            $specialResult = 'and +1 Sphere dot';
            break;
        case 10:
            $specialResult = 'and +1 Sphere dot';
            break;
        default:
            return $specialResult;
    }
}

function mageRankResult()
{
    $roll = mt_rand(1, 10);
    $rankResult = '';
    switch ($roll) {
        case 8:
        case 9:
        case 10:
            $rankResult = 'and May purchase Arete upto 4';
            break;
        default:
            $rankResult = 'and Arete 3 (must purchase Arete 2)';
            break;
    }
    return $rankResult;
}
//-
// Endowment list for Mortal
function mortalSpecialResult()
{
    $roll = mt_rand(1, 4);
    $specialResult = '';
    switch ($roll) {
        case 4:
            $specialResult = 'and Free Merit up to 4pt value.';
            break;
        default:
            return $specialResult;
    }
    return $specialResult;
}
//-
// Endowment list for Sorcerer
function sorcererSpecialResult()
{
    $specialResult = '';
    $roll = mt_rand(1, 10);
    switch ($roll) {
        case 7:
            $specialResult = 'and Free Merit up to 4pt value.';
            break;
        case 8:
        case 9:
            $specialResult = 'and +1 Path dot';
            break;
        case 10:
            $specialResult = 'and +2 Path dots';
            break;
        default:
            return $specialResult;
    }
    return $specialResult;
}

//-
// Endowment list for Vampire
function vampireRankResult()
{
    $rankResult = '';
    $genRoll = mt_rand(1, 100);
    if ($genRoll >= 1 && $genRoll <= 20) {
        $rankResult = "12th Generation";
    } elseif ($genRoll >= 21 && $genRoll <= 36) {
        $rankResult = "11th Generation";
    } elseif ($genRoll >= 37 && $genRoll <= 52) {
        $rankResult = "10th Generation";
    } elseif ($genRoll >= 53 && $genRoll <= 68) {
        $rankResult = "9th Generation";
    } elseif ($genRoll >= 69 && $genRoll <= 84) {
        $rankResult = "8th Generation";
    } elseif ($genRoll >= 85 && $genRoll <= 100) {
        $rankResult = "7th Generation";
    }
    return $rankResult;
}

function vampireSpecialResult()
{
    $specialResult = '';
    $specialRoll = mt_rand(1, 100);
    if ($specialRoll >= 1 && $specialRoll <= 60) {
        $specialResult = "";
    } elseif ($specialRoll >= 61 && $specialRoll <= 68) {
        $specialResult = "and Free Merit up to 4pt value";
    } elseif ($specialRoll >= 69 && $specialRoll <= 88) {
        $specialResult = "and +1 discipline dots";
    } elseif ($specialRoll >= 89 && $specialRoll <= 98) {
        $specialResult = "and +2 discipline dots";
    } elseif ($specialRoll >= 99 && $specialRoll <= 100) {
        $specialResult = "and Age 1";
    }
    return $specialResult;
}

//-
// Endowment list for Werewolf
// Don't forget to call feraRankResult() as well.
function werewolfSpecialResult()
{
    $specialResult = '';
    $roll = mt_rand(1, 10);
    switch ($roll) {
        case 8:
            $specialResult = 'and Free Merit up to 4pt value.';
            break;
        case 9:
            $specialResult = 'and +1 Gift';
            break;
        case 10:
            $specialResult = 'and +1 Gift +1 Perm Renown';
            break;
        default:
            return $specialResult;
    }
    return $specialResult;
}
function werewolfRankResult()
{
    $roll = mt_rand(1, 10);
    $rankResult = '';
    switch ($roll) {
        case 8:
        case 9:
        case 10:
            $rankResult = 'Rank 2';
            break;
        default:
            $rankResult = 'Rank 1';
    }
    return $rankResult;
}

//-
// Endowment list for Wraith
function wraithSpecialResult()
{
    $specialResult = '';
    $roll = mt_rand(1, 10);
    switch ($roll) {
        case 7:
        case 8:
            $specialResult = 'and Free Merit up to 4pt value.';
            break;
        case 9:
            $specialResult = 'and staff discretion.';
            break;
        case 10:
            $specialResult = 'and staff discretion.';
            break;
        default:
            return $specialResult;
    }
    return $specialResult;
}
?>