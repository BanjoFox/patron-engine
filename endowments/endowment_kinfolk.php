<?php
include "/var/www/shadowsofthebayou.com/site-inc/header.php";
require "/var/www/shadowsofthebayou.com/site-inc/gamengdb.php";
include "/var/www/shadowsofthebayou.com/endowments/endow_kinfolk.php";
?>

<div id="wrapper">
  <div id="title"></div><!-- title close -->

  <div class="container">

    <div class="column-nav">
      <?php require '/var/www/shadowsofthebayou.com/site-inc/check-access.php'; ?>


    </div><!-- menu close -->

    <div class="column-main">
      <form name="form1" action="" method="POST">
        <table width="60%" border="0" align="center">
          <caption>
            <div class='pagetopic'>Endowment Generator for Kinfolk</div>
          </caption>
          <tr>
            <td>
              <div class='item'>Name: <?php echo $_COOKIE['logname'] ?></div>
            </td>>
          </tr>
          <tr>
            <td colspan="2">
              <div><input type='submit' name='submit' value='Award Endowment' id='submit' class='form'></div>
            </td>
          </tr>
          <tr>
            <td>
              <div class='item'>You are awarded: <?php echo "$display"; ?></div>
            </td>
          </tr> 
        </table>
      </form>
      <table border="1" width=50% align="center" cellspacing="0">
        <tr>
          <td>
            <div align="center" class="warning">Do not generate endowments for characters you cannot make, or have more
              than one for a type unused or you will find all your unused endowments deleted.<br \><br \>Do not refresh
              this page!</div>
          </td>
        </tr>
        <table>
    </div><!-- content close -->


  </div><!-- container close -->
</div><!-- wrapper close -->